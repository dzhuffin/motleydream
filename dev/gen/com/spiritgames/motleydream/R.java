/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.spiritgames.motleydream;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int background=0x7f020000;
        public static final int background_game=0x7f020001;
        public static final int ball_blue=0x7f020002;
        public static final int ball_dead001=0x7f020003;
        public static final int ball_dead002=0x7f020004;
        public static final int ball_dead003=0x7f020005;
        public static final int ball_dead004=0x7f020006;
        public static final int ball_dead005=0x7f020007;
        public static final int ball_dead006=0x7f020008;
        public static final int ball_dead007=0x7f020009;
        public static final int ball_dead008=0x7f02000a;
        public static final int ball_dead009=0x7f02000b;
        public static final int ball_dead010=0x7f02000c;
        public static final int ball_dead011=0x7f02000d;
        public static final int ball_dead012=0x7f02000e;
        public static final int ball_green=0x7f02000f;
        public static final int ball_red=0x7f020010;
        public static final int ball_yellow=0x7f020011;
        public static final int block_blue=0x7f020012;
        public static final int block_blue_c=0x7f020013;
        public static final int block_blue_l=0x7f020014;
        public static final int block_blue_r=0x7f020015;
        public static final int block_floor=0x7f020016;
        public static final int block_green=0x7f020017;
        public static final int block_green_c=0x7f020018;
        public static final int block_green_l=0x7f020019;
        public static final int block_green_r=0x7f02001a;
        public static final int block_red=0x7f02001b;
        public static final int block_red_c=0x7f02001c;
        public static final int block_red_l=0x7f02001d;
        public static final int block_red_r=0x7f02001e;
        public static final int block_usual=0x7f02001f;
        public static final int block_usual_c=0x7f020020;
        public static final int block_usual_l=0x7f020021;
        public static final int block_usual_r=0x7f020022;
        public static final int block_yellow=0x7f020023;
        public static final int block_yellow_c=0x7f020024;
        public static final int block_yellow_l=0x7f020025;
        public static final int block_yellow_r=0x7f020026;
        public static final int btn_jump=0x7f020027;
        public static final int btn_jump_press=0x7f020028;
        public static final int btn_restart=0x7f020029;
        public static final int btn_restart_press=0x7f02002a;
        public static final int changer_blue_1=0x7f02002b;
        public static final int changer_blue_2=0x7f02002c;
        public static final int changer_blue_3=0x7f02002d;
        public static final int changer_blue_4=0x7f02002e;
        public static final int changer_blue_5=0x7f02002f;
        public static final int changer_blue_6=0x7f020030;
        public static final int changer_green_1=0x7f020031;
        public static final int changer_green_2=0x7f020032;
        public static final int changer_green_3=0x7f020033;
        public static final int changer_green_4=0x7f020034;
        public static final int changer_green_5=0x7f020035;
        public static final int changer_green_6=0x7f020036;
        public static final int changer_red_1=0x7f020037;
        public static final int changer_red_2=0x7f020038;
        public static final int changer_red_3=0x7f020039;
        public static final int changer_red_4=0x7f02003a;
        public static final int changer_red_5=0x7f02003b;
        public static final int changer_red_6=0x7f02003c;
        public static final int changer_yellow_1=0x7f02003d;
        public static final int changer_yellow_2=0x7f02003e;
        public static final int changer_yellow_3=0x7f02003f;
        public static final int changer_yellow_4=0x7f020040;
        public static final int changer_yellow_5=0x7f020041;
        public static final int changer_yellow_6=0x7f020042;
        public static final int color_change001=0x7f020043;
        public static final int color_change002=0x7f020044;
        public static final int color_change003=0x7f020045;
        public static final int color_change004=0x7f020046;
        public static final int color_change005=0x7f020047;
        public static final int color_change006=0x7f020048;
        public static final int color_change007=0x7f020049;
        public static final int icon=0x7f02004a;
        public static final int level_icon_border=0x7f02004b;
        public static final int level_icon_border_locked=0x7f02004c;
        public static final int level_icon_border_press=0x7f02004d;
        public static final int loading=0x7f02004e;
        public static final int logo=0x7f02004f;
        public static final int logo2=0x7f020050;
        public static final int mm_about=0x7f020051;
        public static final int mm_about_press=0x7f020052;
        public static final int mm_hiscores=0x7f020053;
        public static final int mm_hiscores_press=0x7f020054;
        public static final int mm_options=0x7f020055;
        public static final int mm_options_gsensor_checked=0x7f020056;
        public static final int mm_options_gsensor_sensitivity=0x7f020057;
        public static final int mm_options_gsensor_unchecked=0x7f020058;
        public static final int mm_options_press=0x7f020059;
        public static final int mm_options_sound_volume=0x7f02005a;
        public static final int mm_options_vibration_checked=0x7f02005b;
        public static final int mm_options_vibration_unchecked=0x7f02005c;
        public static final int mm_play=0x7f02005d;
        public static final int mm_play_press=0x7f02005e;
        public static final int prize=0x7f02005f;
        public static final int target001=0x7f020060;
        public static final int target002=0x7f020061;
        public static final int target003=0x7f020062;
        public static final int target004=0x7f020063;
        public static final int zoom_scale_h=0x7f020064;
        public static final int zoom_scale_v=0x7f020065;
        public static final int zoom_track_h=0x7f020066;
        public static final int zoom_track_v=0x7f020067;
    }
    public static final class layout {
        public static final int main=0x7f030000;
    }
    public static final class raw {
        public static final int lev001=0x7f040000;
        public static final int lev002=0x7f040001;
        public static final int lev003=0x7f040002;
        public static final int lev004=0x7f040003;
        public static final int lev005=0x7f040004;
        public static final int loading=0x7f040005;
        public static final int snd_ball_contact=0x7f040006;
        public static final int snd_color_change=0x7f040007;
        public static final int snd_jump=0x7f040008;
    }
    public static final class string {
        public static final int app_name=0x7f050001;
        public static final int hello=0x7f050000;
    }
}
