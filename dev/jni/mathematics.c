/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <math.h>
#include <jni.h>

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/HelloJni/HelloJni.java
 */
jboolean Java_com_spiritgames_spiritengine_SEMath_checkPointInsideRect(JNIEnv* env, jobject thiz, jfloatArray vertsArr, jfloat sx, jfloat sy)
{
	if ((*env)->GetArrayLength(env, vertsArr) != 8)
		return JNI_FALSE;

	jfloat *verts = (*env)->GetFloatArrayElements(env, vertsArr, 0);

	jint pj, pk = 0, n = 8;
	jdouble wrkx, yu, yl;
	for (pj = 0; pj < n / 2; pj++)
	{
		yu = verts[pj * 2 + 1] > verts[((pj + 1) * 2 + 1) % n] ? verts[pj * 2 + 1] : verts[((pj + 1) * 2 + 1) % n];
		yl = verts[pj * 2 + 1] < verts[((pj + 1) * 2 + 1) % n] ? verts[pj * 2 + 1] : verts[((pj + 1) * 2 + 1) % n];
		if ((verts[((pj + 1) * 2 + 1) % n] - verts[pj * 2 + 1]) > 0)
			wrkx = verts[pj * 2] + (verts[((pj + 1) * 2) % n] - verts[pj * 2]) * (sy - verts[pj * 2 + 1]) / (verts[((pj + 1) * 2 + 1) % n] - verts[pj * 2]);
		else
			wrkx = verts[pj * 2];
		if (yu >= sy)
			if (yl < sy)
			{
				if (sx > wrkx)
						pk++;
				if (fabs(sx - wrkx) < 0.00001f)
				{
					(*env)->ReleaseFloatArrayElements(env, vertsArr, verts, JNI_ABORT);
					return JNI_TRUE;
				}
			}
		if ((fabs(sy - yl) < 0.00001f) && (fabs(yu - yl) < 0.00001f) && (fabs(fabs(wrkx - verts[pj * 2]) + fabs(wrkx - verts[((pj + 1) * 2) % n]) - fabs(verts[pj * 2] - verts[((pj + 1) * 2) % n])) < 0.0001f))
		{
			(*env)->ReleaseFloatArrayElements(env, vertsArr, verts, JNI_ABORT);
			return JNI_TRUE;
		}
	}

	(*env)->ReleaseFloatArrayElements(env, vertsArr, verts, JNI_ABORT);

	if ((pk % 2) > 0)
		return JNI_TRUE;
	else
		return JNI_FALSE;
}

jint Java_com_spiritgames_spiritengine_SEMath_getNearPowerOfTwo(JNIEnv* env, jobject thiz, jint src, jboolean lower)
{
	jint start = 1;
	jint power = 0;

	while (start <= src)
	{
		start *= 2;
		power++;
	}

	if (lower)
		return (jint)pow(2.0f, (jdouble)(power - 1));
	else
		return (jint)pow(2.0f, (jdouble)power);
}
