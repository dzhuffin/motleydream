package com.spiritgames.spiritengine;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.spiritgames.motleydream.MBSoundManager;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.menu.SEMenuManager;
import com.spiritgames.spiritengine.sound.SESoundManager;
import com.spiritgames.spiritengine.text.SETextManager;
import com.spiritgames.spiritengine.ui.SEGameControlManager;
import com.spiritgames.spiritengine.ui.SESensorReader;

abstract public class SEGameManager 
{
	protected SETextManager textManager;
	
	public SEMenuManager inGameMenuManager;
	
	private boolean paused = false;	
	public boolean isGamePaused() {
		return this.paused;
	}
	public void setGamePaused(boolean paused) {
		this.paused = paused;
	}
	
	protected SEGameControlManager gcManager;
	
	protected SESensorReader sensorReader;
	
	protected Vibrator vibrator = null;
	
	public SEGameManager()
	{
		this.vibrator = (Vibrator) SEManagers.getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
		this.gcManager = new SEGameControlManager();
		if (this.inGameMenuManager == null)
			this.inGameMenuManager = new SEMenuManager();
	}
	
	public void setTextManager(SETextManager textManager)
	{
		this.textManager = textManager;
	}
	
	public void setSensorReader(SESensorReader sensorReader)
	{
		this.sensorReader = sensorReader;
	}
	
	public void Render(float screenW, float screenH)
	{
		this.gcManager.Render(screenW, screenH);
	}
	
	public boolean onTouch(MotionEvent event)
	{
		return this.gcManager.onTouch(event);
	}
	
	public void release()
	{
		if (this.gcManager != null)
			this.gcManager.release();
		
		if (this.inGameMenuManager != null)
			this.inGameMenuManager.release();
	}
	
	public void restore()
	{
		if (this.gcManager != null)
			this.gcManager.restore();
		
		if (this.inGameMenuManager != null)
			this.inGameMenuManager.restore();
	}
	
	abstract public boolean onKey(int keyCode, KeyEvent event);
	abstract public void Init();
}
