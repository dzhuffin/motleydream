package com.spiritgames.spiritengine.sound;

import java.io.IOException;
import java.util.HashMap;

import com.spiritgames.spiritengine.SEManagers;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class SESoundManager 
{
	protected SoundPool soundPool;
	protected HashMap <Integer, Integer> soundPoolMap;
	
	protected MediaPlayer mediaPlayer;
	protected HashMap <Integer, AssetFileDescriptor> mediaPlayerMap;
	
	protected int maxSounds;
	
	protected float volume = 1.0f;
	public float getVolume() {
		return volume;
	}
	public void setVolume(float volume) {
		this.volume = volume;
	}

	protected boolean soundEnable = true;
	public boolean getSoundEnable()
	{
		return this.soundEnable;
	}
	public void setSoundEnable(boolean val)
	{
		if (!val)
			this.stopBackgroundMusic();
		
		this.soundEnable = val;
	}
	
	public SESoundManager(int maxSounds)
	{
		this.maxSounds = maxSounds;
		
		this.checkForInit();
	}
	
	private void checkForInit()
	{
		if (this.soundPool == null)
			this.soundPool = new SoundPool(this.maxSounds, AudioManager.STREAM_MUSIC, 0);
		
		if (this.soundPoolMap == null)
			this.soundPoolMap = new HashMap<Integer, Integer>();
		
		if (this.mediaPlayerMap == null)
			this.mediaPlayerMap = new HashMap<Integer, AssetFileDescriptor>();
		
		if (this.mediaPlayer == null)
		{
			this.mediaPlayer = new MediaPlayer();
			this.mediaPlayer.setLooping(true);
		}
	}
	
	public void playBackgroundMusic(int sndID)
	{
		this.checkForInit();
		
		if (this.soundEnable && (this.mediaPlayer != null) && (!this.mediaPlayer.isPlaying()))
		{
			try 
			{
				this.mediaPlayer.reset();
				AssetFileDescriptor descriptor = this.mediaPlayerMap.get(sndID);
				this.mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
				this.mediaPlayer.setVolume(this.volume * 0.1f, this.volume * 0.1f);
				this.mediaPlayer.prepare();
				this.mediaPlayer.setLooping(true);
				this.mediaPlayer.start();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				return;
			}
		}
	}
	
	public void stopBackgroundMusic()
	{
		if (this.mediaPlayer != null)
		{
			this.mediaPlayer.stop();
		}
	}
	
	public void pauseBackgroundMusic()
	{
		if (this.mediaPlayer != null)
		{
			this.mediaPlayer.pause();
		}
	}
	
	public void resumeBackgroundMusic()
	{
		if (this.mediaPlayer != null)
		{
			this.mediaPlayer.start();
		}
	}
	
	public boolean isBackgroundMusicPlaying()
	{
		if (this.mediaPlayer == null)
			return false;
		
		return this.mediaPlayer.isPlaying();
	}
	
	public void addBackgroundMusic(int sndID, String fileName)
	{			
		this.checkForInit();
		
		try 
		{
			this.mediaPlayerMap.put(sndID, SEManagers.getApplicationContext().getAssets().openFd(fileName));
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}
	
	public void addSound(int sndID, int resID, int priority)
	{			
		this.checkForInit();
		
		this.soundPoolMap.put(sndID, this.soundPool.load(SEManagers.getApplicationContext(), resID, priority));	
	}
	
	public void addSound(int sndID, int resID)
	{	
		this.checkForInit();
		
		this.soundPoolMap.put(sndID, this.soundPool.load(SEManagers.getApplicationContext(), resID, 0));	
	}
	
	public int playSound(int sndID)
	{
		return this.playSound(sndID, false, 0);
	}
	
	public int playSound(int sndID, boolean loop)
	{
		return this.playSound(sndID, loop, 0);
	}
	
	public int playSound(int sndID, boolean loop, int priority)
	{
		this.checkForInit();
		
		int streamID = 0;
		
		if ((!this.soundEnable) || (this.soundPool == null) || (this.soundPoolMap == null))
			return streamID;
			
		//AudioManager mgr = (AudioManager)this.context.getSystemService(Context.AUDIO_SERVICE);
		//int volume = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
		
		if (loop)
			streamID = this.soundPool.play(this.soundPoolMap.get(sndID), this.volume, this.volume, priority, -1, 1.0f);
		else
			streamID = this.soundPool.play(this.soundPoolMap.get(sndID), this.volume, this.volume, priority, 0, 1.0f);
		
		return streamID;
	}
	
	public void stopSound(int streamID)
	{
		if (this.soundPool != null)
			this.soundPool.stop(streamID);
	}
	
	public void release()
	{
		if (this.mediaPlayer != null)
		{
			if (this.mediaPlayer.isPlaying())
				this.mediaPlayer.stop();
			
			this.mediaPlayer.release();
			this.mediaPlayer = null;
		}
		
		if (this.soundPool != null)
		{
			this.soundPool.release();
			this.soundPool = null;
		}
		
		if (this.soundPoolMap != null)
		{
			this.soundPoolMap.clear();
			this.soundPoolMap = null;
		}
		
		if (this.mediaPlayerMap != null)
		{
			this.mediaPlayerMap.clear();
			this.mediaPlayerMap = null;
		}
	}
}
