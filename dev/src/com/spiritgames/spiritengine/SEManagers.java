package com.spiritgames.spiritengine;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;

import com.spiritgames.spiritengine.graphics.SERenderManager;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.sound.SESoundManager;

public class SEManagers 
{
	protected static SERenderManager sceneRenderer;
	protected static SEGameManager gameManager;
	protected static SESoundManager soundManager;
	protected static SEActivity activity;
	protected static SEVertexBuffer vBuffer;
	protected static GL10 gl10;
	
	public static GL10 getGL10Interface()
	{
		return SEManagers.gl10;
	}
	
	public static void setGL10Interface(GL10 val)
	{
		SEManagers.gl10 = val;
	}
	
	public static SEActivity getActivity()
	{
		return SEManagers.activity;
	}
	
	public static Context getApplicationContext()
	{
		return SEManagers.activity.getApplicationContext();
	}
	
	public static void setActivity(SEActivity val)
	{
		SEManagers.activity = val;
	}
	
	public static SEVertexBuffer getVertexBuffer()
	{
		return SEManagers.vBuffer;
	}
	
	public static void setVertexBuffer(SEVertexBuffer val)
	{
		SEManagers.vBuffer = val;
	}
	
	public static SERenderManager getRenderManager()
	{
		return SEManagers.sceneRenderer;
	}
	
	public static void setRenderManager(SERenderManager val)
	{
		SEManagers.sceneRenderer = val;
	}
	
	public static SEGameManager getGameManager()
	{
		return SEManagers.gameManager;
	}
	
	public static void setGameManager(SEGameManager val)
	{
		SEManagers.gameManager = val;
	}
	
	public static SESoundManager getSoundManager()
	{
		return SEManagers.soundManager;
	}
	
	public static void setSoundManager(SESoundManager val)
	{
		SEManagers.soundManager = val;
	}
}
