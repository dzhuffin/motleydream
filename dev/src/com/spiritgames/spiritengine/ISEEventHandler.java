package com.spiritgames.spiritengine;

public interface ISEEventHandler 
{
	public void onHandle();
}
