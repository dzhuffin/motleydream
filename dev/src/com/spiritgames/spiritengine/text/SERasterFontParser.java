package com.spiritgames.spiritengine.text;

import java.util.Vector;

import org.xmlpull.v1.XmlPullParser;

import com.spiritgames.spiritengine.SEManagers;

import android.content.Context;

/***
 * ������ ���������� � ��������� ������� (xml-����), ��������������� Bitmap Font Generator
 */
class SERasterFontParser 
{
	/***
	 * ������ xml-����, ��������������� Bitmap Font Generator
	 * @param con
	 * �������� ��� ������� � xml-�����
	 * @param xmlResID
	 * id ������� ���������������� xml-����� (������ ���� � ����� res/xml/)
	 * @return
	 * ������ ������� � ����������� � �������� ������
	 */
	public static Vector<SERasterFontInfo> parseFontData(int xmlResID)
	{
		Vector<SERasterFontInfo> result = new Vector<SERasterFontInfo>();
		
		float fontSize = 0.0f;
		
		try 
		{
			XmlPullParser xpp = SEManagers.getApplicationContext().getResources().getXml(xmlResID);			
			
			while (xpp.getEventType() != XmlPullParser.END_DOCUMENT)
			{
				if (xpp.getEventType() == XmlPullParser.START_TAG)
				{
					if (xpp.getName().equals("info")) 
					{
						for (int i = 0; i < xpp.getAttributeCount(); i++)
						{							
							String attrName = xpp.getAttributeName(i);
							
							if (attrName.compareTo("size") == 0)
							{
								fontSize = Math.abs(Float.parseFloat(xpp.getAttributeValue(i)));
								break;
							}
						}
					}
					
					if (xpp.getName().equals("char")) 
					{
						SERasterFontInfo info = new SERasterFontInfo();
						
						for (int i = 0; i < xpp.getAttributeCount(); i++)
						{
							String attrName = xpp.getAttributeName(i);							
							
							if (attrName.compareTo("id") == 0)
							{
								info.id = Integer.parseInt(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("x") == 0)
							{
								info.x = Float.parseFloat(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("y") == 0)
							{
								info.y = Float.parseFloat(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("width") == 0)
							{
								info.width = Float.parseFloat(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("height") == 0)
							{
								info.height = Float.parseFloat(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("xoffset") == 0)
							{
								info.xoffset = Float.parseFloat(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("yoffset") == 0)
							{
								info.yoffset = Float.parseFloat(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("xadvance") == 0)
							{
								info.xadvance = Float.parseFloat(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("page") == 0)
							{
								info.page = Integer.parseInt(xpp.getAttributeValue(i));
								continue;
							}
							
							if (attrName.compareTo("chnl") == 0)
							{
								info.chnl = Integer.parseInt(xpp.getAttributeValue(i));
								continue;
							}
						}						
						
						info.size = fontSize;
						result.add(info);
					}
				}
				
				xpp.next();
			}
			
			return result;
		}
		catch (Throwable t) 
		{
			return result;
		} 
	}
}
