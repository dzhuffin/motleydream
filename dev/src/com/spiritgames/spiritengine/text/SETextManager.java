package com.spiritgames.spiritengine.text;

import java.util.Date;
import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

import com.spiritgames.spiritengine.SEManagers;


import android.graphics.Paint;
import android.graphics.Point;

public class SETextManager 
{
	private SELabelMaker mLabels;
	private Vector<SEFormatLabel> mNumericLabels; 
	
	private Vector<Point> labelsPositions;
	private Vector<Point> numLabelsPositions;
	public void setLabelPosition(int n, float x, float y)
	{
		if ((n >= 0) && (n < this.labelsPositions.size()))
			this.labelsPositions.get(n).set((int)x, (int)y);
	}
	public void setNumericLabelPosition(int n, float x, float y)
	{
		if ((n >= 0) && (n < this.numLabelsPositions.size()))
			this.numLabelsPositions.get(n).set((int)x, (int)y);
	}
	
	public float getLabelWidth(int n)
	{
		return this.mLabels.getWidth(n);
	}
	public float getLabelHeight(int n)
	{
		return this.mLabels.getHeight(n);
	}
	
	public SETextManager()
	{	
	}
	
	public void clearLabels()
	{
		if (this.mLabels != null)
			this.mLabels.shutdown(SEManagers.getGL10Interface());
		this.mLabels = null;
		
		for (int i = 0; i < this.mNumericLabels.size(); i++)
			this.mNumericLabels.get(i).shutdown(SEManagers.getGL10Interface());
		
		this.mNumericLabels.clear();		
		this.labelsPositions.clear();
		this.numLabelsPositions.clear();
		
		this.mNumericLabels = null;		
		this.labelsPositions = null;
		this.numLabelsPositions = null;
	}
	
	public void beginInitLabels(int textStrikeWidth, int textStrikeHeight)
	{
		if (this.mNumericLabels == null)
			this.mNumericLabels = new Vector<SEFormatLabel>();
		
		if (this.labelsPositions == null)
			this.labelsPositions = new Vector<Point>();
		
		if (this.numLabelsPositions == null)
			this.numLabelsPositions = new Vector<Point>();
		
		if (this.mLabels != null)
			this.mLabels.shutdown(SEManagers.getGL10Interface());
        else
        	this.mLabels = new SELabelMaker(true, textStrikeWidth, textStrikeHeight);
		
		this.mLabels.initialize(SEManagers.getGL10Interface());
		this.mLabels.beginAdding(SEManagers.getGL10Interface());
	}	
	
	public int addLabel(String labelText, int x, int y, Paint labelPaint)
	{
		this.labelsPositions.add(new Point(x, y));
		return this.mLabels.add(SEManagers.getGL10Interface(), labelText, labelPaint);
	}
	
	public void endInitLabels()
	{
		this.mLabels.endAdding(SEManagers.getGL10Interface());
	}
	
	public int addNumericLabel(int x, int y, Paint labelPaint)
	{
		SEFormatLabel nLabel = new SEFormatLabel();		
        nLabel.initialize(SEManagers.getGL10Interface(), labelPaint);
        this.numLabelsPositions.add(new Point(x, y));
        this.mNumericLabels.add(nLabel);
        return this.mNumericLabels.size() - 1; 
	}
	
	public void setIntValue(int value, int labelID)
	{
		if ((labelID >= 0) && (labelID < this.mNumericLabels.size()))
			this.mNumericLabels.get(labelID).setIntValue(value);
	}
	
	public void setTimeValue(Date value, int labelID)
	{
		if ((labelID >= 0) && (labelID < this.mNumericLabels.size()))
			this.mNumericLabels.get(labelID).setTimeValue(value);
	}
	
	public void Render(float screenWidth, float screenHeight)
	{
		this.mLabels.beginDrawing(SEManagers.getGL10Interface(), screenWidth, screenHeight);
		
		Point pos;
		
		for (int i = 0; i < this.mLabels.getLabelsCount(); i++)
		{
			pos = this.labelsPositions.get(i);
	        this.mLabels.draw(SEManagers.getGL10Interface(), (float)pos.x, (float)pos.y, i);
		}
		
		this.mLabels.endDrawing(SEManagers.getGL10Interface());

		for (int i = 0; i < this.mNumericLabels.size(); i++)
		{
			pos = this.numLabelsPositions.get(i);
			this.mNumericLabels.get(i).draw(SEManagers.getGL10Interface(), (float)pos.x, (float)pos.y, screenWidth, screenHeight);
		}
	}
}
