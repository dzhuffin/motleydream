package com.spiritgames.spiritengine.text;

/***
 * ���������� � ������� ���������� �� xml �����, ���������������� Bitmap Font Generator
 */
public class SERasterFontInfo 
{
	public int id = 0;
	public float size = 0.0f;
	public float x = 0.0f;
	public float y = 0.0f;
	public float width = 0.0f;
	public float height = 0.0f;
	public float xoffset = 0.0f;
	public float yoffset = 0.0f;
	public float xadvance = 0.0f;
	public int page = 0;
	public int chnl = 0;
	public int textureNumber = 0;
}
