package com.spiritgames.spiritengine.text;

import java.util.HashMap;
import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.PointF;

import com.spiritgames.motleydream.R;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.graphics.SETextureInfo;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class SERasterFont 
{
	private SESprite fontImage;

	private float density;
	
	/***
	 * ���������� ������������ ����� �������� � ������� �������� �����
	 */
	private HashMap<Character, SERasterFontInfo> fontMap;
	
	private Vector<SERasterFontInfo> symbols;
	
	public SERasterFont()
	{
		this.density = SEManagers.getApplicationContext().getResources().getDisplayMetrics().density;
	}
	
	public void initFont(int fontImg_id, int fontXML_id)
	{
		this.symbols = SERasterFontParser.parseFontData(fontXML_id);
		
		this.fontImage = new SESprite(true);
		this.fontMap = new HashMap<Character, SERasterFontInfo>();
		
		for (int i = 0; i < symbols.size(); i++)
		{
			SERasterFontInfo cur = symbols.get(i);
			cur.textureNumber = this.fontImage.addTexture(fontImg_id, cur.x, cur.y, cur.width, cur.height);
			this.fontMap.put((char)cur.id, cur);
		}
	}
	
	public void release()
	{
		if (this.fontImage != null)
			this.fontImage.release();
		
		if (this.fontMap != null)
			this.fontMap.clear();
		
		this.fontImage = null;
		this.fontMap = null;
	}
	
	public void restore()
	{
		this.fontImage.restore();
	}
	
	/*public void restore(GL10 gl)
	{
		this.fontImage.release(gl);
		this.fontMap.clear();
		
		for (int i = 0; i < symbols.size(); i++)
		{
			SERasterFontInfo cur = symbols.get(i);
			SERestoreTextureInfo info = this.fontImage.getRestoreInfo().get(i);
			cur.textureNumber = this.fontImage.addTexture(info.resID, (int)cur.x, (int)cur.y, (int)cur.width, (int)cur.height, false);
			this.fontMap.put((char)cur.id, cur);
		}
	}*/
	
	/***
	 * �������� ������ � ������ ������ � ������� �������
	 * @param size
	 * ������ ������
	 * @param text
	 * ����� ��� ���������
	 */
	public PointF MeasureText(float size, String text)
	{
		if ((this.fontImage == null) || (this.fontMap == null))
			throw new NullPointerException("font hasn't been initialized!");

		float tx = 0.0f;
		float ty = 0.0f;
		
		float ratio = 0.0f;
		float w = 0.0f;
		float h = 0.0f;
		float maxH = 0.0f;
		
		float startX = 0.0f;
		float finX = 0.0f;
		
		for (int i = 0; i < text.length(); i++)
		{
			SERasterFontInfo chr = this.fontMap.get(text.charAt(i));
		
			if (chr == null)
				continue;
			
			ratio = (size / chr.size) * this.density;			
			
			w = chr.width * ratio;
			h = chr.height * ratio;
			
			if (maxH < h + chr.yoffset)
				maxH = h + chr.yoffset;
			
			if (i == 0)
				startX = tx + chr.xoffset;
			
			if (i == text.length() - 1)
				finX = (tx + w) + chr.xoffset;
			
			tx += chr.xadvance * ratio;
		}
		
		return new PointF(finX - startX, maxH);
	}
	
	/***
	 * �������� ����� ������� ������� � ������� �������
	 * @param x
	 * x ���������� ����� ������ ������
	 * @param y
	 * y ���������� ����� ������ ������
	 * @param size
	 * ������ ������
	 * @param text
	 * ����� ��� ������
	 * @param useDensity
	 * ������������� �� ������� ������ � ������������ � ���������� ������ (���� true, �� ����� �������������� HVGA-���������� ��� ������ ����������)
	 */
	public void RenderText(float x, float y, float size, String text, boolean useDensity)
	{
		if ((this.fontImage == null) || (this.fontMap == null))
			throw new NullPointerException("font hasn't been initialized!");

		float tx = x;
		float ty = y;
		if (useDensity)
		{
			tx *= this.density;
			ty *= this.density;
		}
		float ratio = 0.0f;
		float w = 0.0f;
		float h = 0.0f;
		for (int i = 0; i < text.length(); i++)
		{
			SERasterFontInfo chr = this.fontMap.get(text.charAt(i));
		
			if (chr == null)
				continue;
			
			ratio = (size / chr.size) * this.density;			
			
			w = chr.width * ratio;
			h = chr.height * ratio;
			this.fontImage.setCurrentTexture(chr.textureNumber);			
			this.fontImage.setSize(w, h);
			this.fontImage.setPos((tx + w / 2.0f) + chr.xoffset, (ty - h / 2.0f) - chr.yoffset);
			this.fontImage.Render();
			
			tx += chr.xadvance * ratio;
		}
	}
}
