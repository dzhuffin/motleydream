package com.spiritgames.spiritengine.text;

import java.util.Date;

import javax.microedition.khronos.opengles.GL10;


import android.graphics.Paint;

class SEFormatLabel 
{
	private SELabelMaker mLabelMaker;
    private String mText;
    private int[] mWidth = new int[11];
    private int[] mLabelId = new int[11];
    private final static String sStrike = "0123456789:";
    
    public SEFormatLabel() 
    {
        this.mText = "";
        this.mLabelMaker = null;
    }

    public void initialize(GL10 gl, Paint paint) 
    {
        int height = roundUpPower2((int) paint.getFontSpacing());
        final float interDigitGaps = 9 * 1.0f;
        int width = roundUpPower2((int) (interDigitGaps + paint.measureText(sStrike)));
        this.mLabelMaker = new SELabelMaker(true, width, height);
        this.mLabelMaker.initialize(gl);
        this.mLabelMaker.beginAdding(gl);
        for (int i = 0; i < 11; i++) 
        {
            String digit = sStrike.substring(i, i + 1);
            this.mLabelId[i] = this.mLabelMaker.add(gl, digit, paint);
            this.mWidth[i] = (int) Math.ceil(this.mLabelMaker.getWidth(i));
        }
        this.mLabelMaker.endAdding(gl);
    }

    public void shutdown(GL10 gl) 
    {
    	this.mLabelMaker.shutdown(gl);
    	this.mLabelMaker = null;
    }

    /**
     * Find the smallest power of two >= the input value.
     * (Doesn't work for negative numbers.)
     */
    private int roundUpPower2(int x) 
    {
        x = x - 1;
        x = x | (x >> 1);
        x = x | (x >> 2);
        x = x | (x >> 4);
        x = x | (x >> 8);
        x = x | (x >>16);
        return x + 1;
    }

    public void setIntValue(int value) 
    {
        mText = Integer.toString(value);
    }
    
    public void setTimeValue(Date value) 
    {
        mText = Integer.toString(value.getHours()) + ":" + 
        		Integer.toString(value.getMinutes()) + ":" + 
        		Integer.toString(value.getSeconds());
    }

    public void draw(GL10 gl, float x, float y, float viewWidth, float viewHeight) 
    {
        int length = this.mText.length();
        this.mLabelMaker.beginDrawing(gl, viewWidth, viewHeight);
        for(int i = 0; i < length; i++) 
        {
            char c = this.mText.charAt(i);
            int digit = (c == ':') ? 10 : c - '0';
            this.mLabelMaker.draw(gl, x, y, this.mLabelId[digit]);
            x += this.mWidth[digit];
        }
        this.mLabelMaker.endDrawing(gl);
    }

    public float width() 
    {
        float width = 0.0f;
        int length = this.mText.length();
        for(int i = 0; i < length; i++) 
        {
            char c = this.mText.charAt(i);
            width += this.mWidth[(c == ':') ? 10 : c - '0'];
        }
        return width;
    } 
}
