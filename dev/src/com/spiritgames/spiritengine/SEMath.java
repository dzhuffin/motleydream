package com.spiritgames.spiritengine;

import android.graphics.PointF;

public class SEMath 
{
	static { System.loadLibrary("mathematics"); }
	
	/***
	 * ��������� ����� �� ����� ������ ��������������
	 * @param verts
	 * ������ ����� ��������������
	 * @param sx
	 * ���������� x �����
	 * @param sy
	 * ���������� x �����
	 * @return
	 * true, ���� �����, ����� false
	 */
	public static native boolean checkPointInsideRect(float[] verts, float sx, float sy);
	
	/***
	 * �������� ��������� � ����� ������� ������
	 * @param src
	 * �����
	 * @param lower
	 * ���� true, �� ��������� ������� ������ ����� �����, ����� ������
	 * @return
	 * ��������� ������� ������
	 */
	public static native int getNearPowerOfTwo(int src, boolean lower);
	
	/***
	 * ������������ ������ �������� �� 90, -90 ��� 180 ��������
	 * @param pixels
	 * �������� ������ ��������. ��� ������ ������ ��������� ������������ w �� h
	 * @param w
	 * ������ �������� ��������
	 * @param h
	 * ������ �������� ��������
	 * @param angle
	 * ���� �������� ��������. ����� ��������� �������� 90, -90 � 180
	 * @return
	 */
	public static int[] rotateBitmapArray(int pixels[], int w, int h, int angle)
	{
		if ((angle != 90) && (angle != -90) && (angle != 180))
			throw new IllegalArgumentException("Parameter angle can be only 90, -90 or 180 value");
		
		if (pixels.length != w * h)
			throw new IllegalArgumentException("Length of pixels array must be equal (w * h) ");
		
		int result[] = new int[w * h];
		
		if (angle == 180)
		{
			for (int j = 0; j < h; j++)
				for (int i = 0; i < w; i++)
					result[j * h + i] = pixels[(h - 1 - j) * h + i];
		}
		
		if (angle == -90)
		{
			for (int j = 0; j < h; j++)
				for (int i = 0; i < w; i++)
					result[j * h + i] = pixels[(h - 1 - i) * w + j];
		}
		
		if (angle == 90)
		{
			for (int j = 0; j < h; j++)
				for (int i = 0; i < w; i++)
					result[j * h + i] = pixels[i * h + j];
		}
		
		return result;
	}
	
	/***
	 * ����������� ������
	 * @param vec
	 * ������ ��� ������������
	 * @return
	 * ��������������� ������
	 */
	public static PointF normalize2DVector(PointF vec)
	{
		float length = vec.length();
        return new PointF(vec.x / length, vec.y / length);
	}
	
	/***
	 * ???
	 * @param pixels
	 * ������� ������ ��� ���������
	 * @param alpha
	 * ����� �������� �����-����������
	 */
	public static void setBitmapAlpha(int[] pixels, int alpha, int skippedColor)
	{
		int val = alpha << 24;
		for (int i = 0; i < pixels.length; i++)
		{
			if (pixels[i] != skippedColor)
			{
				pixels[i] &= 0x00FFFFFF;
				pixels[i] |= val;
			}
		}
	}
}
