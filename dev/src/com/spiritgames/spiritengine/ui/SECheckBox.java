package com.spiritgames.spiritengine.ui;

import android.content.Context;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class SECheckBox extends SESprite implements ISEUIElement 
{
	protected boolean checked = false;
	protected ISEFloatEventHandler onClk;

	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public SECheckBox(ISEFloatEventHandler onClk) 
	{
		super(true);
		this.onClk = onClk;
	}
	
	public void loadTextures(int unCheckID, int checkID)
	{
		if (unCheckID > 0)
		{
			super.addTexture(unCheckID);
			
			if (checkID > 0)
			{
				super.addTexture(checkID);
			}
		}
	}

	@Override
	public float getFloatValue() 
	{
		if (this.checked)
			return 1.0f;
		else
			return 0.0f;
	}
	
	@Override
	public void setFloatValue(float val) 
	{
		this.checked = (val > 0.0f); 
	}
	
	@Override
	public void Render()
	{
		if (this.checked)
			this.currentTexture = 1;
		else
			this.currentTexture = 0;
		
		super.Render();
	}
	
	@Override
	public boolean onTouch(MotionEvent event, float scrH) 
	{
		if ((!this.enable) || (!this.visible))
			return false;
		
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			if (this.checkPointInside(event.getX(), scrH - event.getY()))
			{
				this.checked = !this.checked;
				this.onClk.onHandle(0.0f);
				return true;
			}
		}
		
		return false;
	}

	@Override
	public void resetState() 
	{
		this.checked = false;
	}
}
