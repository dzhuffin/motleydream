package com.spiritgames.spiritengine.ui;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.menu.ISEMenuItem;

public class SETrackControl implements ISEUIElement, ISEMenuItem
{
	protected SESprite track;
	protected SESprite scale;
	
	protected boolean enable = true;	
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
		
	public boolean isVisible() {
		return (this.track.isVisible() && this.scale.isVisible());
	}
	public void setVisible(boolean visible) {
		this.track.setVisible(visible);
		this.scale.setVisible(visible);
	}

	protected float x;
	protected float y;
	
	protected float relative_x = -1.0f;	
	protected float relative_y = -1.0f;	
	public float getRelativePosX()
	{
		return this.relative_x;
	}
	public float getRelativePosY()
	{
		return this.relative_y;
	}
	public void setRelativePos(float px, float py)
	{
		this.relative_x = px;
		this.relative_y = py;
	}
	
	protected int pointerCheckIndex = -1;
	
	protected float width;
	protected float height;
	protected float relative_width = -1.0f;
	protected float relative_height = -1.0f;
	protected float relativeTrackWidth;
	protected float relativeTrackHeight;
	
	protected float value = 1.0f;	
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	
	public float getRelativeWidth()
	{
		return this.relative_width;
	}
	public float getRelativeHeight()
	{
		return this.relative_height;
	}
	public void setRelativeSize(float w, float h)
	{
		this.relative_width = w;
		this.relative_height = h;
	}

	protected boolean isVertical = false;
		
	public void setRelativeTrackSizeMem(float rWidth, float rHeight) 
	{	
		this.track.setRelativeSize(rWidth, rHeight);
	}
	public void setRelativeTrackSize(float rWidth, float rHeight) 
	{	
		if (this.isVertical)
		{

			this.relativeTrackWidth = this.width * rWidth;
			this.relativeTrackHeight = this.height * rHeight;
		}
		else
		{
			this.relativeTrackWidth = this.height * rWidth;
			this.relativeTrackHeight = this.width * rHeight;
		}
	}
	public void setSize(float width, float height) 
	{
		if (this.isVertical)
		{
			this.width = width;
			this.height = height;
		}
		else
		{
			this.width = height;
			this.height = width;
		}
		
		this.setRelativeTrackSize(this.track.getRelativeWidth(), this.track.getRelativeHeight());
	}
	public float getWidth() 
	{
		return width;
	}
	public float getHeight() 
	{		
		return height;
	}	
	public float getX() 
	{
		return x;
	}
	public float getY() 
	{
		return y;
	}
	public void setPos(float x, float y) 
	{
		this.x = x;
		this.y = y;
	}	
	
	public void setRatio(float r)
	{
		this.scale.setRatio(r);
	}
	
	@Override
	public void release() 
	{	
		this.track.release();
		this.scale.release();
	}
	
	@Override
	public void restore()
	{
		this.track.restore();
		this.scale.restore();
	}
	
	public void loadTextures(int scaleID, int trackID)
	{
		if (trackID > 0)
			this.track.setCurrentTexture(this.track.addTexture(trackID));
		
		if (scaleID > 0)
			this.scale.setCurrentTexture(this.scale.addTexture(scaleID));
	}

	public SETrackControl(float width, float height, float trWidth, float trHeight, boolean isVertical)
	{
		this.track = new SESprite(true);
		this.scale = new SESprite(true);
		
		this.isVertical = isVertical;
		
		this.scale.setRelativeSize(width, height);
		this.track.setRelativeSize(trWidth, trHeight);
		
		this.setSize(width, height);
	}
	
	public SETrackControl()
	{
		this.track = new SESprite(true);
		this.scale = new SESprite(true);
		
		this.isVertical = false;
	}
	
	@Override
	public void Render() 
	{	
		this.scale.setPos(this.x, this.y);
		this.scale.setSize(this.width, this.height);		
		this.scale.Render();
		
		float tmpH = Math.max(this.width, this.height);
				
		if (this.isVertical)
			this.track.setPos(this.x, this.y - (tmpH / 2.0f) + (tmpH * this.value));
		else
			this.track.setPos(this.x - (tmpH / 2.0f) + (tmpH * this.value), this.y);
		
		this.track.setSize(this.relativeTrackWidth, this.relativeTrackHeight);
		this.track.Render();
	}

	@Override
	public boolean onTouch(MotionEvent event, float screenH) 
	{				
		if ((!this.enable) || (!this.isVisible()))
			return false;
		
		this.pointerCheckIndex = -1;
		
		if ((event.getAction() == MotionEvent.ACTION_DOWN) || ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_DOWN) || 
			(event.getAction() == MotionEvent.ACTION_MOVE))
		{			
			int pointersCount = event.getPointerCount();
			
			for (int i = 0; i < pointersCount; i++)
				if (this.scale.checkPointInside(event.getX(i), screenH - event.getY(i)))
				{
					this.pointerCheckIndex = i;
					break;
				}			
			
			float tmpH = Math.max(this.width, this.height);
			
			if (this.pointerCheckIndex >= 0)
			{
				if (this.isVertical)
					this.value = ((screenH - event.getY(this.pointerCheckIndex)) - (tmpH / 2.0f) - this.y + tmpH) / tmpH;
				else
					this.value = ((event.getX(this.pointerCheckIndex)) - (tmpH / 2.0f) - this.x + tmpH) / tmpH;
			}
		}
		
		return (this.pointerCheckIndex >= 0);
	}
	@Override
	public float getFloatValue() 
	{
		return this.value;
	}
	
	@Override
	public void setFloatValue(float val) 
	{
		if ((val >= 0.0f) && (val <= 1.0f))			
			this.value = val; 
		else
		{
			if (val < 0.0f)
				this.value = 0.0f;
				
			if (val > 1.0f)
				this.value = 1.0f;
		}
	}
	
	@Override
	public boolean checkPointInside(float pX, float pY) 
	{
		return this.scale.checkPointInside(pX, pY);
	}
	@Override
	public Rect getBounds() 
	{
		return this.scale.getBounds();
	}
	@Override
	public float getRatio() 
	{
		return this.scale.getRatio();
	}
	@Override
	public boolean onTouchDown() 
	{
		return false;	
	}
	@Override
	public boolean onTouchUp(boolean contains) 
	{
		return false;
	}
	@Override
	public void resetState() 
	{
		this.value = 0.5f;
	}	
}