package com.spiritgames.spiritengine.ui;

import java.util.HashMap;
import java.util.Iterator;

import javax.microedition.khronos.opengles.GL10;

import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.menu.ISEMenuItem;

import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;

public class SERadioGroup implements ISEUIElement, ISEMenuItem
{
	private boolean enable = true;
	private boolean visible = true;
	
	private ISEFloatEventHandler onChange;
	
	private HashMap<Integer, SECheckBox> buttons = new HashMap<Integer, SECheckBox>();
	private int checkedButton = Integer.MIN_VALUE;
	public int getCheckedButton() {
		return checkedButton;
	}
	public void setCheckedButton(int checkedButton) {
		this.checkedButton = checkedButton;
	}

	private int defaultButton = Integer.MIN_VALUE;
	
	public SERadioGroup(ISEFloatEventHandler onChange)
	{
		this.onChange = onChange;
	}
	
	public void addButton(int btnKey, float x, float y, float w, float h, int unCheckID, int checkID, boolean setAsDefault, ISEFloatEventHandler onClk)
	{
		SECheckBox newBtn = new SECheckBox(onClk);
		
		newBtn.setPos(x, y);
		newBtn.setSize(w, h);
		newBtn.loadTextures(unCheckID, checkID);
		
		if (setAsDefault)
		{
			this.defaultButton = btnKey;
			newBtn.setChecked(true);
		}
		
		this.buttons.put(btnKey, newBtn);
	}
	
	@Override
	public void resetState() 
	{
		if (!this.buttons.containsKey(this.defaultButton))
			return;
		
		Iterator<Integer> it = this.buttons.keySet().iterator();
		
		while (it.hasNext())
		{
			int nextKey = it.next();
			this.buttons.get(nextKey).setChecked(nextKey == this.defaultButton);
		}
	}

	@Override
	public void release() 
	{
		Iterator<Integer> it = this.buttons.keySet().iterator();
		
		while (it.hasNext())
		{
			this.buttons.get(it.next()).release();
		}
	}

	@Override
	public void restore() 
	{
		Iterator<Integer> it = this.buttons.keySet().iterator();
		
		while (it.hasNext())
		{
			this.buttons.get(it.next()).restore();
		}
	}

	@Override
	public boolean onTouch(MotionEvent event, float screenH) 
	{
		if (!this.enable || !this.visible)
			return false;
		
		Iterator<Integer> it = this.buttons.keySet().iterator();
		
		int checkedKey = Integer.MIN_VALUE;
		while (it.hasNext())
		{
			int currentKey = it.next();
			SECheckBox chBox = this.buttons.get(currentKey);
			
			if (!chBox.isChecked())
			{
				chBox.onTouch(event, screenH);
			
				if (chBox.isChecked())
				{
					checkedKey = currentKey;
					break;
				}
			}
		}
		
		if (checkedKey == Integer.MIN_VALUE)
			return false;
		
		this.checkedButton = checkedKey;
		it = this.buttons.keySet().iterator();
		
		while (it.hasNext())
		{
			int nextKey = it.next();
			this.buttons.get(nextKey).setChecked(nextKey == checkedKey);
		}
		
		this.onChange.onHandle(this.checkedButton);
		
		return true;
	}

	@Override
	public void Render() 
	{
		if (!this.visible)
			return;
		
		Iterator<Integer> it = this.buttons.keySet().iterator();
		
		while (it.hasNext())
		{
			this.buttons.get(it.next()).Render();
		}
	}

	@Override
	public float getFloatValue() {
		return this.checkedButton;
	}

	@Override
	public void setFloatValue(float val) {
		this.checkedButton = (int)val;
		Iterator<Integer> it = this.buttons.keySet().iterator();
		
		while (it.hasNext())
		{
			int nextKey = it.next();
			this.buttons.get(nextKey).setChecked(nextKey == this.checkedButton);
		}
	}

	@Override
	public void setPos(float px, float py) {
	}

	@Override
	public void setRelativePos(float px, float py) {
	}

	@Override
	public float getRelativePosX() {
		return 0;
	}

	@Override
	public float getRelativePosY() {
		return 0;
	}

	@Override
	public void setSize(float w, float h) {
	}

	@Override
	public void setRelativeSize(float w, float h) {	
	}

	@Override
	public float getRelativeWidth() {
		return 0;
	}

	@Override
	public float getRelativeHeight() {
		return 0;
	}

	@Override
	public boolean isEnable() {
		return this.enable;
	}

	@Override
	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public boolean isVisible() {
		return this.visible;
	}

	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	@Override
	public float getWidth() {
		return 0;
	}
	@Override
	public float getHeight() {
		return 0;
	}
	@Override
	public Rect getBounds() {
		return null;
	}
	@Override
	public float getRatio() {
		return 1.0f;
	}
	@Override
	public boolean checkPointInside(float pX, float pY) {
		return false;
	}
	@Override
	public float getX() {
		return 0;
	}
	@Override
	public float getY() {
		return 0;
	}
	@Override
	public boolean onTouchUp(boolean contains) {
		return false;
	}
	@Override
	public boolean onTouchDown() {
		return false;
	}

}
