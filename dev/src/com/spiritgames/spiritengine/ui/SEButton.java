package com.spiritgames.spiritengine.ui;

import android.content.Context;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SEShiftingSprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.menu.ISEMenuItem;
import com.spiritgames.spiritengine.sound.SESoundManager;

public class SEButton extends SEShiftingSprite implements ISEMenuItem, ISEUIElement
{
	protected ISEFloatEventHandler onClick;
	protected float floatParam = 0.0f;
	
	protected boolean pressed = false;
	public void setPressed(boolean val)
	{
		this.pressed = val;
	}
	public boolean getPressed()
	{
		return this.pressed;
	}
	
	public SEButton(ISEFloatEventHandler onClk, float floatParam)
	{
		super(true);		
		
		this.onClick = onClk;
		this.floatParam = floatParam;
	}
	
	public void loadTextures(int enableID, int pressedID, int disableID)
	{
		if (enableID > 0)
		{
			super.addTexture(enableID);
			
			if (pressedID > 0)
			{
				super.addTexture(pressedID);
				
				if (disableID > 0)
					super.addTexture(disableID);
			}
		}
	}
	
	@Override
	public void Render()
	{
		if (!this.pressed)
		{
			if (this.enable)
				this.currentTexture = 0;
			else
				this.currentTexture = 2;
		}
		else
			this.currentTexture = 1;
		
		if (this.texInfo.size() < this.currentTexture + 1)
			this.currentTexture = -1;
		
		super.Render();
	}
	
	@Override
	public boolean onTouchDown() 
	{
		this.pressed = true;
		return true;
	}
	
	@Override
	public boolean onTouchUp(boolean contains) 
	{
		this.pressed = false;
		
		if ((contains) && (this.onClick != null))
		{
			this.onClick.onHandle(this.floatParam);
			return true;
		}
		else
			return false;
	}
	
	@Override
	public boolean onTouch(MotionEvent event, float scrH) 
	{
		if ((!this.enable) || (!this.visible))
		{
			this.pressed = false;
			return false;
		}
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			if (this.checkPointInside(event.getX(), scrH - event.getY()))
			{
				this.onTouchDown();
				return true;
			}
		}
					
		if ((event.getAction() == MotionEvent.ACTION_UP) && (this.pressed))
			return this.onTouchUp(this.checkPointInside(event.getX(), scrH - event.getY()));
		
		return false;
	}
	
	@Override
	public float getFloatValue() 
	{
		return 0.0f;
	}
	
	@Override
	public void setFloatValue(float val) 
	{
		//
	}
	@Override
	public void resetState() 
	{
		this.pressed = false;
	}
}
