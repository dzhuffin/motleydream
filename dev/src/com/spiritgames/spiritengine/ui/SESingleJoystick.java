package com.spiritgames.spiritengine.ui;

import android.content.Context;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class SESingleJoystick extends SETrackControl 
{
	public SESingleJoystick(float width,
			float height, float trWidth, float trHeight, boolean isVertical) 
	{
		super(width, height, trWidth, trHeight, isVertical);
	}

	@Override
	public boolean onTouch(MotionEvent event, float screenH) 
	{
		if ((!this.enable) || (!this.isVisible()))
			return false;
		
		super.onTouch(event, screenH);
		
		boolean checked = false;
		
		if ((event.getAction() == MotionEvent.ACTION_UP) || ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_UP))
		{			
			int pointersCount = event.getPointerCount();
			
			for (int i = 0; i < pointersCount; i++)
				if (this.scale.checkPointInside(event.getX(i), screenH - event.getY(i)))
				{
					checked = true;
					break;
				}	
			if ((!checked) || (checked && pointersCount == 1))
			{
				this.value = 0.5f;
				this.pointerCheckIndex = -1;
			}
		}
		
		return checked;
	}
}