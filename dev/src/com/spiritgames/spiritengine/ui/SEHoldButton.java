package com.spiritgames.spiritengine.ui;

import android.content.Context;
import android.os.SystemClock;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class SEHoldButton extends SEButton implements ISEUIElement
{
	protected ISEFloatEventHandler onHoldOut;
	protected float floatParam2 = 0.0f;
	
	private long repeatTickCount = 0;	
	private long repeatDelay = 50;	
	public long getRepeatDelay() 
	{
		return this.repeatDelay;
	}
	public void setRepeatDelay(long repeatDelay) 
	{
		if (repeatDelay >= 0)
			this.repeatDelay = repeatDelay;
	}

	public SEHoldButton(
			ISEFloatEventHandler onHold, ISEFloatEventHandler onHoldOut, float floatParam, float floatParam2, long repeatDelay) 
	{
		super(onHold, floatParam);
		this.onHoldOut = onHoldOut;
		this.floatParam2 = floatParam2;
		this.setRepeatDelay(repeatDelay);
	}
	
	@Override
	public boolean onTouch(MotionEvent event, float scrH) 
	{
		if ((!this.enable) || (!this.visible))
			return false;
		
		boolean newPress = false;
		
		int pointerCheckIndex = -1;
		
		if ((event.getAction() == MotionEvent.ACTION_MOVE) || (event.getAction() == MotionEvent.ACTION_DOWN) ||
		   ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_DOWN))
		{			
			int pointersCount = event.getPointerCount();
			
			for (int i = 0; i < pointersCount; i++)
				if (this.checkPointInside(event.getX(i), scrH - event.getY(i)))
				{
					pointerCheckIndex = i;
					break;
				}			
			
			newPress = (pointerCheckIndex >= 0);
			
			if ((!newPress) && (this.pressed) && (this.onHoldOut != null))
				this.onHoldOut.onHandle(this.floatParam2);					
		}
		
		this.pressed = newPress;
		
		return (pointerCheckIndex >= 0);
	}
	
	@Override
	public void Render()
	{
		if ((this.pressed) && (SystemClock.uptimeMillis() - this.repeatTickCount > this.repeatDelay))
		{
			if (this.onClick != null)
				this.onClick.onHandle(this.floatParam);
			
			this.repeatTickCount = SystemClock.uptimeMillis();
		}
		
		super.Render();
	}
}
