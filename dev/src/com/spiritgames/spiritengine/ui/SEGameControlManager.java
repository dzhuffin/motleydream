package com.spiritgames.spiritengine.ui;

import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

import com.spiritgames.motleydream.R;
import com.spiritgames.spiritengine.ISEEventHandler;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

import android.content.Context;
import android.view.MotionEvent;

public class SEGameControlManager 
{
	protected float scrW = 0.0f;
	protected float scrH = 0.0f;
	
	private Vector<ISEUIElement> controls;
	
	public float getFloatValue(int n)
	{
		if ((n >= 0) && (n < this.controls.size()) && (this.controls.size() != 0))
			return this.controls.get(n).getFloatValue();
		
		return 0.0f;
	}
	
	public void setFloatValue(int n, float val)
	{
		if ((n >= 0) && (n < this.controls.size()) && (this.controls.size() != 0))
			this.controls.get(n).setFloatValue(val);
	}
	
	public void resetState()
	{
		for (int i = 0; i < this.controls.size(); i++)
			this.controls.get(i).resetState();
	}
	
	public boolean isEnable(int n)
	{
		return this.controls.get(n).isEnable();
	}
	
	public void setEnable(int n, boolean enable)
	{
		this.controls.get(n).setEnable(enable);
	}
	
	public boolean isVisible(int n)
	{
		return this.controls.get(n).isVisible();
	}
	
	public void setVisible(int n, boolean enable)
	{
		this.controls.get(n).setVisible(enable);
	}
	
	public SEGameControlManager()
	{
		this.controls = new Vector<ISEUIElement>();
	}
	
	public int AddButton(int enableID, int pressedID, int disableID, float width, float height, float x, float y, ISEFloatEventHandler onHold, float floatParam)
	{
		SEButton newButton = new SEButton(onHold, floatParam);
		newButton.loadTextures(enableID, pressedID, disableID);
		
		newButton.setRelativeSize(width, height);
		newButton.setRelativePos(x, y);
		newButton.setSize(width, height);
		this.controls.add(newButton);
		
		return this.controls.size() - 1;
	}
	
	public int AddHoldButton(int enableID, int pressedID, int disableID, float width, float height, float x, float y, ISEFloatEventHandler onHold, ISEFloatEventHandler onHoldOut, float floatParam, float floatParam2, long repeatDelay)
	{
		SEHoldButton newButton = new SEHoldButton(onHold, onHoldOut, floatParam, floatParam2, repeatDelay);
		newButton.loadTextures(enableID, pressedID, disableID);
		
		newButton.setRelativeSize(width, height);
		newButton.setRelativePos(x, y);
		newButton.setSize(width, height);
		this.controls.add(newButton);
		
		return this.controls.size() - 1;
	}
	
	public int AddTrackControl(int scaleID, int trackID, float width, float height, float trWidth, float trHeight, boolean isVertical, float x, float y, float value)
	{
		SETrackControl newTrackControl = new SETrackControl(width, height, trWidth, trHeight, isVertical);
		newTrackControl.loadTextures(scaleID, trackID);

		newTrackControl.setRelativeSize(width, height);
		newTrackControl.setRelativePos(x, y);
		newTrackControl.setValue(value);
		
		this.controls.add(newTrackControl);
		
		return this.controls.size() - 1;
	}
	
	public int AddSingleJoystick(int scaleID, int trackID, float width, float height, float trWidth, float trHeight, boolean isVertical, float x, float y)
	{
		SESingleJoystick newSingleJoystick = new SESingleJoystick(width, height, trWidth, trHeight, isVertical);
		newSingleJoystick.loadTextures(scaleID, trackID);

		newSingleJoystick.setRelativeSize(width, height);
		newSingleJoystick.setRelativePos(x, y);		
		newSingleJoystick.setValue(0.5f);
		
		this.controls.add(newSingleJoystick);
		
		return this.controls.size() - 1;
	}
	
	public void release()
	{
		for (int i = 0; i < this.controls.size(); i++)
			((ISEUIElement)this.controls.get(i)).release();
			
		this.controls.clear();
		
		this.scrW = this.scrH = 0.0f;
	}
	
	public void restore()
	{
		for (int i = 0; i < this.controls.size(); i++)
			((ISEUIElement)this.controls.get(i)).restore();
	}
	
	public boolean onTouch(MotionEvent event)
	{	
		boolean touched = false;
		
		for (int i = 0; i < this.controls.size(); i++)
			if (((ISEUIElement)this.controls.get(i)).onTouch(event, this.scrH))
				touched = true;
		
		return touched;
	}
	
	public void Render(float screenW, float screenH)
	{		
		if ((this.scrW != screenW) || (this.scrH != screenH))
		{
			this.scrW = screenW;
			this.scrH = screenH;
			for (int i = 0; i < this.controls.size(); i++)
			{
				ISEUIElement element = ((ISEUIElement)this.controls.get(i));
				element.setPos(element.getRelativePosX() * screenW, element.getRelativePosY() * screenH);
				element.setSize(element.getRelativeWidth() * screenW, element.getRelativeHeight() * screenH);
			}
		}
		
		for (int i = 0; i < this.controls.size(); i++)
			((ISEUIElement)this.controls.get(i)).Render();
	}
}
