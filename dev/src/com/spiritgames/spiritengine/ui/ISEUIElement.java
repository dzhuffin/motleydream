package com.spiritgames.spiritengine.ui;

import javax.microedition.khronos.opengles.GL10;

import com.spiritgames.spiritengine.sound.SESoundManager;

import android.view.MotionEvent;

public interface ISEUIElement 
{
	public void resetState();
	public void release();
	public void restore();
	public boolean onTouch(MotionEvent event, float screenH);
	public void Render();
	
	public float getFloatValue();
	public void setFloatValue(float val);
	
	public void setPos(float px, float py);
	public void setRelativePos(float px, float py);
	public float getRelativePosX();
	public float getRelativePosY();
	
	public void setSize(float w, float h);
	public void setRelativeSize(float w, float h);
	public float getRelativeWidth();
	public float getRelativeHeight();	
		
	public boolean isEnable();
	public void setEnable(boolean enable);
	
	public boolean isVisible();
	public void setVisible(boolean visible);
}