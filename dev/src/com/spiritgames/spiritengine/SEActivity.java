package com.spiritgames.spiritengine;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.FloatMath;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;

import com.spiritgames.spiritengine.graphics.SERenderManager;
import com.spiritgames.spiritengine.graphics.SETextureManager;
import com.spiritgames.spiritengine.sound.SESoundManager;
import com.spiritgames.spiritengine.ui.SESensorReader;

abstract public class SEActivity extends Activity implements OnTouchListener, OnGestureListener, OnKeyListener
{
	protected PowerManager.WakeLock wakeLock; 
	
	protected GLSurfaceView mGLSurfaceView;
	protected GestureDetector gestureScanner;
	protected SESensorReader sensorReader; 
	
	private E_SEActivityState state = E_SEActivityState.MainMenu;
	public E_SEActivityState getState()
	{
		return this.state;
	}
	public void setState(E_SEActivityState val)
	{
		this.state = val;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        SEManagers.setActivity(this);
        
        this.mGLSurfaceView = new GLSurfaceView(this);
        setContentView(this.mGLSurfaceView);
        
        this.mGLSurfaceView.setLongClickable(true);
        this.mGLSurfaceView.setOnTouchListener(this);
        this.mGLSurfaceView.setOnKeyListener(this);
        this.mGLSurfaceView.setFocusable(true);
        this.mGLSurfaceView.setFocusableInTouchMode(true);
        
        this.gestureScanner = new GestureDetector(this);  
        this.gestureScanner.setIsLongpressEnabled(true);
        
        this.sensorReader = new SESensorReader(this);
        
        // ����� ����� �� ����������
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);  
        this.wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
                
        // ����� ������� ��������� ��������/���������� ��������� �����, � �� ������
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }
    
    public void setManagers(SERenderManager rManager, SEGameManager gManager)
    {
    	SEManagers.setRenderManager(rManager);
    	SEManagers.setGameManager(gManager);
    	SEManagers.getGameManager().setSensorReader(this.sensorReader);
    	this.mGLSurfaceView.setRenderer(rManager);
    }       
    
    public boolean onTouch(View v, MotionEvent event) 
	{										    	
		if (this.state == E_SEActivityState.MainMenu)
			return this.menuTouch(v, event);
		
		if (this.state == E_SEActivityState.InGame)
			return this.gameTouch(v, event);
		
		return false;
	}
    
    @Override
	public boolean onKey(View v, int keyCode, KeyEvent event) 
    {
    	if (this.state == E_SEActivityState.MainMenu)
			return this.menuKey(keyCode, event);
		
		if (this.state == E_SEActivityState.InGame)
			return this.gameKey(keyCode, event);
    	
		return false;
	}
    
    private boolean menuKey(int keyCode, KeyEvent event) 
	{
    	if (SEManagers.getRenderManager() != null)
			return SEManagers.getRenderManager().mainMenu.onKey(keyCode, event);
    	
    	return false;
	}
    
    private boolean gameKey(int keyCode, KeyEvent event) 
	{
    	if (SEManagers.getGameManager() != null)
			return SEManagers.getGameManager().onKey(keyCode, event);
    	
    	return false;
	}
    
    @Override
    protected void onResume() 
    {
        super.onResume();
        this.mGLSurfaceView.onResume();
        this.sensorReader.register();
        this.wakeLock.acquire();
    }

    @Override
    protected void onPause() 
    {
        super.onPause();
        
        if (SEManagers.getRenderManager() != null && SEManagers.getGL10Interface() != null) 
        	SETextureManager.release();
        
        this.mGLSurfaceView.onPause();
        this.wakeLock.release();
    }
    
    @Override
    protected void onStop() 
    {
    	this.sensorReader.unRegister();
    	
    	if (SEManagers.getRenderManager() != null && SEManagers.getGL10Interface() != null) 
        	SETextureManager.release();
    	
        super.onStop();
    }
    
    private float pinchScale = 3.5f;
    public float getPinchScale() {
		return this.pinchScale;
	}
    public void resetPinchScale() {
		this.pinchScale = 3.5f;
	}
	private float oldDistance = 0.0f;
    private float spacing(MotionEvent event) 
    {    	
    	if (event.getPointerCount() != 2)
    		return 0.0f;
    	
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
    }
    
	private boolean gameTouch(View v, MotionEvent event) 
	{
		boolean gameProccessed = false;
		
		if (SEManagers.getGameManager() != null)
			gameProccessed = SEManagers.getGameManager().onTouch(event);
		
		// ���� �� ������ �� �� ���� ������� � ����, �� ������������ ���������-���
		if (!gameProccessed)
		{
			if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_UP)
	    	{
	    		this.oldDistance = 0.0f;
	    	}
			
			if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_DOWN)
	    	{
	    		this.oldDistance = this.spacing(event);
	    	}
	    	
	    	if (event.getAction() == MotionEvent.ACTION_MOVE)
	    	{
	    		float newDistance = spacing(event);
	    		if (newDistance > 10.0f) 
	    		{
	    			if (Math.abs(this.oldDistance) > 0.0f)
	    				this.pinchScale += (this.oldDistance - newDistance) / 100.0f;
	    			
	    			if (this.pinchScale > 5.0f)
	    				this.pinchScale = 5.0f;
	    			
	    			if (this.pinchScale < 0.2f)
	    				this.pinchScale = 0.2f;
	    			
	    			this.oldDistance = newDistance;
	    		}
	    	}
		}
		
		return gestureScanner.onTouchEvent(event);
	}
	
	private boolean menuTouch(View v, MotionEvent event) 
	{		
		if ((SEManagers.getRenderManager() != null) && ((event.getAction() == MotionEvent.ACTION_UP) || 
				(event.getAction() == MotionEvent.ACTION_DOWN) || 
				(event.getAction() == MotionEvent.ACTION_MOVE)))
			SEManagers.getRenderManager().mainMenu.onTouch(event);
		
		return gestureScanner.onTouchEvent(event);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {	
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}
}
