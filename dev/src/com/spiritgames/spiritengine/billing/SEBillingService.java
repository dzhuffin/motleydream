package com.spiritgames.spiritengine.billing;

import com.android.vending.billing.IMarketBillingService;
import com.spiritgames.spiritengine.billing.SEBillingConsts.ResponseCode;
import com.spiritgames.spiritengine.billing.SESecurity.VerifiedPurchase;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class SEBillingService extends Service implements ServiceConnection 
{
    private static final String TAG = "BillingService";

    private static IMarketBillingService service;

    private static LinkedList<SEBillingRequest> pendingRequests = new LinkedList<SEBillingRequest>();

    private static HashMap<Long, SEBillingRequest> sentRequests = new HashMap<Long, SEBillingRequest>();

    abstract class SEBillingRequest 
    {
        private final int startId;
        protected long requestId;

        public SEBillingRequest(int startId) 
        {
            this.startId = startId;
        }

        public int getStartId() 
        {
            return startId;
        }

        public boolean runRequest() 
        {
            if (runIfConnected()) 
            {
                return true;
            }

            if (bindToMarketBillingService()) 
            {
                pendingRequests.add(this);
                return true;
            }
            return false;
        }

        public boolean runIfConnected() 
        {
            if (SEBillingConsts.DEBUG) 
            {
                Log.d(TAG, getClass().getSimpleName());
            }
            if (service != null) 
            {
                try 
                {
                    requestId = run();
                    if (SEBillingConsts.DEBUG) 
                    {
                        Log.d(TAG, "request id: " + requestId);
                    }
                    if (requestId >= 0) 
                    {
                        sentRequests.put(requestId, this);
                    }
                    return true;
                } 
                catch (RemoteException e) 
                {
                    onRemoteException(e);
                }
            }
            return false;
        }

        protected void onRemoteException(RemoteException e) 
        {
            Log.w(TAG, "remote billing service crashed");
            service = null;
        }

        abstract protected long run() throws RemoteException;

        protected void responseCodeReceived(ResponseCode responseCode) 
        {
        }

        protected Bundle makeRequestBundle(String method) 
        {
            Bundle request = new Bundle();
            request.putString(SEBillingConsts.BILLING_REQUEST_METHOD, method);
            request.putInt(SEBillingConsts.BILLING_REQUEST_API_VERSION, 1);
            request.putString(SEBillingConsts.BILLING_REQUEST_PACKAGE_NAME, getPackageName());
            return request;
        }

        protected void logResponseCode(String method, Bundle response) 
        {
            ResponseCode responseCode = ResponseCode.valueOf(
                    response.getInt(SEBillingConsts.BILLING_RESPONSE_RESPONSE_CODE));
            if (SEBillingConsts.DEBUG) 
            {
                Log.e(TAG, method + " received " + responseCode.toString());
            }
        }
    }

    class SECheckBillingSupported extends SEBillingRequest 
    {
        public SECheckBillingSupported() 
        {
            super(-1);
        }

        @Override
        protected long run() throws RemoteException 
        {
            Bundle request = makeRequestBundle("CHECK_BILLING_SUPPORTED");
            Bundle response = service.sendBillingRequest(request);
            int responseCode = response.getInt(SEBillingConsts.BILLING_RESPONSE_RESPONSE_CODE);
            if (SEBillingConsts.DEBUG) 
            {
                Log.i(TAG, "CheckBillingSupported response code: " +
                        ResponseCode.valueOf(responseCode));
            }
            boolean billingSupported = (responseCode == ResponseCode.RESULT_OK.ordinal());
            SEResponseHandler.checkBillingSupportedResponse(billingSupported);
            return SEBillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID;
        }
    }

    public class SERequestPurchase extends SEBillingRequest 
    {
        public final String productId;
        public final String developerPayload;

        public SERequestPurchase(String itemId) 
        {
            this(itemId, null);
        }

        public SERequestPurchase(String itemId, String developerPayload) 
        {
            super(-1);
            productId = itemId;
            this.developerPayload = developerPayload;
        }

        @Override
        protected long run() throws RemoteException 
        {
            Bundle request = makeRequestBundle("REQUEST_PURCHASE");
            request.putString(SEBillingConsts.BILLING_REQUEST_ITEM_ID, productId);

            if (developerPayload != null) 
            {
                request.putString(SEBillingConsts.BILLING_REQUEST_DEVELOPER_PAYLOAD, developerPayload);
            }
            Bundle response = service.sendBillingRequest(request);
            PendingIntent pendingIntent
                    = response.getParcelable(SEBillingConsts.BILLING_RESPONSE_PURCHASE_INTENT);
            
            if (pendingIntent == null) 
            {
                Log.e(TAG, "Error with requestPurchase");
                return SEBillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID;
            }

            Intent intent = new Intent();
            SEResponseHandler.buyPageIntentResponse(pendingIntent, intent);
            return response.getLong(SEBillingConsts.BILLING_RESPONSE_REQUEST_ID,
                    SEBillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        @Override
        protected void responseCodeReceived(ResponseCode responseCode) 
        {
        	SEResponseHandler.responseCodeReceived(SEBillingService.this, this, responseCode);
        }
    }

    class SEConfirmNotifications extends SEBillingRequest 
    {
        final String[] notifyIds;

        public SEConfirmNotifications(int startId, String[] notifyIds) 
        {
            super(startId);
            this.notifyIds = notifyIds;
        }

        @Override
        protected long run() throws RemoteException 
        {
            Bundle request = makeRequestBundle("CONFIRM_NOTIFICATIONS");
            request.putStringArray(SEBillingConsts.BILLING_REQUEST_NOTIFY_IDS, notifyIds);
            Bundle response = service.sendBillingRequest(request);
            logResponseCode("confirmNotifications", response);
            return response.getLong(SEBillingConsts.BILLING_RESPONSE_REQUEST_ID,
                    SEBillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }
    }

    class SEGetPurchaseInformation extends SEBillingRequest 
    {
        long mNonce;
        final String[] mNotifyIds;

        public SEGetPurchaseInformation(int startId, String[] notifyIds) {
            super(startId);
            mNotifyIds = notifyIds;
        }

        @Override
        protected long run() throws RemoteException 
        {
            mNonce = SESecurity.generateNonce();

            Bundle request = makeRequestBundle("GET_PURCHASE_INFORMATION");
            request.putLong(SEBillingConsts.BILLING_REQUEST_NONCE, mNonce);
            request.putStringArray(SEBillingConsts.BILLING_REQUEST_NOTIFY_IDS, mNotifyIds);
            Bundle response = service.sendBillingRequest(request);
            logResponseCode("getPurchaseInformation", response);
            return response.getLong(SEBillingConsts.BILLING_RESPONSE_REQUEST_ID,
                    SEBillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        @Override
        protected void onRemoteException(RemoteException e) 
        {
            super.onRemoteException(e);
            SESecurity.removeNonce(mNonce);
        }
    }

    public class RestoreTransactions extends SEBillingRequest 
    {
        long nonce;

        public RestoreTransactions() {
            // This object is never created as a side effect of starting this
            // service so we pass -1 as the startId to indicate that we should
            // not stop this service after executing this request.
            super(-1);
        }

        @Override
        protected long run() throws RemoteException 
        {
            this.nonce = SESecurity.generateNonce();

            Bundle request = makeRequestBundle("RESTORE_TRANSACTIONS");
            request.putLong(SEBillingConsts.BILLING_REQUEST_NONCE, nonce);
            Bundle response = service.sendBillingRequest(request);
            logResponseCode("restoreTransactions", response);
            return response.getLong(SEBillingConsts.BILLING_RESPONSE_REQUEST_ID,
                    SEBillingConsts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        @Override
        protected void onRemoteException(RemoteException e) 
        {
            super.onRemoteException(e);
            SESecurity.removeNonce(nonce);
        }

        @Override
        protected void responseCodeReceived(ResponseCode responseCode) 
        {
            SEResponseHandler.responseCodeReceived(SEBillingService.this, this, responseCode);
        }
    }

    public SEBillingService() 
    {
        super();
    }

    public void setContext(Context context) 
    {
        attachBaseContext(context);
    }

    @Override
    public IBinder onBind(Intent intent) 
    {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        handleCommand(intent, startId);
    }

    public void handleCommand(Intent intent, int startId) 
    {
    	if (intent == null)
    		return;
    	
        String action = intent.getAction();
        if (SEBillingConsts.DEBUG) 
        {
            Log.i(TAG, "handleCommand() action: " + action);
        }
        if (SEBillingConsts.ACTION_CONFIRM_NOTIFICATION.equals(action)) 
        {
            String[] notifyIds = intent.getStringArrayExtra(SEBillingConsts.NOTIFICATION_ID);
            confirmNotifications(startId, notifyIds);
        } 
        else if (SEBillingConsts.ACTION_GET_PURCHASE_INFORMATION.equals(action)) 
        {
            String notifyId = intent.getStringExtra(SEBillingConsts.NOTIFICATION_ID);
            getPurchaseInformation(startId, new String[] { notifyId });
        } 
        else if (SEBillingConsts.ACTION_PURCHASE_STATE_CHANGED.equals(action)) 
        {
            String signedData = intent.getStringExtra(SEBillingConsts.INAPP_SIGNED_DATA);
            String signature = intent.getStringExtra(SEBillingConsts.INAPP_SIGNATURE);
            purchaseStateChanged(startId, signedData, signature);
        } 
        else if (SEBillingConsts.ACTION_RESPONSE_CODE.equals(action)) 
        {
            long requestId = intent.getLongExtra(SEBillingConsts.INAPP_REQUEST_ID, -1);
            int responseCodeIndex = intent.getIntExtra(SEBillingConsts.INAPP_RESPONSE_CODE,
                    ResponseCode.RESULT_ERROR.ordinal());
            ResponseCode responseCode = ResponseCode.valueOf(responseCodeIndex);
            checkResponseCode(requestId, responseCode);
        }
    }

    private boolean bindToMarketBillingService() 
    {
        try 
        {
            if (SEBillingConsts.DEBUG) 
            {
                Log.i(TAG, "binding to Market billing service");
            }
            boolean bindResult = bindService(
                    new Intent(SEBillingConsts.MARKET_BILLING_SERVICE_ACTION),
                    this,  // ServiceConnection.
                    Context.BIND_AUTO_CREATE);

            if (bindResult) 
            {
                return true;
            } else 
            {
                Log.e(TAG, "Could not bind to service.");
            }
        } catch (SecurityException e) 
        {
            Log.e(TAG, "Security exception: " + e);
        }
        return false;
    }

    public boolean checkBillingSupported() 
    {
        return new SECheckBillingSupported().runRequest();
    }

    public boolean requestPurchase(String productId, String developerPayload) 
    {
        return new SERequestPurchase(productId, developerPayload).runRequest();
    }

    public boolean restoreTransactions() 
    {
        return new RestoreTransactions().runRequest();
    }

    private boolean confirmNotifications(int startId, String[] notifyIds) 
    {
        return new SEConfirmNotifications(startId, notifyIds).runRequest();
    }

    private boolean getPurchaseInformation(int startId, String[] notifyIds) 
    {
        return new SEGetPurchaseInformation(startId, notifyIds).runRequest();
    }

    private void purchaseStateChanged(int startId, String signedData, String signature) 
    {
        ArrayList<VerifiedPurchase> purchases;
        purchases = SESecurity.verifyPurchase(signedData, signature);
        if (purchases == null) {
            return;
        }

        ArrayList<String> notifyList = new ArrayList<String>();
        for (VerifiedPurchase vp : purchases) 
        {
            if (vp.notificationId != null) 
            {
                notifyList.add(vp.notificationId);
            }
            SEResponseHandler.purchaseResponse(this, vp.purchaseState, vp.productId,
                    vp.orderId, vp.purchaseTime, vp.developerPayload);
        }
        if (!notifyList.isEmpty()) 
        {
            String[] notifyIds = notifyList.toArray(new String[notifyList.size()]);
            confirmNotifications(startId, notifyIds);
        }
    }

    private void checkResponseCode(long requestId, ResponseCode responseCode) 
    {
        SEBillingRequest request = sentRequests.get(requestId);
        if (request != null) 
        {
            if (SEBillingConsts.DEBUG) 
            {
                Log.d(TAG, request.getClass().getSimpleName() + ": " + responseCode);
            }
            request.responseCodeReceived(responseCode);
        }
        sentRequests.remove(requestId);
    }

    private void runPendingRequests() 
    {
        int maxStartId = -1;
        SEBillingRequest request;
        while ((request = pendingRequests.peek()) != null) 
        {
            if (request.runIfConnected()) 
            {
                pendingRequests.remove();

                if (maxStartId < request.getStartId()) 
                {
                    maxStartId = request.getStartId();
                }
            } 
            else 
            {
                bindToMarketBillingService();
                return;
            }
        }

        if (maxStartId >= 0) 
        {
            if (SEBillingConsts.DEBUG) 
            {
                Log.i(TAG, "stopping service, startId: " + maxStartId);
            }
            stopSelf(maxStartId);
        }
    }

    public void onServiceConnected(ComponentName name, IBinder service) 
    {
        if (SEBillingConsts.DEBUG) 
        {
            Log.d(TAG, "Billing service connected");
        }
        
        SEBillingService.service = IMarketBillingService.Stub.asInterface(service);
        
        runPendingRequests();
    }

    public void onServiceDisconnected(ComponentName name) 
    {
        Log.w(TAG, "Billing service disconnected");
        service = null;
    }

    public void unbind() 
    {
        try 
        {
            unbindService(this);
        } 
        catch (IllegalArgumentException e) 
        {
            // This might happen if the service was disconnected
        }
    }
}
