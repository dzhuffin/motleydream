package com.spiritgames.spiritengine.billing;

import com.spiritgames.spiritengine.billing.SEBillingConsts.PurchaseState;
import com.spiritgames.spiritengine.billing.SEBillingConsts.ResponseCode;
import com.spiritgames.spiritengine.billing.SEBillingService.RestoreTransactions;
import com.spiritgames.spiritengine.billing.SEBillingService.SERequestPurchase;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SEResponseHandler 
{
    private static final String TAG = "ResponseHandler";

    private static SEPurchaseObserver purchaseObserver;

    public static synchronized void register(SEPurchaseObserver observer) 
    {
        purchaseObserver = observer;
    }

    public static synchronized void unregister(SEPurchaseObserver observer) 
    {
        purchaseObserver = null;
    }

    public static void checkBillingSupportedResponse(boolean supported) 
    {
        if (purchaseObserver != null) {
            purchaseObserver.onBillingSupported(supported);
        }
    }

    public static void buyPageIntentResponse(PendingIntent pendingIntent, Intent intent) 
    {
        if (purchaseObserver == null) 
        {
            if (SEBillingConsts.DEBUG) 
            {
                Log.d(TAG, "UI is not running");
            }
            return;
        }
        purchaseObserver.startBuyPageActivity(pendingIntent, intent);
    }

    public static void purchaseResponse
    (
            final Context context, final PurchaseState purchaseState, final String productId,
            final String orderId, final long purchaseTime, final String developerPayload) {

        // Update the database with the purchase state. We shouldn't do that
        // from the main thread so we do the work in a background thread.
        // We don't update the UI here. We will update the UI after we update
        // the database because we need to read and update the current quantity
        // first.
        new Thread(new Runnable() 
        {
            public void run() 
            {
                synchronized(SEResponseHandler.class) 
                {
                    if (purchaseObserver != null) 
                    {
                        purchaseObserver.postPurchaseStateChange(
                                purchaseState, productId, 1, purchaseTime, developerPayload);
                    }
                }
            }
        }).start();
    }

    public static void responseCodeReceived(Context context, SERequestPurchase request,
            ResponseCode responseCode) 
    {
        if (purchaseObserver != null) 
        {
            purchaseObserver.onRequestPurchaseResponse(request, responseCode);
        }
    }

    public static void responseCodeReceived(Context context, RestoreTransactions request,
            ResponseCode responseCode) 
    {
        if (purchaseObserver != null) 
        {
            purchaseObserver.onRestoreTransactionsResponse(request, responseCode);
        }
    }
}