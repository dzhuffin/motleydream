package com.spiritgames.spiritengine.billing;

import java.lang.reflect.Method;

import com.spiritgames.spiritengine.billing.SEBillingConsts.PurchaseState;
import com.spiritgames.spiritengine.billing.SEBillingConsts.ResponseCode;
import com.spiritgames.spiritengine.billing.SEBillingService.RestoreTransactions;
import com.spiritgames.spiritengine.billing.SEBillingService.SERequestPurchase;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Handler;
import android.util.Log;

public abstract class SEPurchaseObserver 
{
	private static final String TAG = "PurchaseObserver";
    private final Activity activity;
    private final Handler handler;
    private Method startIntentSender;
    private Object[] mStartIntentSenderArgs = new Object[5];
    private static final Class[] START_INTENT_SENDER_SIG = new Class[] {
        IntentSender.class, Intent.class, int.class, int.class, int.class
    };

    public SEPurchaseObserver(Activity activity, Handler handler) 
    {
        this.activity = activity;
        this.handler = handler;
        initCompatibilityLayer();
    }

    public abstract void onBillingSupported(boolean supported);

    public abstract void onPurchaseStateChange(PurchaseState purchaseState,
            String itemId, int quantity, long purchaseTime, String developerPayload);


    public abstract void onRequestPurchaseResponse(SERequestPurchase request,
            ResponseCode responseCode);

    public abstract void onRestoreTransactionsResponse(RestoreTransactions request,
            ResponseCode responseCode);

    private void initCompatibilityLayer() 
    {
        try 
        {
            startIntentSender = activity.getClass().getMethod("startIntentSender",
                    START_INTENT_SENDER_SIG);
        } 
        catch (SecurityException e) 
        {
            startIntentSender = null;
        } 
        catch (NoSuchMethodException e) 
        {
            startIntentSender = null;
        }
    }

    void startBuyPageActivity(PendingIntent pendingIntent, Intent intent) 
    {
        if (startIntentSender != null) 
        {
            try 
            {
                mStartIntentSenderArgs[0] = pendingIntent.getIntentSender();
                mStartIntentSenderArgs[1] = intent;
                mStartIntentSenderArgs[2] = Integer.valueOf(0);
                mStartIntentSenderArgs[3] = Integer.valueOf(0);
                mStartIntentSenderArgs[4] = Integer.valueOf(0);
                startIntentSender.invoke(activity, mStartIntentSenderArgs);
            } 
            catch (Exception e) 
            {
                Log.e(TAG, "error starting activity", e);
            }
        } else 
        {
            try 
            {
                pendingIntent.send(activity, 0 /* code */, intent);
            } 
            catch (CanceledException e) 
            {
                Log.e(TAG, "error starting activity", e);
            }
        }
    }

    void postPurchaseStateChange(final PurchaseState purchaseState, final String itemId,
            final int quantity, final long purchaseTime, final String developerPayload) 
    {
        handler.post(new Runnable() 
        {
            public void run() 
            {
                onPurchaseStateChange(purchaseState, itemId, quantity, purchaseTime, developerPayload);
            }
        });
    }
}
