package com.spiritgames.spiritengine.billing;

import com.spiritgames.spiritengine.billing.SEBillingConsts.ResponseCode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SEBillingReceiver extends BroadcastReceiver 
{
    private static final String TAG = "BillingReceiver";

    @Override
    public void onReceive(Context context, Intent intent) 
    {
        String action = intent.getAction();
        if (SEBillingConsts.ACTION_PURCHASE_STATE_CHANGED.equals(action)) 
        {
            String signedData = intent.getStringExtra(SEBillingConsts.INAPP_SIGNED_DATA);
            String signature = intent.getStringExtra(SEBillingConsts.INAPP_SIGNATURE);
            purchaseStateChanged(context, signedData, signature);
        } 
        else if (SEBillingConsts.ACTION_NOTIFY.equals(action)) 
        {
            String notifyId = intent.getStringExtra(SEBillingConsts.NOTIFICATION_ID);
            if (SEBillingConsts.DEBUG) {
                Log.i(TAG, "notifyId: " + notifyId);
            }
            notify(context, notifyId);
        } 
        else if (SEBillingConsts.ACTION_RESPONSE_CODE.equals(action)) 
        {
            long requestId = intent.getLongExtra(SEBillingConsts.INAPP_REQUEST_ID, -1);
            int responseCodeIndex = intent.getIntExtra(SEBillingConsts.INAPP_RESPONSE_CODE,
                    ResponseCode.RESULT_ERROR.ordinal());
            checkResponseCode(context, requestId, responseCodeIndex);
        } 
        else 
        {
            Log.w(TAG, "unexpected action: " + action);
        }
    }

    private void purchaseStateChanged(Context context, String signedData, String signature) 
    {
        Intent intent = new Intent(SEBillingConsts.ACTION_PURCHASE_STATE_CHANGED);
        intent.setClass(context, SEBillingService.class);
        intent.putExtra(SEBillingConsts.INAPP_SIGNED_DATA, signedData);
        intent.putExtra(SEBillingConsts.INAPP_SIGNATURE, signature);
        context.startService(intent);
    }

    private void notify(Context context, String notifyId) 
    {
        Intent intent = new Intent(SEBillingConsts.ACTION_GET_PURCHASE_INFORMATION);
        intent.setClass(context, SEBillingService.class);
        intent.putExtra(SEBillingConsts.NOTIFICATION_ID, notifyId);
        context.startService(intent);
    }

    private void checkResponseCode(Context context, long requestId, int responseCodeIndex) 
    {
        Intent intent = new Intent(SEBillingConsts.ACTION_RESPONSE_CODE);
        intent.setClass(context, SEBillingService.class);
        intent.putExtra(SEBillingConsts.INAPP_REQUEST_ID, requestId);
        intent.putExtra(SEBillingConsts.INAPP_RESPONSE_CODE, responseCodeIndex);
        context.startService(intent);
    }
}
