package com.spiritgames.spiritengine;

public interface ISEFloatEventHandler 
{
	public void onHandle(float param);
}
