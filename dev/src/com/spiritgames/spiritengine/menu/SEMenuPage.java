package com.spiritgames.spiritengine.menu;

import java.util.HashMap;
import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

import com.spiritgames.spiritengine.ISEEventHandler;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.graphics.SEAnimatedSprite;
import com.spiritgames.spiritengine.graphics.SEShiftingSprite;
import com.spiritgames.spiritengine.graphics.SESimpleShiftingParams;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.sound.SESoundManager;
import com.spiritgames.spiritengine.text.SERasterFont;
import com.spiritgames.spiritengine.ui.SEButton;
import com.spiritgames.spiritengine.ui.SECheckBox;
import com.spiritgames.spiritengine.ui.SERadioGroup;
import com.spiritgames.spiritengine.ui.SETrackControl;
import com.spiritgames.spiritengine.ui.ISEUIElement;

import android.content.Context;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class SEMenuPage 
{
	protected Vector<ISEMenuItem> items;
	protected E_SEMenuItemsArrangement arrangement = E_SEMenuItemsArrangement.Column;	
	public E_SEMenuItemsArrangement getItemsArrangement() {
		return arrangement;
	}
	public void setItemsArrangement(E_SEMenuItemsArrangement arrangement) {
		this.arrangement = arrangement;
	}
	
	protected HashMap<Integer, SESimpleShiftingParams> startShiftingMap = new HashMap<Integer, SESimpleShiftingParams>();
	protected HashMap<Integer, SESimpleShiftingParams> endShiftingMap = new HashMap<Integer, SESimpleShiftingParams>();

	protected float leftOffset = 0.0f;
	protected float rightOffset = 0.0f;
	protected float topOffset = 0.0f;
	protected float bottomOffset = 0.0f;
	
	protected float scrW = 0.0f;
	protected float scrH = 0.0f;
	
	protected boolean recalc = false;	
	
	public SEMenuPage()
	{
		this.items = new Vector<ISEMenuItem>();
	}
	
	public int addLabel(SERasterFont font, String text, float textSize)
	{		
		this.items.add(new SEMenuTextItem(font, text, textSize));
		
		return this.items.size() - 1;
	}
	
	public int addButton(float ratio, int enableID, int pressedID, int disableID, ISEFloatEventHandler onClk, float floatParam)
	{
		this.items.add(new SEButton(onClk, floatParam));
		((SEButton)this.items.get(this.items.size() - 1)).setRatio(ratio);
		((SEButton)this.items.get(this.items.size() - 1)).loadTextures(enableID, pressedID, disableID);
		
		return this.items.size() - 1;
	}
	
	public int addCheckBox(float ratio, int unCheckID, int checkID, ISEFloatEventHandler onClk)
	{
		this.items.add(new SECheckBox(onClk));
		((SECheckBox)this.items.get(this.items.size() - 1)).setRatio(ratio);
		((SECheckBox)this.items.get(this.items.size() - 1)).loadTextures(unCheckID, checkID);
		
		return this.items.size() - 1;
	}
	
	public int addRadioGroup(SERadioGroup group)
	{
		this.items.add(group);
		
		return this.items.size() - 1;
	}
	
	public int addTrackControl(float ratio, float relativeTrackWidth, float relativeTrackHeight, int scaleID, int trackID)
	{
		this.items.add(new SETrackControl());
		((SETrackControl)this.items.get(this.items.size() - 1)).setRatio(ratio);
		((SETrackControl)this.items.get(this.items.size() - 1)).loadTextures(scaleID, trackID);
		((SETrackControl)this.items.get(this.items.size() - 1)).setRelativeTrackSizeMem(relativeTrackWidth, relativeTrackHeight);
		
		return this.items.size() - 1;
	}
	
	public int addImage(float w, float h, int imageID)
	{
		this.items.add(new SESprite(true));
		((SESprite)this.items.get(this.items.size() - 1)).setCurrentTexture(
				((SESprite)this.items.get(this.items.size() - 1)).addTexture(imageID));
		
		int i = this.items.size() - 1;
		this.setItemSize(i, w, h);
		
		return i;
	}
	
	public int addAnimatedImage(float ratio, int firstFrameResID, int framesCount, int animationSpeed)
	{
		this.items.add(new SEAnimatedSprite(true));
		((SEAnimatedSprite)this.items.get(this.items.size() - 1)).setRatio(ratio);
		((SEAnimatedSprite)this.items.get(this.items.size() - 1)).loadAnimation(firstFrameResID, framesCount, animationSpeed);
		((SEAnimatedSprite)this.items.get(this.items.size() - 1)).startAnimation();
		
		return this.items.size() - 1;
	}
	
	public int addShiftingImage(float ratio, int imageID, SESimpleShiftingParams startShifting, SESimpleShiftingParams endShifting)
	{
		this.items.add(new SEShiftingSprite(true));
		((SEShiftingSprite)this.items.get(this.items.size() - 1)).setRatio(ratio);
		((SEShiftingSprite)this.items.get(this.items.size() - 1)).setCurrentTexture(
				((SEShiftingSprite)this.items.get(this.items.size() - 1)).addTexture(imageID));
		
		this.startShiftingMap.put(this.items.size() - 1, startShifting);
		this.endShiftingMap.put(this.items.size() - 1, endShifting);
		
		return this.items.size() - 1;
	}
	
	public boolean isShiftingFinish()
	{
		for (int i = 0; i < this.items.size(); i++)
			if ((((ISEMenuItem)this.items.get(i)).getClass() == SEShiftingSprite.class) &&
				(((SEShiftingSprite)this.items.get(i)).isShiftingPlaying()))
					return false;
		
		return true;
	}
	
	public void runStartShifting()
	{
		for (int i = 0; i < this.items.size(); i++)
			if (((ISEMenuItem)this.items.get(i)).getClass() == SEShiftingSprite.class)
			{
				((SEShiftingSprite)this.items.get(i)).setupSimpleShifting(this.startShiftingMap.get(i));
				((SEShiftingSprite)this.items.get(i)).startShifting();
			}
	}
	
	public void runEndShifting()
	{
		for (int i = 0; i < this.items.size(); i++)
			if (((ISEMenuItem)this.items.get(i)).getClass() == SEShiftingSprite.class)
			{
				((SEShiftingSprite)this.items.get(i)).setupSimpleShifting(this.endShiftingMap.get(i));
				((SEShiftingSprite)this.items.get(i)).startShifting();
			}
	}
	
	public float getItemFloatValue(int i)
	{
		return ((ISEUIElement)this.items.get(i)).getFloatValue();
	}
	
	public void setItemFloatValue(int i, float val)
	{
		((ISEUIElement)this.items.get(i)).setFloatValue(val);
	}
	
	public boolean isItemEnable(int i)
	{
		return ((ISEUIElement)this.items.get(i)).isEnable();
	}
	
	public void setItemEnable(int i, boolean val)
	{		
		((ISEMenuItem)this.items.get(i)).setEnable(val);
	}
	
	public void setItemVisible(int i, boolean val)
	{		
		((ISEMenuItem)this.items.get(i)).setVisible(val);
	}
	
	public void setItemText(int i, String val)
	{
		if ((i < 0) || (i >= this.items.size()))
			return;
		
		ISEMenuItem item = ((ISEMenuItem)this.items.get(i)); 
		if (item.getClass() != SEMenuTextItem.class)
			return;
		
		((SEMenuTextItem)item).setText(val);
	}
	
	/***
	 * ������������� ������������� ������� menu item �� ������. �.�. ��������� ��� ����� HVGA(480x320), ��� ����������� �� ��� �������� ��������
	 * @param i
	 * ����� menu item
	 * @param x
	 * ���������� x menu item 
	 * @param y
	 * ���������� y menu item
	 */
	public void setItemPos(int i, float x, float y)
	{
		float xd = (float)SEManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480.0f;
		float yd = (float)SEManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320.0f;
		((ISEMenuItem)this.items.get(i)).setPos(x * xd, y * yd);
	}
	
	/***
	 * ������������� ������������� ������� menu item. �.�. ��������� ��� ����� HVGA(480x320), ��� ����������� �� ��� �������� ��������
	 * @param i
	 * ����� menu item
	 * @param w
	 * ������������� ������ menu item 
	 * @param h
	 * ������������� ������ menu item
	 */
	public void setItemSize(int i, float w, float h)
	{
		float dx = (float)SEManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480.0f;
		float dy = (float)SEManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320.0f;
		((ISEMenuItem)this.items.get(i)).setSize(w * dx, h * dy);
	}
	
	public void setOffset(int type, float val)
	{
		if ((val >= 0.0f) && (val <= 1.0f))
		{
			switch (type)
			{
				case SEMenuManager.PAGE_OFFSET_LEFT:
					this.leftOffset = val;
					break;
				case SEMenuManager.PAGE_OFFSET_RIGHT:
					this.rightOffset = val;
					break;
				case SEMenuManager.PAGE_OFFSET_TOP:
					this.topOffset = val;
					break;
				case SEMenuManager.PAGE_OFFSET_BOTTOM:
					this.bottomOffset = val;
					break;
				default:
					break;
			}
			
			this.recalc = true;
		}
	}
	
	protected void ProcessItemArrangement(int i, Rect pageRect, float screenW, float screenH)
	{
		boolean trackControl = false;
		int bSize;
		
		int rectCenter = (int)(pageRect.left + (float)pageRect.width() / 2.0f);
		int calcH = 0;		
		
		if (this.arrangement == E_SEMenuItemsArrangement.Column)
		{
			bSize = (int)((Math.abs((float)(pageRect.top - pageRect.bottom))) / (float)(this.items.size() + 1));
			trackControl = (this.items.get(i).getClass() == SETrackControl.class);
			
			if (screenW <= screenH)
			{
				if (trackControl)
					((ISEMenuItem)this.items.get(i)).setSize((float)pageRect.width() / ((ISEMenuItem)this.items.get(i)).getRatio(), pageRect.width());
				else
					((ISEMenuItem)this.items.get(i)).setSize(pageRect.width(), (float)pageRect.width() / ((ISEMenuItem)this.items.get(i)).getRatio());			
			}
			else
			{
				calcH = (int)((Math.abs((float)(pageRect.top - pageRect.bottom))) / (float)this.items.size() - (Math.abs((float)(pageRect.top - pageRect.bottom))) * 0.1f);
				if (trackControl)
					((ISEMenuItem)this.items.get(i)).setSize(calcH, calcH * ((ISEMenuItem)this.items.get(i)).getRatio());
				else
					((ISEMenuItem)this.items.get(i)).setSize(calcH * ((ISEMenuItem)this.items.get(i)).getRatio(), calcH);
			}
			((ISEMenuItem)this.items.get(i)).setPos(rectCenter, pageRect.top - (i * bSize + bSize));
		}
		
		if (this.arrangement == E_SEMenuItemsArrangement.Table)
		{
			bSize = (int)((Math.abs((float)(pageRect.top - pageRect.bottom))) / (float)(this.items.size() / 2 + 1));
			trackControl = (this.items.get(i).getClass() == SETrackControl.class);
						
			if (screenW <= screenH)
			{
				if (trackControl)
					((ISEMenuItem)this.items.get(i)).setSize((float)pageRect.width() / ((ISEMenuItem)this.items.get(i)).getRatio(), pageRect.width());
				else
					((ISEMenuItem)this.items.get(i)).setSize(pageRect.width(), (float)pageRect.width() / ((ISEMenuItem)this.items.get(i)).getRatio());			
			}
			else
			{
				if (trackControl)
					((ISEMenuItem)this.items.get(i)).setSize((pageRect.width() / 2) / ((ISEMenuItem)this.items.get(i)).getRatio(), pageRect.width() / 2);
				else
					((ISEMenuItem)this.items.get(i)).setSize(pageRect.width() / 2, (pageRect.width() / 2) / ((ISEMenuItem)this.items.get(i)).getRatio());
			}
			
			if (i + 1 <= this.items.size() / 2)
				((ISEMenuItem)this.items.get(i)).setPos(rectCenter - pageRect.width() / 4, pageRect.top - (i * bSize + bSize));
			else
				((ISEMenuItem)this.items.get(i)).setPos(rectCenter + pageRect.width() / 4, pageRect.top - ((i - this.items.size() / 2) * bSize + bSize));
		}
	}
	
	protected void recalcItemsPos(float screenW, float screenH)
	{
		Rect pageRect = new Rect();

		pageRect.left = (int)(screenW * this.leftOffset);
		pageRect.right = (int)(screenW - (screenW * this.rightOffset));
		pageRect.top = (int)(screenH - (screenH * this.topOffset));
		pageRect.bottom = (int)(screenH * this.bottomOffset);					
		
		for (int i = 0; i < this.items.size(); i++)
			this.ProcessItemArrangement(i, pageRect, screenW, screenH);
		
		this.recalc = false;
	}
	
	public void release()
	{			
		if (this.items != null)
		{
			for (int i = 0 ; i < this.items.size(); i++)
				this.items.get(i).release();
			
			this.items.clear();
		}
	}
	
	public void restore()
	{
		for (int i = 0 ; i < this.items.size(); i++)
			this.items.get(i).restore();
	}
	
	public boolean onTouch(MotionEvent event)
	{	
		if (!this.isShiftingFinish())
			return false;
		
		boolean result = false;
		
		for (int i = 0; i < this.items.size(); i++)
		{
			if (((ISEMenuItem)this.items.get(i)).onTouch(event, this.scrH))
				result = true;
		}
		
		return result;
	}
	
	public boolean onKey(int keyCode, KeyEvent event) 
    {
		if (keyCode == KeyEvent.KEYCODE_BACK)
			return true;
		
		return false;
    }
	
	public void Render(float screenW, float screenH)
	{
		if ((this.scrW != screenW) || (this.scrH != screenH))
		{
			this.scrW = screenW;
			this.scrH = screenH;
			this.recalc = true;
		}
		
		if ((this.recalc) && (this.arrangement != E_SEMenuItemsArrangement.Custom))
			this.recalcItemsPos(screenW, screenH);
		
		for (int i = 0; i < this.items.size(); i++)
			((ISEMenuItem)this.items.get(i)).Render();
	}
}
