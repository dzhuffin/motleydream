package com.spiritgames.spiritengine.menu;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Rect;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.SEMath;
import com.spiritgames.spiritengine.text.SERasterFont;

public class SEMenuTextItem implements ISEMenuItem
{
	private SERasterFont font;
	
	private float x = 0.0f;
	private float y = 0.0f;
	
	private float textSize = 30.0f;	
	public float getTextSize() {
		return textSize;
	}
	public void setTextSize(float textSize) {
		this.textSize = textSize;
	}
	
	private String text;	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public SEMenuTextItem(SERasterFont font, float textSize)
	{
		this.font = font;
	}
	
	public SEMenuTextItem(SERasterFont font, String text, float textSize)
	{
		this.font = font;
		this.setText(text);
		this.setTextSize(textSize);
	}
	@Override
	public void Render() 
	{
		if (this.visible)
			this.font.RenderText(this.x, this.y, this.textSize, this.text, false);		
	}
	@Override
	public float getWidth() 
	{
		return this.font.MeasureText(this.textSize, this.text).x;
	}
	@Override
	public float getHeight() {
		return this.font.MeasureText(this.textSize, this.text).y;
	}
	@Override
	public Rect getBounds() {
		Rect result = new Rect();
		
		result.left = (int)this.x;
		result.right = (int)(this.x + this.getWidth());
		result.top = (int)this.y;
		result.bottom = (int)(this.y + this.getHeight());
		
		return result;
	}
	@Override
	public float getRatio() {
		return this.getWidth() / this.getHeight();
	}
	@Override
	public boolean isEnable() {
		return true;
	}
	@Override
	public void setEnable(boolean enable) {
	}
	@Override
	public boolean checkPointInside(float pX, float pY) {
		float[] verts = new float[8];
		Rect rect = this.getBounds();
		
		verts[0] = rect.left;
		verts[1] = rect.top;
		verts[2] = rect.right;
		verts[3] = rect.top;
		verts[4] = rect.right;
		verts[5] = rect.bottom;
		verts[6] = rect.left;
		verts[7] = rect.bottom;
		
		return SEMath.checkPointInsideRect(verts, pX, pY);
	}
	@Override
	public float getX() {
		return this.x;
	}
	@Override
	public float getY() {
		return this.y;
	}
	@Override
	public void setPos(float px, float py) {
		this.x = px;
		this.y = py;
	}
	@Override
	public void setSize(float w, float h) {
		this.textSize = h;
	}
	@Override
	public boolean onTouch(MotionEvent event, float scrH) {
		return false;
	}
	@Override
	public boolean onTouchUp(boolean contains) {
		return false;
	}
	@Override
	public boolean onTouchDown() {
		return false;
	}
	@Override
	public void release() {
	}
	@Override
	public void restore() {
	}
	
	private boolean visible = true;
	@Override
	public boolean isVisible() 
	{
		return this.visible;
	}
	@Override
	public void setVisible(boolean visible) 
	{
		this.visible = visible;
	}

}
