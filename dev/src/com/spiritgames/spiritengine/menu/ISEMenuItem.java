package com.spiritgames.spiritengine.menu;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Rect;
import android.view.MotionEvent;

public interface ISEMenuItem 
{
	public void Render();
	
	public float getWidth();
	public float getHeight();
	public Rect getBounds();
	public float getRatio();
	
	public boolean isEnable();
	public void setEnable(boolean enable);
	
	public boolean isVisible();
	public void setVisible(boolean visible);
	
	public boolean checkPointInside(float pX, float pY);
	
	public float getX();
	public float getY();
	public void setPos(float px, float py);
	public void setSize(float w, float h);
	
	public boolean onTouch(MotionEvent event, float scrH);
	public boolean onTouchUp(boolean contains);
	public boolean onTouchDown();
	
	public void release();
	public void restore();
}
