package com.spiritgames.spiritengine.menu;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.sound.SESoundManager;
import com.spiritgames.spiritengine.ui.SEButton;

public class SEKineticMenuPage extends SEMenuPage  
{
	// kinetic scrolling params
	private boolean nowScrolling = false;
	private float oldX = 0.0f;
	private float scrollPosMin = 0.0f;
	private float scrollPosMax = 0.0f;
	protected float scrollPos = 0.0f;
	private int scrollVelocity = 0;
	private int maxScrollVelocity = 50;
	private int timerCount = 0;
	
	private float widthFactor = 1.0f;
	
	protected int buttonsCount = 0;
	
	protected ISEFloatEventHandler onClick;
		
	protected Rect pageRect = new Rect();
	
	private void clipScrollPosition()
    {      
        if (this.scrollPos < this.scrollPosMin)
        {
			this.scrollPos = this.scrollPosMin;
			this.scrollVelocity = 0;
        }
        else
        {		
			if (this.scrollPos > this.scrollPosMax)
			{
				this.scrollPos = this.scrollPosMax;
				this.scrollVelocity = 0;
			}
        }
    }
	
    private void clipVelocity()
    {
        this.scrollVelocity = Math.min(this.scrollVelocity, this.maxScrollVelocity);
        this.scrollVelocity = Math.max(this.scrollVelocity, -this.maxScrollVelocity);
    }
    
    public void updateButtonsList(int reachedLevel)
    {
    	for (int i = 1; i <= this.buttonsCount; i++)
			this.setItemEnable(i - 1, (i <= reachedLevel));
    }
	
	public SEKineticMenuPage(int buttonsCount, ISEFloatEventHandler loadLevel, float widthFactor) 
	{
		super();
		
		this.widthFactor = widthFactor;
		
		this.buttonsCount = buttonsCount;
		
		this.onClick = loadLevel;
		
		this.recalc = true;
	}
	
	@Override
	public void release()
	{			
		super.release();
	}
	
	@Override
	protected void recalcItemsPos(float screenW, float screenH)
	{
		this.pageRect.left = (int)(screenW * this.leftOffset);
		this.pageRect.right = (int)(screenW - (screenW * this.rightOffset));
		this.pageRect.top = (int)(screenH - (screenH * this.topOffset));
		this.pageRect.bottom = (int)(screenH * this.bottomOffset);		

		float w = 0.0f, h = 0.0f;
		
		int buttonsCount = 0;
		for (int i = 0; i < this.items.size(); i++)
		{
			if (this.items.get(i).getClass() != SEButton.class)
				continue;
			
			buttonsCount++;
			
			ISEMenuItem item = ((ISEMenuItem)this.items.get(i));						
			
			if (screenW <= screenH)
			{
				w = Math.abs(this.pageRect.width());
				h = Math.abs((float)this.pageRect.width() / item.getRatio());
			}
			else
			{
				h = Math.abs(this.pageRect.height());
				w = Math.abs(h * item.getRatio());
				item.setSize(w * this.widthFactor, h);
			}
			
			item.setSize(w * this.widthFactor, h);
			
			float cX = (1.2f * w / 2.0f) + (1.2f * w * i);
			float cY = this.pageRect.centerY();
			
			item.setPos(cX, cY);
		}
		
		this.scrollPosMax = screenW / 2.0f;
		this.scrollPosMin = this.scrollPosMax - 1.2f * buttonsCount * w * this.widthFactor;
		
		this.recalc = false;
	}
	
	@Override
	public boolean onTouch(MotionEvent event)
	{				
		if (!((this.scrH - (int)event.getY() < this.pageRect.top) &&
			  (this.scrH - (int)event.getY() > this.pageRect.bottom)))
			return false;
		
		MotionEvent fakeEvent = MotionEvent.obtain(event);
		fakeEvent.setAction(MotionEvent.ACTION_UP);
		fakeEvent.setLocation(-10.0f, -10.0f);				
		
		boolean result;
		if ((!this.nowScrolling) && (this.scrollVelocity < 10))
			result = super.onTouch(event);
		else
			result = super.onTouch(fakeEvent);
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			this.oldX = event.getX();
		}
		
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			this.nowScrolling = false;
		}
		
		if (event.getAction() == MotionEvent.ACTION_MOVE)
		{
			float distance = event.getX() - this.oldX;
            this.scrollVelocity = (int)(distance / 3.0f);
            this.clipVelocity();

            this.scrollPos += distance;
            this.clipScrollPosition();

            this.oldX = event.getX();
            
            this.nowScrolling = (Math.abs(distance) > 2.0f);
		}
		
		return result;
	}
	
	protected void updateScroll(float screenW, float screenH)
	{
        if (this.scrollVelocity != 0)
        {
            this.scrollPos += this.scrollVelocity;
            this.clipScrollPosition();

            if (((++this.timerCount) % 10) == 0)
            {
                if (this.scrollVelocity < 0)
                {
                	this.scrollVelocity++;
                }
                else if (this.scrollVelocity > 0)
                {
                	this.scrollVelocity--;
                }
            }
        }	
	}
	
	@Override
	public void Render(float screenW, float screenH)
	{
		if ((this.scrW != screenW) || (this.scrH != screenH))
		{
			this.scrW = screenW;
			this.scrH = screenH;
			this.recalc = true;
		}		
		
		if (this.recalc)
			this.recalcItemsPos(screenW, screenH);
		
		this.updateScroll(screenW, screenH);
		
		for (int i = 0; i < this.items.size(); i++)
			((ISEMenuItem)this.items.get(i)).Render();
	}
}
