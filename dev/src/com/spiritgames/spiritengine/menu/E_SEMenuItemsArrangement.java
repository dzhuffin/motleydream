package com.spiritgames.spiritengine.menu;

public enum E_SEMenuItemsArrangement 
{
	Custom(0),
	Column(1),
	Table(2);
    
    private int index;
	
    E_SEMenuItemsArrangement(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_SEMenuItemsArrangement getByIndex(int i)
	{
		switch(i)
		{	
			case 0:
				return E_SEMenuItemsArrangement.Custom;
			case 1:
				return E_SEMenuItemsArrangement.Column;
			case 2:
				return E_SEMenuItemsArrangement.Table;
			default:
				return E_SEMenuItemsArrangement.Column;
		}		
	}
}
