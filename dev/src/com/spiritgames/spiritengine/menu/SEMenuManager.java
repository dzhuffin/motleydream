package com.spiritgames.spiritengine.menu;

import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SESimpleShiftingParams;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.text.SERasterFont;

public class SEMenuManager 
{
	private int activeLogoPage = -1;
	private long logoTickCount = 0;
	
	private Vector<SEMenuPage> pages;
	private Vector<SEMenuPage> logoPages;
	private SESprite background;
	
	public final static int PAGE_OFFSET_LEFT = 1;
	public final static int PAGE_OFFSET_RIGHT = 2;
	public final static int PAGE_OFFSET_TOP = 3;
	public final static int PAGE_OFFSET_BOTTOM = 4;
	
	public final static int ACTIVE_PAGE_NONE = -1;
	
	private int activePage = -1;
	private int newActivePage = -1;
	public int getActivePage()
	{
		return this.activePage;
	}
	public void setActivePage(int val, boolean force)
	{
		if ((val >= SEMenuManager.ACTIVE_PAGE_NONE) && (val <= this.pages.size() - 1))
		{
			if (force)
			{
				this.activePage = val;
			}
			else
			{
				if ((this.activePage > SEMenuManager.ACTIVE_PAGE_NONE) && (this.activePage <= this.pages.size() - 1))
					((SEMenuPage)this.pages.get(this.activePage)).runEndShifting();
				
				this.newActivePage = val;
			}
		}
	}
	
	public SEMenuManager()
	{
		this.pages = new Vector<SEMenuPage>();
		this.logoPages = new Vector<SEMenuPage>();
		this.background = new SESprite(true);
	}
	
	public boolean isLogoCompleted()
	{
		return (this.activeLogoPage >= this.logoPages.size());
	}
	
	public void setItemArrangement(int index, float x, float y, float w, float h)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
		{
			((SEMenuPage)this.pages.get(this.activePage)).setItemPos(index, x, y);
			((SEMenuPage)this.pages.get(this.activePage)).setItemSize(index, w, h);
		}
	}
	
	public void setItemText(int index, String text)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
		{
			((SEMenuPage)this.pages.get(this.activePage)).setItemText(index, text);
		}
	}
	
	public void setItemVisible(int index, boolean val)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
		{
			((SEMenuPage)this.pages.get(this.activePage)).setItemVisible(index, val);
		}
	}
	
	public void setPageOffset(int type, float val)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			((SEMenuPage)this.pages.get(this.activePage)).setOffset(type, val);
	}
	
	public void setPageArrangement(E_SEMenuItemsArrangement val)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			((SEMenuPage)this.pages.get(this.activePage)).setItemsArrangement(val);
	}
	
	public void setPageArrangement(int pageID, E_SEMenuItemsArrangement val)
	{
		if ((pageID >= 0) && (pageID <= this.pages.size() - 1))
			((SEMenuPage)this.pages.get(pageID)).setItemsArrangement(val);
	}
	
	public int addBackground(int backgroundResID)
	{
		return this.background.addTexture(backgroundResID);
	}
	
	public void setCurrentBackground(int val)
	{
		this.background.setCurrentTexture(val);
	}
	
	public int addPage()
	{
		this.pages.add(new SEMenuPage());		
		return this.pages.size() - 1;
	}
	
	public int addPage(SEMenuPage page)
	{
		this.pages.add(page);		
		return this.pages.size() - 1;
	}
	
	public int addLogoPage(int logoID, float w, float h)
	{
		SEMenuPage logoPage = new SEMenuPage();
		logoPage.setItemPos(logoPage.addImage(w, h, logoID), 
				240.0f, 160.0f);
		this.logoPages.add(logoPage);
		
		int i = this.logoPages.size() - 1;
		this.logoPages.get(i).setItemsArrangement(E_SEMenuItemsArrangement.Custom);
		return i;
	}
	
	public int addLabel(SERasterFont font, String text, float textSize)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addLabel(font, text, textSize);
		
		return 0;
	}
	
	public int addButton(float ratio, int enableID, int pressedID, int disableID, ISEFloatEventHandler onClk, float floatParam)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addButton(ratio, enableID, pressedID, disableID, onClk, floatParam);
		
		return 0;
	}	
	
	public int addCheckBox(float ratio, int unCheckID, int checkID, ISEFloatEventHandler onClk)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addCheckBox(ratio, unCheckID, checkID, onClk);
		
		return 0;
	}
	
	public int addTrackControl(float ratio, float relativeTrackWidth, float relativeTrackHeight, int scaleID, int trackID)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addTrackControl(ratio, relativeTrackWidth, relativeTrackHeight, scaleID, trackID);
		
		return 0;
	}
	
	public int addImage(float w, float h, int imageID)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addImage( w, h, imageID);
		
		return 0;
	}
	
	public int addImage(int imageID)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addImage(0.0f, 0.0f, imageID);
		
		return 0;
	}
	
	public int addAnimatedImage(float ratio, int firstFrameResID, int framesCount, int animationSpeed)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addAnimatedImage(ratio, firstFrameResID, framesCount, animationSpeed);
		
		return 0;
	}
	
	public int addShiftingImage(float ratio, int imageID, SESimpleShiftingParams startShifting, SESimpleShiftingParams endShifting)
	{
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).addShiftingImage(ratio, imageID, startShifting, endShifting);
		
		return 0;
	}
	
	public void release()
	{
		for (int i = 0; i < this.pages.size(); i++)
			((SEMenuPage)this.pages.get(i)).release();
			
		this.pages.clear();
		
		for (int i = 0; i < this.logoPages.size(); i++)
			((SEMenuPage)this.logoPages.get(i)).release();
			
		this.logoPages.clear();
		
		this.background.release();
	}
	
	public void restore()
	{
		for (int i = 0; i < this.pages.size(); i++)
			((SEMenuPage)this.pages.get(i)).restore();
		
		for (int i = 0; i < this.logoPages.size(); i++)
			((SEMenuPage)this.logoPages.get(i)).restore();
		
		this.background.restore();
	}
	
	public boolean onTouch(MotionEvent event)
	{		
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			return ((SEMenuPage)this.pages.get(this.activePage)).onTouch(event);
		
		return false;
	}
	
	public boolean onKey(int keyCode, KeyEvent event) 
    {
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
		{
			if (((SEMenuPage)this.pages.get(this.activePage)).onKey(keyCode, event))
			{
				if (this.activePage == 0)
				{
					return false;
				}
				else
				{
					this.setActivePage(0, false);
					return true;
				}
			}
		}
		
		return false;
    }
	
	public void Render(float screenW, float screenH)
	{	
		if (!this.isLogoCompleted())
		{
			if ((SystemClock.uptimeMillis() - this.logoTickCount) > 3000L)
			{
				this.activeLogoPage++;
				this.logoTickCount = SystemClock.uptimeMillis();
			}
			
			if (!this.isLogoCompleted())
				((SEMenuPage)this.logoPages.get(this.activeLogoPage)).Render(screenW, screenH);
			
			return;
		}
		
		if (this.newActivePage > SEMenuManager.ACTIVE_PAGE_NONE)
		{
			if (this.activePage > SEMenuManager.ACTIVE_PAGE_NONE)
			{
				if (((SEMenuPage)this.pages.get(this.activePage)).isShiftingFinish())
				{
					this.activePage = this.newActivePage;
					this.newActivePage = SEMenuManager.ACTIVE_PAGE_NONE;
					((SEMenuPage)this.pages.get(this.activePage)).runStartShifting();
				}
			}
			else
			{
				this.activePage = this.newActivePage;
				this.newActivePage = SEMenuManager.ACTIVE_PAGE_NONE;
			}
		}
		
		this.background.setSize(screenW, screenH);
		this.background.setPos(screenW / 2.0f, screenH / 2.0f);		
		this.background.Render();
		
		if ((this.activePage >= 0) && (this.activePage <= this.pages.size() - 1))
			((SEMenuPage)this.pages.get(this.activePage)).Render(screenW, screenH);		
	}
}