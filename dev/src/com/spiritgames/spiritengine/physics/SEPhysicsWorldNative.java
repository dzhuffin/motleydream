package com.spiritgames.spiritengine.physics;

import java.util.ArrayList;
import java.util.Vector;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.spiritgames.motleydream.enums.E_MBOrientation;

import android.os.SystemClock;
import android.util.Log;

public class SEPhysicsWorldNative 
{
	static { System.loadLibrary("gdx"); }

	public static float PHYSICS_SCALE = 100.0f;
	
	public int targetFPS = 60;  
    //public float timeStep = (1.0f / (float)targetFPS);  
	public float timeStep = 0.05f;
    public int iterations = 5;
    private long tickCount = 0;
	
    private Vector<Body> bodies;
    private Vector<Joint> joints;
    
    private Body staticBody;
  
    private World world;
    
    public SEPhysicsWorldNative(float xGravity, float yGravity, float screenW, float screenH)
    {
    	this.bodies = new Vector<Body>();
    	this.joints = new Vector<Joint>();
    	
        Vector2 gravity = new Vector2(xGravity, yGravity);  
        boolean doSleep = true;  
        this.world = new World(gravity, doSleep);
        ////////////////////////////////////////////////////
        BodyDef groundBodyDef = new BodyDef();
        groundBodyDef.type = BodyType.StaticBody;
        this.staticBody = this.world.createBody(groundBodyDef);
    }
    
    public void deleteBody(Body body)
    {
    	this.bodies.remove(body);
    	this.world.destroyBody(body);
    }
    
    public void deleteJoint(Joint joint)
    {
    	this.joints.remove(joint);
    	this.world.destroyJoint(joint);
    }
    
    public void setContactListener(ContactListener cl) 
    { 
    	this.world.setContactListener(cl);
    }
    
    public int addGroundBox(float xPos, float yPos, float width, float height, float elasticity, Object userData)
    {
    	BodyDef groundBodyDef = new BodyDef();  
        groundBodyDef.position.set(new Vector2(xPos, yPos));
        Body groundBody = this.world.createBody(groundBodyDef);
        FixtureDef groundShapeDef = new FixtureDef();
        PolygonShape groundShape = new PolygonShape();
        groundShape.setAsBox(width / 2.0f, height / 2.0f);
        groundShapeDef.shape =  groundShape;
        groundShapeDef.friction = 0.5f;
        groundShapeDef.restitution = elasticity;
        groundBody.createFixture(groundShapeDef);
        groundBody.setUserData(userData);
        
        this.bodies.add(groundBody);
        return this.bodies.size() - 1;
    }
    
    public int addFreeBox2(float xPos, float yPos, float width, float height, float elasticity, Object userData)
    {
    	BodyDef groundBodyDef = new BodyDef();  
        groundBodyDef.position.set(new Vector2(xPos, yPos));
        groundBodyDef.type = BodyType.DynamicBody;
        groundBodyDef.bullet = true;
        Body groundBody = this.world.createBody(groundBodyDef);
        FixtureDef groundShapeDef = new FixtureDef();
        groundShapeDef.density = 0.02f;
        groundShapeDef.friction = 0.5f;
        groundShapeDef.restitution = elasticity;
        PolygonShape groundShape = new PolygonShape();
        groundShape.setAsBox(width / 2.0f, height / 2.0f);
        groundShapeDef.shape = groundShape;
        groundBody.createFixture(groundShapeDef);
        groundBody.setUserData(userData);
        groundBody.resetMassData();
        
        this.bodies.add(groundBody);
        return this.bodies.size() - 1;
    }
    
    public int addMovingGroundBox(float xPos, float yPos, float width, float height, float motorSpeed, boolean moveVertical, float elasticity, Object userData)
    {
    	BodyDef groundBodyDef = new BodyDef();
    	groundBodyDef.type = BodyType.KinematicBody;
        groundBodyDef.position.set(new Vector2(xPos, yPos));
        groundBodyDef.bullet = true;
        Body groundBody = this.world.createBody(groundBodyDef);
        FixtureDef groundShapeDef = new FixtureDef();
        groundShapeDef.friction = 0.5f;
        groundShapeDef.density = 0.02f;
        groundShapeDef.restitution = elasticity;
        PolygonShape groundShape = new PolygonShape();
        groundShape.setAsBox(width / 2.0f, height / 2.0f);
        groundShapeDef.shape = groundShape;
        groundBody.createFixture(groundShapeDef);
        groundBody.setUserData(userData);
        groundBody.resetMassData();
        if (moveVertical)
        	groundBody.setLinearVelocity(new Vector2(0.0f, -1.0f));
        else
        	groundBody.setLinearVelocity(new Vector2(-1.0f, 0.0f));
        
        this.bodies.add(groundBody);
        return this.bodies.size() - 1;

    }

    public int addFreeBox(float xPos, float yPos, float width, float height, float elasticity, boolean rotating, Object userData)
    {
    	BodyDef groundBodyDef = new BodyDef();  
        groundBodyDef.position.set(new Vector2(xPos, yPos));
        groundBodyDef.type = BodyType.DynamicBody;
        groundBodyDef.bullet = true;
        Body groundBody = this.world.createBody(groundBodyDef);
        FixtureDef groundShapeDef = new FixtureDef();
        groundShapeDef.density = (rotating) ? 1.0f : 0.02f;
        groundShapeDef.friction = 0.5f;
        groundShapeDef.restitution = elasticity;
        PolygonShape groundShape = new PolygonShape();
        groundShape.setAsBox(width / 2.0f, height / 2.0f);
        groundShapeDef.shape = groundShape;
        groundBody.createFixture(groundShapeDef);
        groundBody.setUserData(userData);
        
        if (rotating)
        {
	        groundBody.resetMassData();
	        
	        DistanceJointDef jointDef = new DistanceJointDef();
	        
	        jointDef.initialize(groundBody, this.staticBody, groundBody.getWorldCenter(), groundBody.getWorldCenter());
	        jointDef.collideConnected = false;
	        jointDef.frequencyHz = 4.0f;
	        jointDef.dampingRatio = 0.5f;
	        
	        DistanceJoint j = (DistanceJoint)this.world.createJoint(jointDef);
	        this.joints.add(j);
        }
        
        this.bodies.add(groundBody);
        return this.bodies.size() - 1;
    }
    
    public int addSensorBall(float xPos, float yPos, float radius, Object userData) 
    {  
    	Body newBody;
    	
        BodyDef bodyDef = new BodyDef();  
        bodyDef.position.set(xPos, yPos);
        newBody = world.createBody(bodyDef);
    
        FixtureDef circle = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(radius);
        circle.shape = circleShape; 
        circle.isSensor = true;        
        newBody.createFixture(circle);
        newBody.setUserData(userData);
        
        this.bodies.add(newBody);
        return this.bodies.size() - 1;
    }
    
    public int addBall(float xPos, float yPos, float radius, float density, float elasticity, boolean rotating, Object userData) 
    {  
    	Body newBody;
    	
        BodyDef bodyDef = new BodyDef();  
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(xPos, yPos);
        bodyDef.bullet = true;
        newBody = world.createBody(bodyDef);
    
        FixtureDef circle = new FixtureDef();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(radius);
        circle.shape = circleShape;
        circle.density = density;
        circle.restitution = elasticity;
        circle.friction = 0.5f;
  
        newBody.createFixture(circle);
        newBody.resetMassData();
        newBody.setUserData(userData);
        
        if (rotating)
        {
	        DistanceJointDef jointDef = new DistanceJointDef();
	        
	        jointDef.initialize(newBody, this.staticBody, newBody.getWorldCenter(), newBody.getWorldCenter());
	        jointDef.collideConnected = false;
	        jointDef.frequencyHz = 4.0f;
	        jointDef.dampingRatio = 0.5f;
	        
	        DistanceJoint j = (DistanceJoint)this.world.createJoint(jointDef);
	        this.joints.add(j);
        }
        
        this.bodies.add(newBody);
        return this.bodies.size() - 1;
    }
    
    public void setBodyPosition(int n, float pX, float pY)
    {
    	this.bodies.get(n).setTransform(new Vector2(pX, pY), this.bodies.get(n).getAngle());
    }
    
    public void setBodyAngle(int n, float angle)
    {
    	this.bodies.get(n).setTransform(this.bodies.get(n).getPosition(), angle);
    }
    
    public float getBodyXPosition(int n)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		return this.bodies.get(n).getPosition().x;
    	else
    		return 0.0f;
    }
    
    public float getBodyYPosition(int n)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		return this.bodies.get(n).getPosition().y;
    	else
    		return 0.0f;
    }
    
    public float getBodyAngle(int n)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		return ((float)Math.PI - this.bodies.get(n).getAngle());
    	else
    		return 0.0f;
    }
    
    public Object getBodyUserDate(int n)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		return this.bodies.get(n).getUserData();
    	else
    		return null;
    }
    
    public Joint getJoint(int n)
    {
    	if ((n >= 0) && (n < this.joints.size()))
    		return this.joints.get(n);
    	else
    		return null;
    }
    
    public Fixture getShape(int n)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		return this.bodies.get(n).getFixtureList().get(0);
    	else
    		return null;
    }
    
    public Body getBody(int n)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		return this.bodies.get(n);
    	else
    		return null;
    }
    
    public Joint getLastJoint()
    {
    	if (this.joints.size() > 0)
    		return this.joints.get(this.joints.size() - 1);
    	else
    		return null;
    }
    
    public Body getLastBody()
    {
    	if (this.bodies.size() > 0)
    		return this.bodies.get(this.bodies.size() - 1);
    	else
    		return null;
    }
    
    public void applyForce(int n, float fX, float fY)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		this.bodies.get(n).applyForce(new Vector2(fX, fY), this.bodies.get(n).getPosition());
    }
    
    public void applyImpulse(int n, float iX, float iY)
    {
    	if ((n >= 0) && (n < this.bodies.size()))
    		this.bodies.get(n).applyLinearImpulse(new Vector2(iX, iY), this.bodies.get(n).getPosition());
    }
  
    public void update() 
    {  
    	if ((SystemClock.uptimeMillis() - this.tickCount) > 10)
    	{
    		if (this.world != null)
    			this.world.step(this.timeStep, this.iterations, this.iterations);
    		
    		this.tickCount = SystemClock.uptimeMillis();    	    		
    	}
    }
    
    public void release()
    {
    	if (this.world != null)
    	{
    		if (this.bodies != null)
    		{
    			if (this.joints != null)
        		{
        			for (int i = 0; i < this.joints.size(); i++)
        				this.world.destroyJoint(this.joints.get(i));
        			
        			this.joints.clear();
    		    	this.joints = null;
        		}
    			
		    	for (int i = 0; i < this.bodies.size(); i++)
		    	{
		    		Body b = this.bodies.get(i);
		    		ArrayList<Fixture> fList = b.getFixtureList();
		    		if (fList != null)
		    		{
		    			for (int j = 0; j < fList.size(); j++)
		    				b.destroyFixture(fList.get(j));
		    		}
		    		
		    		fList.clear();
		    		
		    		this.world.destroyBody(b);
		    	}
		    	
		    	this.bodies.clear();
		    	this.bodies = null;
    		}

    		this.world.dispose();
	    	this.world = null;
    	}
    }
}
