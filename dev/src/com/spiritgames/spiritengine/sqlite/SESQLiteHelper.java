package com.spiritgames.spiritengine.sqlite;

import java.util.Iterator;
import java.util.Vector;

import com.spiritgames.spiritengine.SEManagers;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class SESQLiteHelper extends SQLiteOpenHelper 
{
	protected Vector<SESQLiteTable> tables;
	
	public SESQLiteHelper(String name) 
	{
		super(SEManagers.getApplicationContext(), name, null, 1);
		this.tables = new Vector<SESQLiteTable>();
		this.fillDBStructure();
	}
	
	protected abstract void fillDBStructure();

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		String CREATE_TABLE;
			
		for (SESQLiteTable t : this.tables)
		{
			CREATE_TABLE = "create table " + t.getName() + " (";
			
			Iterator<SESQLiteField> fItr = t.getFieldsIterator();
			
			while (fItr.hasNext())
			{
				SESQLiteField f = fItr.next();
				CREATE_TABLE += f.name + " " + f.type + 
					(f.isPrimaryKey ? " primary key" : "") + 
					(f.isAutoincrement ? " autoincrement" : "") + ", ";
			}
			
			CREATE_TABLE = CREATE_TABLE.substring(0, CREATE_TABLE.length() - 2) + ")";
			
			db.execSQL(CREATE_TABLE);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
	}
}
