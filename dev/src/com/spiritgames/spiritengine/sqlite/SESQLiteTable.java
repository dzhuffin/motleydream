package com.spiritgames.spiritengine.sqlite;

import java.util.Iterator;
import java.util.Vector;

public class SESQLiteTable 
{
	private String name;
	private Vector<SESQLiteField> fields;
	
	public String getName() 
	{
		return name;
	}
	
	public Iterator<SESQLiteField> getFieldsIterator() 
	{
		return fields.iterator();
	}
	
	public int getFieldsCount() 
	{
		return fields.size();
	}

	public SESQLiteTable(String tableName)
	{
		this.name = tableName;
		this.fields = new Vector<SESQLiteField>();
	}
	
	public boolean addField(SESQLiteField f)
	{
		return this.fields.add(f);
	}
}
