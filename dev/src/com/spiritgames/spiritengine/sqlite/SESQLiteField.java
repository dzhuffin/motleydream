package com.spiritgames.spiritengine.sqlite;

public class SESQLiteField 
{
	public String name;
	public boolean isPrimaryKey;
	public String type;
	public boolean isAutoincrement;
	
	public SESQLiteField(String fieldName, String type, boolean isPrimaryKey, boolean isAutoincrement)
	{
		this.name = fieldName;
		this.type = type;
		this.isAutoincrement = isAutoincrement;
		this.isPrimaryKey = isPrimaryKey;
	}
}
