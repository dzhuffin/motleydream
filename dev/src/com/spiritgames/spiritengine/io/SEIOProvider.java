package com.spiritgames.spiritengine.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.spiritgames.spiritengine.SEManagers;

import android.content.Context;
import android.content.res.Resources;

public abstract class SEIOProvider 
{		
	protected InputStream reader;
	protected FileOutputStream writer;

	public SEIOProvider()
	{
	}
	
	public boolean savePref(String filename, int val)
	{
		FileOutputStream fOut = null; 
	    OutputStreamWriter osw = null; 
		
		try 
		{	
			fOut = SEManagers.getApplicationContext().openFileOutput(filename, Context.MODE_PRIVATE);
			osw = new OutputStreamWriter(fOut);
	        osw.write(Integer.toString(val));
	        osw.flush(); 
		} 		
		 catch (Exception e) 
		 {       
	          e.printStackTrace(); 	    
	     }
		 finally
		 { 
             try 
             { 
                    osw.close(); 
                    fOut.close(); 
             }
             catch (Exception e) 
             { 
                    e.printStackTrace(); 
                    return false;
             } 
          } 
		
		return true;
	}
	
	public boolean savePrefF(String filename, float val)
	{
		FileOutputStream fOut = null; 
	    OutputStreamWriter osw = null; 
		
		try 
		{	
			fOut = SEManagers.getApplicationContext().openFileOutput(filename, Context.MODE_PRIVATE);
			osw = new OutputStreamWriter(fOut);
	        osw.write(Float.toString(val));
	        osw.flush(); 
		} 		
		 catch (Exception e) 
		 {       
	          e.printStackTrace(); 	    
	     }
		 finally
		 { 
             try 
             { 
                    osw.close(); 
                    fOut.close(); 
             }
             catch (Exception e) 
             { 
                    e.printStackTrace(); 
                    return false;
             } 
          } 
		
		return true;
	}	
	
	public boolean savePrefB(String filename, boolean val)
	{
		FileOutputStream fOut = null; 
	    OutputStreamWriter osw = null; 
		
		try 
		{	
			fOut = SEManagers.getApplicationContext().openFileOutput(filename, Context.MODE_PRIVATE);
			osw = new OutputStreamWriter(fOut);
	        osw.write(Boolean.toString(val));
	        osw.flush(); 
		} 		
		 catch (Exception e) 
		 {       
	          e.printStackTrace(); 	    
	     }
		 finally
		 { 
             try 
             { 
                    osw.close(); 
                    fOut.close(); 
             }
             catch (Exception e) 
             { 
                    e.printStackTrace(); 
                    return false;
             } 
          } 
		
		return true;
	}	
	
	public int loadPref(String filename, String def)
	{	
		FileInputStream fIn = null; 
	    InputStreamReader isr = null; 
	      
	    char[] inputBuffer = new char[10]; 
	    String data = def;	    
	      
	    try
	    { 
	    	fIn = SEManagers.getApplicationContext().openFileInput(filename);       
	        isr = new InputStreamReader(fIn); 
	        isr.read(inputBuffer); 
	        data = new String(inputBuffer); 	          
	    } 
	    catch (Exception e) 
	    {       
	          e.printStackTrace();  
	    }
	    finally 
	    { 
	    	try
	    	{ 
	        	isr.close(); 
	            fIn.close(); 	            
	        } 
	    	catch (Exception e) 
	    	{ 
	        	e.printStackTrace(); 	        	
	    	} 
	    }
	    	
	    return Integer.parseInt(data.trim()); 
	}
	
	public float loadPrefF(String filename, String def)
	{	
		FileInputStream fIn = null; 
	    InputStreamReader isr = null; 
	      
	    char[] inputBuffer = new char[10]; 
	    String data = def;	    
	      
	    try
	    { 
	    	fIn = SEManagers.getApplicationContext().openFileInput(filename);       
	        isr = new InputStreamReader(fIn); 
	        isr.read(inputBuffer); 
	        data = new String(inputBuffer); 	          
	    } 
	    catch (Exception e) 
	    {       
	          e.printStackTrace();  
	    }
	    finally 
	    { 
	    	try
	    	{ 
	        	isr.close(); 
	            fIn.close(); 	            
	        } 
	    	catch (Exception e) 
	    	{ 
	        	e.printStackTrace(); 	        	
	    	} 
	    }
	    	
	    return Float.parseFloat(data.trim()); 
	}
	
	public boolean loadPrefB(String filename, String def)
	{	
		FileInputStream fIn = null; 
	    InputStreamReader isr = null; 
	      
	    char[] inputBuffer = new char[10]; 
	    String data = def;	    
	      
	    try
	    { 
	    	fIn = SEManagers.getApplicationContext().openFileInput(filename);       
	        isr = new InputStreamReader(fIn); 
	        isr.read(inputBuffer); 
	        data = new String(inputBuffer); 	          
	    } 
	    catch (Exception e) 
	    {       
	          e.printStackTrace();  
	    }
	    finally 
	    { 
	    	try
	    	{ 
	        	isr.close(); 
	            fIn.close(); 	            
	        } 
	    	catch (Exception e) 
	    	{ 
	        	e.printStackTrace(); 	        	
	    	} 
	    }
	    	
	    return Boolean.parseBoolean(data.trim()); 
	}
	
	abstract protected void saveCustom();
	abstract protected void loadCustom();
	
	public boolean saveToAppData(String fileName)
	{
		try 
		{			
			this.writer = SEManagers.getApplicationContext().openFileOutput(fileName, 0);
			
			this.saveCustom();
			
			this.writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		}  
		
		return true;
	}
	
	public boolean saveToSDCard(String sdFilePath)
	{
		try 
		{		
			this.writer = new FileOutputStream("/sdcard/" + sdFilePath);
			
			this.saveCustom();
			
			this.writer.close();
		} 
		catch (FileNotFoundException e) 
		{					
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		}  
		
		return true;
	}
	
	public boolean loadFromAppData(String fileName)
	{
		try 
		{			
			this.reader = SEManagers.getApplicationContext().openFileInput(fileName);
			
			this.loadCustom();
			
			this.reader.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		}  
		
		return true;
	}	
	
	public boolean loadFromSDCard(String sdFilePath)
	{	
		try 
		{			
			this.reader = new FileInputStream("/sdcard/" + sdFilePath);
			
			this.loadCustom();
			
			this.reader.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		}  
		
		return true;
	}	
	
	public boolean loadFromResource(int id)
	{
		Resources res = SEManagers.getApplicationContext().getResources();
		
		try 
		{			
			this.reader = res.openRawResource(id);
			
			this.loadCustom();
			
			this.reader.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		} 
		
		return true;
	}
	
	protected void writeByte(int val)
	{
		try 
		{
			this.writer.write(val & 0xFF);
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return;
		}
	}

	protected void writeInt(int val)
	{
		this.writeByte(val & 0xFF);
		this.writeByte((val >> 8) & 0xFF);
		this.writeByte((val >> 16) & 0xFF);
		this.writeByte((val >> 24) & 0xFF);
	}

	protected byte readByte()
	{
		try
		{
			return (byte)this.reader.read();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return 0;
		}   
	}	

	protected int readInt()
	{
		int byte1, byte2, byte3, byte4;
		
		try 
		{
			byte1 = this.reader.read();
			byte2 = this.reader.read();
			byte3 = this.reader.read();
			byte4 = this.reader.read();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return 0;
		}        
        return ((byte4 << 24) | (byte3 << 16) | (byte2 << 8) | byte1);
	}	
}

