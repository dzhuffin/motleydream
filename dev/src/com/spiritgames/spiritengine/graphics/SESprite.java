package com.spiritgames.spiritengine.graphics;

import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.MotionEvent;

import com.spiritgames.motleydream.MBManagers;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.SEMath;
import com.spiritgames.spiritengine.menu.ISEMenuItem;

public class SESprite implements ISEMenuItem
{	
	protected float[] realVertexes;
	
	protected boolean enable = true;	
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	protected boolean visible = true;	
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	protected float x = 0.0f;	
	protected float y = 0.0f;	
	protected float relative_x = -1.0f;	
	protected float relative_y = -1.0f;
	
	protected float texFactor_x = 1.0f;
	protected float texFactor_y = 1.0f;
	
	protected float old_x = 0.0f;
	protected float old_y = 0.0f;
	public float getX()
	{
		return this.x;
	}
	public float getY()
	{
		return this.y;
	}

	public void setPos(float px, float py)
	{
		this.x = px;
		this.y = py;
	}
	
	public float getRelativePosX()
	{
		return this.relative_x;
	}
	public float getRelativePosY()
	{
		return this.relative_y;
	}
	public void setRelativePos(float px, float py)
	{
		this.relative_x = px;
		this.relative_y = py;
	}
	
	public float getTextureFactorX()
	{
		return this.texFactor_x;
	}
	public float getTextureFactorY()
	{
		return this.texFactor_y;
	}
	
	public void setTextureFactorX(float val)
	{
		this.texFactor_x = val;
	}
	public void setTextureFactorY(float val)
	{
		this.texFactor_y = val;
	}
	
	protected float ratio = 0.0f;
	public float getRatio()
	{
		return this.ratio;
	}
	public void setRatio(float r)
	{
		this.ratio = r;
	}
	
	protected float width = 100.0f;
	protected float height = 100.0f;
	protected float relative_width = -1.0f;
	protected float relative_height = -1.0f;
	protected float old_width = 100.0f;
	protected float old_height = 100.0f;
	public float getWidth()
	{
		return this.width;
	}
	public float getHeight()
	{
		return this.height;
	}
	public void setSize(float w, float h)
	{
		this.width = w;
		this.height = h;
		this.ratio = w / h;
	}
	public float getRelativeWidth()
	{
		return this.relative_width;
	}
	public float getRelativeHeight()
	{
		return this.relative_height;
	}
	public void setRelativeSize(float w, float h)
	{
		this.relative_width = w;
		this.relative_height = h;
	}
	
	protected float angle = 0.0f;
	protected float old_angle = 0.0f;
	public float getAngle()
	{
		return this.angle;
	}
	public void setAngle(float a)
	{
		this.angle = a;
	}
	protected int currentTexture = -1;
	public void setCurrentTexture(int currentTexture) 
	{
		this.currentTexture = currentTexture;
	}
	public int getCurrentTexture() 
	{
		return currentTexture;
	}
	
	protected Vector<SETextureInfo> texInfo = new Vector<SETextureInfo>();
	protected Vector<SETextureInfo> oldTexInfo = new Vector<SETextureInfo>();
	
	public Vector<SETextureInfo> getTextureInfo() {
		return this.texInfo;
	}

	protected int screenWidth = 0;
	protected int screenHeight = 0;
	
	protected boolean useTextureRenderExtension = false;
	
	public SESprite(boolean useTextureRenderExtension)
	{
		// TODO ������� �� �����
		if ((SEManagers.getVertexBuffer() == null) || (SEManagers.getGL10Interface() == null))
			this.useTextureRenderExtension = false;
		else
			this.useTextureRenderExtension = useTextureRenderExtension && SEManagers.getGL10Interface().glGetString(GL10.GL_EXTENSIONS) != null && 
				SEManagers.getGL10Interface().glGetString(GL10.GL_EXTENSIONS).contains("GL_OES_draw_texture");
		
		this.realVertexes = new float[8];
		
		this.screenWidth = SEManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
		this.screenHeight = SEManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
	}
	
	public Rect getBounds()
	{
		return new Rect((int)this.x, (int)this.y, (int)(this.x + this.width), (int)(this.y + this.height));
	}
	
	public int addTexture(SETextureInfo texInfo)
	{
		this.texInfo.add(texInfo);
		
		return this.texInfo.size() - 1;
	}
	
	public int addTexture(int resID)
	{
		this.texInfo.add(SETextureManager.getTexture(resID, this.useTextureRenderExtension));
		
		return this.texInfo.size() - 1;
	}
	
	public int addTexture(int resID, float x, float y, float w, float h)
	{
		SETextureInfo texInfo = new SETextureInfo(SETextureManager.getTexture(resID, this.useTextureRenderExtension));
		
		texInfo.extended = true;
		texInfo.region_x = x;
		texInfo.region_y = y;
		texInfo.region_w = w;
		texInfo.region_h = h;

		this.texInfo.add(texInfo);
		
		return this.texInfo.size() - 1;
	}
	
	public boolean checkPointInside(float pX, float pY)
	{
		return SEMath.checkPointInsideRect(this.realVertexes, pX, pY);
	}
	
	public void release() 
    {		
		/*if (this.texInfo.size() > 0)
			this.oldTexInfo = (Vector<SETextureInfo>)this.texInfo.clone();*/
		
		int[] textures;
		
		for (int i = 0; i < this.texInfo.size(); i++)
		{
			if (this.texInfo.get(i).resID == 0)
			{
				textures = new int[1];
				textures[0] = this.texInfo.get(i).texID;
				SEManagers.getGL10Interface().glDeleteTextures(textures.length, textures, 0);
			}
		}
		
        this.texInfo.clear();
    }
	
	public void restore()
	{
		Vector<SETextureInfo> newTexInfo = (Vector<SETextureInfo>)this.texInfo.clone();
		/*Vector<SETextureInfo> newTexInfo;
		
		if (this.oldTexInfo.size() > 0)
			newTexInfo = (Vector<SETextureInfo>)this.oldTexInfo.clone();
		else
			newTexInfo = (Vector<SETextureInfo>)this.texInfo.clone();*/
		
		this.release();
		
		for(int i = 0; i < newTexInfo.size(); i++)
		{
			SETextureInfo info = newTexInfo.get(i);
			if (info.resID != 0)
			{
				if (info.extended)
					this.addTexture(info.resID, info.region_x, info.region_y, info.region_w, info.region_h);
				else
					this.addTexture(info.resID);
			}
		}
	}

	private final boolean isCliping()
	{
		float half_s = ((this.width > this.height) ? this.width : this.height) / 2.0f;
		
		return ((this.x + half_s <= 0.0f) ||
				(this.x - half_s >= this.screenWidth) ||
				(this.y + half_s <= 0.0f) ||
				(this.y - half_s >= this.screenHeight));
	}
	
	public void Render()
	{
		if (!this.visible || this.isCliping())
			return;
		
		SEVertexBuffer vBuffer = SEManagers.getVertexBuffer();
        GL10 gl = MBManagers.getGL10Interface();
		
       	vBuffer.setTransform(this.x, this.y, this.angle, this.width, this.height);
       	
       	this.realVertexes = vBuffer.getVertexes();
       	
       	if ((this.currentTexture < 0) || (this.currentTexture >= this.texInfo.size()))
        	return;
       	
    	SETextureInfo info = this.texInfo.get(this.currentTexture);
    	gl.glBindTexture(GL10.GL_TEXTURE_2D, info.texID);
   	
       	if ((this.useTextureRenderExtension) && (!info.extended))
       	{
			((GL11Ext) gl).glDrawTexfOES(this.x - this.width / 2.0f, this.y - this.height / 2.0f, 0, this.width, this.height);
       	}
       	else
       	{
       		if (info.extended)
       			vBuffer.setTextureRegion(info.region_x, info.region_y, info.region_w, info.region_h, info.bitmap_w, info.bitmap_h);
       		else
       			vBuffer.resetTextureRegion();
       				
       		vBuffer.Render();	
       	}
	}
	@Override
	public boolean onTouch(MotionEvent event, float scrH) {
		return false;
	}
	@Override
	public boolean onTouchDown() {
		return false;
	}
	@Override
	public boolean onTouchUp(boolean contains) {
		return false;
	}
}
