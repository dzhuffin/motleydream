package com.spiritgames.spiritengine.graphics;

public class SETextureInfo 
{
	public int resID;
	public int texID;
	public boolean extended;
	public float region_x;
	public float region_y;
	public float region_w;
	public float region_h;
	public float bitmap_w;
	public float bitmap_h;
	
	public SETextureInfo(int resID, int texID, int bitmap_w, float bitmap_h, float region_x, float region_y, float region_w, float region_h)
	{
		this.resID = resID;
		this.texID = texID;
		this.extended = true;
		this.region_x = region_x;
		this.region_y = region_y;
		this.region_w = region_w;
		this.region_h = region_h;
		this.bitmap_w = bitmap_w;
		this.bitmap_h = bitmap_h;
	}
	
	public SETextureInfo(int resID, int texID, float bitmap_w, float bitmap_h)
	{
		this.resID = resID;
		this.texID = texID;
		this.bitmap_w = bitmap_w;
		this.bitmap_h = bitmap_h;
		this.extended = false;
	}
	
	public SETextureInfo(int resID, int texID)
	{
		this.resID = resID;
		this.texID = texID;
		this.bitmap_w = 0.0f;
		this.bitmap_h = 0.0f;
		this.extended = false;
	}
	
	public SETextureInfo(SETextureInfo info)
	{
		this.resID = info.resID;
		this.texID = info.texID;
		this.extended = info.extended;
		this.region_x = info.region_x;
		this.region_y = info.region_y;
		this.region_w = info.region_w;
		this.region_h = info.region_h;
		this.bitmap_w = info.bitmap_w;
		this.bitmap_h = info.bitmap_h;
	}
}
