package com.spiritgames.spiritengine.graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import com.spiritgames.motleydream.MBManagers;
import com.spiritgames.spiritengine.SEManagers;

public class SEVertexBuffer 
{
	private final static int VERTS = 4;

    private FloatBuffer mFVertexBuffer;
    private FloatBuffer mTexBuffer;
    private ShortBuffer mIndexBuffer;
	
	public SEVertexBuffer()
	{				
		ByteBuffer vbb = ByteBuffer.allocateDirect(VERTS * 2 * 4);
        vbb.order(ByteOrder.nativeOrder());
        mFVertexBuffer = vbb.asFloatBuffer();

        ByteBuffer tbb = ByteBuffer.allocateDirect(VERTS * 2 * 4);
        tbb.order(ByteOrder.nativeOrder());
        mTexBuffer = tbb.asFloatBuffer();

        ByteBuffer ibb = ByteBuffer.allocateDirect(VERTS * 2);
        ibb.order(ByteOrder.nativeOrder());
        mIndexBuffer = ibb.asShortBuffer();

        float[] coords = 
        {
        		0.0f, 0.0f,
                1.0f, 0.0f,
                0.0f, 1.0f,
                1.0f, 1.0f                 
        };
        
        float[] tex_coords = 
        {        		
        		
        		0.0f, 1.0f,
        		1.0f, 1.0f,                
                0.0f, 0.0f,
                1.0f, 0.0f
        };
        
        for (int i = 0; i < VERTS; i++) 
        {
            for(int j = 0; j < 2; j++) 
            {
            	this.mFVertexBuffer.put(coords[i * 2 + j]);
            	this.mTexBuffer.put(tex_coords[i * 2 + j]);
            }
        }

        for(int i = 0; i < VERTS; i++) 
        {
        	this.mIndexBuffer.put((short) i);
        }

        this.mFVertexBuffer.position(0);
        this.mTexBuffer.position(0);
        this.mIndexBuffer.position(0);
	}
	
	public void LoadIdentity()
	{
		this.mFVertexBuffer.clear();
		
		float[] coords = 
        {
        		0.0f, 0.0f,
                1.0f, 0.0f,
                0.0f, 1.0f,
                1.0f, 1.0f                 
        };
		
		for (int i = 0; i < VERTS; i++) 
        {
            this.mFVertexBuffer.put(coords[i * 2]);
            this.mFVertexBuffer.put(coords[i * 2 + 1]);
        }
	}
	
	public void setTransform(float tx, float ty, float angle, float sx, float sy)
	{	
		this.mFVertexBuffer.rewind();
		
		float[] coords = 
        {
        		0.0f, 0.0f,
                sx, 0.0f,
                0.0f, sy,
                sx, sy                 
        };
		
		float sinAngle = (float)Math.sin(angle);
        float cosAngle = (float)Math.cos(angle);
        float centerX = sx / 2.0f;
        float centerY = sy / 2.0f;
        float x = 0.0f;
        float y = 0.0f;
        
        for (int i = 0; i < VERTS; i++) 
        {
        	// rotate
        	x = coords[i * 2];
        	y = coords[i * 2 + 1];
        	coords[i * 2] = centerX + (x - centerX) * cosAngle - (y - centerY) * sinAngle; 
            coords[i * 2 + 1] = centerY + (x - centerX) * sinAngle + (y - centerY) * cosAngle;	
        }

        for (int i = 0; i < VERTS; i++) 
        {        	        	                       
            // translate
            coords[i * 2] += tx - centerX; 
            coords[i * 2 + 1] += ty - centerY;
        }
        
        for (int i = 0; i < VERTS; i++) 
        {
            this.mFVertexBuffer.put(coords[i * 2]);
            this.mFVertexBuffer.put(coords[i * 2 + 1]);
        }
        
        this.mFVertexBuffer.rewind();
	}
	
	public void setTextureFactor(float tx, float ty)
	{
		this.mTexBuffer.rewind();
		
		float[] tex_coords = 
        {        		
        		
        		0.0f, ty,
        		tx, ty,                
                0.0f, 0.0f,
                tx, 0.0f
        };
		
		for (int i = 0; i < VERTS; i++) 
            for(int j = 0; j < 2; j++) 
            	this.mTexBuffer.put(tex_coords[i * 2 + j]);
		
		this.mTexBuffer.rewind();
	}
	
	public void resetTextureRegion()
	{
		this.mTexBuffer.rewind();
		
		float[] tex_coords = 
        {
				0.0f, 1.0f,
        		1.0f, 1.0f,                
                0.0f, 0.0f,
                1.0f, 0.0f
        };
		
		for (int i = 0; i < VERTS; i++) 
            for(int j = 0; j < 2; j++) 
            	this.mTexBuffer.put(tex_coords[i * 2 + j]);
		
		this.mTexBuffer.rewind();
	}
	
	public void setTextureRegion(float tx, float ty, float tw, float th, float bmp_w, float bmp_h)
	{
		this.mTexBuffer.rewind();
		
		//tw *= 2.0f;
		//th *= 2.0f;
		//bmp_w = 256.0f;
		//bmp_h = 256.0f;
		
		float[] tex_coords = 
        {
				tx / bmp_w, (ty + th) / bmp_h,
        		(tx + tw) / bmp_w, (ty + th) / bmp_h,
				tx / bmp_w, ty / bmp_h,
				(tx + tw) / bmp_w, ty / bmp_h
        };
		
		for (int i = 0; i < VERTS; i++) 
            for(int j = 0; j < 2; j++) 
            	this.mTexBuffer.put(tex_coords[i * 2 + j]);
		
		this.mTexBuffer.rewind();
	}
	
	public float[] getVertexes()
	{		
		float[] coords = new float[8];
		this.mFVertexBuffer.get(coords);
		this.mFVertexBuffer.rewind();
		float tmpX = coords[4];
		float tmpY = coords[5];
		coords[4] = coords[6];
		coords[5] = coords[7];
		coords[6] = tmpX;
		coords[7] = tmpY;
		
		return coords;
	}
	
	public void Render()
	{	
		GL10 gl = SEManagers.getGL10Interface();
		
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);  

		gl.glVertexPointer(2, GL10.GL_FLOAT, 0, this.mFVertexBuffer);
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, this.mTexBuffer);		
		gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, VERTS, GL10.GL_UNSIGNED_SHORT, this.mIndexBuffer);		
	}
}