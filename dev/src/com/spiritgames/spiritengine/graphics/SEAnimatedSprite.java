package com.spiritgames.spiritengine.graphics;

import java.util.Vector;

import com.spiritgames.spiritengine.SEManagers;

import android.os.SystemClock;

public class SEAnimatedSprite extends SESprite
{
	private int framesCount = 0;
	private int animationSpeed = 0;
	private long animationTick = 0;	
	private boolean loop = true;
	private boolean playing = false;
	
	public boolean isAnimationPlaying()
	{
		return this.playing;
	}
	
	public void startAnimation()
	{
		this.playing = true;
		this.currentTexture = 0;
	}
	
	public void setAnimationLoop(boolean val)
	{
		this.loop = val;
	}
	
	public SEAnimatedSprite(boolean useTextureRenderExtension) 
	{
		super(useTextureRenderExtension);
	}

	public void loadAnimation(int firstFrameResID, int framesCount, int animationSpeed)
	{
		this.framesCount = framesCount;
		this.animationSpeed = animationSpeed;
		
		this.release();
		
		int currentResID = firstFrameResID;
		
		while (currentResID <= (this.framesCount + firstFrameResID - 1))
			this.addTexture(currentResID++);
		
		this.currentTexture = 0;
	}
	
	@Override
	public void Render()
	{	
		if ((SystemClock.uptimeMillis() - this.animationTick > this.animationSpeed) && (this.playing))
		{
			this.animationTick = SystemClock.uptimeMillis();
			this.currentTexture++;
			if (this.currentTexture >= this.framesCount - 1)
			{
				if (this.loop)
					this.currentTexture = 0;
				else					
					this.playing = false;
			}
		}
		
		super.Render();
	}
	
	@Override
	public void restore()
	{
		super.restore();
		
		this.currentTexture = 0;
		this.animationTick = 0;
	}
}
