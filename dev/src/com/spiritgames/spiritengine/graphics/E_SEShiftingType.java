package com.spiritgames.spiritengine.graphics;

/***
 * ��� ������� ����������� ��� SEShiftingSprite
 */
public enum E_SEShiftingType 
{
	/***
	 * ����������� ���
	 */
	Unknown(-1),
	/***
	 * ������� ����������� �� ����� �� ���� 
	 */
	SimpleShifting(0),
	/***
	 * ������ ���������� �������� � ����� ������� � ������� �����
	 */
    ScalingToPoint(1),
	/***
	 * ������ ���������� �������� � ����� ������� � ����� ������
	 */
	ScalingToCenter(2);
    
    private int index;
	
    E_SEShiftingType(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_SEShiftingType getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_SEShiftingType.SimpleShifting;
			case 1:
				return E_SEShiftingType.ScalingToPoint;
			case 2:
				return E_SEShiftingType.ScalingToCenter;
			default:
				return E_SEShiftingType.Unknown;
		}		
	}
}
