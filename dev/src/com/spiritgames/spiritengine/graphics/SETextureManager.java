package com.spiritgames.spiritengine.graphics;

import java.util.HashMap;
import java.util.Iterator;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.DisplayMetrics;

import com.spiritgames.motleydream.MBManagers;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.SEMath;

public class SETextureManager 
{	
	protected static HashMap<Integer, SETextureInfo> texMap = new HashMap<Integer, SETextureInfo>();
	
	public static SETextureInfo getTexture(int resID, boolean useTextureRenderExtension)
	{
		if (SETextureManager.texMap.containsKey(resID))
			return SETextureManager.texMap.get(resID);

		GL10 gl = SEManagers.getGL10Interface();
		
		int[] textures = new int[1];
		gl.glGenTextures(1, textures, 0);
		
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        
        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
        gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
        
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);        
        gl.glTexEnvx(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);
        
        gl.glActiveTexture(GL10.GL_TEXTURE0);
          
        Bitmap bitmap = BitmapFactory.decodeResource(SEManagers.getApplicationContext().getResources(), resID);
        int scale_width = SEMath.getNearPowerOfTwo(bitmap.getWidth(), true);
        int scale_height = SEMath.getNearPowerOfTwo(bitmap.getHeight(), true);
        
        SETextureInfo result = new SETextureInfo(resID, textures[0], bitmap.getScaledWidth(DisplayMetrics.DENSITY_MEDIUM), bitmap.getScaledHeight(DisplayMetrics.DENSITY_MEDIUM));
        
    	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(bitmap, scale_width, scale_height, true), 0);
    	//
    	if (useTextureRenderExtension)
    	{
			int crop[] = new int[4];
			crop[0] = 0;
			crop[1] = 0;
			crop[2] = scale_width;
			crop[3] = -scale_height;
			
			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, crop, 0);
    	}
    	//
        bitmap.recycle();
        bitmap = null;
        
        SETextureManager.texMap.put(resID, result);
        
        return result;
	}
	
	public static void release()
	{
		int[] textures;
		
		if (SETextureManager.texMap.size() > 0)
		{
			textures = new int[SETextureManager.texMap.values().size()];
			
			Iterator<SETextureInfo> itr = SETextureManager.texMap.values().iterator();
			int i = 0;
			while(itr.hasNext()) 
				textures[i++] = (int)itr.next().texID; 
			
			SEManagers.getGL10Interface().glDeleteTextures(textures.length, textures, 0);
			
			SETextureManager.texMap.clear();
		}
	}
}
