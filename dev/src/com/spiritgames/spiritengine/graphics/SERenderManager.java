package com.spiritgames.spiritengine.graphics;

import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLU;
import android.opengl.GLSurfaceView.Renderer;
import android.os.SystemClock;

import com.spiritgames.motleydream.MBGameManager;
import com.spiritgames.spiritengine.E_SEActivityState;
import com.spiritgames.spiritengine.SEActivity;
import com.spiritgames.spiritengine.SEGameManager;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.menu.SEMenuManager;
import com.spiritgames.spiritengine.text.SERasterFont;

abstract public class SERenderManager implements Renderer 
{
	public SEMenuManager mainMenu;
	
	private static Vector<SERasterFont> fonts;
	
	protected float screenWidth;
	protected float screenHeight;
	
	protected boolean inited = false;
	
	protected int fps;
	public int getFPS()
	{
		return this.fps;
	}
	
	public SERenderManager() 
	{
		SEManagers.setVertexBuffer(new SEVertexBuffer());
    }
	
	public int addFont(int fontImg_id, int fontXML_id)
	{
		SERasterFont newFont = new SERasterFont();
		newFont.initFont(fontImg_id, fontXML_id);
		if (SERenderManager.fonts.add(newFont))
			return SERenderManager.fonts.size() - 1;
		else
			return -1;
	}
	
	public static SERasterFont getFont(int i)
	{
		return SERenderManager.fonts.get(i);
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height)
	{
		SEManagers.setGL10Interface(gl);
		gl.glViewport(0, 0, width, height);

		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();				
		
		GLU.gluOrtho2D(gl, 0.0f, (float)width, 0.0f, (float)height);
		
		this.screenWidth = (float)width;
		this.screenHeight = (float)height;
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) 
	{
		SEManagers.setGL10Interface(gl);
		
		gl.glDisable(GL10.GL_DITHER);
		
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 
		
		gl.glEnable(GL10.GL_TEXTURE_2D);
		
		gl.glEnable(GL10.GL_SMOOTH);
		gl.glEnable(GL10.GL_MULTISAMPLE);
		
		gl.glEnable(GL10.GL_LINE_SMOOTH);
		gl.glEnable(GL10.GL_POINT_SMOOTH);
		gl.glHint(GL10.GL_LINE_SMOOTH_HINT, GL10.GL_NICEST);
		gl.glHint(GL10.GL_POINT_SMOOTH_HINT, GL10.GL_NICEST);
		gl.glHint(GL10.GL_POLYGON_SMOOTH_HINT, GL10.GL_NICEST);
        
		SEGameManager gameManager = SEManagers.getGameManager();
		
		if (!this.inited)
		{
	        if (this.mainMenu != null)
	        	this.mainMenu.release();
	        
	        if ((SERenderManager.fonts != null) && (SERenderManager.fonts.size() > 0))
	        {
	        	for (int i = 0; i < SERenderManager.fonts.size(); i++)
	        		SERenderManager.getFont(i).release();
	        	
	        	SERenderManager.fonts.clear();
	        	SERenderManager.fonts = null;
	        }
	
	        SERenderManager.fonts = new Vector<SERasterFont>();
	
	        this.mainMenu = new SEMenuManager();
			               
			this.Init();
			
			if (gameManager != null)
			{
				gameManager.release();
				gameManager.Init();
			}
		}
		else
		{
			for (int i = 0; i < SERenderManager.fonts.size(); i++)
			{
				SERasterFont font = SERenderManager.fonts.get(i);
				font.restore();
			}
			
			this.mainMenu.restore();
			gameManager.restore();
		}
		
		this.inited = true;
	}

	@Override
	public void onDrawFrame(GL10 gl) 
	{
		SEManagers.setGL10Interface(gl);
		long fps_time = SystemClock.uptimeMillis();
		
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);		             
        
        gl.glFrontFace(GL10.GL_CCW);

        if (SEManagers.getActivity().getState() == E_SEActivityState.InGame)
        	this.GameRender();
        else
        	this.MenuRender();

        long fpsDivider = SystemClock.uptimeMillis() - fps_time;
        
        this.fps = (fpsDivider == 0) ? 0 : (int)(1000L / fpsDivider);                    
	}
	
	abstract public void MenuRender();
	abstract public void GameRender();
	abstract public void Init();
}
