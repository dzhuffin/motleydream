package com.spiritgames.spiritengine.graphics;

import com.spiritgames.spiritengine.SEManagers;

import android.content.Context;
import android.graphics.PointF;
import android.os.SystemClock;

public class SEShiftingSprite extends SEAnimatedSprite 
{
	public static final float TIME_RATIO = 100.0f;
		
	private SESimpleShiftingParams simpleShiftingParams;
	
	public class ScalingToPointParams
	{
		public PointF StartSize;
		public float FinalScale = 1.0f;
		public float ScaleSpeed = 0.0f;
		
		public ScalingToPointParams(PointF startSize, float finalScale) 
		{
			this.StartSize = new PointF(startSize.x, startSize.y);
			this.FinalScale = finalScale;
			this.ScaleSpeed = 1.0f;
		}
	}
	private ScalingToPointParams scalingToPointParams;	
	
	private E_SEShiftingType shiftingType;	
	public E_SEShiftingType getShiftingType() {
		return this.shiftingType;
	}
	public void setShiftingType(E_SEShiftingType shiftingType) {
		this.shiftingType = shiftingType;
	}

	private long lastTick = 0;
	private boolean shifting = false;
	
	private float screenWidth;
	private float screenHeight;
	private float xDensity;
	private float yDensity;
	
	public boolean isShiftingPlaying()
	{
		return this.shifting;
	}
	
	public void startShifting()
	{
		this.shifting = true;
		this.lastTick = SystemClock.uptimeMillis();		
	}
	
	public SEShiftingSprite(boolean useTextureRenderExtension) 
	{
		super(useTextureRenderExtension);		
		
		Context con = SEManagers.getApplicationContext();
		
		this.screenWidth = con.getResources().getDisplayMetrics().widthPixels;
		this.screenHeight = con.getResources().getDisplayMetrics().heightPixels;
		this.xDensity = con.getResources().getDisplayMetrics().widthPixels / 480;
		this.yDensity = con.getResources().getDisplayMetrics().heightPixels / 320;
		this.shiftingType = E_SEShiftingType.SimpleShifting;
	}
	
	public void setupSimpleShifting(PointF start, PointF dir, float speed, float velocity, float length)
	{
		this.setupSimpleShifting(new SESimpleShiftingParams(start, dir, speed, velocity, length, this.xDensity, this.yDensity)); 
	}
	
	public void setupSimpleShifting(SESimpleShiftingParams simpleShiftingParams)
	{
		this.simpleShiftingParams = simpleShiftingParams;
		
		this.shiftingType = E_SEShiftingType.SimpleShifting;
	}
	
	public void setupScalingToPoint(PointF start, PointF dir, float speed, float velocity, float length, PointF startSize, float finalScale)
	{
		this.setupSimpleShifting(new SESimpleShiftingParams(start, dir, speed, velocity, length, this.xDensity, this.yDensity));
		this.setupScalingToPoint(new ScalingToPointParams(startSize, finalScale)); 
		this.scalingToPointParams.ScaleSpeed = (length / speed) / SEShiftingSprite.TIME_RATIO;
	}
	
	public void setupScalingToCenter(PointF start, float speed, float velocity, PointF startSize, float finalScale)
	{
		PointF dir = new PointF(this.screenWidth / 2.0f - start.x, this.screenHeight / 2.0f - start.y);

		this.setupSimpleShifting(new SESimpleShiftingParams(start, dir, speed, velocity, dir.length(), this.xDensity, this.yDensity));
		this.setupScalingToPoint(new ScalingToPointParams(startSize, finalScale)); 
		this.scalingToPointParams.ScaleSpeed = (dir.length() / speed) / SEShiftingSprite.TIME_RATIO;
		
		this.shiftingType = E_SEShiftingType.ScalingToCenter;
	}
	
	public void setupScalingToPoint(ScalingToPointParams scalingToCenterParams)
	{
		this.scalingToPointParams = scalingToCenterParams; 
		
		this.shiftingType = E_SEShiftingType.ScalingToPoint;
	}
	
	private void updateShifting()
	{
		if (!this.shifting)
			return;
		
		float time = (float)(SystemClock.uptimeMillis() - this.lastTick) / SEShiftingSprite.TIME_RATIO;
		
		if ((this.shiftingType == E_SEShiftingType.SimpleShifting) || 
			(this.shiftingType == E_SEShiftingType.ScalingToPoint) ||
			(this.shiftingType == E_SEShiftingType.ScalingToCenter))
		{
			if ((this.shiftingType == E_SEShiftingType.ScalingToPoint) ||
				(this.shiftingType == E_SEShiftingType.ScalingToCenter))
			{
				float ds = this.scalingToPointParams.ScaleSpeed * time; // v0 * t
				
				if (ds > this.scalingToPointParams.FinalScale)
					ds = this.scalingToPointParams.FinalScale;
				
				this.width = this.scalingToPointParams.StartSize.x * (1.0f + ds);
				this.height = this.scalingToPointParams.StartSize.y * (1.0f + ds);
			}
			
			float dx = this.simpleShiftingParams.Speed.x * time + // vx0 * t
			 		   this.simpleShiftingParams.Velocity.x * time * time / 2.0f; // ax * t^2 / 2;
			
			float dy = this.simpleShiftingParams.Speed.y * time + // vy0 * t
			 		   this.simpleShiftingParams.Velocity.y * time * time / 2.0f; // ay * t^2 / 2
			
			if (((dx >= Math.abs(this.simpleShiftingParams.Length.x)) && (Math.abs(this.simpleShiftingParams.Length.x) > 0.0f)) ||
				((dy >= Math.abs(this.simpleShiftingParams.Length.y)) && (Math.abs(this.simpleShiftingParams.Length.y) > 0.0f)))
			{
				this.x = this.simpleShiftingParams.Start.x + this.simpleShiftingParams.Length.x;
				this.y = this.simpleShiftingParams.Start.y + this.simpleShiftingParams.Length.y;
				
				this.shifting = false;
				return;
			}
			
			if (this.simpleShiftingParams.Direction.x < 0.0f)
				dx *= -1;
			
			if (this.simpleShiftingParams.Direction.y < 0.0f)
				dy *= -1;
			
			this.x = this.simpleShiftingParams.Start.x + dx;
			this.y = this.simpleShiftingParams.Start.y + dy;			
		}		
	}

	@Override
	public void Render()
	{
		this.updateShifting();
		
		super.Render();
	}
}
