package com.spiritgames.spiritengine.graphics;

import com.spiritgames.spiritengine.SEMath;

import android.graphics.PointF;

public class SESimpleShiftingParams
{
	public PointF Start;
	public PointF Direction;
	public PointF Speed;
	public PointF Velocity;
	public PointF Length;
	public PointF Finish;
	public boolean SecondHalfPath = false;
	
	public SESimpleShiftingParams(PointF start, PointF dir, float speed, float velocity, float length, float xDensity, float yDensity)
	{			
		this.Start = new PointF(start.x * xDensity, start.y * yDensity);
		this.Direction = SEMath.normalize2DVector(dir);			
		this.Speed = new PointF(Math.abs(this.Direction.x) * speed, Math.abs(this.Direction.y * speed));
		this.Velocity = new PointF(Math.abs(this.Direction.x * velocity), Math.abs(this.Direction.y * velocity));
		if (velocity < 0.0f)
		{
			this.Velocity.x *= -1.0f;
			this.Velocity.y *= -1.0f;
		}
		this.Length = new PointF(this.Direction.x * length * xDensity, this.Direction.y * length * yDensity);
		this.Finish = new PointF(this.Start.x + this.Length.x, this.Start.y + this.Length.y);
	}
	
	public SESimpleShiftingParams(PointF start, PointF finish, float speed, float velocity, float xDensity, float yDensity)
	{	
		this.Start = new PointF(start.x * 1, start.y * 1);
		this.Finish = new PointF(finish.x * 1, finish.y * 1);

		this.Direction = SEMath.normalize2DVector(new PointF(this.Finish.x - this.Start.x, this.Finish.y - this.Start.y));			
		this.Speed = new PointF(Math.abs(this.Direction.x) * speed, Math.abs(this.Direction.y * speed));
		this.Velocity = new PointF(Math.abs(this.Direction.x * velocity), Math.abs(this.Direction.y * velocity));
		if (velocity < 0.0f)
		{
			this.Velocity.x *= -1.0f;
			this.Velocity.y *= -1.0f;
		}
		
		float length = (float)Math.sqrt((this.Finish.x - this.Start.x) * (this.Finish.x - this.Start.x) +
							            (this.Finish.y - this.Start.y) * (this.Finish.y - this.Start.y));		
		this.Length = new PointF(this.Direction.x * length * 1, this.Direction.y * length * 1);
	}
}