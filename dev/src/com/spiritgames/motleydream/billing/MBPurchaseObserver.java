package com.spiritgames.motleydream.billing;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.spiritgames.motleydream.MBActivity;
import com.spiritgames.motleydream.MBPreferencesSaver;
import com.spiritgames.motleydream.R;
import com.spiritgames.spiritengine.billing.SEBillingService.RestoreTransactions;
import com.spiritgames.spiritengine.billing.SEBillingService.SERequestPurchase;
import com.spiritgames.spiritengine.billing.SEPurchaseObserver;
import com.spiritgames.spiritengine.billing.SEBillingConsts.PurchaseState;
import com.spiritgames.spiritengine.billing.SEBillingConsts.ResponseCode;

public class MBPurchaseObserver extends SEPurchaseObserver 
{
	private Activity activity;
	private MBPreferencesSaver fullVersionWriter;
	
	public MBPurchaseObserver(Activity activity, Handler handler) 
	{
		super(activity, handler);
		
		this.activity = activity;
	}

	@Override
	public void onBillingSupported(boolean supported) 
	{
		Log.d("MotleyDreamBilling", "onBillingSupported");
	}

	@Override
	public void onPurchaseStateChange(PurchaseState purchaseState,
			String itemId, int quantity, long purchaseTime,
			String developerPayload) 
	{
		Log.d("MotleyDreamBilling", "onPurchaseStateChange");
		
		if (itemId.equalsIgnoreCase(this.activity.getResources().getString(R.string.first_three_episodes)))
		{
			if (purchaseState == PurchaseState.PURCHASED)
			{
				try
				{
					MBActivity.UnlockFullVersion(true);
				}
				catch (Exception e)
				{
					Log.e("MotleyDream", "onPurchaseStateChange(PURCHASED) exception: " + e.getMessage());
				}
	        }
			
			if (purchaseState == PurchaseState.REFUNDED)
			{
				try
				{
					MBActivity.UnlockFullVersion(false);
				}
				catch (Exception e)
				{
					Log.e("MotleyDream", "onPurchaseStateChange(REFUNDED) exception: " + e.getMessage());
				}
			}
		}
	}

	@Override
	public void onRequestPurchaseResponse(SERequestPurchase request,
			ResponseCode responseCode) 
	{
		if (responseCode == ResponseCode.RESULT_OK) 
		{
			Log.d("MotleyDreamBilling", "onRequestPurchaseResponse - OK");
		}
		
		Log.d("MotleyDreamBilling", "onRequestPurchaseResponse - end");
	}

	@Override
	public void onRestoreTransactionsResponse(RestoreTransactions request,
			ResponseCode responseCode) 
	{
		if (responseCode == ResponseCode.RESULT_OK) 
		{
			Log.d("MotleyDreamBilling", "onRestoreTransactionsResponse - OK");
		}
		
		Log.d("MotleyDreamBilling", "onRestoreTransactionsResponse - end");
	}

}
