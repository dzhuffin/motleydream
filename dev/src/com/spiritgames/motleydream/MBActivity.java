package com.spiritgames.motleydream;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.spiritgames.motleydream.billing.MBPurchaseObserver;
import com.spiritgames.motleydream.enums.E_MBGameEpisodes;
import com.spiritgames.spiritengine.E_SEActivityState;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.SEActivity;
import com.spiritgames.spiritengine.billing.SEBillingService;
import com.spiritgames.spiritengine.billing.SEResponseHandler;

public class MBActivity extends SEActivity 
{
	private MBPurchaseObserver purchaseObserver;
    private Handler billingHandler;
    private SEBillingService billingService;
    
    private static MBPreferencesSaver fullVersionIO;
    
    private static boolean IsFullVersionAvailable = false;
    public static int LevelsInFreeVersionCount = 7;
    public static boolean IsFullVersionUnlocked()
    {
    	return MBActivity.IsFullVersionAvailable;
    }
    public static boolean IsThisLevelPaid(E_MBGameEpisodes episode, int level)
    {
    	return (episode == E_MBGameEpisodes.DAY && level > MBActivity.LevelsInFreeVersionCount || episode != E_MBGameEpisodes.DAY);
    }
    public static void UnlockFullVersion(boolean enable)
    {
    	MBActivity.IsFullVersionAvailable = enable;
    	MBManagers.getGameManager().setNeedUpdateLevelsPage(true);
    	
    	if (enable)
    		MBActivity.fullVersionIO.savePrefB("debug", true);
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        this.billingHandler = new Handler();
        this.purchaseObserver = new MBPurchaseObserver(this, this.billingHandler);
        
        this.billingService = new SEBillingService();
        this.billingService.setContext(this);
        
        SEResponseHandler.register(this.purchaseObserver);
        MBActivity.fullVersionIO = new MBPreferencesSaver();
        
        //if (!MBActivity.IsFullVersionAvailable && this.billingService.checkBillingSupported())
        	//this.billingService.restoreTransactions();
        
        this.setManagers(new MBRenderManager(
        		new ISEFloatEventHandler() 
        		{ 
        			public void onHandle(float param) 
        			{
        				//MBActivity.this.requestUnlockFullVersion();
        			}
        		}), 
        		new MBGameManager());
        
        MBActivity.UnlockFullVersion(true);
        MBActivity.IsFullVersionAvailable = MBActivity.fullVersionIO.loadPrefB("debug", "true");
    }
    
    @Override
    protected void onDestroy() 
    {
        super.onDestroy();
        this.billingService.unbind();
    }
    
    @Override
    protected void onResume() 
    {
        super.onResume();
        
        if (MBManagers.getSoundManager() != null)
        	MBManagers.getSoundManager().resumeBackgroundMusic();
    }
    
    @Override
    protected void onPause() 
    {
        super.onPause();

        if (MBManagers.getSoundManager() != null)
        	MBManagers.getSoundManager().pauseBackgroundMusic();
        
        if ((this.getState() == E_SEActivityState.InGame) && 
        	(MBManagers.getGameManager().inGameMenuManager.getActivePage() != MBGameManager.FINISH_MENU_ID))
        {
        	MBManagers.getGameManager().setGamePaused(true);
        	MBManagers.getGameManager().inGameMenuManager.setActivePage(MBGameManager.PAUSE_MENU_ID, false);
        }
    }
    
    @Override
    protected void onStart() 
    {    	
        super.onStart();
        
        //SEResponseHandler.register(this.purchaseObserver);
    }
    
    @Override
    protected void onStop() 
    {    	
    	if (MBManagers.getSoundManager() != null)
    		MBManagers.getSoundManager().pauseBackgroundMusic();
    	
    	//SEResponseHandler.unregister(this.purchaseObserver);
    	
        super.onStop();
    }
    
    public void requestUnlockFullVersion()
    {
    	//MBActivity.UnlockFullVersion(!MBActivity.IsFullVersionUnlocked());
    	if (!this.billingService.requestPurchase(this.getResources().getString(R.string.first_three_episodes), null)) 
    	{
    		Toast.makeText(this, "billing not supported", Toast.LENGTH_LONG).show();
        }
    }
}