package com.spiritgames.motleydream;

import java.util.HashMap;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.PointF;

import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.motleydream.enums.E_MBGameEpisodes;
import com.spiritgames.motleydream.level.MBGameProgressInfoDB;
import com.spiritgames.motleydream.level.MBLevelCompleteInfo;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.graphics.SERenderManager;
import com.spiritgames.spiritengine.graphics.SESimpleShiftingParams;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.menu.ISEMenuItem;
import com.spiritgames.spiritengine.menu.SEKineticMenuPage;
import com.spiritgames.spiritengine.sound.SESoundManager;
import com.spiritgames.spiritengine.ui.SEButton;

public class MBLevelsMenuPage extends SEKineticMenuPage 
{
	private float levelTextSize = 30.0f;
	
	private SESprite prizeEnable;
	private SESprite prizeDisable;
	
	private MBGameProgressInfoDB progressInfo;
	private E_MBGameEpisodes episode;
	private E_MBDifficultyLevel difLevel;
	
	private HashMap<Integer, MBLevelCompleteInfo> episodeCompleteInfo;
	
	public MBLevelsMenuPage(int levelsCount,
			MBGameProgressInfoDB progressInfo, E_MBGameEpisodes episode, 
			E_MBDifficultyLevel difLevel, ISEFloatEventHandler loadLevel) 
	{
		super(levelsCount, loadLevel, 1.0f);
		
		this.difLevel = difLevel;
		
		this.prizeEnable = new SESprite(true);
		this.prizeEnable.setCurrentTexture(this.prizeEnable.addTexture(R.drawable.icon));
		
		this.prizeDisable = new SESprite(true);
		this.prizeDisable.setCurrentTexture(this.prizeDisable.addTexture(R.drawable.disable_icon));
		
		this.episode = episode;
		
		float xDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480.0f;
		float yDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320.0f;
		
		int HEADER_ID = this.addShiftingImage(1.75f, R.drawable.menu_levels_header, 
				new SESimpleShiftingParams(new PointF(240.0f, 395.0f), new PointF(0.0f, -1.0f), 1.0f, 10.0f, 140.0f, xDensity, yDensity), 
				new SESimpleShiftingParams(new PointF(240.0f, 260.0f), new PointF(0.0f, 1.0f), 1.0f, 10.0f, 135.0f, xDensity, yDensity));
		this.setItemSize(HEADER_ID, 230.0f, 130.0f);
		
		int IMG_ID = this.addAnimatedImage(1.0f, R.drawable.changer_blue_1, 6, 100);
		this.setItemPos(IMG_ID, 60.0f, 255.0f);
		this.setItemSize(IMG_ID, 50.0f, 50.0f);
		
		IMG_ID = this.addAnimatedImage(1.0f, R.drawable.changer_red_1, 6, 100);
		this.setItemPos(IMG_ID, 420.0f, 255.0f);
		this.setItemSize(IMG_ID, 50.0f, 50.0f);
		
		this.progressInfo = progressInfo;
		
		this.levelTextSize = (int)(MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 5.0f);
		
		this.episodeCompleteInfo = this.progressInfo.getCompleteInfoForEpisode(this.episode, this.difLevel);
		MBLevelCompleteInfo dayLastCompleteInfo = this.progressInfo.getCompleteInfo(E_MBGameEpisodes.DAY, MBGameManager.getLevelsCount(E_MBGameEpisodes.DAY), this.difLevel);
		MBLevelCompleteInfo eveningLastCompleteInfo = this.progressInfo.getCompleteInfo(E_MBGameEpisodes.EVENING, MBGameManager.getLevelsCount(E_MBGameEpisodes.EVENING), this.difLevel);
        
        for (int i = 1; i <= levelsCount; i++)
		{
        	boolean enable;
        	
        	if ((this.episode == E_MBGameEpisodes.DAY) && (i == 1)) 
	    		enable = true;
	    	else
	    	{
	    		if ((this.episode == E_MBGameEpisodes.EVENING) && (i == 1))
	    			enable = dayLastCompleteInfo.complited > 0;
	    		else
	    		{
	    			if ((this.episode == E_MBGameEpisodes.NIGHT) && (i == 1))
		    			enable = eveningLastCompleteInfo.complited > 0;
		    		else
		    			enable = this.episodeCompleteInfo.get(i - 1).complited > 0;
	    		}
	    	}
        	
        	// �������� ��������
        	if (MBActivity.IsThisLevelPaid(this.episode, i))
        		enable = enable & MBActivity.IsFullVersionUnlocked();
        	
			this.setItemEnable(this.addButton(1.0f, R.drawable.level_icon_border, R.drawable.level_icon_border_press, R.drawable.level_icon_border_locked, new ISEFloatEventHandler() 
			{ 
				public void onHandle(float param) 
				{ 
					MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
					MBLevelsMenuPage.this.onClick.onHandle(param);
				}
			}, (float)i), enable);
		}
	}

	public void RefreshLevels(E_MBDifficultyLevel difLevel)
	{	
		this.episodeCompleteInfo = this.progressInfo.getCompleteInfoForEpisode(this.episode, this.difLevel);
		MBLevelCompleteInfo dayLastCompleteInfo = this.progressInfo.getCompleteInfo(E_MBGameEpisodes.DAY, MBGameManager.getLevelsCount(E_MBGameEpisodes.DAY), this.difLevel);
		MBLevelCompleteInfo eveningLastCompleteInfo = this.progressInfo.getCompleteInfo(E_MBGameEpisodes.EVENING, MBGameManager.getLevelsCount(E_MBGameEpisodes.EVENING), this.difLevel);
		
		this.difLevel = difLevel;
		
		for (int j = 0, i = 1; j < this.items.size(); j++)
		{
			if (this.items.get(j).getClass() == SEButton.class)
			{
				boolean enable;
		    	
		    	if ((this.episode == E_MBGameEpisodes.DAY) && (i == 1)) 
		    		enable = true;
		    	else
		    	{
		    		if ((this.episode == E_MBGameEpisodes.EVENING) && (i == 1))
		    			enable = dayLastCompleteInfo.complited > 0;
		    		else
		    		{
		    			if ((this.episode == E_MBGameEpisodes.NIGHT) && (i == 1))
			    			enable = eveningLastCompleteInfo.complited > 0;
			    		else
			    			enable = this.episodeCompleteInfo.get(i - 1).complited > 0;
		    		}
		    	}
		    	
	        	// �������� ��������
		    	if (MBActivity.IsThisLevelPaid(this.episode, i))
	        		enable = enable & MBActivity.IsFullVersionUnlocked();
				
				this.setItemEnable(j, enable);
				
				i++;
			}
		}
	}
	
	@Override
	protected void recalcItemsPos(float screenW, float screenH)
	{
		super.recalcItemsPos(screenW, screenH);
		
		if (this.items.size() < 1)
			return;
		
		this.levelTextSize = 20.0f;
		PointF size;
		for (int i = 10; i < 1000; i += 10)
		{
			size = SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).MeasureText((float)i / 10.0f, "1");
			
			if (size.y >= this.items.get(0).getHeight() * 0.25f)
			{
				this.levelTextSize = (float)i / 10.0f;
				break;
			}
		}
	}
	
	@Override
	public void restore()
	{
		super.restore();
		
		this.prizeEnable.restore();
		this.prizeDisable.restore();
	}
	
	@Override
	public void Render(float screenW, float screenH)
	{		
		if ((this.scrW != screenW) || (this.scrH != screenH))
		{
			this.scrW = screenW;
			this.scrH = screenH;
			this.recalc = true;
		}		
		
		if (this.recalc)
			this.recalcItemsPos(screenW, screenH);
		
		this.updateScroll(screenW, screenH);
		
		MBLevelCompleteInfo info;
		for (int j = 0, i = 0; j < this.items.size(); j++)
		{
			if (this.items.get(j).getClass() != SEButton.class)
			{
				((ISEMenuItem)this.items.get(j)).Render();
				continue;
			}
			
			ISEMenuItem item = ((ISEMenuItem)this.items.get(j));
			
			info = this.episodeCompleteInfo.get(i + 1);
			
			float cX = (1.2f * item.getWidth() / 2.0f) + (1.2f * item.getWidth() * i) + this.scrollPos;
			float cY = this.pageRect.centerY();
			item.setPos(cX, cY);		
			
			((ISEMenuItem)this.items.get(j)).Render();
			
			PointF textSize = SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).MeasureText(this.levelTextSize, Integer.toString(i + 1));
			SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).RenderText(cX - (textSize.x / 2.0f), cY + (textSize.y / 1.5f), this.levelTextSize, 
																				 Integer.toString(i + 1), false);
			
			if (info != null)
			{
				if (info.complited > 0)
				{
					textSize = SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).MeasureText(this.levelTextSize / 3.0f, info.toTimeString_SMs());
					SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).RenderText(cX - (2.0f * textSize.x / 3.0f), cY + item.getHeight() / 2.5f, 
																						 this.levelTextSize / 2.0f, info.toTimeString_SMs(), false);
				}
				
				if (((ISEMenuItem)this.items.get(j)).isEnable())
				{
					this.prizeEnable.setSize(item.getHeight() / 5.0f, item.getHeight() / 5.0f);
					this.prizeDisable.setSize(item.getHeight() / 5.0f, item.getHeight() / 5.0f);
					float py = cY - item.getHeight() / 4.0f;
					
					for (int s = 1; s <= info.maxStarsCount; s++)
					{
						float px = (cX - item.getWidth() / 2.0f) + (float)s * item.getWidth() / ((float)info.maxStarsCount + 1.0f);
						
						this.prizeEnable.setPos(px, py);
						this.prizeDisable.setPos(px, py);
						
						if (s <= info.starsCount)
							this.prizeEnable.Render();
						else
							this.prizeDisable.Render();
					}
				}
			}

			i++;
		}
	}
}