package com.spiritgames.motleydream;

import android.view.KeyEvent;
import android.view.MotionEvent;

import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.motleydream.enums.E_MBGameEpisodes;
import com.spiritgames.motleydream.level.MBGameProgressInfoDB;
import com.spiritgames.motleydream.level.MBLevelCompleteInfo;
import com.spiritgames.motleydream.level.MBLevelLoader;
import com.spiritgames.motleydream.level.MBLevelManager;
import com.spiritgames.spiritengine.E_SEActivityState;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.SEGameManager;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.menu.E_SEMenuItemsArrangement;
import com.spiritgames.spiritengine.menu.SEMenuManager;

public class MBGameManager extends SEGameManager 
{	
	private SESprite loadingScreen;
	
	public MBGameProgressInfoDB gameProgressInfo;
	
	private E_MBDifficultyLevel difficultyLevel = E_MBDifficultyLevel.Normal;	
	public E_MBDifficultyLevel getDifficultyLevel() {
		return difficultyLevel;
	}
	public void setDifficultyLevel(E_MBDifficultyLevel difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

	private float scrW;
	private float scrH;
	
	public static int FINISH_MENU_ID;
	public static int PAUSE_MENU_ID;
	
	public static int RESULT_TIME_ID;
	public static int RESULT_LEVEL_ID;
	public static int RESULT_PRIZES_ID;
	
	public static int RESULT_LEVEL_NEXT_ID;
	public static int RESULT_UNLOCK_ID;

	private MBLevelLoader levelLoader;
	private int currentLevel = 1;
	private E_MBGameEpisodes currentEpisode = E_MBGameEpisodes.DAY;
	private int lastLevel = 0;
	private E_MBGameEpisodes lastEpisode = E_MBGameEpisodes.DAY;
	public static int getLevelsCount(E_MBGameEpisodes episode)
	{
		if (episode == E_MBGameEpisodes.DAY)
			return 10;
		
		if (episode == E_MBGameEpisodes.EVENING)
			return 10;
		
		if (episode == E_MBGameEpisodes.NIGHT)
			return 10;
		
		return 0;
	}
	
	@Override
	public void setGamePaused(boolean paused) {
		super.setGamePaused(paused);
	}
	
	private MBLevelManager levelManager;
	private int joystick_id = -1;
	
	private float gSensorSensitivity = 0.5f;
	public float isGSensorSensitivity() {
		return this.gSensorSensitivity;
	}
	public void setGSensorSensitivity(float gSensorSensitivity) {
		this.gSensorSensitivity = gSensorSensitivity;
	}

	private boolean gSensorEnable = false;	
	public boolean isGSensorEnable() {
		return gSensorEnable;
	}
	public void setGSensorEnable(boolean sensorEnable) {
		this.gSensorEnable = sensorEnable;
	}
	
	private boolean vibrationEnable = false;	
	public boolean isVibrationEnable() {
		return vibrationEnable;
	}
	public void setVibrationEnable(boolean vibrationEnable) {
		this.vibrationEnable = vibrationEnable;
	}
	
	private boolean showLoading = false;
	private boolean letRestart = false;
	private boolean clearGameInfo = false;

	private boolean updateLevelsPage = false;	
	public boolean needUpdateLevelsPage() {
		boolean res = this.updateLevelsPage;
		this.updateLevelsPage = false;
		return res;
	}
	public void setNeedUpdateLevelsPage(boolean val)
	{
		this.updateLevelsPage = val;
	}

	public MBGameManager()
	{
		super();		
		
		this.gameProgressInfo = new MBGameProgressInfoDB();
	}
	
	public boolean isLevelLoading()
	{
		return this.letRestart || this.showLoading;
	}
	
	private void restartLevel()
	{				
		if (this.levelManager != null)
		{
			this.levelManager.release();
			this.levelManager = null;
			System.gc();
		}
		
		this.holdScreen = false;
		
		this.levelManager = this.levelLoader.loadLevel(this.currentEpisode, this.currentLevel, this.difficultyLevel);
		
		if ((this.currentEpisode == this.lastEpisode) && (this.currentLevel == this.lastLevel))
			this.skipShowingFinish();
		
		this.lastLevel = this.currentLevel;
		this.lastEpisode = this.currentEpisode;
		
		this.levelManager.setVibrator(this.vibrator);
		
		if (!this.clearGameInfo)
		{
			this.levelManager.gameInfo.setPlayTime(0);
		}
		else
			this.clearGameInfo = false;
		
		/**
		 * ��������� � true ���������� ������� Loading �� ������ � �� ��������� ����� ��������� �������� ������
		 */
		this.showLoading = false;
		/**
		 * ��������� � true ��������� �������� ������
		 */
		this.letRestart = false;
		
		this.setGamePaused(true);
		MBGameManager.this.inGameMenuManager.setActivePage(SEMenuManager.ACTIVE_PAGE_NONE, true);
		
		this.gcManager.resetState();
	}
	
	public void loadLevel(E_MBGameEpisodes episode, float level)
	{						
		SEManagers.getActivity().resetPinchScale();
		this.currentLevel = (int)level;
		this.currentEpisode = episode;
		this.clearGameInfo = this.showLoading = true;
	}

	@Override
	public void Init() 
	{	
		this.levelLoader = new MBLevelLoader(false);
		
		this.loadingScreen = new SESprite(true);
		this.loadingScreen.setCurrentTexture(this.loadingScreen.addTexture(R.drawable.loading));
		
		this.showLoading = true;
		
		this.joystick_id = this.gcManager.AddSingleJoystick(R.drawable.zoom_scale_h, R.drawable.zoom_track_h, 0.062f, 0.825f, 2.0f, 0.2f, false, 0.27f, 0.085f);
		// -----FINISH-PAGE-------
		this.inGameMenuManager.setActivePage(this.inGameMenuManager.addPage(), true);
		this.inGameMenuManager.setPageArrangement(E_SEMenuItemsArrangement.Custom);

        this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addImage(R.drawable.goal),
        240.0f, 160.0f, 370.0f, 260.0f);
		this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addButton(4.5f, R.drawable.game_btn_restart_unpress, R.drawable.game_btn_restart_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{				
				MBGameManager.this.showLoading = true;
				MBGameManager.this.setGamePaused(false);
			}
		}, 0.0f),
		160.0f, 115.0f, 60.0f, 60.0f);
		
		MBGameManager.RESULT_LEVEL_NEXT_ID = this.inGameMenuManager.addButton(4.5f, R.drawable.game_btn_next_unpress, R.drawable.game_btn_next_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{				
				MBGameManager.this.prepareNewLevel();
			}
		}, 0.0f);
		this.inGameMenuManager.setItemArrangement(MBGameManager.RESULT_LEVEL_NEXT_ID, 240.0f, 115.0f, 60.0f, 60.0f);
		//
		MBGameManager.RESULT_UNLOCK_ID = this.inGameMenuManager.addButton(4.5f, R.drawable.game_btn_unlock_unpress, R.drawable.game_btn_unlock_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{				
				MBManagers.getActivity().requestUnlockFullVersion();
			}
		}, 0.0f);
		this.inGameMenuManager.setItemArrangement(MBGameManager.RESULT_UNLOCK_ID, 240.0f, 115.0f, 60.0f, 60.0f);
		this.inGameMenuManager.setItemVisible(MBGameManager.RESULT_UNLOCK_ID, false);
		//
		this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addButton(4.5f, R.drawable.game_btn_home_unpress, R.drawable.game_btn_home_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{
				MBGameManager.this.lastLevel = 0;
				MBGameManager.this.setGamePaused(false);
				SEManagers.getActivity().setState(E_SEActivityState.MainMenu);
			}
		}, 0.0f),
		320.0f, 115.0f, 60.0f, 60.0f);
		//
		MBGameManager.RESULT_TIME_ID = this.inGameMenuManager.addLabel(MBRenderManager.getFont(MBRenderManager.FONT_COMPLETE_INFO), 
				"Time 23:56", 20.0f);
		this.inGameMenuManager.setItemArrangement(MBGameManager.RESULT_TIME_ID, 130.0f, 190.0f, 20.0f, 20.0f);
		//
		MBGameManager.RESULT_LEVEL_ID = this.inGameMenuManager.addLabel(MBRenderManager.getFont(MBRenderManager.FONT_COMPLETE_INFO), 
				"Time 23:56", 25.0f);
		this.inGameMenuManager.setItemArrangement(MBGameManager.RESULT_LEVEL_ID, 160.0f, 240.0f, 25.0f, 25.0f);
		//
		MBGameManager.RESULT_PRIZES_ID = this.inGameMenuManager.addLabel(MBRenderManager.getFont(MBRenderManager.FONT_COMPLETE_INFO), 
				"Time 23:56", 15.0f); 
		this.inGameMenuManager.setItemArrangement(MBGameManager.RESULT_PRIZES_ID, 350.0f, 245.0f, 15.0f, 15.0f);
		//
		this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addImage(R.drawable.mini_prize),
				320.0f, 230.0f, 32.0f, 32.0f);
		
		MBGameManager.FINISH_MENU_ID = this.inGameMenuManager.getActivePage();
		// ----------------------- 
		// -----PAUSE-PAGE--------
		this.inGameMenuManager.setActivePage(this.inGameMenuManager.addPage(), true);
		this.inGameMenuManager.setPageArrangement(E_SEMenuItemsArrangement.Custom);

        this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addImage(R.drawable.pause),
		240.0f, 160.0f, 370.0f, 260.0f);
		this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addButton(1.0f, R.drawable.game_btn_back_unpress, R.drawable.game_btn_back_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{
				MBGameManager.this.setGamePaused(false);
			}
		}, 0.0f),
		160.0f, 115.0f, 60.0f, 60.0f);
		this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addButton(4.5f, R.drawable.game_btn_restart_unpress, R.drawable.game_btn_restart_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{	
				MBGameManager.this.setGamePaused(false);
				MBGameManager.this.showLoading = true;
			}
		}, 0.0f),
		240.0f, 115.0f, 60.0f, 60.0f);
		this.inGameMenuManager.setItemArrangement(this.inGameMenuManager.addButton(4.5f, R.drawable.game_btn_home_unpress, R.drawable.game_btn_home_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{	
				MBGameManager.this.lastLevel = 0;
				MBGameManager.this.setGamePaused(false);
				SEManagers.getActivity().setState(E_SEActivityState.MainMenu);
			}
		}, 0.0f),
		320.0f, 115.0f, 60.0f, 60.0f);
		MBGameManager.PAUSE_MENU_ID = this.inGameMenuManager.getActivePage();
		// ----------------------- 
		MBManagers.setSoundManager(new MBSoundManager());
	}
	
	@Override
	public void release()
	{
		if (this.levelManager != null)
			this.levelManager.release();
		
		if (this.loadingScreen != null)
			this.loadingScreen.release();
		
		if (MBManagers.getSoundManager() != null)
			MBManagers.getSoundManager().release();
		
		super.release();
	}
	
	@Override
	public void restore()
	{
		if (this.levelManager != null)
			this.levelManager.restore();
		
		if (this.loadingScreen != null)
			this.loadingScreen.restore();
		
		super.restore();
	}
	
	public void setGameSoundEnable(boolean enable)
	{
		MBManagers.getSoundManager().setSoundEnable(enable);
	}
	
	private void prepareNewLevel()
	{
		if (this.letRestart)
			return;
		
		this.setGamePaused(false);
		
		this.currentLevel++;
		
		if (this.currentLevel > MBGameManager.getLevelsCount(this.currentEpisode))
			this.currentLevel = 1;

		this.showLoading = true;
	}
	
	public void skipShowingFinish() 
	{
		this.levelManager.skipShowingFinish();
	}
	
	public boolean isShowingFinish()
	{
		return this.levelManager.isShowingFinish();
	}
	
	private boolean holdScreen = false;
	@Override
	public boolean onTouch(MotionEvent event)
	{
		if (this.isGamePaused())
			return this.inGameMenuManager.onTouch(event);
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			this.holdScreen = event.getPointerCount() == 1;
		}
			
		if(event.getAction() == MotionEvent.ACTION_UP || event.getPointerCount() > 1)
		{
			this.holdScreen = false;
		}
		
		if (this.holdScreen)
		{
			return true;
		}
					
		return super.onTouch(event);
	}
	
	@Override
	public boolean onKey(int keyCode, KeyEvent event) 
	{
		if (event.getAction() == KeyEvent.ACTION_DOWN)
    	{
	    	switch (keyCode)
			{				
				case KeyEvent.KEYCODE_BACK:
				case KeyEvent.KEYCODE_MENU:
					if (!this.isGamePaused())
					{
						MBGameManager.this.setGamePaused(true);
						MBGameManager.this.inGameMenuManager.setActivePage(MBGameManager.PAUSE_MENU_ID, false);
					}
					return true;
				default:
					break;
			}
    	}
		
    	return false;
	}
	
	@Override
	public void Render(float screenW, float screenH)
	{
		if (!this.isGamePaused())
		{
			this.gcManager.setVisible(this.joystick_id, !this.gSensorEnable);
			
			if (this.gSensorEnable)
				this.gcManager.setFloatValue(this.joystick_id, 0.5f - (this.sensorReader.getYOrientation() * (1.0f + this.gSensorSensitivity * 3.0f)) / 180.0f);
			
			if ((this.levelManager != null))
			{
				if (this.vibrationEnable)
					this.levelManager.setVibrator(this.vibrator);
				else
					this.levelManager.setVibrator(null);
			}
			
			this.scrW = screenW;
			this.scrH = screenH;
			
			if (this.holdScreen)
			{
				this.levelManager.BallJump();
			}
			
			if (this.letRestart)
			{
				this.restartLevel();
				return;
			}
			
			if (this.showLoading)
			{
				MBManagers.getSoundManager().stopBackgroundMusic();
				this.loadingScreen.setPos(screenW / 2.0f, screenH / 2.0f);
				this.loadingScreen.setSize(screenW, screenH);
				this.loadingScreen.Render();
				this.letRestart = true;
				return;
			}
			
			float scale = 1.5f - ((SEManagers.getActivity().getPinchScale() - 0.2f) / 4.8f);
			scale *= (float)SEManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320.0f;
			float joyValue = this.gcManager.getFloatValue(this.joystick_id) - 0.5f;
			
			this.levelManager.BallMove(joyValue);
			
			this.levelManager.setScaleX(scale);
			this.levelManager.setScaleY(scale);
		}
		
		this.levelManager.Render(this.scrW, this.scrH, this.isGamePaused());
		
		super.Render(screenW, screenH);
		
		if (!this.isGamePaused())
		{			
			if (this.levelManager.isDead())
				this.showLoading = true;
			
			if (this.levelManager.isFinished() && !this.showLoading)
			{
				MBLevelCompleteInfo newCompleteInfo = MBGameManager.this.levelManager.gameInfo.getCompleteInfo();
				newCompleteInfo.complited = 1;
				if (newCompleteInfo.level < 1)
				{
					newCompleteInfo.level = MBGameManager.this.currentLevel;
					newCompleteInfo.episode = MBGameManager.this.currentEpisode;
					newCompleteInfo.difficultyLevel = MBGameManager.this.difficultyLevel;
				}
				
				MBLevelCompleteInfo oldCompleteInfo = MBGameManager.this.gameProgressInfo.getCompleteInfo(MBGameManager.this.currentEpisode, MBGameManager.this.currentLevel, MBGameManager.this.difficultyLevel);

				MBGameManager.this.gameProgressInfo.collectCompleteInfo(
						MBLevelCompleteInfo.combineToBestCompleteInfo(newCompleteInfo, oldCompleteInfo));
				
				MBGameManager.this.updateLevelsPage = true;
				
				MBGameManager.this.setGamePaused(true);
				MBGameManager.this.inGameMenuManager.setActivePage(MBGameManager.FINISH_MENU_ID, false);
			}
		}
		else
		{
			if (this.inGameMenuManager.getActivePage() == MBGameManager.FINISH_MENU_ID)
			{
				String levelStr;
				if ((!MBActivity.IsFullVersionUnlocked() && MBActivity.IsThisLevelPaid(this.currentEpisode, this.currentLevel + 1)) ||
					    (this.currentLevel >= MBGameManager.getLevelsCount(this.currentEpisode)))
				{
					this.inGameMenuManager.setItemVisible(MBGameManager.RESULT_LEVEL_NEXT_ID, false);
					
					if (!MBActivity.IsFullVersionUnlocked() && MBActivity.IsThisLevelPaid(this.currentEpisode, this.currentLevel + 1))
						this.inGameMenuManager.setItemVisible(MBGameManager.RESULT_UNLOCK_ID, true);
					
					levelStr = "Last level";
				}
				else
				{
					this.inGameMenuManager.setItemVisible(MBGameManager.RESULT_LEVEL_NEXT_ID, true);
					this.inGameMenuManager.setItemVisible(MBGameManager.RESULT_UNLOCK_ID, false);
					levelStr = "Level  " + this.currentLevel;
				}
				
				this.inGameMenuManager.setItemText(MBGameManager.RESULT_LEVEL_ID, levelStr);
				
				String timeStr = this.levelManager.gameInfo.toCompleteTimeString();
				this.inGameMenuManager.setItemText(MBGameManager.RESULT_TIME_ID, timeStr);
				
				String prizeStr = "x " + this.levelManager.gameInfo.toCompleteStarsString();
				this.inGameMenuManager.setItemText(MBGameManager.RESULT_PRIZES_ID, prizeStr);
				
				
			}
			
			this.inGameMenuManager.Render(screenW, screenH);
		}
		
		if (this.levelManager.isJustShowingFinishComplete())
		{
			this.setGamePaused(false);
		}
	}
}
