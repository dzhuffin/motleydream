package com.spiritgames.motleydream;

import android.graphics.PointF;

import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SESimpleShiftingParams;
import com.spiritgames.spiritengine.menu.E_SEMenuItemsArrangement;
import com.spiritgames.spiritengine.menu.SEMenuPage;

public class MBOptionsMenuPage extends SEMenuPage 
{
	private int SOUND_ENABLE_ID = 0;
	private int GSENSOR_SENSITIVITY_ID = 0;
	private int VIBRATION_ENABLE_ID = 0;
	private int GSENSOR_ENABLE_ID = 0;
	private int DIF_LEVEL_ID = 0;
	
	private MBPreferencesSaver prefSaver = null;
	
	public MBOptionsMenuPage(ISEFloatEventHandler onDifLevelChange) 
	{
		super();
		
		this.prefSaver = new MBPreferencesSaver();
		
		//this.addImage(vBuf, 5.0f, R.drawable.mm_options_sound_volume);
		//this.SOUND_VOLUME_ID = this.addTrackControl(vBuf, 8.0f, 1.0f, 0.15f, R.drawable.mm_options_scale_h, R.drawable.mm_options_track);				
		//this.addImage(vBuf, 5.75f, R.drawable.mm_options_gsensor_sensitivity);
		//this.GSENSOR_SENSITIVITY_ID = this.addTrackControl(vBuf, 8.0f, 1.0f, 0.15f, R.drawable.mm_options_scale_h, R.drawable.mm_options_track);
		
		this.setItemsArrangement(E_SEMenuItemsArrangement.Custom);
		 
		float xDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480.0f;
		float yDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320.0f;
		
		int HEADER_ID = this.addShiftingImage(1.75f, R.drawable.menu_options_header, 
				new SESimpleShiftingParams(new PointF(240.0f, 395.0f), new PointF(0.0f, -1.0f), 1.0f, 10.0f, 140.0f, xDensity, yDensity), 
				new SESimpleShiftingParams(new PointF(240.0f, 260.0f), new PointF(0.0f, 1.0f), 1.0f, 10.0f, 135.0f, xDensity, yDensity));
		this.setItemSize(HEADER_ID, 230.0f, 130.0f);
		
		
		/*int IMG_ID = this.addAnimatedImage(1.0f, R.drawable.changer_blue_1, 6, 100);
		this.setItemPos(IMG_ID, 100.0f, 120.0f);
		this.setItemSize(IMG_ID, 120.0f, 120.0f);*/
		
		int IMG_ID = this.addAnimatedImage(1.0f, R.drawable.changer_red_1, 6, 100);
		this.setItemPos(IMG_ID, 60.0f, 255.0f);
		this.setItemSize(IMG_ID, 50.0f, 50.0f);
		
		IMG_ID = this.addAnimatedImage(1.0f, R.drawable.changer_green_1, 6, 100);
		this.setItemPos(IMG_ID, 420.0f, 255.0f);
		this.setItemSize(IMG_ID, 50.0f, 50.0f);
		
		this.SOUND_ENABLE_ID = this.addCheckBox(4.0f, R.drawable.mm_options_sound_unchecked, R.drawable.mm_options_sound_checked, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{ 
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
			}
		});
		this.setItemPos(this.SOUND_ENABLE_ID, 240.0f, 130.0f);
		this.setItemSize(this.SOUND_ENABLE_ID, 180.0f, 45.0f);
		
		this.VIBRATION_ENABLE_ID = this.addCheckBox(4.0f, R.drawable.mm_options_vibration_unchecked, R.drawable.mm_options_vibration_checked, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{ 
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
			}
		});
		this.setItemPos(this.VIBRATION_ENABLE_ID, 240.0f, 60.0f);
		this.setItemSize(this.VIBRATION_ENABLE_ID, 180.0f, 45.0f);
		
		/*this.GSENSOR_ENABLE_ID = this.addCheckBox(4.0f, R.drawable.mm_options_gsensor_unchecked, R.drawable.mm_options_gsensor_checked);
		this.setItemPos(this.GSENSOR_ENABLE_ID, 360.0f, 50.0f);
		this.setItemSize(this.GSENSOR_ENABLE_ID, 140.0f, 32.0f);*/
		
		/*SERadioGroup group = new SERadioGroup(onDifLevelChange);
		group.addButton(E_MBDifficultyLevel.Normal.getIndex(), 50.0f, 150.0f, 140.0f, 32.0f, 
				R.drawable.mm_options_sound_unchecked, R.drawable.mm_options_sound_checked, true);
		group.addButton(E_MBDifficultyLevel.Hardcore.getIndex(), 50.0f, 100.0f, 140.0f, 32.0f, 
				R.drawable.mm_options_vibration_unchecked, R.drawable.mm_options_vibration_checked, false);
		this.DIF_LEVEL_ID = this.addRadioGroup(group);*/
	}
	
	public E_MBDifficultyLevel getCurrentDifficultyLevel()
	{
		//return E_MBDifficultyLevel.getByIndex((int)this.getItemFloatValue(this.DIF_LEVEL_ID));
		return E_MBDifficultyLevel.Normal;
	}
	
	public boolean getSoundEnable()
	{
		return (this.getItemFloatValue(this.SOUND_ENABLE_ID) == 1.0f);
	}
	
	public float getGSensorSensitivity()
	{
		//return this.getItemFloatValue(this.GSENSOR_SENSITIVITY_ID);
		return 1.0f;
	}
	
	public boolean getVibrationEnable()
	{
		return (this.getItemFloatValue(this.VIBRATION_ENABLE_ID) == 1.0f);
	}
	
	public boolean getGSensorEnable()
	{
		//return (this.getItemFloatValue(this.GSENSOR_ENABLE_ID) == 1.0f);
		return true;
	}
	
	public void SavePrefs()
	{
		this.prefSaver.savePrefB("sound_enable", this.getSoundEnable());
		this.prefSaver.savePrefF("sensor_sensitivity", this.getGSensorSensitivity());
		this.prefSaver.savePrefB("vibration_enable", this.getVibrationEnable());
		//this.prefSaver.savePrefB("sensor_enable", this.getGSensorEnable());
		//this.prefSaver.savePref("dif_level", this.getCurrentDifficultyLevel().getIndex());
	}
	
	public void LoadPrefs()
	{
		if (this.prefSaver.loadPrefB("sound_enable", "true"))
			this.setItemFloatValue(this.SOUND_ENABLE_ID, 1.0f);
		else
			this.setItemFloatValue(this.SOUND_ENABLE_ID, 0.0f);
		//
		//this.setItemFloatValue(this.GSENSOR_SENSITIVITY_ID, this.prefSaver.loadPrefF("sensor_sensitivity", "1.0"));
		//
		if (this.prefSaver.loadPrefB("vibration_enable", "true"))
			this.setItemFloatValue(this.VIBRATION_ENABLE_ID, 1.0f);
		else
			this.setItemFloatValue(this.VIBRATION_ENABLE_ID, 0.0f);
		//
		/*if (this.prefSaver.loadPrefB("sensor_enable", "true"))
			this.setItemFloatValue(this.GSENSOR_ENABLE_ID, 1.0f);
		else
			this.setItemFloatValue(this.GSENSOR_ENABLE_ID, 0.0f);	*/	
		//
		//this.setItemFloatValue(this.DIF_LEVEL_ID, this.prefSaver.loadPref("dif_level", "0"));
	}
}