package com.spiritgames.motleydream;

import android.content.Intent;
import android.graphics.PointF;
import android.net.Uri;
import android.os.SystemClock;

import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.motleydream.enums.E_MBGameEpisodes;
import com.spiritgames.spiritengine.E_SEActivityState;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.graphics.SERenderManager;
import com.spiritgames.spiritengine.graphics.SESimpleShiftingParams;
import com.spiritgames.spiritengine.menu.E_SEMenuItemsArrangement;
import com.spiritgames.spiritengine.menu.SEMenuManager;

public class MBRenderManager extends SERenderManager 
{
    public static int FONT_RAGE_ITALIC = 0;
    public static int FONT_ARIAL_BLACK = 1;
    public static int FONT_COMPLETE_INFO = 2;
    
    private long fpsUpdateTickCount = 0;
    
    private MBOptionsMenuPage options;
    
    private int needPrefsSave = 0; // 0 - �� ���� ���������, 1 - ����� � ���������, 2 - ����� �� ��������
    private MBLevelsMenuPage dayLevels;
    private MBLevelsMenuPage nightLevels;
    private MBLevelsMenuPage eveningLevels;
    private MBEpisodesMenuPage episodesPage;
    public static int MAIN_PAGE_ID;
    public static int DAY_LEVELS_PAGE_ID;
    public static int NIGHT_LEVELS_PAGE_ID;
    public static int EVENING_LEVELS_PAGE_ID;
    public static int EPISODES_PAGE_ID;
    public static int OPTIONS_PAGE_ID;
    public static int ABOUT_PAGE_ID;
    
    public static int ABOUT_BTN_ID;
    public static int PLAY_BTN_ID;
    public static int OPTIONS_BTN_ID;
    public static int UNLOCK_BTN_ID;
    public static int LOGO_IMG_ID;
    public static int LIKE_BTN_ID;
    
    private int fpsValue = 0;
    
    private ISEFloatEventHandler unlockFullVersionHandler = null;
	
	public MBRenderManager(ISEFloatEventHandler unlockFullVersionHandler)
	{
		super();
		
		this.unlockFullVersionHandler = unlockFullVersionHandler;
	}
	
	@Override
	public void Init() 
	{
		MBGameManager gManager = MBManagers.getGameManager();
		// -----LOGO-PAGE-----
		this.mainMenu.addLogoPage(R.drawable.logo, 300.0f, 300.0f);
		this.mainMenu.addLogoPage(R.drawable.logo2, 480.0f, 320.0f);
		
		// -----MAIN-PAGE-----
		this.mainMenu.setActivePage(this.mainMenu.addPage(), true);
		this.mainMenu.setPageArrangement(E_SEMenuItemsArrangement.Custom);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_TOP, 0.2f);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_BOTTOM, -0.2f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_LEFT, -0.4f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_RIGHT, -0.4f);
        float xDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480.0f;
		float yDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320.0f;
		
        int HEADER_ID = this.mainMenu.addShiftingImage(1.75f, R.drawable.menu_main_header, 
				new SESimpleShiftingParams(new PointF(300.0f, 395.0f), new PointF(0.0f, -1.0f), 1.0f, 10.0f, 130.0f, xDensity, yDensity), 
				new SESimpleShiftingParams(new PointF(300.0f, 265.0f), new PointF(0.0f, 1.0f), 1.0f, 10.0f, 130.0f, xDensity, yDensity));
		this.mainMenu.setItemArrangement(HEADER_ID, 300.0f, 265.0f, 300.0f, 120.0f);
		//
		MBRenderManager.LIKE_BTN_ID = this.mainMenu.addButton(1.0f, R.drawable.like_btn, R.drawable.like_btn, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{
				Intent likeIntent = new Intent(android.content.Intent.ACTION_SEND);
	            likeIntent.setType("text/plain");
	            likeIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Motley Dream");
	            likeIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://market.android.com/details?id=com.spiritgames.motleydream&feature=search_result");
	            likeIntent.putExtra(android.content.Intent.EXTRA_TITLE, "Motley Dream");
	            likeIntent.putExtra(android.content.Intent.EXTRA_TEMPLATE, "https://market.android.com/details?id=com.spiritgames.motleydream&feature=search_result");
	            SEManagers.getActivity().startActivity(likeIntent);
			}
		}, 0.0f);
		this.mainMenu.setItemArrangement(MBRenderManager.LIKE_BTN_ID,
				75.0f, 260.0f, 100.0f, 50.0f);
		//
		MBRenderManager.PLAY_BTN_ID = this.mainMenu.addButton(4.5f, R.drawable.mm_play, R.drawable.mm_play_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{
				MBRenderManager.this.mainMenu.setActivePage(MBRenderManager.EPISODES_PAGE_ID, false);
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
			}
		}, 0.0f);
		this.mainMenu.setItemArrangement(MBRenderManager.PLAY_BTN_ID,
		320.0f, 160.0f, 250.0f, 55.0f);
		//
		MBRenderManager.OPTIONS_BTN_ID = this.mainMenu.addButton(4.5f, R.drawable.mm_options, R.drawable.mm_options_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{ 
				MBRenderManager.this.mainMenu.setActivePage(MBRenderManager.OPTIONS_PAGE_ID, false);
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
			}
		}, 0.0f);
		this.mainMenu.setItemArrangement(MBRenderManager.OPTIONS_BTN_ID,
		320.0f, 100.0f, 250.0f, 55.0f);
		//
		MBRenderManager.ABOUT_BTN_ID = this.mainMenu.addButton(4.5f, R.drawable.mm_about, R.drawable.mm_about_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{ 
				MBRenderManager.this.mainMenu.setActivePage(MBRenderManager.ABOUT_PAGE_ID, false);
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
			}
		}, 0.0f);
		this.mainMenu.setItemArrangement(MBRenderManager.ABOUT_BTN_ID,
		320.0f, 40.0f, 250.0f, 55.0f);
		//
		MBRenderManager.UNLOCK_BTN_ID = this.mainMenu.addButton(1.0f, R.drawable.mm_unlock, R.drawable.mm_unlock_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{
				if (MBRenderManager.this.unlockFullVersionHandler != null)
					MBRenderManager.this.unlockFullVersionHandler.onHandle(0.0f);
			}
		}, 0.0f);
		this.mainMenu.setItemArrangement(MBRenderManager.UNLOCK_BTN_ID,
		100.0f, 100.0f, 150.0f, 120.0f);
		//
		MBRenderManager.LOGO_IMG_ID = this.mainMenu.addAnimatedImage(1.0f, R.drawable.about_animation_001, 17, 75);
		this.mainMenu.setItemArrangement(MBRenderManager.LOGO_IMG_ID,
				100.0f, 100.0f, 120.0f, 120.0f);
		MBRenderManager.MAIN_PAGE_ID = this.mainMenu.getActivePage();
		// -----ABOUT---PAGE-----
		this.mainMenu.setActivePage(this.mainMenu.addPage(), true);
		this.mainMenu.setPageArrangement(E_SEMenuItemsArrangement.Custom);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_TOP, 0.2f);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_BOTTOM, -0.2f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_LEFT, -0.4f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_RIGHT, -0.4f);
        int ABOUT_HEADER_ID = this.mainMenu.addShiftingImage(1.11f, R.drawable.menu_about_header, 
				new SESimpleShiftingParams(new PointF(240.0f, 395.0f), new PointF(0.0f, -1.0f), 1.0f, 10.0f, 180.0f, xDensity, yDensity), 
				new SESimpleShiftingParams(new PointF(240.0f, 215.0f), new PointF(0.0f, 1.0f), 1.0f, 10.0f, 180.0f, xDensity, yDensity));
		this.mainMenu.setItemArrangement(ABOUT_HEADER_ID, 240.0f, 255.0f, 300.0f, 270.0f);		
		this.mainMenu.setItemArrangement(this.mainMenu.addButton(1.11f, 0, 0, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{
				Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"Spirit Games\""));
				SEManagers.getActivity().startActivity(browserIntent);        
			}
		}, 0.0f),
		240.0f, 215.0f, 300.0f, 270.0f);
		MBRenderManager.ABOUT_PAGE_ID = this.mainMenu.getActivePage();
		// -----OPTIONS-PAGE-----        
        this.options = new MBOptionsMenuPage(
	        new ISEFloatEventHandler() 
			{ 
				public void onHandle(float param) 
				{ 
					MBManagers.getGameManager().setNeedUpdateLevelsPage(true);
				}
			});
		this.mainMenu.setActivePage(this.mainMenu.addPage(this.options), true);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_LEFT, 0.15f);        
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_RIGHT, 0.15f);
        MBRenderManager.OPTIONS_PAGE_ID = this.mainMenu.getActivePage();
        this.options.LoadPrefs();
        // -----EPISODES-PAGE---
        this.episodesPage = new MBEpisodesMenuPage(
        		E_MBGameEpisodes.getEpisodesCount(), MBManagers.getGameManager().gameProgressInfo, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 		
			{ 
				MBRenderManager.this.dayLevels.RefreshLevels(MBRenderManager.this.options.getCurrentDifficultyLevel());
				MBRenderManager.this.eveningLevels.RefreshLevels(MBRenderManager.this.options.getCurrentDifficultyLevel());
				MBRenderManager.this.nightLevels.RefreshLevels(MBRenderManager.this.options.getCurrentDifficultyLevel());
				MBRenderManager.this.mainMenu.setActivePage((int)param, false);
			}
		});
        this.mainMenu.setActivePage(this.mainMenu.addPage(this.episodesPage), true);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_LEFT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_RIGHT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_TOP, 0.5f);        
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_BOTTOM, 0.1f);
        MBRenderManager.EPISODES_PAGE_ID = this.mainMenu.getActivePage();
        // -----------------------        
        // -----DAY-LEVELS-PAGE---
        this.dayLevels = new MBLevelsMenuPage(
        		MBGameManager.getLevelsCount(E_MBGameEpisodes.DAY), MBManagers.getGameManager().gameProgressInfo, 
        		E_MBGameEpisodes.DAY, E_MBDifficultyLevel.Normal, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 		
			{ 
				SEManagers.getActivity().setState(E_SEActivityState.InGame);
				MBManagers.getGameManager().loadLevel(E_MBGameEpisodes.DAY, param);
			}
		});      
		this.mainMenu.setActivePage(this.mainMenu.addPage(this.dayLevels), true);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_LEFT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_RIGHT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_TOP, 0.5f);        
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_BOTTOM, 0.1f);
        MBRenderManager.DAY_LEVELS_PAGE_ID = this.mainMenu.getActivePage();
        // ---EVENING-LEVELS-PAGE---
        this.eveningLevels = new MBLevelsMenuPage(
        		MBGameManager.getLevelsCount(E_MBGameEpisodes.EVENING), MBManagers.getGameManager().gameProgressInfo, 
        		E_MBGameEpisodes.EVENING, E_MBDifficultyLevel.Normal, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 		
			{ 
				SEManagers.getActivity().setState(E_SEActivityState.InGame);
				MBManagers.getGameManager().loadLevel(E_MBGameEpisodes.EVENING, param);
			}
		});      
		this.mainMenu.setActivePage(this.mainMenu.addPage(this.eveningLevels), true);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_LEFT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_RIGHT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_TOP, 0.5f);        
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_BOTTOM, 0.1f);
        MBRenderManager.EVENING_LEVELS_PAGE_ID = this.mainMenu.getActivePage();
        // ----------------------- 
        // ---NIGHT-LEVELS-PAGE---
        this.nightLevels = new MBLevelsMenuPage(
        		MBGameManager.getLevelsCount(E_MBGameEpisodes.NIGHT), MBManagers.getGameManager().gameProgressInfo, 
        		E_MBGameEpisodes.NIGHT, E_MBDifficultyLevel.Normal, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 		
			{ 
				SEManagers.getActivity().setState(E_SEActivityState.InGame);
				MBManagers.getGameManager().loadLevel(E_MBGameEpisodes.NIGHT, param);
			}
		});      
		this.mainMenu.setActivePage(this.mainMenu.addPage(this.nightLevels), true);
		this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_LEFT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_RIGHT, 0.15f);
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_TOP, 0.5f);        
        this.mainMenu.setPageOffset(SEMenuManager.PAGE_OFFSET_BOTTOM, 0.1f);
        MBRenderManager.NIGHT_LEVELS_PAGE_ID = this.mainMenu.getActivePage();
        // -----------------------        
        this.mainMenu.setActivePage(0, true);
		this.mainMenu.setCurrentBackground(this.mainMenu.addBackground(R.drawable.background));
		
		MBRenderManager.FONT_RAGE_ITALIC = this.addFont(R.drawable.font_rage_italic, R.xml.font_rage_italic);
		MBRenderManager.FONT_COMPLETE_INFO = this.addFont(R.drawable.font_complete_info, R.xml.font_complete_info);
		MBRenderManager.FONT_ARIAL_BLACK = this.addFont(R.drawable.font_arial_black, R.xml.font_arial_black);
	}
	
	private void CheckOptionsForSave()
	{
		if (this.needPrefsSave >= 2)
		{
			this.options.SavePrefs();
			this.needPrefsSave = 0;
		}
		
		if ((this.mainMenu.getActivePage() == MBRenderManager.OPTIONS_PAGE_ID) && (this.needPrefsSave < 1))
			this.needPrefsSave = 1;
		
		if ((this.mainMenu.getActivePage() != MBRenderManager.OPTIONS_PAGE_ID) && (this.needPrefsSave == 1))
			this.needPrefsSave = 2;	
	}
	
	@Override
	public void MenuRender() 
	{
		if (MBManagers.getGameManager() != null && this.mainMenu.isLogoCompleted())
		{
			MBGameManager gManager = MBManagers.getGameManager();
			
			MBManagers.getSoundManager().setSoundEnable(this.options.getSoundEnable());
			if (this.options.getSoundEnable())
				MBManagers.getSoundManager().enableMenuMusic();
			
			if (gManager.needUpdateLevelsPage())
			{
				this.dayLevels.RefreshLevels(this.options.getCurrentDifficultyLevel());
				this.eveningLevels.RefreshLevels(this.options.getCurrentDifficultyLevel());
				this.nightLevels.RefreshLevels(this.options.getCurrentDifficultyLevel());
			}
		}
		
		this.CheckOptionsForSave();
		
		if (this.mainMenu.getActivePage() == MBRenderManager.MAIN_PAGE_ID)
		{
			if (MBActivity.IsFullVersionUnlocked())
			{
				this.mainMenu.setItemVisible(MBRenderManager.LOGO_IMG_ID, true);
				this.mainMenu.setItemVisible(MBRenderManager.UNLOCK_BTN_ID, false);
			}
			else
			{
				this.mainMenu.setItemVisible(MBRenderManager.LOGO_IMG_ID, false);
				this.mainMenu.setItemVisible(MBRenderManager.UNLOCK_BTN_ID, true);
			}
		}
		
        this.mainMenu.Render(this.screenWidth, this.screenHeight);               
        
        if (SystemClock.uptimeMillis() - this.fpsUpdateTickCount > 500)
		{
        	this.fpsValue = this.getFPS();
			this.fpsUpdateTickCount = SystemClock.uptimeMillis();
		}
        //SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).RenderText(0.0f, 320.0f, 30.0f, Integer.toString(this.fpsValue), true);
        //Log.d("FPS", Integer.toString(this.fpsValue));
	}
	
	@Override
	public void GameRender() 
	{
		MBGameManager gManager = MBManagers.getGameManager();
		
		gManager.setGameSoundEnable(this.options.getSoundEnable());
		gManager.setGSensorEnable(this.options.getGSensorEnable());
		gManager.setGSensorSensitivity(this.options.getGSensorSensitivity());
		gManager.setVibrationEnable(this.options.getVibrationEnable());
		gManager.setDifficultyLevel(this.options.getCurrentDifficultyLevel());
		
		if (gManager.needUpdateLevelsPage())
		{
			this.dayLevels.RefreshLevels(this.options.getCurrentDifficultyLevel());
			this.eveningLevels.RefreshLevels(this.options.getCurrentDifficultyLevel());
			this.nightLevels.RefreshLevels(this.options.getCurrentDifficultyLevel());
		}
		
		gManager.Render(this.screenWidth, this.screenHeight);
				
		if (SystemClock.uptimeMillis() - this.fpsUpdateTickCount > 500)
		{
			this.fpsValue = this.getFPS();
			this.fpsUpdateTickCount = SystemClock.uptimeMillis();
		}
		//SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).RenderText(0.0f, 320.0f, 30.0f, Integer.toString(this.fpsValue), true);
		//Log.d("FPS", Integer.toString(this.fpsValue));
	}
}