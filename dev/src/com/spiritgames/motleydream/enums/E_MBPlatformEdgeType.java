package com.spiritgames.motleydream.enums;

public enum E_MBPlatformEdgeType 
{
	None(0),
    LeftOnly(1),
    RightOnly(2),
    All(3);
    
    private int index;
	
    E_MBPlatformEdgeType(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBPlatformEdgeType getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBPlatformEdgeType.None;
			case 1:
				return E_MBPlatformEdgeType.LeftOnly;
			case 2:
				return E_MBPlatformEdgeType.RightOnly;
			case 3:
				return E_MBPlatformEdgeType.All;
			default:
				return E_MBPlatformEdgeType.All;
		}		
	}
}
