package com.spiritgames.motleydream.enums;

public enum E_MBOrientation 
{
	Unknown(-1),
	Vertical(0),
    Horizontal(1);
	
	private int index;
		
	E_MBOrientation(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBOrientation getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBOrientation.Vertical;
			case 1:
				return E_MBOrientation.Horizontal;
			default:
				return E_MBOrientation.Unknown;
		}		
	}
}
