package com.spiritgames.motleydream.enums;

public enum E_MBHorizontalCameraPosition 
{
	Left(0),
	Right(1),
	Center(2),
	Free(3);
 
    private int index;
	
    E_MBHorizontalCameraPosition(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBHorizontalCameraPosition getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBHorizontalCameraPosition.Left;
			case 1:
				return E_MBHorizontalCameraPosition.Right;
			case 2:
				return E_MBHorizontalCameraPosition.Center;
			case 3:
				return E_MBHorizontalCameraPosition.Free;
			default:
				return E_MBHorizontalCameraPosition.Free;
		}		
	}
}
