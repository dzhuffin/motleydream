package com.spiritgames.motleydream.enums;

public enum E_MBGameEpisodes
{
	DAY(0),
    NIGHT(1),
    EVENING(2);
    
    private int index;
	
    E_MBGameEpisodes(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBGameEpisodes getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBGameEpisodes.DAY;
			case 1:
				return E_MBGameEpisodes.NIGHT;
			case 2:
				return E_MBGameEpisodes.EVENING;
			default:
				return E_MBGameEpisodes.DAY;
		}		
	}
	
	public static int[] getAllIndexes()
	{
		return new int[] {0, 1, 2};	
	}
	
	public static int getEpisodesCount()
	{
		return 3;
	}
}
