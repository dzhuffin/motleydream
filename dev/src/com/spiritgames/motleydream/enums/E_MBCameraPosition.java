package com.spiritgames.motleydream.enums;

import com.spiritgames.motleydream.MBManagers;

import android.graphics.PointF;

public enum E_MBCameraPosition 
{
	MiddleTop(0),
	MiddleBottom(1),
	MiddleCenter(2),
	LeftTop(3),
    LeftBottom(4),
    LeftCenter(5),
    RightTop(6),
    RightBottom(7),
    RightCenter(8);
 
    private int index;
	
    E_MBCameraPosition(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBCameraPosition getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBCameraPosition.MiddleTop;
			case 1:
				return E_MBCameraPosition.MiddleBottom;
			case 2:
				return E_MBCameraPosition.MiddleCenter;
			case 3:
				return E_MBCameraPosition.LeftTop;
			case 4:
				return E_MBCameraPosition.LeftBottom;
			case 5:
				return E_MBCameraPosition.LeftCenter;
			case 6:
				return E_MBCameraPosition.RightTop;
			case 7:
				return E_MBCameraPosition.RightBottom;
			case 8:
				return E_MBCameraPosition.RightCenter;
			default:
				return E_MBCameraPosition.MiddleCenter;
		}		
	}
	
	public PointF getOffset()
	{
		float wOffset = 220.0f;
		float hOffset = 145.0f;
		
		if (this == E_MBCameraPosition.MiddleTop)
			return new PointF(0.0f, hOffset);
		if (this == E_MBCameraPosition.MiddleBottom)
			return new PointF(0.0f, -hOffset);
		if (this == E_MBCameraPosition.MiddleCenter)
			return new PointF(0.0f, 0.0f);
		if (this == E_MBCameraPosition.LeftTop)
			return new PointF(wOffset, hOffset);
		if (this == E_MBCameraPosition.LeftBottom)
			return new PointF(wOffset, -hOffset);
		if (this == E_MBCameraPosition.LeftCenter)
			return new PointF(wOffset, 0.0f);
		if (this == E_MBCameraPosition.RightTop)
			return new PointF(-wOffset, hOffset);
		if (this == E_MBCameraPosition.RightBottom)
			return new PointF(-wOffset, -hOffset);
		if (this == E_MBCameraPosition.RightCenter)
			return new PointF(-wOffset, 0.0f);

		return new PointF(0.0f, 0.0f);	
	}
	
	public E_MBCameraPosition getSwitchedToLeft()
	{
		if (this == E_MBCameraPosition.MiddleTop)
			return E_MBCameraPosition.LeftTop;
		if (this == E_MBCameraPosition.MiddleBottom)
			return E_MBCameraPosition.LeftBottom;
		if (this == E_MBCameraPosition.MiddleCenter)
			return E_MBCameraPosition.LeftCenter;
		if (this == E_MBCameraPosition.LeftTop)
			return E_MBCameraPosition.LeftTop;
		if (this == E_MBCameraPosition.LeftBottom)
			return E_MBCameraPosition.LeftBottom;
		if (this == E_MBCameraPosition.LeftCenter)
			return E_MBCameraPosition.LeftCenter;
		if (this == E_MBCameraPosition.RightTop)
			return E_MBCameraPosition.LeftTop;
		if (this == E_MBCameraPosition.RightBottom)
			return E_MBCameraPosition.LeftBottom;
		if (this == E_MBCameraPosition.RightCenter)
			return E_MBCameraPosition.LeftCenter;

		return E_MBCameraPosition.LeftCenter;	
	}
	
	public E_MBCameraPosition getSwitchedToRight()
	{
		if (this == E_MBCameraPosition.MiddleTop)
			return E_MBCameraPosition.RightTop;
		if (this == E_MBCameraPosition.MiddleBottom)
			return E_MBCameraPosition.RightBottom;
		if (this == E_MBCameraPosition.MiddleCenter)
			return E_MBCameraPosition.RightCenter;
		if (this == E_MBCameraPosition.LeftTop)
			return E_MBCameraPosition.RightTop;
		if (this == E_MBCameraPosition.LeftBottom)
			return E_MBCameraPosition.RightBottom;
		if (this == E_MBCameraPosition.LeftCenter)
			return E_MBCameraPosition.RightCenter;
		if (this == E_MBCameraPosition.RightTop)
			return E_MBCameraPosition.RightTop;
		if (this == E_MBCameraPosition.RightBottom)
			return E_MBCameraPosition.RightBottom;
		if (this == E_MBCameraPosition.RightCenter)
			return E_MBCameraPosition.RightCenter;

		return E_MBCameraPosition.RightCenter;	
	}
	
	public E_MBCameraPosition getSwitchedToMiddle()
	{
		if (this == E_MBCameraPosition.MiddleTop)
			return E_MBCameraPosition.MiddleTop;
		if (this == E_MBCameraPosition.MiddleBottom)
			return E_MBCameraPosition.MiddleBottom;
		if (this == E_MBCameraPosition.MiddleCenter)
			return E_MBCameraPosition.MiddleCenter;
		if (this == E_MBCameraPosition.LeftTop)
			return E_MBCameraPosition.MiddleTop;
		if (this == E_MBCameraPosition.LeftBottom)
			return E_MBCameraPosition.MiddleBottom;
		if (this == E_MBCameraPosition.LeftCenter)
			return E_MBCameraPosition.MiddleCenter;
		if (this == E_MBCameraPosition.RightTop)
			return E_MBCameraPosition.MiddleTop;
		if (this == E_MBCameraPosition.RightBottom)
			return E_MBCameraPosition.MiddleBottom;
		if (this == E_MBCameraPosition.RightCenter)
			return E_MBCameraPosition.MiddleCenter;

		return E_MBCameraPosition.MiddleCenter;	
	}
	
	public E_MBCameraPosition getSwitchedToTop()
	{
		if (this == E_MBCameraPosition.MiddleTop)
			return E_MBCameraPosition.MiddleTop;
		if (this == E_MBCameraPosition.MiddleBottom)
			return E_MBCameraPosition.MiddleTop;
		if (this == E_MBCameraPosition.MiddleCenter)
			return E_MBCameraPosition.MiddleTop;
		if (this == E_MBCameraPosition.LeftTop)
			return E_MBCameraPosition.LeftTop;
		if (this == E_MBCameraPosition.LeftBottom)
			return E_MBCameraPosition.LeftTop;
		if (this == E_MBCameraPosition.LeftCenter)
			return E_MBCameraPosition.LeftTop;
		if (this == E_MBCameraPosition.RightTop)
			return E_MBCameraPosition.RightTop;
		if (this == E_MBCameraPosition.RightBottom)
			return E_MBCameraPosition.RightTop;
		if (this == E_MBCameraPosition.RightCenter)
			return E_MBCameraPosition.RightTop;

		return E_MBCameraPosition.MiddleTop;	
	}
	
	public E_MBCameraPosition getSwitchedToBottom()
	{
		if (this == E_MBCameraPosition.MiddleTop)
			return E_MBCameraPosition.MiddleBottom;
		if (this == E_MBCameraPosition.MiddleBottom)
			return E_MBCameraPosition.MiddleBottom;
		if (this == E_MBCameraPosition.MiddleCenter)
			return E_MBCameraPosition.MiddleBottom;
		if (this == E_MBCameraPosition.LeftTop)
			return E_MBCameraPosition.LeftBottom;
		if (this == E_MBCameraPosition.LeftBottom)
			return E_MBCameraPosition.LeftBottom;
		if (this == E_MBCameraPosition.LeftCenter)
			return E_MBCameraPosition.LeftBottom;
		if (this == E_MBCameraPosition.RightTop)
			return E_MBCameraPosition.RightBottom;
		if (this == E_MBCameraPosition.RightBottom)
			return E_MBCameraPosition.RightBottom;
		if (this == E_MBCameraPosition.RightCenter)
			return E_MBCameraPosition.RightBottom;

		return E_MBCameraPosition.MiddleBottom;	
	}
	
	public E_MBCameraPosition getSwitchedToCenter()
	{
		if (this == E_MBCameraPosition.MiddleTop)
			return E_MBCameraPosition.MiddleCenter;
		if (this == E_MBCameraPosition.MiddleBottom)
			return E_MBCameraPosition.MiddleCenter;
		if (this == E_MBCameraPosition.MiddleCenter)
			return E_MBCameraPosition.MiddleCenter;
		if (this == E_MBCameraPosition.LeftTop)
			return E_MBCameraPosition.LeftCenter;
		if (this == E_MBCameraPosition.LeftBottom)
			return E_MBCameraPosition.LeftCenter;
		if (this == E_MBCameraPosition.LeftCenter)
			return E_MBCameraPosition.LeftCenter;
		if (this == E_MBCameraPosition.RightTop)
			return E_MBCameraPosition.RightCenter;
		if (this == E_MBCameraPosition.RightBottom)
			return E_MBCameraPosition.RightCenter;
		if (this == E_MBCameraPosition.RightCenter)
			return E_MBCameraPosition.RightCenter;

		return E_MBCameraPosition.MiddleCenter;	
	}
}
