package com.spiritgames.motleydream.enums;

public enum E_MBObjectTypes 
{
	Unknown(-1),
	Ball(0),
    Basket(1),
    ColorChanger(2),
    Platform(3),
    MovingPlatform(4),
    Target(5),
	FreeBox(6),
	FreeBall(7),
	Star(8),
	MegaJump(9),
	Tip(10);
    
    private int index;
	
	E_MBObjectTypes(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBObjectTypes getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBObjectTypes.Ball;
			case 1:
				return E_MBObjectTypes.Basket;
			case 2:
				return E_MBObjectTypes.ColorChanger;
			case 3:
				return E_MBObjectTypes.Platform;
			case 4:
				return E_MBObjectTypes.MovingPlatform;
			case 5:
				return E_MBObjectTypes.Target;
			case 6:
				return E_MBObjectTypes.FreeBox;
			case 7:
				return E_MBObjectTypes.FreeBall;
			case 8:
				return E_MBObjectTypes.Star;
			case 9:
				return E_MBObjectTypes.MegaJump;
			case 10:
				return E_MBObjectTypes.Tip;
			default:
				return E_MBObjectTypes.Unknown;
		}		
	}
}
