package com.spiritgames.motleydream.enums;

public enum E_MBObjectColors 
{
	Unknown(-1),
	Floor(0),
    Usual(1),
    Red(2),
    Green(3),
    Blue(4),
    Yellow(5),
    LeftWall(6),
    RightWall(7),
    Roof(8);
    
    private int index;
	
    E_MBObjectColors(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBObjectColors getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBObjectColors.Floor;
			case 1:
				return E_MBObjectColors.Usual;
			case 2:
				return E_MBObjectColors.Red;
			case 3:
				return E_MBObjectColors.Green;
			case 4:
				return E_MBObjectColors.Blue;
			case 5:
				return E_MBObjectColors.Yellow;
			case 6:
				return E_MBObjectColors.LeftWall;
			case 7:
				return E_MBObjectColors.RightWall;
			case 8:
				return E_MBObjectColors.Roof;
			default:
				return E_MBObjectColors.Unknown;
		}		
	}
}
