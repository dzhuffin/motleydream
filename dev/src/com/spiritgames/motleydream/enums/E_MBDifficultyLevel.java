package com.spiritgames.motleydream.enums;

public enum E_MBDifficultyLevel 
{
	Normal(0),
    Hardcore(1);
    
    private int index;
	
    E_MBDifficultyLevel(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBDifficultyLevel getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBDifficultyLevel.Normal;
			case 1:
				return E_MBDifficultyLevel.Hardcore;
			default:
				return E_MBDifficultyLevel.Normal;
		}		
	}
}
