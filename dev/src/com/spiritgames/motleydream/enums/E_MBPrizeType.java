package com.spiritgames.motleydream.enums;

public enum E_MBPrizeType 
{
	Unknown(-1),
	Star(1),
    MegaJump(2);
    
    private int index;
	
    E_MBPrizeType(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBPrizeType getByIndex(int i)
	{
		switch(i)
		{			
			case 1:
				return E_MBPrizeType.Star;
			case 2:
				return E_MBPrizeType.MegaJump;
			default:
				return E_MBPrizeType.Unknown;
		}		
	}
}
