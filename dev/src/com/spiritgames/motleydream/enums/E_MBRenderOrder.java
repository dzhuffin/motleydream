package com.spiritgames.motleydream.enums;

public enum E_MBRenderOrder 
{
	ColorChanger(0),
	Prize(1),	
	FreeBox(2),
    Target(3),
    Ball(4), 
    RotatingFreeBox(5),
    VerticalPlatform(6),
    HorizontalPlatform(7),
    VerticalMovingPlatform(8),
    HorizontalMovingPlatform(9),    
    Wall(10),
    Floor(11),
    Roof(12);
    
    private int index;
	
    E_MBRenderOrder(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBRenderOrder getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBRenderOrder.ColorChanger;
			case 1:
				return E_MBRenderOrder.Prize;
			case 2:
				return E_MBRenderOrder.FreeBox;
			case 3:
				return E_MBRenderOrder.RotatingFreeBox;
			case 4:
				return E_MBRenderOrder.Target;
			case 5:
				return E_MBRenderOrder.Ball;
			case 6:
				return E_MBRenderOrder.VerticalPlatform;
			case 7:
				return E_MBRenderOrder.HorizontalPlatform;
			case 8:
				return E_MBRenderOrder.VerticalMovingPlatform;
			case 9:
				return E_MBRenderOrder.HorizontalMovingPlatform;
			case 10:
				return E_MBRenderOrder.Wall;
			case 11:
				return E_MBRenderOrder.Floor;
			case 12:
				return E_MBRenderOrder.Roof;
			default:
				return E_MBRenderOrder.ColorChanger;
		}		
	}
}
