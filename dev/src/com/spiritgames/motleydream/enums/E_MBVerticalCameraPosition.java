package com.spiritgames.motleydream.enums;

public enum E_MBVerticalCameraPosition 
{
	Top(0),
	Bottom(1),
	Center(2),
	Free(3);
 
    private int index;
	
    E_MBVerticalCameraPosition(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBVerticalCameraPosition getByIndex(int i)
	{
		switch(i)
		{			
			case 0:
				return E_MBVerticalCameraPosition.Top;
			case 1:
				return E_MBVerticalCameraPosition.Bottom;
			case 2:
				return E_MBVerticalCameraPosition.Center;
			case 3:
				return E_MBVerticalCameraPosition.Free;
			default:
				return E_MBVerticalCameraPosition.Free;
		}		
	}
}
