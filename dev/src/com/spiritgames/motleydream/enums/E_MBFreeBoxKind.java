package com.spiritgames.motleydream.enums;

public enum E_MBFreeBoxKind 
{
	Unknown(-1),
	Ball(1),
    Box(2),
    Tip(3);
    
    private int index;
	
    E_MBFreeBoxKind(int m)
	{
		this.index = m;
	}
	
	public int getIndex() 
	{
		return this.index;
	}
	
	public static E_MBFreeBoxKind getByIndex(int i)
	{
		switch(i)
		{			
			case 1:
				return E_MBFreeBoxKind.Ball;
			case 2:
				return E_MBFreeBoxKind.Box;
			case 3:
				return E_MBFreeBoxKind.Tip;
			default:
				return E_MBFreeBoxKind.Unknown;
		}		
	}
}
