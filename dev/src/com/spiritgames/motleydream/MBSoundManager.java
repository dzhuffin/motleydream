package com.spiritgames.motleydream;

import com.spiritgames.spiritengine.sound.SESoundManager;

public class MBSoundManager extends SESoundManager 
{
	public static int SOUND_JUMP = 0;
	public static int SOUND_COLOR_CHANGE = 1;
	public static int SOUND_BALL_CONTACT = 2;
	public static int SOUND_MENU_PRESS = 3;
	public static int SOUND_BALL_DEAD = 4;
	public static int SOUND_BALL_FINISH = 5;
	public static int SOUND_BALL_PRIZE = 6;
	
	public static int MUSIC_GAMEPLAY = 7;
	public static int MUSIC_MENU = 8;

	private int currentMusic = 0;	
	public int getCurrentMusic() {
		return this.currentMusic;
	}

	public MBSoundManager()
	{
		super(20);
		
		this.addSound(MBSoundManager.SOUND_JUMP, R.raw.snd_jump, 0);
		this.addSound(MBSoundManager.SOUND_COLOR_CHANGE, R.raw.snd_color_change, 0);
		this.addSound(MBSoundManager.SOUND_BALL_CONTACT, R.raw.snd_ball_contact, 0);
		this.addSound(MBSoundManager.SOUND_MENU_PRESS, R.raw.snd_mm_click, 0);
		this.addSound(MBSoundManager.SOUND_BALL_DEAD, R.raw.snd_dead, 0);
		this.addSound(MBSoundManager.SOUND_BALL_FINISH, R.raw.snd_finish, 0);
		this.addSound(MBSoundManager.SOUND_BALL_PRIZE, R.raw.snd_prize, 0);
		
		this.addBackgroundMusic(MBSoundManager.MUSIC_GAMEPLAY, new String("music_gameplay.ogg"));
		this.addBackgroundMusic(MBSoundManager.MUSIC_MENU, new String("music_menu.ogg"));
	}

	public void enableMenuMusic()
	{
		if (this.currentMusic != MBSoundManager.MUSIC_MENU)
		{
			this.stopBackgroundMusic();
			this.playBackgroundMusic(MBSoundManager.MUSIC_MENU);
			this.currentMusic = MBSoundManager.MUSIC_MENU;
		}
	}
	
	public void enableGameplayMusic()
	{
		//if (this.currentMusic != MBSoundManager.MUSIC_GAMEPLAY && !this.mediaPlayer.isPlaying())
		{
			this.stopBackgroundMusic();
			this.playBackgroundMusic(MBSoundManager.MUSIC_GAMEPLAY);
			this.currentMusic = MBSoundManager.MUSIC_GAMEPLAY;
		}
	}
}
