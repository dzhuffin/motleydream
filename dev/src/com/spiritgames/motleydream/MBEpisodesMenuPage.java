package com.spiritgames.motleydream;

import android.graphics.PointF;

import com.spiritgames.motleydream.level.MBGameProgressInfoDB;
import com.spiritgames.spiritengine.ISEFloatEventHandler;
import com.spiritgames.spiritengine.graphics.SESimpleShiftingParams;
import com.spiritgames.spiritengine.menu.ISEMenuItem;
import com.spiritgames.spiritengine.menu.SEKineticMenuPage;
import com.spiritgames.spiritengine.ui.SEButton;

public class MBEpisodesMenuPage extends SEKineticMenuPage
{
	private MBGameProgressInfoDB progressInfo;
	
	public MBEpisodesMenuPage(int buttonsCount, MBGameProgressInfoDB progressInfo,
			ISEFloatEventHandler loadLevel) 
	{
		super(buttonsCount, loadLevel, 2.0f);
		
		this.progressInfo = progressInfo;
		//this.progressInfo.loadFromAppData("progress");
		
		float xDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480.0f;
		float yDensity = (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320.0f;
		
		int HEADER_ID = this.addShiftingImage(1.75f, R.drawable.menu_episodes_header, 
				new SESimpleShiftingParams(new PointF(240.0f, 395.0f), new PointF(0.0f, -1.0f), 1.0f, 10.0f, 140.0f, xDensity, yDensity), 
				new SESimpleShiftingParams(new PointF(240.0f, 260.0f), new PointF(0.0f, 1.0f), 1.0f, 10.0f, 135.0f, xDensity, yDensity));
		this.setItemSize(HEADER_ID, 230.0f, 130.0f);
		
		int IMG_ID = this.addAnimatedImage(1.0f, R.drawable.changer_blue_1, 6, 100);
		this.setItemPos(IMG_ID, 60.0f, 255.0f);
		this.setItemSize(IMG_ID, 50.0f, 50.0f);
		
		IMG_ID = this.addAnimatedImage(1.0f, R.drawable.changer_red_1, 6, 100);
		this.setItemPos(IMG_ID, 420.0f, 255.0f);
		this.setItemSize(IMG_ID, 50.0f, 50.0f);

		this.setItemEnable(this.addButton(1.0f, R.drawable.episode_day_icon_border, R.drawable.episode_day_icon_border_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{ 
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
				MBEpisodesMenuPage.this.onClick.onHandle(MBRenderManager.DAY_LEVELS_PAGE_ID);
			}
		}, 1), true);
		
		this.setItemEnable(this.addButton(1.0f, R.drawable.episode_evening_icon_border, R.drawable.episode_evening_icon_border_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{ 
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
				MBEpisodesMenuPage.this.onClick.onHandle(MBRenderManager.EVENING_LEVELS_PAGE_ID);
			}
		}, 2), true);
		
		this.setItemEnable(this.addButton(1.0f, R.drawable.episode_night_icon_border, R.drawable.episode_night_icon_border_press, 0, new ISEFloatEventHandler() 
		{ 
			public void onHandle(float param) 
			{ 
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_MENU_PRESS);
				MBEpisodesMenuPage.this.onClick.onHandle(MBRenderManager.NIGHT_LEVELS_PAGE_ID);
			}
		}, 3), true);
	}

	public void RefreshEpisodes(int reachedEpisode)
	{		
		for (int j = 0, i = 0; j < this.items.size(); j++)
		{
			if (this.items.get(j).getClass() == SEButton.class)
			{
				this.setItemEnable(j, ((i + 1) <= reachedEpisode));			
				i++;
			}
		}
		
		//this.progressInfo.loadFromAppData("progress");
	}
	
	@Override
	public void Render(float screenW, float screenH)
	{		
		for (int j = 0, i = 0; j < this.items.size(); j++)
		{
			if (this.items.get(j).getClass() != SEButton.class)
			{
				((ISEMenuItem)this.items.get(j)).Render();
				continue;
			}
			
			ISEMenuItem item = ((ISEMenuItem)this.items.get(j));
			
			float cX = (1.2f * item.getWidth() / 2.0f) + (1.2f * item.getWidth() * i) + this.scrollPos;
			float cY = this.pageRect.centerY();
			item.setPos(cX, cY);		

			i++;
		}
		
		super.Render(screenW, screenH);
	}
}
