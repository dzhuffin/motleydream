package com.spiritgames.motleydream;

import android.content.Context;

import com.spiritgames.spiritengine.SEActivity;
import com.spiritgames.spiritengine.SEGameManager;
import com.spiritgames.spiritengine.SEManagers;

public class MBManagers extends SEManagers
{
	public static MBActivity getActivity()
	{
		return (MBActivity)SEManagers.activity;
	}
	
	public static Context getApplicationContext()
	{
		return SEManagers.activity.getApplicationContext();
	}
	
	public static void setActivity(MBActivity val)
	{
		SEManagers.activity = val;
	}
	
	public static MBRenderManager getRenderManager()
	{
		return (MBRenderManager)SEManagers.sceneRenderer;
	}
	
	public static MBGameManager getGameManager()
	{
		return (MBGameManager)SEManagers.gameManager;
	}
	
	public static MBSoundManager getSoundManager()
	{
		return (MBSoundManager)SEManagers.soundManager;
	}
	
	public static void setSoundManager(MBSoundManager val)
	{
		SEManagers.soundManager = val;
	}
}
