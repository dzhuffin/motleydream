package com.spiritgames.motleydream.level;

import java.util.Vector;

import android.os.SystemClock;
import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.spiritgames.motleydream.enums.E_MBHorizontalCameraPosition;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBOrientation;
import com.spiritgames.motleydream.enums.E_MBVerticalCameraPosition;

public class MBContactListenerNative implements ContactListener 
{
	private Body ballBody;
	private Body targetBody;
	
	private Vector<MBContactType> jumpContacts = new Vector<MBContactType>();
	private Vector<MBContactType> otherContacts = new Vector<MBContactType>();
	private MBContactType ballContactType = new MBContactType(null, E_MBObjectTypes.Ball, E_MBObjectColors.Red);
	
	private E_MBVerticalCameraPosition currentVCameraPosition = E_MBVerticalCameraPosition.Center;	
	public E_MBVerticalCameraPosition getCurrentVCameraPosition() {
		return currentVCameraPosition;
	}
	private E_MBHorizontalCameraPosition currentHCameraPosition = E_MBHorizontalCameraPosition.Free;	
	public E_MBHorizontalCameraPosition getCurrentHCameraPosition() {
		return currentHCameraPosition;
	}

	private MBColoredItem lastNotBallObject = null;
	public MBColoredItem getLastNotBallObject() {
		return lastNotBallObject;
	}
	private MBColoredItem lastStarCollected = null;
	private MBColoredItem lastMegaJumpCollected = null;
	public MBColoredItem getLastStarCollected()
	{
		MBColoredItem result = this.lastStarCollected;
		this.lastStarCollected = null;
		return result;
	}
	public MBColoredItem getLastMegaJumpCollected()
	{
		MBColoredItem result = this.lastMegaJumpCollected;
		this.lastMegaJumpCollected = null;
		return result;
	}
	
	private boolean finished = false;
	private boolean colorChanged = false;
	private boolean ballContacted = false;
	private long ballContactDelay = 100;
	private long ballContactTick = 0;
	
	public void newStep()
	{
		this.finished = false;
		this.colorChanged = false;
		this.ballContacted = false;
	}
	
	private long canJumpTick = 0;
	public boolean canJump()
	{
		if (this.canJumpTick != 0 && SystemClock.uptimeMillis() - this.canJumpTick < 200)
			return false;
		
		if (this.canJumpTick == 0 && this.jumpContacts.size() > 0)
		{
			this.canJumpTick = SystemClock.uptimeMillis();
			return true;
		}
		else
			this.canJumpTick = 0;	
		
		return false;
	}
	
	public boolean isFinished()
	{
		boolean result = this.finished;
		this.finished = false;
		return result;
	}
	
	public boolean isBallContacted()
	{
		boolean result = this.ballContacted;
		this.ballContacted = false;
		return result;
	}
	
	public boolean isColorChanged()
	{
		boolean result = this.colorChanged;
		this.colorChanged = false;
		return result;
	}
	
	public boolean isDead()
	{
		MBContactType ballUD = (MBContactType)this.ballBody.getUserData();
		
		for (int i = 0; i < this.jumpContacts.size(); i++)
			if (this.isColorConflict(ballUD, ((MBContactType)this.jumpContacts.get(i))))
				return true;
		
		for (int i = 0; i < this.otherContacts.size(); i++)
			if (this.isColorConflict(ballUD, ((MBContactType)this.otherContacts.get(i))))
				return true;
		
		return false;
	}
	
	public void restart()
	{
		this.finished = false;
	}
	
	public void setBallBody(Body ballShape)
	{
		this.ballBody = ballShape;
	}
	
	public void setTargetBody(Body targetShape)
	{
		this.targetBody = targetShape;
	}
	
	private boolean isBall(MBContactType ud)
	{
		return (ud.getType() == E_MBObjectTypes.Ball);
	}
	
	private boolean isTarget(MBContactType ud)
	{
		return (ud.getType() == E_MBObjectTypes.Target);
	}
	
	private boolean isColorChanger(MBContactType ud)
	{
		return (ud.getType() == E_MBObjectTypes.ColorChanger);
	}
	
	private boolean isStar(MBContactType ud)
	{
		return (ud.getType() == E_MBObjectTypes.Star);
	}
	
	private boolean isMegaJump(MBContactType ud)
	{
		return (ud.getType() == E_MBObjectTypes.MegaJump);
	}
	
	private boolean isColorConflict(MBContactType ud1, MBContactType ud2)
	{
		return ((ud1.getColor() != ud2.getColor()) &&
				(ud1.getColor() != E_MBObjectColors.Usual) &&
				(ud2.getColor() != E_MBObjectColors.Usual) &&
				(ud1.getColor() != E_MBObjectColors.LeftWall) &&
				(ud2.getColor() != E_MBObjectColors.LeftWall) &&
				(ud1.getColor() != E_MBObjectColors.RightWall) &&
				(ud2.getColor() != E_MBObjectColors.RightWall) &&
				(ud1.getColor() != E_MBObjectColors.Roof) &&
				(ud2.getColor() != E_MBObjectColors.Roof) &&
				(ud1.getColor() != E_MBObjectColors.Unknown) &&
				(ud2.getColor() != E_MBObjectColors.Unknown));
	}

	@Override
	public void beginContact(Contact point) 
	{		
		MBContactType ud1 = (point.getFixtureA().getBody().getUserData().getClass() == MBContactType.class) ? (MBContactType)point.getFixtureA().getBody().getUserData() : null;
		MBContactType ud2 = (point.getFixtureB().getBody().getUserData().getClass() == MBContactType.class) ? (MBContactType)point.getFixtureB().getBody().getUserData() : null;
		MBContactType notBallUD;
		
		Vector2 normal = point.GetWorldManifold().getNormal();
		
		if ((ud1 != null) && (ud2 != null))
		{
			if ((this.isBall(ud1) && ((ud2.getType() == E_MBObjectTypes.Platform) || (ud2.getType() == E_MBObjectTypes.MovingPlatform) || (ud2.getType() == E_MBObjectTypes.FreeBox) || (ud2.getType() == E_MBObjectTypes.FreeBall))) ||
				(this.isBall(ud2) && ((ud1.getType() == E_MBObjectTypes.Platform) || (ud1.getType() == E_MBObjectTypes.MovingPlatform) || (ud1.getType() == E_MBObjectTypes.FreeBox) || (ud1.getType() == E_MBObjectTypes.FreeBall))))
			{
				notBallUD = null;
				
				if (ud1.getType() == E_MBObjectTypes.Ball)
					notBallUD = ud2;
				
				if (ud2.getType() == E_MBObjectTypes.Ball)
					notBallUD = ud1;
				
				this.lastNotBallObject = notBallUD.getItemLink();
					
				if (notBallUD != null)
				{
					this.currentVCameraPosition = notBallUD.getVCameraPosition();
					this.currentHCameraPosition = notBallUD.getHCameraPosition();
				}
				
				boolean normalCond;
				
				if ((ud1.getType() == E_MBObjectTypes.FreeBall) ||
					(ud2.getType() == E_MBObjectTypes.FreeBall))
					normalCond = (normal.y >= 0.5f);
				else
					normalCond = (normal.y <= -0.5f);
				
				if (normalCond)
				{
					if (this.isBall(ud1))
						this.jumpContacts.add(ud2);
					else
						this.jumpContacts.add(ud1);
				}
				else
				{
					if (this.isBall(ud1))
						this.otherContacts.add(ud2);
					else
						this.otherContacts.add(ud1);
				}
			}
			
			if (this.isBall(ud1) || (this.isBall(ud2)))		
			{
				if (SystemClock.uptimeMillis() - this.ballContactTick > this.ballContactDelay)
				{
					this.ballContacted = true;
					this.ballContactTick = SystemClock.uptimeMillis();
				}
			}
			
			if (this.isBall(ud1) && this.isColorChanger(ud2))
			{
				this.ballBody.setUserData(new MBContactType(ud1.getItemLink(), ud1.getType(), ud2.getColor()));
				this.colorChanged = this.isColorConflict(ud1, ud2);
			}
			
			if (this.isBall(ud2) && this.isColorChanger(ud1))
			{
				this.ballBody.setUserData(new MBContactType(ud2.getItemLink(), ud2.getType(), ud1.getColor()));
				this.colorChanged = this.isColorConflict(ud1, ud2);
			}
			
			if (this.isBall(ud1) && this.isStar(ud2))
			{
				this.lastStarCollected = ud2.getItemLink();
			}
			
			if (this.isBall(ud2) && this.isStar(ud1))
			{
				this.lastStarCollected = ud1.getItemLink();
			}
			
			if (this.isBall(ud1) && this.isMegaJump(ud2))
			{
				this.lastMegaJumpCollected = ud2.getItemLink();
			}
			
			if (this.isBall(ud2) && this.isMegaJump(ud1))
			{
				this.lastMegaJumpCollected = ud1.getItemLink();
			}
			
			this.finished = ((this.isBall(ud1) && this.isTarget(ud2)) || (this.isBall(ud2) && this.isTarget(ud1)));
		}
	}
	@Override
	public void endContact(Contact point) 
	{
		if ((point.getFixtureA().getBody() == null) || (point.getFixtureB().getBody() == null) ||
			(point.getFixtureA().getBody().getUserData() == null) || (point.getFixtureB().getBody().getUserData() == null))
				return;
		
		MBContactType ud1 = (point.getFixtureA().getBody().getUserData().getClass() == MBContactType.class) ? (MBContactType)point.getFixtureA().getBody().getUserData() : null;
		MBContactType ud2 = (point.getFixtureB().getBody().getUserData().getClass() == MBContactType.class) ? (MBContactType)point.getFixtureB().getBody().getUserData() : null;		
		
		if ((ud1 != null) && (ud2 != null))
		{
			for (int i = 0; i < this.jumpContacts.size(); i++)
			{
				if ((this.isBall(ud1)) && (this.jumpContacts.get(i).equals(ud2)))
				{
					this.jumpContacts.removeElementAt(i);
					break;
				}
				
				if ((this.isBall(ud2)) && (this.jumpContacts.get(i).equals(ud1)))
				{
					this.jumpContacts.removeElementAt(i);
					break;
				}
			}
			
			for (int i = 0; i < this.otherContacts.size(); i++)
			{
				if ((this.isBall(ud1)) && (this.otherContacts.get(i).equals(ud2)))
				{
					this.otherContacts.removeElementAt(i);
					break;
				}
				
				if ((this.isBall(ud2)) && (this.otherContacts.get(i).equals(ud1)))
				{
					this.otherContacts.removeElementAt(i);
					break;
				}
			}
		}
	}
}
