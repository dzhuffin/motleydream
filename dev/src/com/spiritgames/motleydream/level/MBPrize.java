package com.spiritgames.motleydream.level;

import android.content.Context;
import android.os.SystemClock;

import com.spiritgames.motleydream.R;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBPrizeType;
import com.spiritgames.motleydream.enums.E_MBRenderOrder;
import com.spiritgames.spiritengine.graphics.SEAnimatedSprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class MBPrize extends MBMovingItem
{
	private long changeSizeTick = 0;
	private float currentSize;
	private float currentSizeVelocity;
	
	private SEAnimatedSprite collectAnimation;
	
	public MBPrize(E_MBPrizeType type, float blockSize) 
	{
		super(E_MBObjectColors.Usual, blockSize, true);
		
		this.currentSize = blockSize * 2.0f;
		this.currentSizeVelocity = -3.0f;
		
		this.renderOrder = E_MBRenderOrder.Prize.getIndex();
		
		this.type = E_MBObjectTypes.Unknown;
		
		if (type == E_MBPrizeType.Star)
			this.type = E_MBObjectTypes.Star;
		
		if (type == E_MBPrizeType.MegaJump)
			this.type = E_MBObjectTypes.MegaJump;
		
		this.setCurrentTexture(this.addTexture(R.drawable.prize));
		
		this.InitCollectAnimation();
	}
	
	@Override
	public void release() 
    {		
        super.release();
        
        if (this.collectAnimation != null)
			this.collectAnimation.release();
    }
	
	@Override
	public void restore() 
    {		
        super.restore();
        
        if (this.collectAnimation != null)
			this.collectAnimation.restore();
    }
	
	private void InitCollectAnimation()
	{
		this.collectAnimation = new SEAnimatedSprite(false);
		this.collectAnimation.loadAnimation(R.drawable.prize_collect_001, 18, 15);	
		
		this.collectAnimation.setSize(64.0f, 64.0f);
		this.collectAnimation.setAnimationLoop(false);	
	}
	
	public void showCollectAnimation()
	{
		if (this.collectAnimation != null)
			this.collectAnimation.startAnimation();
	}
	
	public boolean isCollectAnimationPlaying()
	{
		return (this.collectAnimation.isAnimationPlaying());
	}

	@Override
	public void Render(float blockSize, int renderOffset_x, int renderOffset_y,
			float scaleX, float scaleY, float scrH) 
	{
		if (SystemClock.uptimeMillis() - this.changeSizeTick > 100)
		{
			this.currentSize += this.currentSizeVelocity;
			if ((this.currentSize <= blockSize * 1.5f) || (this.currentSize >= blockSize * 2.0f))
				this.currentSizeVelocity *= -1.0f;
			this.changeSizeTick = SystemClock.uptimeMillis();
		}
		
		this.blockSize = blockSize;

		if (this.collectAnimation.isAnimationPlaying())
		{
			this.collectAnimation.setPos((float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x, 
					(float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y);
			this.collectAnimation.setSize(blockSize * 4.0f *  scaleX, blockSize * 4.0f * scaleY);
			this.collectAnimation.Render();
		}
		else
		{
			this.setPos((float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x, 
					(float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y);
			this.setSize(this.currentSize * scaleX, this.currentSize * scaleY);
			super.Render();
		}
	}
}
