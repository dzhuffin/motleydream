package com.spiritgames.motleydream.level;

import java.util.UUID;

import com.spiritgames.motleydream.enums.E_MBHorizontalCameraPosition;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBVerticalCameraPosition;

public class MBContactType 
{
	private UUID id;
	private E_MBVerticalCameraPosition vCameraPosition;
	private E_MBHorizontalCameraPosition hCameraPosition;
	private MBColoredItem itemLink;	
	private E_MBObjectTypes type;
	private E_MBObjectColors color;
	public E_MBObjectTypes getType() {
		return this.type;
	}
	public E_MBObjectColors getColor() {
		return this.color;
	}
	public E_MBVerticalCameraPosition getVCameraPosition() {
		return this.vCameraPosition;
	}
	public E_MBHorizontalCameraPosition getHCameraPosition() {
		return this.hCameraPosition;
	}
	public MBColoredItem getItemLink() {
		return this.itemLink;
	}
	
	public MBContactType(MBColoredItem itemLink, E_MBObjectTypes type, E_MBObjectColors color, 
			E_MBVerticalCameraPosition vCameraPosition, E_MBHorizontalCameraPosition hCameraPosition)
	{
		this.itemLink = itemLink;
		this.type = type;
		this.color = color;
		this.id = UUID.randomUUID();
		this.vCameraPosition = vCameraPosition;
		this.hCameraPosition = hCameraPosition;
	}
	
	public MBContactType(MBColoredItem itemLink, E_MBObjectTypes type, E_MBObjectColors color)
	{
		this.itemLink = itemLink;
		this.type = type;
		this.color = color;
		this.id = UUID.randomUUID();
		this.vCameraPosition = E_MBVerticalCameraPosition.Center;
		this.hCameraPosition = E_MBHorizontalCameraPosition.Free;
	}
	
	public MBContactType(MBColoredItem itemLink, E_MBObjectTypes type)
	{
		this.itemLink = itemLink;
		this.type = type;
		this.color = E_MBObjectColors.Unknown;
		this.id = UUID.randomUUID();
		this.vCameraPosition = E_MBVerticalCameraPosition.Center;
		this.hCameraPosition = E_MBHorizontalCameraPosition.Free;
	}
	
	@Override
	public boolean equals(Object aThat)
	{
		if ((aThat == null) || (aThat.getClass() != MBContactType.class) ||
			(((MBContactType)aThat).id == null))
			return false;
		
		return this.id.toString().equals(((MBContactType)aThat).id.toString());		
	}
	
	@Override
	public int hashCode() 
	{
	  return this.id.hashCode();
	}
}
