package com.spiritgames.motleydream.level;

import java.util.Iterator;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import com.spiritgames.motleydream.MBManagers;
import com.spiritgames.motleydream.R;
import com.spiritgames.motleydream.enums.E_MBFreeBoxKind;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBRenderOrder;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.SEMath;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.graphics.SETextureInfo;
import com.spiritgames.spiritengine.graphics.SETextureManager;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.physics.SEPhysicsWorldNative;

public class MBFreeBox extends MBPlatform 
{
	private float levelPixelPos_x = 0.0f;
	private float levelPixelPos_y = 0.0f;	
	
	private int xSize = 0;
	private int ySize = 0;
	
	private int chainLength = 0;

	private boolean isRotating;
	
	private E_MBFreeBoxKind kind;
	
	private boolean needToDeleteTextures = false;
	
	private SESprite boltSprite; 
	
	public boolean isRotating() {
		return isRotating;
	}

	public int getXSize() {
		return xSize;
	}
	public void setXSize(int xSize) {
		this.xSize = xSize;
	}
	public int getYSize() {
		return ySize;
	}
	public void setYSize(int ySize) {
		this.ySize = ySize;
	}		
	
	public void setLevelPixelPos_x(float lPos_x) 
	{
		this.levelPixelPos_x = lPos_x * SEPhysicsWorldNative.PHYSICS_SCALE;
	}
	
	public void setLevelPixelPos_y(float lPos_y) 
	{
		this.levelPixelPos_y = lPos_y * SEPhysicsWorldNative.PHYSICS_SCALE;
	}

	public MBFreeBox(E_MBObjectColors color, int pos_x, int pos_y, 
			int xSize, int ySize, E_MBFreeBoxKind kind, boolean isRotating, 
			float blockSize, int freeBoxChainLength) 
	{
		super(color, blockSize, false);
		
		this.setLevelPos_x(pos_x);
		this.setLevelPos_y(pos_y);
		
		this.xSize = xSize;
		this.ySize = ySize;
		
		this.chainLength = freeBoxChainLength;
		
		this.kind = kind;
		
		this.isRotating = isRotating;
		
		if (this.isRotating)
			this.renderOrder = E_MBRenderOrder.RotatingFreeBox.getIndex();
		else
			this.renderOrder = E_MBRenderOrder.FreeBox.getIndex();
		
		this.restore();
	}
	
	@Override
	public void restore()
	{
		if (this.boltSprite != null)
			this.boltSprite.release();
		
		this.generateTexture(this.color, this.xSize, this.ySize, this.kind);
	}
	
	@Override
	public void release()
	{
		int[] textures;
		
		if (this.texInfo.size() > 0 && this.needToDeleteTextures)
		{
			textures = new int[this.texInfo.size()];
			
			Iterator<SETextureInfo> itr = this.texInfo.iterator();
			int i = 0;
			while(itr.hasNext()) 
				textures[i++] = (int)itr.next().texID; 
			
			SEManagers.getGL10Interface().glDeleteTextures(textures.length, textures, 0);
			
			this.texInfo.clear();
		}
		else
			this.texInfo.clear();
		
		if (this.boltSprite != null)
			this.boltSprite.release();
	}

	public int generateTexture(E_MBObjectColors color, int xSize, int ySize, E_MBFreeBoxKind kind)
	{
		this.texInfo.clear();
		
		GL10 gl = MBManagers.getGL10Interface();
        
        Bitmap bitmap_t1 = null;
        Bitmap bitmap_t2 = null;
        Bitmap bitmap_c = null;
        Bitmap bitmap_chain = null;
        Bitmap bitmap_chain_start = null;
        
        // ���� ����������� ������, ����� ������������� �������� ��� ����
        if  (this.isRotating)
        {
        	int[] textures = new int[1];
    		gl.glGenTextures(1, textures, 0);

            //this.texInfo.add(new SETextureInfo(0, new Integer(textures[0])));
    		this.boltSprite = new SESprite(false);
    		this.boltSprite.setCurrentTexture(this.boltSprite.addTexture(new SETextureInfo(0, new Integer(textures[0]))));
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
            
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        	
        	bitmap_chain = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.chain);
        	bitmap_chain_start = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.chain_start);
        	
        	int pixels_chain[] = new int[bitmap_chain.getWidth() * bitmap_chain.getHeight()];
        	int pixels_chain_start[] = new int[bitmap_chain_start.getWidth() * bitmap_chain_start.getHeight()];
        	
        	bitmap_chain.getPixels(pixels_chain, 0, bitmap_chain.getWidth(), 0, 0, bitmap_chain.getWidth(), bitmap_chain.getHeight());
        	bitmap_chain_start.getPixels(pixels_chain_start, 0, bitmap_chain_start.getWidth(), 0, 0, bitmap_chain_start.getWidth(), bitmap_chain_start.getHeight());

        	Bitmap texBMP = Bitmap.createBitmap(bitmap_chain.getWidth(), bitmap_chain.getHeight() * this.chainLength, bitmap_chain.getConfig());
        	
        	for (int i = 0; i < this.chainLength; i++)
        	{
        		if (i == this.chainLength - 1)
        		{
        			texBMP.setPixels(pixels_chain_start, 0, bitmap_chain.getWidth(), 0, i * bitmap_chain.getHeight(), 
	        				bitmap_chain.getWidth(), bitmap_chain.getHeight());
        			break;
        		}
        		
        		texBMP.setPixels(pixels_chain, 0, bitmap_chain.getWidth(), 0, i * bitmap_chain.getHeight(), 
        				bitmap_chain.getWidth(), bitmap_chain.getHeight());
        	}
        	
        	int scale_width = SEMath.getNearPowerOfTwo(texBMP.getWidth(), true);
        	int scale_height = SEMath.getNearPowerOfTwo(texBMP.getHeight(), true);
        	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(texBMP, scale_width, scale_height, true), 0);
        	texBMP.recycle();
        	texBMP = null;
        }
                
        int[] textures = new int[1];
		gl.glGenTextures(1, textures, 0);

        this.texInfo.add(new SETextureInfo(0, new Integer(textures[0])));
        gl.glBindTexture(GL10.GL_TEXTURE_2D, (Integer)(this.texInfo.lastElement().texID));
        
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        
		//
		if (this.kind == E_MBFreeBoxKind.Box)
		{
			this.type = E_MBObjectTypes.FreeBox;
			
			// ���� ��������� ������ �������
			if (xSize == ySize)
			{
				this.needToDeleteTextures = false;
				
				if (color == E_MBObjectColors.Yellow)
				{
					this.addTexture(R.drawable.free_sqr_yellow);
				}
				
				if (color == E_MBObjectColors.Red)
				{
					this.addTexture(R.drawable.free_sqr_red);
				}
				
				if (color == E_MBObjectColors.Green)
				{
					this.addTexture(R.drawable.free_sqr_green);
				}
				
				if (color == E_MBObjectColors.Blue)
				{
					this.addTexture(R.drawable.free_sqr_blue);
				}
				
				this.setCurrentTexture(0);
			}
			
			// ���� ��������� ������ �������������
			if ((xSize != ySize) && (xSize > 1) && (ySize > 1))
			{
				this.needToDeleteTextures = false;
				
				if (color == E_MBObjectColors.Yellow)
				{
					if (xSize > ySize)
						this.addTexture(R.drawable.free_box_yellow_h);
					else
						this.addTexture(R.drawable.free_box_yellow);
				}
				
				if (color == E_MBObjectColors.Red)
				{
					if (xSize > ySize)
						this.addTexture(R.drawable.free_box_red_h);
					else
						this.addTexture(R.drawable.free_box_red);
				}
				
				if (color == E_MBObjectColors.Green)
				{
					if (xSize > ySize)
						this.addTexture(R.drawable.free_box_green_h);
					else
						this.addTexture(R.drawable.free_box_green);
				}
				
				if (color == E_MBObjectColors.Blue)
				{
					if (xSize > ySize)
						this.addTexture(R.drawable.free_box_blue_h);
					else
						this.addTexture(R.drawable.free_box_blue);
				}
				
				this.setCurrentTexture(0);
			}
			
			// ���� ��������� ������ - �����
			if (((xSize == 1) && (ySize >= 4)) || ((ySize == 1) && (xSize >= 4)))
			{
				this.needToDeleteTextures = true;
				
				if (color == E_MBObjectColors.Red)
		        {
			        bitmap_t1 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_red_top1);
			        bitmap_t2 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_red_top2);
			        bitmap_c = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_red_center);
		        }
		        
		        if (color == E_MBObjectColors.Green)
		        {
			        bitmap_t1 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_green_top1);
			        bitmap_t2 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_green_top2);
			        bitmap_c = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_green_center);
		        }
		        
		        if (color == E_MBObjectColors.Blue)
		        {
			        bitmap_t1 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_blue_top1);
			        bitmap_t2 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_blue_top2);
			        bitmap_c = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_blue_center);
		        }
		        
		        if (color == E_MBObjectColors.Yellow)
		        {
			        bitmap_t1 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_yellow_top1);
			        bitmap_t2 = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_yellow_top2);
			        bitmap_c = BitmapFactory.decodeResource(MBManagers.getApplicationContext().getResources(), R.drawable.free_long_yellow_center);
		        }
		        
		        if ((bitmap_t1 == null) || (bitmap_t2 == null) || (bitmap_c == null))
		        	return -1;
		        
		        // ���� ������� ������������
				if ((xSize == 1) && (ySize >= 4))
				{
					int texLength = ySize;
		        	Bitmap texBMP = Bitmap.createBitmap(bitmap_c.getWidth(), texLength * bitmap_c.getHeight(), bitmap_c.getConfig());
		        	
		        	int pixels_t1[] = new int[bitmap_t1.getWidth() * bitmap_t1.getHeight()];
		        	int pixels_t2[] = new int[bitmap_t2.getWidth() * bitmap_t2.getHeight()];
		        	int pixels_c[] = new int[bitmap_c.getWidth() * bitmap_c.getHeight()];
		        	
		        	bitmap_t1.getPixels(pixels_t1, 0, bitmap_t1.getWidth(), 0, 0, bitmap_t1.getWidth(), bitmap_t1.getHeight());
		        	bitmap_t2.getPixels(pixels_t2, 0, bitmap_t2.getWidth(), 0, 0, bitmap_t2.getWidth(), bitmap_t2.getHeight());
		        	bitmap_c.getPixels(pixels_c, 0, bitmap_c.getWidth(), 0, 0, bitmap_c.getWidth(), bitmap_c.getHeight());
		        	for (int i = 0; i < texLength; i++)
		        	{
		        		if ((i == 0) && (texLength > 1))
		        		{
		        			texBMP.setPixels(pixels_t1, 0, bitmap_c.getWidth(), 0, i * bitmap_c.getHeight(), bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		if ((i == 1) && (texLength > 1))
		        		{
		        			texBMP.setPixels(pixels_t2, 0, bitmap_c.getWidth(), 0, i * bitmap_c.getHeight(), bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		if ((i == texLength - 2) && (texLength > 1))
		        		{
		        			texBMP.setPixels(SEMath.rotateBitmapArray(pixels_t2, bitmap_c.getWidth(), bitmap_c.getHeight(), 180), 
		        					0, bitmap_c.getWidth(), 0, i * bitmap_c.getHeight(), bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		if ((i == texLength - 1) && (texLength > 1))
		        		{
		        			texBMP.setPixels(SEMath.rotateBitmapArray(pixels_t1, bitmap_c.getWidth(), bitmap_c.getHeight(), 180),
		        					0, bitmap_c.getWidth(), 0, i * bitmap_c.getHeight(), bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		texBMP.setPixels(pixels_c, 0, bitmap_c.getWidth(), 0, i * bitmap_c.getHeight(), bitmap_c.getWidth(), bitmap_c.getHeight());
		        	}

		        	int scale_width = SEMath.getNearPowerOfTwo(texBMP.getWidth(), true);
		        	int scale_height = SEMath.getNearPowerOfTwo(texBMP.getHeight(), true);
		        	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(texBMP, scale_width, scale_height, true), 0);
		        	texBMP.recycle();
		        	texBMP = null;
				}

				// ���� ������� �������������� 
				if ((ySize == 1) && (xSize >= 4))
				{
					int texLength = xSize;
		        	Bitmap texBMP = Bitmap.createBitmap(texLength * bitmap_c.getWidth(), bitmap_c.getHeight(), bitmap_c.getConfig());
		        	
		        	int pixels_t1[] = new int[bitmap_t1.getWidth() * bitmap_t1.getHeight()];
		        	int pixels_t2[] = new int[bitmap_t2.getWidth() * bitmap_t2.getHeight()];
		        	int pixels_c[] = new int[bitmap_c.getWidth() * bitmap_c.getHeight()];
		        	
		        	bitmap_t1.getPixels(pixels_t1, 0, bitmap_t1.getWidth(), 0, 0, bitmap_t1.getWidth(), bitmap_t1.getHeight());
		        	bitmap_t2.getPixels(pixels_t2, 0, bitmap_t2.getWidth(), 0, 0, bitmap_t2.getWidth(), bitmap_t2.getHeight());
		        	bitmap_c.getPixels(pixels_c, 0, bitmap_c.getWidth(), 0, 0, bitmap_c.getWidth(), bitmap_c.getHeight());
		        	
		        	for (int i = 0; i < texLength; i++)
		        	{
		        		if ((i == 0) && (texLength > 1))
		        		{
		        			texBMP.setPixels(SEMath.rotateBitmapArray(pixels_t1, bitmap_c.getWidth(), bitmap_c.getHeight(), 90),
		        					0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		if ((i == 1) && (texLength > 1))
		        		{
		        			texBMP.setPixels(SEMath.rotateBitmapArray(pixels_t2, bitmap_c.getWidth(), bitmap_c.getHeight(), 90), 
		        					0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		if ((i == texLength - 2) && (texLength > 1))
		        		{
		        			texBMP.setPixels(SEMath.rotateBitmapArray(pixels_t2, bitmap_c.getWidth(), bitmap_c.getHeight(), -90), 
		        					0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		if ((i == texLength - 1) && (texLength > 1))
		        		{
		        			texBMP.setPixels(SEMath.rotateBitmapArray(pixels_t1, bitmap_c.getWidth(), bitmap_c.getHeight(), -90),
		        					0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
		        			continue;
		        		}
		        		
		        		texBMP.setPixels(SEMath.rotateBitmapArray(pixels_c, bitmap_c.getWidth(), bitmap_c.getHeight(), -90), 
	        					0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
		        	}

		        	int scale_width = SEMath.getNearPowerOfTwo(texBMP.getWidth(), true);
		        	int scale_height = SEMath.getNearPowerOfTwo(texBMP.getHeight(), true);
		        	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(texBMP, scale_width, scale_height, true), 0);
		        	texBMP.recycle();
		        	texBMP = null;
				}				
			}
		}
		else
		{
			if (this.kind == E_MBFreeBoxKind.Ball)
			{
				this.needToDeleteTextures = false;
				
				this.type = E_MBObjectTypes.FreeBall;
				
				if (color == E_MBObjectColors.Yellow)
				{
					if (xSize > 2)
						this.addTexture(R.drawable.free_ball_big_yellow);
					else
						this.addTexture(R.drawable.free_ball_small_yellow);
				}
				
				if (color == E_MBObjectColors.Red)
				{
					if (xSize > 2)
						this.addTexture(R.drawable.free_ball_big_red);
					else
						this.addTexture(R.drawable.free_ball_small_red);
				}
				
				if (color == E_MBObjectColors.Green)
				{
					if (xSize > 2)
						this.addTexture(R.drawable.free_ball_big_green);
					else
						this.addTexture(R.drawable.free_ball_small_green);
				}
				
				if (color == E_MBObjectColors.Blue)
				{
					if (xSize > 2)
						this.addTexture(R.drawable.free_ball_big_blue);
					else
						this.addTexture(R.drawable.free_ball_small_blue);
				}
				
				this.setCurrentTexture(0);
			}
			else
			{
				this.needToDeleteTextures = false;
				
				this.type = E_MBObjectTypes.Tip;
				
				this.addTexture(R.drawable.tip_001 + this.chainLength - 1);
				
				this.setCurrentTexture(0);
			}
		}
                
		if (bitmap_t1 != null)
			bitmap_t1.recycle();
		
		if (bitmap_t2 != null)
			bitmap_t2.recycle();
		
		if (bitmap_c != null)
			bitmap_c.recycle();
		
        bitmap_t1 = null;
        bitmap_t2 = null;
        bitmap_c = null;
        
        this.setCurrentTexture(this.texInfo.size() - 1);
        
        return this.getCurrentTexture();
	}
	
	@Override
	public void Render(float blockSize, int renderOffset_x, int renderOffset_y,
			float scaleX, float scaleY, float scrH) 
	{		
		this.blockSize = blockSize;
		
		this.setPos((float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x, 
				(float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y);
		
		this.setSize((float)blockSize * (float)this.xSize * scaleX, (float)blockSize * (float)this.ySize * scaleY);
		
		this.Render();
		
		if (this.isRotating && this.kind != E_MBFreeBoxKind.Tip)
		{
			float chainW = (float)blockSize * scaleX;
			float chainH = (float)blockSize * this.chainLength * scaleY;
			this.boltSprite.setSize(chainW, chainH);
			this.boltSprite.setPos(this.x, (this.y + chainH / 2.0f) - ((float)blockSize / 4.0f));
			this.boltSprite.Render();
		}
	}
}
