package com.spiritgames.motleydream.level;

import java.util.HashMap;
import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.spiritgames.motleydream.MBGameManager;
import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.motleydream.enums.E_MBGameEpisodes;
import com.spiritgames.spiritengine.sqlite.SESQLiteField;
import com.spiritgames.spiritengine.sqlite.SESQLiteHelper;
import com.spiritgames.spiritengine.sqlite.SESQLiteTable;

public class MBGameProgressInfoDB extends SESQLiteHelper 
{
	private static String TABLE_NAME = "progress";
	
	private MBLevelLoader levelLoader;
	
	public MBGameProgressInfoDB() 
	{
		super("progress");
		this.levelLoader = new MBLevelLoader(true);
	}

	@Override
	protected void fillDBStructure()
	{
		SESQLiteTable t = new SESQLiteTable(TABLE_NAME);
		t.addField(new SESQLiteField("_id", "integer", true, true));
		t.addField(new SESQLiteField("level", "integer", false, false));
		t.addField(new SESQLiteField("episode", "integer", false, false));
		t.addField(new SESQLiteField("difficulty", "integer", false, false));
		t.addField(new SESQLiteField("time", "integer", false, false));
		t.addField(new SESQLiteField("prizes", "integer", false, false));
		t.addField(new SESQLiteField("max_prizes", "integer", false, false));
		t.addField(new SESQLiteField("deaths", "integer", false, false));
		t.addField(new SESQLiteField("complited", "integer", false, false));
		
		this.tables.add(t);
	}

	public void collectCompleteInfo(MBLevelCompleteInfo info)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.query(TABLE_NAME, 
				new String[]{"_id", "level", "episode", "difficulty", "time", "prizes", "max_prizes", "deaths", "complited"}, 
				"level=" + Integer.toString(info.level) + " AND episode=" + Integer.toString(info.episode.getIndex()) + 
				" AND difficulty=" + Integer.toString(info.difficultyLevel.getIndex()) + " AND prizes=" +
				Integer.toString(info.starsCount), 
				null, null, null, null);
		
		if (cur.moveToFirst())
		{
			MBLevelCompleteInfo existInfo = new MBLevelCompleteInfo();
			int id = cur.getInt(0);
			existInfo.level = cur.getInt(1);
			existInfo.episode = E_MBGameEpisodes.getByIndex(cur.getInt(2));
			existInfo.difficultyLevel = E_MBDifficultyLevel.getByIndex(cur.getInt(3));
			int time = cur.getInt(4);
			existInfo.timeM = time / 60000;
			existInfo.timeS = (time % 60000) / 1000;
			existInfo.timeMs = (time % 60000) % 1000;
			existInfo.starsCount = cur.getInt(5);
			existInfo.maxStarsCount = cur.getInt(6);
			existInfo.deaths = cur.getInt(7);
			existInfo.complited = cur.getInt(8);
			cur.close();
			
			MBLevelCompleteInfo newInfo = MBLevelCompleteInfo.combineToBestCompleteInfo(info, existInfo);
			
			ContentValues cv = new ContentValues();
			cv.put("time", newInfo.timeM * 60000 + info.timeS * 1000 + info.timeMs);
			cv.put("deaths", newInfo.deaths);
			cv.put("complited", 1);
			db.update(TABLE_NAME, cv, "_id=" + Integer.toString(id), null);
		}
		else
		{
			cur.close();
			
			ContentValues cv = new ContentValues();
			cv.put("level", info.level);
			cv.put("episode", info.episode.getIndex());
			cv.put("difficulty", info.difficultyLevel.getIndex());
			cv.put("time", info.timeM * 60000 + info.timeS * 1000 + info.timeMs);
			cv.put("prizes", info.starsCount);
			cv.put("max_prizes", info.maxStarsCount);
			cv.put("deaths", info.deaths);
			cv.put("complited", 1);
			db.insert(TABLE_NAME, null, cv);
		}
		
		db.close();
	}
	
	public MBLevelCompleteInfo getCompleteInfo(E_MBGameEpisodes episode, int level, E_MBDifficultyLevel dif)
	{
		MBLevelCompleteInfo resultInfo = new MBLevelCompleteInfo();
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.query(TABLE_NAME, 
				new String[]{"_id", "level", "episode", "difficulty", "time", "prizes", "max_prizes", "deaths", "complited"}, 
				"level=" + Integer.toString(level) + " AND episode=" + Integer.toString(episode.getIndex()) + 
				" AND difficulty=" + Integer.toString(dif.getIndex()), 
				null, null, null, "prizes");
		
		if (cur.moveToLast())
		{
			int id = cur.getInt(0);
			resultInfo.level = cur.getInt(1);
			resultInfo.episode = E_MBGameEpisodes.getByIndex(cur.getInt(2));
			resultInfo.difficultyLevel = E_MBDifficultyLevel.getByIndex(cur.getInt(3));
			int time = cur.getInt(4);
			resultInfo.timeM = time / 60000;
			resultInfo.timeS = (time % 60000) / 1000;
			resultInfo.timeMs = (time % 60000) % 1000;
			resultInfo.starsCount = cur.getInt(5);
			resultInfo.maxStarsCount = cur.getInt(6);
			resultInfo.deaths = cur.getInt(7);
			resultInfo.complited = cur.getInt(8);
			cur.close();
		}
				
		db.close();
		
		return resultInfo;
	}
	
	public HashMap<Integer, MBLevelCompleteInfo> getCompleteInfoForEpisode(E_MBGameEpisodes episode, E_MBDifficultyLevel dif)
	{
		HashMap<Integer, MBLevelCompleteInfo> result = new HashMap<Integer, MBLevelCompleteInfo>();
		MBLevelCompleteInfo resultInfo;
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cur = db.query(TABLE_NAME, 
				new String[]{"_id", "level", "episode", "difficulty", "time", "prizes", "max_prizes", "deaths", "complited"}, 
				"episode=" + Integer.toString(episode.getIndex()) + 
				" AND difficulty=" + Integer.toString(dif.getIndex()), 
				null, null, null, "prizes");

		if (cur.moveToFirst())
		{
			do
			{
				resultInfo = new MBLevelCompleteInfo();
	
				resultInfo.level = cur.getInt(1);
				resultInfo.episode = E_MBGameEpisodes.getByIndex(cur.getInt(2));
				resultInfo.difficultyLevel = E_MBDifficultyLevel.getByIndex(cur.getInt(3));
				int time = cur.getInt(4);
				resultInfo.timeM = time / 60000;
				resultInfo.timeS = (time % 60000) / 1000;
				resultInfo.timeMs = (time % 60000) % 1000;
				resultInfo.starsCount = cur.getInt(5);
				resultInfo.maxStarsCount = cur.getInt(6);
				resultInfo.deaths = cur.getInt(7);
				resultInfo.complited = cur.getInt(8);
				
				result.put(resultInfo.level, resultInfo);
			}
			while (cur.moveToNext());
		}
		
		cur.close();
		db.close();
		
		int levelsCount = MBGameManager.getLevelsCount(episode);
		
		for (int i = 1; i <= levelsCount; i++)
		{
			if (!result.containsKey(i))
			{
				resultInfo = new MBLevelCompleteInfo();
				
				resultInfo.level = i;
				resultInfo.episode = episode;
				resultInfo.difficultyLevel = dif;
				resultInfo.timeM = 0;
				resultInfo.timeS = 0;
				resultInfo.timeMs = 0;
				resultInfo.starsCount = 0;
				resultInfo.maxStarsCount = this.levelLoader.getLevelMaxStars(episode, i);
				resultInfo.deaths = 0;
				resultInfo.complited = 0;
				
				result.put(resultInfo.level, resultInfo);
			}
		}
		
		return result;
	}
}
