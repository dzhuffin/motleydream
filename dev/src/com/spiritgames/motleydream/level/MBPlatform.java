package com.spiritgames.motleydream.level;

import java.util.Iterator;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.Log;

import com.spiritgames.motleydream.MBManagers;
import com.spiritgames.motleydream.R;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBOrientation;
import com.spiritgames.motleydream.enums.E_MBPlatformEdgeType;
import com.spiritgames.motleydream.enums.E_MBRenderOrder;
import com.spiritgames.spiritengine.SEManagers;
import com.spiritgames.spiritengine.SEMath;
import com.spiritgames.spiritengine.graphics.SETextureInfo;
import com.spiritgames.spiritengine.graphics.SETextureManager;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class MBPlatform extends MBColoredItem
{
	protected E_MBOrientation orientation;
	protected E_MBPlatformEdgeType edgeType;
	public E_MBOrientation getOrientation() 
	{
		return this.orientation;
	}
	public void setOrientation(E_MBOrientation orientation) 
	{
		this.orientation = orientation;
	}
	
	protected int length;
	public int getLength() 
	{
		return this.length;
	}
	public void setLength(int length) 
	{
		this.length = length;
	}
	
	public float getMinPixelX()
	{
		return this.levelPos_x * this.blockSize;
	}	
	public float getMaxPixelX()
	{
		if (this.orientation == E_MBOrientation.Horizontal)
			return (this.levelPos_x + this.length) * this.blockSize;
		else
			return (this.levelPos_x + 1) * this.blockSize;
	}

	public MBPlatform(E_MBObjectColors color, float blockSize, boolean useTextureRenderExtension) 
	{
		super(color, blockSize, useTextureRenderExtension);
		
		this.type = E_MBObjectTypes.Platform;
	}
	
	public MBPlatform(E_MBObjectColors color, E_MBOrientation orientation, E_MBPlatformEdgeType edgeType, int length, float blockSize, boolean useTextureRenderExtension) 
	{
		super(color, blockSize, useTextureRenderExtension);
		
		this.type = E_MBObjectTypes.Platform;
		this.orientation = orientation;
		this.edgeType = edgeType;
		this.length = length;
		
		if (color == E_MBObjectColors.LeftWall || color == E_MBObjectColors.RightWall)			
			this.renderOrder = E_MBRenderOrder.Wall.getIndex();
		else if (color == E_MBObjectColors.Floor)
			this.renderOrder = E_MBRenderOrder.Floor.getIndex();
		else if (color == E_MBObjectColors.Roof)
			this.renderOrder = E_MBRenderOrder.Roof.getIndex();
		else
		{
			if (this.orientation == E_MBOrientation.Vertical)
				this.renderOrder = E_MBRenderOrder.VerticalPlatform.getIndex();
			else
				this.renderOrder = E_MBRenderOrder.HorizontalPlatform.getIndex();
		}
		
		this.restore();
	}
	
	@Override
	public void release()
	{
		int[] textures;
		
		if (this.texInfo.size() > 0)
		{
			textures = new int[this.texInfo.size()];
			
			Iterator<SETextureInfo> itr = this.texInfo.iterator();
			int i = 0;
			while(itr.hasNext()) 
				textures[i++] = (int)itr.next().texID; 
			
			SEManagers.getGL10Interface().glDeleteTextures(textures.length, textures, 0);
			
			this.texInfo.clear();
		}
	}
	
	@Override
	public void restore()
	{
		this.release();
		
		if (this.color == E_MBObjectColors.Floor)
			this.generateTexture(orientation, R.drawable.block_floor, R.drawable.block_floor, R.drawable.block_floor);
		
		if (this.color == E_MBObjectColors.Usual)
		{
			if (orientation == E_MBOrientation.Horizontal)
				this.generateTexture(orientation, R.drawable.block_usual_l, R.drawable.block_usual_r, R.drawable.block_usual_c);
			else
				this.generateTexture(orientation, R.drawable.block_usual_w, R.drawable.block_usual_w, R.drawable.block_usual_w);
		}
		
		if (this.color == E_MBObjectColors.Red)
		{
			if (orientation == E_MBOrientation.Horizontal)
				this.generateTexture(orientation, R.drawable.block_red_l, R.drawable.block_red_r, R.drawable.block_red_c);
			else
				this.generateTexture(orientation, R.drawable.block_red_w, R.drawable.block_red_w, R.drawable.block_red_w);
		}
		
		if (this.color == E_MBObjectColors.Green)
		{
			if (orientation == E_MBOrientation.Horizontal)
				this.generateTexture(orientation, R.drawable.block_green_l, R.drawable.block_green_r, R.drawable.block_green_c);
			else
				this.generateTexture(orientation, R.drawable.block_green_w, R.drawable.block_green_w, R.drawable.block_green_w);
		}
		
		if (this.color == E_MBObjectColors.Blue)
		{
			if (orientation == E_MBOrientation.Horizontal)
				this.generateTexture(orientation, R.drawable.block_blue_l, R.drawable.block_blue_r, R.drawable.block_blue_c);
			else
				this.generateTexture(orientation, R.drawable.block_blue_w, R.drawable.block_blue_w, R.drawable.block_blue_w);
		}
		
		if (this.color == E_MBObjectColors.Yellow)
		{
			if (orientation == E_MBOrientation.Horizontal)
				this.generateTexture(orientation, R.drawable.block_yellow_l, R.drawable.block_yellow_r, R.drawable.block_yellow_c);
			else
				this.generateTexture(orientation, R.drawable.block_yellow_w, R.drawable.block_yellow_w, R.drawable.block_yellow_w);
		}
		
		if (this.color == E_MBObjectColors.LeftWall)
		{
			this.generateTexture(orientation, R.drawable.wall_left, R.drawable.wall_left, R.drawable.wall_left);
		}
		
		if (this.color == E_MBObjectColors.RightWall)
		{
			this.generateTexture(orientation, R.drawable.wall_right, R.drawable.wall_right, R.drawable.wall_right);
		}
		
		if (this.color == E_MBObjectColors.Roof)
		{
			this.generateTexture(orientation, R.drawable.roof, R.drawable.roof, R.drawable.roof);
		}
		
		this.setCurrentTexture(0);
	}
	
	public int generateTexture( E_MBOrientation orientation, int resID_l, int resID_r, int resID_c)
	{
		GL10 gl = MBManagers.getGL10Interface();
		
		int[] textures = new int[1];
		gl.glGenTextures(1, textures, 0);

		this.texInfo.add(new SETextureInfo(0, new Integer(textures[0])));
        gl.glBindTexture(GL10.GL_TEXTURE_2D, (Integer)(this.texInfo.lastElement().texID));

        if (this.getClass() == MBMovingPlatform.class)
        {
        	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        }
        else
        {
        	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        	gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_NEAREST);
        }
          
        Bitmap bitmap_l;
        Bitmap bitmap_r;
        Bitmap bitmap_c;
        
        if (resID_l == resID_r && resID_r == resID_c)
        {
        	bitmap_l = bitmap_r = bitmap_c = BitmapFactory.decodeResource(SEManagers.getApplicationContext().getResources(), resID_c);
        }
        else
        {
	        bitmap_l = BitmapFactory.decodeResource(SEManagers.getApplicationContext().getResources(), resID_l);
	        bitmap_r = BitmapFactory.decodeResource(SEManagers.getApplicationContext().getResources(), resID_r);
	        bitmap_c = BitmapFactory.decodeResource(SEManagers.getApplicationContext().getResources(), resID_c);
        }
        
        if (this.orientation == E_MBOrientation.Horizontal)
		{
        	if (this.color == E_MBObjectColors.Roof)
        	{
	        	Bitmap texBMP = Bitmap.createBitmap(this.length * bitmap_c.getWidth() / 2, bitmap_c.getHeight(), bitmap_c.getConfig());
	        	
	        	int pixels_c[] = new int[bitmap_c.getWidth() * bitmap_c.getHeight()];

	        	bitmap_c.getPixels(pixels_c, 0, bitmap_c.getWidth(), 0, 0, bitmap_c.getWidth(), bitmap_c.getHeight());
	        	
	        	int texLength = (this.length % 2 == 0) ? this.length / 2 : this.length / 2 + 1;
	        	for (int i = 0; i < texLength; i++)
	        	{
	        		if ((this.length % 2 != 0) && (i == texLength - 1) && (texLength > 1))
	        		{
	        			texBMP.setPixels(pixels_c, 0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth() / 2, bitmap_c.getHeight());
	        			continue;
	        		}
	        		
	        		texBMP.setPixels(pixels_c, 0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());	        		
	        	}
	        	
	        	int scale_width = SEMath.getNearPowerOfTwo(texBMP.getWidth(), true);
	        	int scale_height = SEMath.getNearPowerOfTwo(texBMP.getHeight(), true);
	        	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(texBMP, scale_width, scale_height, true), 0);
	        	//
	        	if (this.useTextureRenderExtension)
	        	{
	    			int crop[] = new int[4];
	    			crop[0] = 0;
	    			crop[1] = 0;
	    			crop[2] = scale_width;
	    			crop[3] = -scale_height;
	    			
	    			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, crop, 0);
	        	}
	        	//
	        	texBMP.recycle();
	        	texBMP = null;
        	}
        	else
        	{
	        	int texLength = (this.length % 2 == 0) ? this.length / 2 : this.length / 2 + 1;
	        	Bitmap texBMP = Bitmap.createBitmap(texLength * bitmap_c.getWidth(), bitmap_c.getHeight(), bitmap_c.getConfig());
	        	
	        	int pixels_l[] = new int[bitmap_l.getWidth() * bitmap_l.getHeight()];
	        	int pixels_r[] = new int[bitmap_r.getWidth() * bitmap_r.getHeight()];
	        	int pixels_c[] = new int[bitmap_c.getWidth() * bitmap_c.getHeight()];
	        	
	        	bitmap_l.getPixels(pixels_l, 0, bitmap_l.getWidth(), 0, 0, bitmap_l.getWidth(), bitmap_l.getHeight());
	        	bitmap_r.getPixels(pixels_r, 0, bitmap_r.getWidth(), 0, 0, bitmap_r.getWidth(), bitmap_r.getHeight());
	        	bitmap_c.getPixels(pixels_c, 0, bitmap_c.getWidth(), 0, 0, bitmap_c.getWidth(), bitmap_c.getHeight());
	        	for (int i = 0; i < texLength; i++)
	        	{
	        		if ((i == 0) && (texLength > 1) && ((this.edgeType == E_MBPlatformEdgeType.LeftOnly) || (this.edgeType == E_MBPlatformEdgeType.All)))
	        		{
	        			texBMP.setPixels(pixels_l, 0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
	        			continue;
	        		}
	        		
	        		if ((i == texLength - 1) && (texLength > 1) && ((this.edgeType == E_MBPlatformEdgeType.RightOnly) || (this.edgeType == E_MBPlatformEdgeType.All)))
	        		{
	        			texBMP.setPixels(pixels_r, 0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
	        			continue;
	        		}
	        		
	        		texBMP.setPixels(pixels_c, 0, bitmap_c.getWidth(), i * bitmap_c.getWidth(), 0, bitmap_c.getWidth(), bitmap_c.getHeight());
	        	}
	
	        	int scale_width = SEMath.getNearPowerOfTwo(texBMP.getWidth(), true);
	        	int scale_height = SEMath.getNearPowerOfTwo(texBMP.getHeight(), true);
	        	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(texBMP, scale_width, scale_height, true), 0);
	        	//
	        	if (this.useTextureRenderExtension)
	        	{
	    			int crop[] = new int[4];
	    			crop[0] = 0;
	    			crop[1] = 0;
	    			crop[2] = scale_width;
	    			crop[3] = -scale_height;
	    			
	    			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, crop, 0);
	        	}
	        	//
	        	texBMP.recycle();
	        	texBMP = null;
        	}
		}
        else
        {        	
        	if (this.color == E_MBObjectColors.LeftWall || this.color == E_MBObjectColors.RightWall)
        	{
        		Bitmap texBMP = Bitmap.createBitmap(bitmap_c.getWidth(), this.length * (bitmap_c.getWidth() / 2), bitmap_c.getConfig());
        		
        		int pixels_c[] = new int[texBMP.getWidth()];
        		
        		int minH = (texBMP.getHeight() > bitmap_c.getHeight()) ?
        				bitmap_c.getHeight() :
        				texBMP.getHeight();
        		for (int i = 0; i < minH; i++)
        		{
        			bitmap_c.getPixels(pixels_c, 0, bitmap_c.getWidth(), 0, i, bitmap_c.getWidth(), 1);
    	        	texBMP.setPixels(pixels_c, 0, texBMP.getWidth(), 0, i, texBMP.getWidth(), 1);
        		}
	        	
	        	int scale_width = SEMath.getNearPowerOfTwo(texBMP.getWidth(), true);
	        	int scale_height = SEMath.getNearPowerOfTwo(texBMP.getHeight(), true);
	        	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(texBMP, scale_width, scale_height, true), 0);
	        	//
	        	if (this.useTextureRenderExtension)
	        	{
	    			int crop[] = new int[4];
	    			crop[0] = 0;
	    			crop[1] = 0;
	    			crop[2] = scale_width;
	    			crop[3] = -scale_height;
	    			
	    			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, crop, 0);
	        	}
	        	//
	        	texBMP.recycle();
	        	texBMP = null;
        	}
        	else
        	{
	        	int texLength = (this.length % 2 == 0) ? this.length / 2 : this.length / 2 + 1;
	        	Bitmap texBMP = Bitmap.createBitmap(bitmap_c.getWidth(), texLength * bitmap_c.getHeight(), bitmap_c.getConfig());
	        	
	        	int pixels_c[] = new int[bitmap_c.getWidth() * bitmap_c.getHeight()];
	
	        	bitmap_c.getPixels(pixels_c, 0, bitmap_c.getWidth(), 0, 0, bitmap_c.getWidth(), bitmap_c.getHeight());
	        	for (int i = 0; i < texLength; i++)
	        	{
	        		texBMP.setPixels(pixels_c, 0, bitmap_c.getWidth(), 0, i * bitmap_c.getHeight(), bitmap_c.getWidth(), bitmap_c.getHeight());
	        	}
	
	        	int scale_width = SEMath.getNearPowerOfTwo(texBMP.getWidth(), true);
	        	int scale_height = SEMath.getNearPowerOfTwo(texBMP.getHeight(), true);
	        	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, Bitmap.createScaledBitmap(texBMP, scale_width, scale_height, true), 0);
	        	//
	        	if (this.useTextureRenderExtension)
	        	{
	    			int crop[] = new int[4];
	    			crop[0] = 0;
	    			crop[1] = 0;
	    			crop[2] = scale_width;
	    			crop[3] = -scale_height;
	    			
	    			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, crop, 0);
	        	}
	        	//
	        	texBMP.recycle();
	        	texBMP = null;
        	}
        }
                
        if (bitmap_c != null)
        {
        	bitmap_c.recycle();
        	bitmap_c = null;
        }
        if (bitmap_l != null)
        {
        	bitmap_l.recycle();
        	bitmap_l = null;
        }
        if (bitmap_r != null)
        {
        	bitmap_r.recycle();
            bitmap_r = null;
        }
        
        return this.texInfo.size() - 1;
	}

	@Override
	public void Render(float blockSize, int renderOffset_x, int renderOffset_y, float scaleX, float scaleY, float scrH) 
	{
		this.blockSize = blockSize;

		float realLength = (float)this.length * (float)blockSize;;
		
		if (this.orientation == E_MBOrientation.Horizontal)
		{
			this.setSize((float)(this.length * blockSize) * scaleX, (float)blockSize * scaleY);
			
			this.setPos((float)((this.levelPos_x * blockSize) + (realLength / 2.0f)) * scaleX + (float)renderOffset_x, 
					(float)(scrH - (this.levelPos_y * blockSize) - (blockSize / 2)) * scaleY + (float)renderOffset_y);

			super.Render();												
		}
		else
		{
			if (this.color == E_MBObjectColors.LeftWall || this.color == E_MBObjectColors.RightWall)
			{
				this.setSize((float)blockSize * scaleX * 2.0f, (float)(this.length * blockSize) * scaleY);
				this.setPos((float)((this.levelPos_x * blockSize) + (blockSize)) * scaleX + (float)renderOffset_x, 
						(float)(scrH - (this.levelPos_y * blockSize) - (realLength / 2.0f)) * scaleY + (float)renderOffset_y);
			}
			else
			{
				this.setSize((float)blockSize * scaleX, (float)(this.length * blockSize) * scaleY);
				this.setPos((float)((this.levelPos_x * blockSize) + (blockSize / 2)) * scaleX + (float)renderOffset_x, 
						(float)(scrH - (this.levelPos_y * blockSize) - (realLength / 2.0f)) * scaleY + (float)renderOffset_y);
			}
			
			super.Render();
		}								
	}

}
