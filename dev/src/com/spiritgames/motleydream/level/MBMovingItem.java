package com.spiritgames.motleydream.level;

import android.content.Context;

import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.physics.SEPhysicsWorldNative;

abstract public class MBMovingItem extends MBColoredItem 
{	
	public MBMovingItem(E_MBObjectColors color, float blockSize, boolean useTextureRenderExtension) 
	{
		super(color, blockSize, useTextureRenderExtension);
	}
	
	@Override
	public void setLevelPos_x(int lPos_x) 
	{
		this.levelPixelPos_x = lPos_x * this.blockSize;
		this.levelPos_x = lPos_x;
	}
	
	@Override
	public void setLevelPos_y(int lPos_y) 
	{
		this.levelPixelPos_y = lPos_y * this.blockSize;
		this.levelPos_y = lPos_y;
	}
	
	@Override
	public int getLevelPos_x() 
	{
		this.levelPos_x = (int)(this.levelPixelPos_x / (float)this.blockSize);
		return this.levelPos_x;
	}
	
	@Override
	public int getLevelPos_y() 
	{
		this.levelPos_y = (int)(this.levelPixelPos_y / (float)this.blockSize);
		return this.levelPos_y;
	}

	protected float levelPixelPos_x = 0.0f;
	protected float levelPixelPos_y = 0.0f;
	public void setLevelPixelPos_x(float lPos_x) 
	{
		this.levelPixelPos_x = lPos_x * SEPhysicsWorldNative.PHYSICS_SCALE;
	}
	
	public void setLevelPixelPos_y(float lPos_y) 
	{
		this.levelPixelPos_y = lPos_y * SEPhysicsWorldNative.PHYSICS_SCALE;
	}
	
	public void move(float dx, float dy)
	{		
		this.levelPixelPos_x += dx;
		this.levelPixelPos_y += dy;
	}
}
