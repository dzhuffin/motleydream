package com.spiritgames.motleydream.level;

import javax.microedition.khronos.opengles.GL10;

import com.spiritgames.motleydream.MBRenderManager;
import com.spiritgames.spiritengine.graphics.SERenderManager;

public class MBGameInfo 
{
	private String playTimeText;
	private int playTimeMin = 0;
	private int playTimeSec = 0;
	private int playTimeMsec = 0;
	public int getPlayTimeMin() 
	{
		return this.playTimeMin;
	}
	public int getPlayTimeSec() 
	{
		return this.playTimeSec;
	}
	public int getPlayTimeMsec() 
	{
		return this.playTimeMsec;
	}
	
	private int starsCollected = 0;
	public int getStarsCollectedCount() 
	{
		return this.starsCollected;
	}
	private int megaJumpsCollected = 0;
	
	private int starsCount = 0;
	public int getStarsCount() 
	{
		return this.starsCount;
	}
	public void setStarsCount(int starsCount) 
	{
		this.starsCount = starsCount;
	}

	public void setPlayTime(long milliseconds) 
	{
		long sec = (milliseconds / 1000);
		
		this.playTimeMsec = (int) ((milliseconds - (sec * 1000)) / 10);
		String playMilliSec = Long.toString(this.playTimeMsec);
		
		this.playTimeSec = (int) (sec - (sec / 60));
		String playSec = Long.toString(this.playTimeSec);
		
		this.playTimeText = playSec + "-" + 
						    playMilliSec;
	}
	
	public void collectStar() 
	{
		this.starsCollected++;
	}
	
	public void collectMegaJump() 
	{
		this.megaJumpsCollected++;
	}
	
	public MBLevelCompleteInfo getCompleteInfo()
	{
		MBLevelCompleteInfo info = new MBLevelCompleteInfo();
		
		info.starsCount = this.starsCollected;
		info.maxStarsCount = this.starsCount;
		info.timeM = this.playTimeMin;
		info.timeS = this.playTimeSec;
		info.timeMs = this.playTimeMsec;
		
		return info;
	}
	
	public void reset()
	{
		this.playTimeText = "00-00";
		this.starsCollected = 0;
		this.megaJumpsCollected = 0;
		this.playTimeMin = 0;
		this.playTimeSec = 0;
		this.playTimeMsec = 0;
	}
	
	public MBGameInfo()
	{
		this.reset();
	}
	
	public String toCompleteTimeString()
	{
		String min = Integer.toString(this.playTimeMin);
		if (this.playTimeMin < 10)
			min = "0" + min;
		
		String sec = Integer.toString(this.playTimeSec);
		if (this.playTimeSec < 10)
			sec = "0" + sec;
		
		String msec = Integer.toString(this.playTimeMsec);
		if (this.playTimeMsec < 10)
			msec = "00" + msec;
		else
		{
			if (this.playTimeMsec < 100)
				msec = "0" + msec;
		}
		
		return  "completed in " + sec + " sec."/*min + ":" + sec + "." + msec*/;
	}
	
	public String toCompleteStarsString()
	{
		return Integer.toString(this.starsCollected) + "/" +
			   Integer.toString(this.starsCount);
	}
		
	public void release()
	{				
		//
	}
	
	public void Render()
	{
		//SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).RenderText(420.0f, 320.0f, 30.0f, this.playTimeText, true);
		//SERenderManager.getFont(MBRenderManager.FONT_RAGE_ITALIC).RenderText(420.0f, 250.0f, 30.0f, Integer.toString(this.starsCollected) + " of " + Integer.toString(this.starsCount), true);
	}
}
