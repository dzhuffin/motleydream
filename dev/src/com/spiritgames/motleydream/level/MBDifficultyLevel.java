package com.spiritgames.motleydream.level;

import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;

public class MBDifficultyLevel 
{
	private E_MBDifficultyLevel currentLevel = E_MBDifficultyLevel.Normal;	
	public E_MBDifficultyLevel getCurrentLevel() {
		return currentLevel;
	}
	public void setCurrentLevel(E_MBDifficultyLevel currentLevel) {
		this.currentLevel = currentLevel;
	}

	public MBDifficultyLevel()
	{
		
	}
	
	public float getBallJumpForce()
	{
		if (this.currentLevel == E_MBDifficultyLevel.Hardcore)
			return -1.0f;
		
		return -2.0f;
	}
	
	public float getBallMoveForce()
	{
		if (this.currentLevel == E_MBDifficultyLevel.Hardcore)
			return 0.04f;
		
		return 0.08f;
	}
	
	public float getBallDensity()
	{
		if (this.currentLevel == E_MBDifficultyLevel.Hardcore)
			return 0.4f;
		
		return 1.0f;
	}
	
	public float getBallElasticity()
	{
		if (this.currentLevel == E_MBDifficultyLevel.Hardcore)
			return 0.4f;
		
		return 0.0f;
	}
	
	public float getFreeObjectsElasticity()
	{
		if (this.currentLevel == E_MBDifficultyLevel.Hardcore)
			return 0.4f;
		
		return 0.4f;
	}
	
	public float getHorisontalElasticity()
	{
		if (this.currentLevel == E_MBDifficultyLevel.Hardcore)
			return 0.4f;
		
		return 0.0f;
	}
	
	public float getVerticalElasticity()
	{
		if (this.currentLevel == E_MBDifficultyLevel.Hardcore)
			return 0.4f;
		
		return 0.4f;
	}
}
