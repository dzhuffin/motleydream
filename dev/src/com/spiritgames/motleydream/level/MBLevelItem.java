package com.spiritgames.motleydream.level;

//import org.jbox2d.dynamics.Body;
import java.util.Comparator;

import com.badlogic.gdx.physics.box2d.Body;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.spiritengine.graphics.SESprite;

abstract public class MBLevelItem extends SESprite
{
	public static class RenderOrderComparator implements Comparator<MBLevelItem>
	{
		@Override
		public int compare(MBLevelItem o1, MBLevelItem o2) 
		{
			if(o1.getRenderOrder() > o2.getRenderOrder())
	            return 1;
	        else if(o1.getRenderOrder() < o2.getRenderOrder())
	            return -1;
	        else
	            return 0;
		}
	}
	
	protected int renderOrder = 0;
	public int getRenderOrder() {
		return renderOrder;
	}

	protected float blockSize = 10;	 
	protected E_MBObjectTypes type;
	protected Body phBody = null;
	
	public E_MBObjectTypes getType() 
	{
		return this.type;
	}
	
	public Body getPhysicBody() {
		return this.phBody;
	}
	public void setPhysicBody(Body phBody) {
		this.phBody = phBody;
	}

	protected int levelPos_x;
	protected int levelPos_y;
	public int getLevelPos_x() 
	{
		return this.levelPos_x;
	}
	public void setLevelPos_x(int levelPosX) 
	{
		this.levelPos_x = levelPosX;
	}
	public int getLevelPos_y() 
	{
		return this.levelPos_y;
	}
	public void setLevelPos_y(int levelPosY) 
	{
		this.levelPos_y = levelPosY;
	}
	public void moveLevel(int dx, int dy)
	{
		this.levelPos_x += dx;
		this.levelPos_y += dy;
	}
	public void move(float dx, float dy)
	{
		this.setPos(this.getX() + dx, this.getY() + dy);
	}

	public MBLevelItem(float blockSize, boolean useTextureRenderExtension) 
	{
		super(useTextureRenderExtension);
		
		this.blockSize = blockSize;
	}
	
	abstract public void Render(float blockSize, int renderOffset_x, int renderOffset_y, float scaleX, float scaleY, float scrH);	
}
