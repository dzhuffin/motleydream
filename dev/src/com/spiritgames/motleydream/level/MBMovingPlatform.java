package com.spiritgames.motleydream.level;

//import org.jbox2d.dynamics.Body;
//import org.jbox2d.dynamics.joints.PrismaticJoint;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBOrientation;
import com.spiritgames.motleydream.enums.E_MBPlatformEdgeType;
import com.spiritgames.motleydream.enums.E_MBRenderOrder;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.physics.SEPhysicsWorldNative;

public class MBMovingPlatform extends MBPlatform 
{
	private float levelPixelPos_x = 0.0f;
	private float levelPixelPos_y = 0.0f;
	
	private float motorSpeed = 0.0f;
	
	private int distance = 5;
    private int startPos = 0;
    
    private E_MBOrientation movingOrientation;
    
    public E_MBOrientation getMovingOrientation() {
		return movingOrientation;
	}

	public Vector2 getLinearVelocity()
    {
    	if (this.phBody != null)
    		return this.phBody.getLinearVelocity();
    	else
    		return new Vector2(0.0f, 0.0f);
    }
	
	public void setLevelPixelPos_x(float lPos_x) 
	{
		this.levelPixelPos_x = lPos_x * SEPhysicsWorldNative.PHYSICS_SCALE;
	}
	
	public void setLevelPixelPos_y(float lPos_y) 
	{
		this.levelPixelPos_y = lPos_y * SEPhysicsWorldNative.PHYSICS_SCALE;
	}
	
	public float getLevelPixelPos_x() 
	{
		return this.levelPixelPos_x;
	}
	
	public float getLevelPixelPos_y() 
	{
		return this.levelPixelPos_y;
	}
	
	public int getLength() 
	{
		return this.length;
	}
	
	@Override
	public void setLevelPos_x(int lPos_x) 
	{
		this.levelPixelPos_x = lPos_x * this.blockSize;
		this.levelPos_x = lPos_x;
	}
	
	@Override
	public void setLevelPos_y(int lPos_y) 
	{
		this.levelPixelPos_y = lPos_y * this.blockSize;
		this.levelPos_y = lPos_y;
	}
	
	@Override
	public int getLevelPos_x() 
	{
		this.levelPos_x = (int)(this.levelPixelPos_x / (float)this.blockSize);
		return this.levelPos_x;
	}
	
	@Override
	public int getLevelPos_y() 
	{
		this.levelPos_y = (int)(this.levelPixelPos_y / (float)this.blockSize);
		return this.levelPos_y;
	}
	@Override
	public float getMinPixelX()
	{
		return this.levelPixelPos_x;
	}	
	@Override
	public float getMaxPixelX()
	{
		if (this.orientation == E_MBOrientation.Horizontal)
			return this.levelPixelPos_x + (this.length * this.blockSize);
		else
			return this.levelPixelPos_x + this.blockSize;
	}
	
	public void InitLevelPos(int pos_x, int pos_y)
	{
		if (this.orientation == E_MBOrientation.Horizontal)
		{
			this.setLevelPos_x(pos_x + this.startPos);
			this.setLevelPos_y(pos_y);
		}
		else
		{
			this.setLevelPos_x(pos_x);
			this.setLevelPos_y(pos_y + this.startPos);
		}
	}
	
	public MBMovingPlatform(
			E_MBObjectColors color, E_MBOrientation orientation, 
			int length, int distance, int startPos, float speed,
			E_MBOrientation movingOrientation, float blockSize) 
	{
		super(color, orientation, E_MBPlatformEdgeType.All, length, blockSize, true);
		this.type = E_MBObjectTypes.MovingPlatform;
		this.movingOrientation = movingOrientation;
		this.distance = distance;
		this.startPos = startPos;
		this.motorSpeed = speed;
		
		if (orientation == E_MBOrientation.Horizontal)
			this.renderOrder = E_MBRenderOrder.HorizontalMovingPlatform.getIndex();
		else
			this.renderOrder = E_MBRenderOrder.VerticalMovingPlatform.getIndex();
	}
	
	private void UpdatePosition(float blockSize)
	{	
		float min, max;
		
		if (this.phBody != null)
		{
			if (this.movingOrientation == E_MBOrientation.Horizontal)
			{
				if (this.orientation == E_MBOrientation.Horizontal)
				{
					min = ((float)this.levelPos_x - (float)this.startPos + ((float)this.length / 2.0f)) * blockSize;
					max = ((float)this.levelPos_x - (float)this.startPos + ((float)this.length / 2.0f) + this.distance) * blockSize;
				}
				else
				{
					min = (this.levelPos_x - this.startPos) * blockSize + ((float)blockSize / 2.0f);
					max = (this.levelPos_x - this.startPos + this.distance) * blockSize + ((float)blockSize / 2.0f);
				}
				
				if (this.phBody.getPosition().x > max / SEPhysicsWorldNative.PHYSICS_SCALE)
					this.phBody.setLinearVelocity(new Vector2(-this.motorSpeed, 0.0f));
				
				if (this.phBody.getPosition().x < min / SEPhysicsWorldNative.PHYSICS_SCALE)
					this.phBody.setLinearVelocity(new Vector2(this.motorSpeed, 0.0f));
			}
			else
			{
				if (this.orientation == E_MBOrientation.Vertical)
				{
					min = (this.levelPos_y - this.startPos + ((float)this.length / 2.0f)) * blockSize;
					max = (this.levelPos_y - this.startPos + ((float)this.length / 2.0f) + this.distance) * blockSize;
				}
				else
				{
					min = (this.levelPos_y - this.startPos) * blockSize + ((float)blockSize / 2.0f);
					max = (this.levelPos_y - this.startPos + this.distance) * blockSize + ((float)blockSize / 2.0f);
				}
				
				if (this.phBody.getPosition().y > max / SEPhysicsWorldNative.PHYSICS_SCALE)
					this.phBody.setLinearVelocity(new Vector2(0.0f, -this.motorSpeed));
				
				if (this.phBody.getPosition().y < min / SEPhysicsWorldNative.PHYSICS_SCALE)
					this.phBody.setLinearVelocity(new Vector2(0.0f, this.motorSpeed));
			}
		}
	}

	@Override
	public void Render(float blockSize, int renderOffset_x, int renderOffset_y,
			float scaleX, float scaleY, float scrH) 
	{		
		this.blockSize = blockSize;
		
		this.UpdatePosition((float)blockSize);
		
		if (this.orientation == E_MBOrientation.Horizontal)
		{
			this.setPos((float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x, 
					(float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y);
			
			this.setSize((float)(this.length * blockSize) * scaleX, (float)blockSize * scaleY);
		}
		else
		{
			this.setPos((float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x, 
					(float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y);
			
			this.setSize((float)blockSize * scaleX, (float)(this.length * blockSize) * scaleY);
		}
		
		this.Render();
	}
}