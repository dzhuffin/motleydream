package com.spiritgames.motleydream.level;

import android.content.Context;

import com.spiritgames.motleydream.MBGameManager;
import com.spiritgames.motleydream.R;
import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.motleydream.enums.E_MBFreeBoxKind;
import com.spiritgames.motleydream.enums.E_MBGameEpisodes;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBOrientation;
import com.spiritgames.motleydream.enums.E_MBPlatformEdgeType;
import com.spiritgames.motleydream.enums.E_MBPrizeType;
import com.spiritgames.motleydream.enums.E_MBVerticalCameraPosition;
import com.spiritgames.motleydream.enums.E_MBHorizontalCameraPosition;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;
import com.spiritgames.spiritengine.io.SEIOProvider;

public class MBLevelLoader extends SEIOProvider 
{
	private boolean loadLevelInfoOnly = false;

	private MBLevelManager newLevel;
	private int maxStarCount = 0;
	
	private E_MBDifficultyLevel difLevel = E_MBDifficultyLevel.Normal;
	
	private int levelBackgroundResID = 0;
	
	public MBLevelLoader(boolean levelInfoOnly) 
	{
		super();
		
		this.loadLevelInfoOnly = levelInfoOnly;
	}

	@Override
	protected void loadCustom() 
	{		
		int levelWidth = this.readInt(); 
		int levelHeight = this.readInt();
		
		int ballPosX, ballPosY, targetPosX, targetPosY;
		
		targetPosX = this.readInt();
		targetPosY = this.readInt();
		
		ballPosX = this.readInt();
		ballPosY = this.readInt();
		
		if (!this.loadLevelInfoOnly)
		{
			this.newLevel = new MBLevelManager(ballPosX, ballPosY, targetPosX, targetPosY,
					this.levelBackgroundResID, this.difLevel);
		
			this.newLevel.setSize(levelWidth, levelHeight);
		}
		
		int platformsCount = this.readInt();
		int platformLength, platformPosX, platformPosY, platformOrientation, 
			platformType, platformEdgeType, platformVCamPos, platformHCamPos;
		
		for (int i = 0; i < platformsCount; i++)
		{
			platformLength = this.readInt();
			platformPosX = this.readInt();
			platformPosY = this.readInt();
			platformOrientation = this.readInt();
			platformType = this.readInt();
			platformEdgeType = this.readInt();
			platformVCamPos = this.readInt();
			platformHCamPos = this.readInt();
			
			if (!this.loadLevelInfoOnly)
			{
				this.newLevel.addPlatform(platformPosX, platformPosY, 
						E_MBObjectColors.getByIndex(platformType), 
						E_MBOrientation.getByIndex(platformOrientation), 
						E_MBPlatformEdgeType.getByIndex(platformEdgeType), 
						platformLength, 
						E_MBVerticalCameraPosition.getByIndex(platformVCamPos),
						E_MBHorizontalCameraPosition.getByIndex(platformHCamPos));
			}
		}
		
		int mPlatformsCount = this.readInt();
		int mPlatformLength, mPlatformPosX, mPlatformPosY, mPlatformOrientation, 
			mPlatformType, mPlatformDistance, mPlatformMovingOrientation, mPlatformStartPos, 
			mPlatformVCamPos, mPlatformHCamPos;
		
		for (int i = 0; i < mPlatformsCount; i++)
		{
			mPlatformLength = this.readInt();
			mPlatformPosX = this.readInt();
			mPlatformPosY = this.readInt();
			mPlatformOrientation = this.readInt();
			mPlatformType = this.readInt();
			mPlatformDistance = this.readInt();
			mPlatformMovingOrientation = this.readInt();
			mPlatformStartPos = this.readInt();
			mPlatformVCamPos = this.readInt();
			mPlatformHCamPos = this.readInt();
			
			if (!this.loadLevelInfoOnly)
			{
				this.newLevel.addMovingPlatform(mPlatformPosX, mPlatformPosY, E_MBObjectColors.getByIndex(mPlatformType), 
						E_MBOrientation.getByIndex(mPlatformOrientation), mPlatformLength, mPlatformDistance, mPlatformStartPos, 0.2f, 
						E_MBOrientation.getByIndex(mPlatformMovingOrientation),
						E_MBVerticalCameraPosition.getByIndex(mPlatformVCamPos),
						E_MBHorizontalCameraPosition.getByIndex(mPlatformHCamPos));
			}
		}
		
		int colorChangersCount = this.readInt();
		int colorChangersPosX, colorChangersPosY, colorChangersType;
		
		for (int i = 0; i < colorChangersCount; i++)
		{
			colorChangersPosX = this.readInt();
			colorChangersPosY = this.readInt();
			colorChangersType = this.readInt();
			
			if (!this.loadLevelInfoOnly)
			{
				this.newLevel.addColorChanger(colorChangersPosX, colorChangersPosY, 
						E_MBObjectColors.getByIndex(colorChangersType));
			}
		}
		
		int freeBoxCount = this.readInt();
		int freeBoxPosX, freeBoxPosY, freeBoxWidth, freeBoxHeight, freeBoxKind, freeBoxType, 
			freeBoxVCamPos, freeBoxHCamPos, freeBoxChainLength;
		boolean freeBoxRotating;
		
		for (int i = 0; i < freeBoxCount; i++)
		{
			freeBoxPosX = this.readInt();
			freeBoxPosY = this.readInt();
			freeBoxWidth = this.readInt();
			freeBoxHeight = this.readInt();
			freeBoxKind = this.readInt();
			freeBoxType = this.readInt();
			freeBoxRotating = (this.readInt() == 1) ? true : false;
			freeBoxVCamPos = this.readInt();
			freeBoxHCamPos = this.readInt();
			freeBoxChainLength = this.readInt();
			
			if (!this.loadLevelInfoOnly)
			{
				this.newLevel.addFreeBox(freeBoxPosX, freeBoxPosY, 
						E_MBObjectColors.getByIndex(freeBoxType), E_MBFreeBoxKind.getByIndex(freeBoxKind), 
						freeBoxWidth, freeBoxHeight, freeBoxRotating,
						E_MBVerticalCameraPosition.getByIndex(freeBoxVCamPos),
						E_MBHorizontalCameraPosition.getByIndex(freeBoxHCamPos),
						freeBoxChainLength);
			}
		}	
		
		this.maxStarCount = this.readInt();
		
		if (!this.loadLevelInfoOnly)
			this.newLevel.setStarsCount(this.readInt());
		
		int prizePosX, prizePosY, prizeType;
		
		for (int i = 0; i < this.maxStarCount; i++)
		{
			prizePosX = this.readInt();
			prizePosY = this.readInt();
			prizeType = this.readInt();
			
			if (!this.loadLevelInfoOnly)
			{
				this.newLevel.addPrize(prizePosX, prizePosY, E_MBPrizeType.getByIndex(prizeType));
			}
		}	
	}
	
	public MBLevelManager loadLevel(E_MBGameEpisodes episode, int levelNumber, E_MBDifficultyLevel difLevel)
	{
		this.difLevel = difLevel;
		
		if (this.loadLevelInfoOnly)
			return null;
		
		int levelToLoad = 0;
		if (levelNumber <= MBGameManager.getLevelsCount(episode))
		{
			if (episode == E_MBGameEpisodes.NIGHT)
			{
				levelToLoad = R.raw.lev_night_001 + levelNumber - 1;
				this.levelBackgroundResID = R.drawable.background_game_night;
			}
			else
			{
				if (episode == E_MBGameEpisodes.EVENING)
				{
					levelToLoad = R.raw.lev_evening_001 + levelNumber - 1;
					this.levelBackgroundResID = R.drawable.background_game_evening;
				}
				else
				{
					levelToLoad = R.raw.lev_day_001 + levelNumber - 1;
					this.levelBackgroundResID = R.drawable.background_game_day;
				}
			}
				
			this.loadFromResource(levelToLoad);
			
			this.newLevel.sortItemsByRenderOrder();
			
			return this.newLevel;
		}
		
		return null;
	}
	
	public int getLevelMaxStars(E_MBGameEpisodes episode, int levelNumber)
	{
		if (!this.loadLevelInfoOnly)
			return 0;
		
		int levelToLoad = 0;
		if (levelNumber <= MBGameManager.getLevelsCount(episode))
		{
			if (episode == E_MBGameEpisodes.NIGHT)
				levelToLoad = R.raw.lev_night_001 + levelNumber - 1;
			else
				levelToLoad = R.raw.lev_day_001 + levelNumber - 1;
			
			this.loadFromResource(levelToLoad);
			return this.maxStarCount;
		}
		
		return 0;
	}

	@Override
	protected void saveCustom() {}
}
