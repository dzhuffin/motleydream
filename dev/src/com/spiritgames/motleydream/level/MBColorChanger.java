package com.spiritgames.motleydream.level;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;

import com.spiritgames.motleydream.R;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBRenderOrder;
import com.spiritgames.spiritengine.graphics.SEAnimatedSprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class MBColorChanger extends MBMovingItem 
{
	private SEAnimatedSprite animation;

	public MBColorChanger(E_MBObjectColors color, float blockSize) 
	{
		super(color, blockSize, true);

		this.renderOrder = E_MBRenderOrder.ColorChanger.getIndex();
		
		this.type = E_MBObjectTypes.ColorChanger;
	}
	
	@Override
	public void release() 
    {		
        super.release();
        
        if (this.animation != null)
			this.animation.release();
    }
	
	@Override
	public void restore() 
    {		
        super.release();
        
        if (this.animation != null)
			this.animation.restore();
    }

	public void InitAnimation()
	{
		int animationRes = 0;
		
		if (this.color == E_MBObjectColors.Red)
			animationRes = R.drawable.changer_red_1;
		
		if (this.color == E_MBObjectColors.Green)
			animationRes = R.drawable.changer_green_1;
		
		if (this.color == E_MBObjectColors.Blue)
			animationRes = R.drawable.changer_blue_1;
		
		if (this.color == E_MBObjectColors.Yellow)
			animationRes = R.drawable.changer_yellow_1;
		

		if (animationRes == 0)
			return;
		
		this.animation = new SEAnimatedSprite(true);
		this.animation.loadAnimation(animationRes, 6, 150);	
		
		this.animation.setAnimationLoop(true);		
		this.animation.startAnimation();
	}

	
	@Override
	public void Render(float blockSize, int renderOffset_x, int renderOffset_y,
			float scaleX, float scaleY, float scrH) 
	{
		this.animation.setPos((float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x, 
				(float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y);
		
		this.animation.setSize(blockSize * 2.0f * scaleX, blockSize * 2.0f * scaleY);
		
		this.animation.Render();
	}
}
