package com.spiritgames.motleydream.level;

import java.util.Collections;
import java.util.Vector;

import android.graphics.PointF;
import android.os.SystemClock;
import android.os.Vibrator;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.spiritgames.motleydream.MBManagers;
import com.spiritgames.motleydream.MBSoundManager;
import com.spiritgames.motleydream.enums.E_MBCameraPosition;
import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.motleydream.enums.E_MBFreeBoxKind;
import com.spiritgames.motleydream.enums.E_MBHorizontalCameraPosition;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBOrientation;
import com.spiritgames.motleydream.enums.E_MBPlatformEdgeType;
import com.spiritgames.motleydream.enums.E_MBPrizeType;
import com.spiritgames.motleydream.enums.E_MBVerticalCameraPosition;
import com.spiritgames.spiritengine.graphics.SEShiftingSprite;
import com.spiritgames.spiritengine.graphics.SESimpleShiftingParams;
import com.spiritgames.spiritengine.graphics.SESprite;
import com.spiritgames.spiritengine.physics.SEPhysicsWorldNative;

public class MBLevelManager 
{
	private MBDifficultyLevel difficultyLevel = new MBDifficultyLevel();
	
	private Vibrator vibrator = null;
	public void setVibrator(Vibrator vibrator) {
		this.vibrator = vibrator;
	}
	private void vibrate(long milliseconds)
	{
		if (this.vibrator != null)
			this.vibrator.vibrate(milliseconds);
	}

	private int background_resID;
	
	private Vector<MBColoredItem> items;
	private MBBall ball;
	private MBTarget target;
	
	private SESprite backGround;
	
	private int showingFinish = 0;
	private boolean showingFinishComplete = false;
	public boolean isJustShowingFinishComplete()
	{
		boolean result = this.showingFinishComplete;
		this.showingFinishComplete = false;
		return result;
	}
	
	private float blockSize = 10.0f;
	
	private E_MBCameraPosition cameraPosition = E_MBCameraPosition.MiddleCenter;
	
	private float old_ball_x = 0.0f;
	private float ball_x = 0.0f;
	public float getBall_x() 
	{
		return this.ball_x;
	}
	private float old_ball_y = 0.0f;
	private float ball_y = 0.0f;
	public float getBall_y() 
	{
		return this.ball_y;
	}
	
	private float target_x = 0.0f;
	public float getTarget_x() 
	{
		return this.target_x;
	}
	private float target_y = 0.0f;
	public float getTarget_y() 
	{
		return this.target_y;
	}
	
	private float renderOffset_x = 0.0f;
	private float renderOffset_y = 0.0f;
	
	private int starsCount = 0;	
	public void setStarsCount(int starsCount) {
		this.gameInfo.setStarsCount(starsCount);
	}

	private int width = 0;
	public int getWidth() 
	{
		return this.width;
	}

	private int height = 0;
	public int getHeight() 
	{
		return this.height;
	}
	public void setSize(int width, int height) 
	{
		this.width = width;
		this.height = height;
	}
	
	private float scaleX = 1.0f;
	
	public float getScaleX() 
	{
		return scaleX;
	}
	public void setScaleX(float scaleX) 
	{
		this.scaleX = scaleX;
	}
	public void incScaleX(float dx) 
	{
		this.scaleX += dx;
	}

	private float scaleY = 1.0f;
	public float getScaleY() 
	{
		return scaleY;
	}
	public void setScaleY(float scaleY) 
	{
		this.scaleY = scaleY;
	}
	public void incScaleY(float dy) 
	{
		this.scaleY += dy;
	}
	
	public void BallJump()
	{
		if (this.contactListener.canJump())
		{
			this.phWorld.applyForce(this.physicsBallNumber, 0.0f, this.difficultyLevel.getBallJumpForce());
			MBSoundManager soundManager = MBManagers.getSoundManager();
			if (soundManager != null)
				soundManager.playSound(MBSoundManager.SOUND_JUMP);
		}
	}
	
	private float lastMoveDirection = 0.0f;
	public void BallMove(float direction)
	{
		this.lastMoveDirection = direction;
		this.phWorld.applyForce(this.physicsBallNumber, this.difficultyLevel.getBallMoveForce() * direction, 0.0f);
	}
	
	//private SEPhysicsWorld phWorld;
	private SEPhysicsWorldNative phWorld;
	//private MBContactListener contactListener;
	private MBContactListenerNative contactListener;
	private int physicsBallNumber = -1;
	private int physicsTargetNumber = -1;
	
	// game info
	public MBGameInfo gameInfo;	
	private long startTicks = SystemClock.uptimeMillis();
	private boolean dead = false; // ����� �������� ������ ������ �����������
	private boolean deadStart = false; // ����� ��� ������ ��� ������������� � ������������ �� ������ �����
	
	private boolean finish = false; // ����� �������� ��������� �� ����� �����������
	private boolean finishStart = false; // ����� ��� ������ ��� ����� �� �����
	
	public boolean isDead(){
		boolean result = this.dead;
		this.dead = false;
		return result;
	}

	public MBLevelManager(	int startPos_x, int startPos_y,
							int finPos_x, int finPos_y,
							int background_resID, 
							E_MBDifficultyLevel difLevel)
	{
		this.difficultyLevel.setCurrentLevel(difLevel);
		
		//this.blockSize = con.getResources().getDisplayMetrics().heightPixels / 14.0f;
		this.blockSize = 34.0f;
		
		this.showingFinish = 4;
		
		this.items = new Vector<MBColoredItem>();
		this.gameInfo = new MBGameInfo();

		this.background_resID = background_resID;
		
		this.backGround = new SESprite(true);
		this.backGround.setCurrentTexture(this.backGround.addTexture(this.background_resID));		
		
		this.ball = new MBBall(E_MBObjectColors.Red, this.blockSize);		
		this.ball.setLevelPos_x(startPos_x);
		this.ball.setLevelPos_y(startPos_y);
		this.items.add(this.ball);
		
		//this.contactListener = new MBContactListener();
		this.contactListener = new MBContactListenerNative();
		this.phWorld = new SEPhysicsWorldNative(0.0f, 1.0f, (float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels,
															(float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels);
		//this.phWorld = new SEPhysicsWorld(-100.0f, -100.0f, 500.0f, 500.0f, 0.0f, 1.0f,
		//													(float)this.context.getResources().getDisplayMetrics().widthPixels,
		//													(float)this.context.getResources().getDisplayMetrics().heightPixels);
		this.phWorld.setContactListener(this.contactListener);		
		this.physicsBallNumber = this.phWorld.addBall((startPos_x * this.blockSize + this.blockSize / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
				(startPos_y * this.blockSize + this.blockSize / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, (this.blockSize * 0.9f / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
				this.difficultyLevel.getBallDensity(), this.difficultyLevel.getBallElasticity(), 
				false, new MBContactType(this.ball, this.ball.type, E_MBObjectColors.Red));
		
		this.addTarget(finPos_x, finPos_y);
		this.target = ((MBTarget)this.items.get(this.items.size() - 1));
		
		//Shape ballShape = this.phWorld.getShape(this.physicsBallNumber);
		Fixture ballShape = this.phWorld.getShape(this.physicsBallNumber);
		if (ballShape != null)
		{
			this.contactListener.setBallBody(ballShape.getBody());
			this.ball.setPhysicBody(ballShape.getBody());
		}
		
		//Shape targetShape = this.phWorld.getShape(this.physicsTargetNumber);
		Fixture targetShape = this.phWorld.getShape(this.physicsTargetNumber);
		if (targetShape != null)
		{
			this.contactListener.setTargetBody(targetShape.getBody());
			this.target.setPhysicBody(targetShape.getBody());
		}
	}
	
	public void addColorChanger(int levelPos_x, int levelPos_y, E_MBObjectColors color)
	{		
		MBColorChanger newColorChanger = new MBColorChanger(color, this.blockSize);
		newColorChanger.setLevelPos_x(levelPos_x);
		newColorChanger.setLevelPos_y(levelPos_y);
		newColorChanger.setLevelPixelPos_x((levelPos_x * this.blockSize + this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE);
		newColorChanger.setLevelPixelPos_y((levelPos_y * this.blockSize + this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE);
		newColorChanger.InitAnimation();
		
		this.phWorld.addSensorBall(((levelPos_x * this.blockSize + this.blockSize)) / SEPhysicsWorldNative.PHYSICS_SCALE, 
				((levelPos_y * this.blockSize + this.blockSize)) / SEPhysicsWorldNative.PHYSICS_SCALE, this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE,
				new MBContactType(newColorChanger, newColorChanger.type, color));
		
		newColorChanger.setPhysicBody(this.phWorld.getLastBody());
		
		this.items.add(newColorChanger);
	}
	
	public void addTarget(int levelPos_x, int levelPos_y)
	{		
		MBTarget newTarget = new MBTarget(this.blockSize);
		newTarget.setLevelPos_x(levelPos_x);
		newTarget.setLevelPos_y(levelPos_y);
		this.target_x = (levelPos_x * this.blockSize + this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE;
		this.target_y = (levelPos_y * this.blockSize + this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE;
		newTarget.setLevelPixelPos_x(this.target_x);
		newTarget.setLevelPixelPos_y(this.target_y);
		this.target_x *= SEPhysicsWorldNative.PHYSICS_SCALE;
		this.target_y *= SEPhysicsWorldNative.PHYSICS_SCALE;
		newTarget.InitPortalAnimation();
		
		this.physicsTargetNumber = this.phWorld.addSensorBall(((levelPos_x * (this.blockSize) + this.blockSize)) / SEPhysicsWorldNative.PHYSICS_SCALE, 
															  ((levelPos_y * (this.blockSize) + this.blockSize * 2.0f)) / SEPhysicsWorldNative.PHYSICS_SCALE, this.blockSize * 1.2f / SEPhysicsWorldNative.PHYSICS_SCALE,
															  new MBContactType(newTarget, newTarget.type));
		
		newTarget.setPhysicBody(this.phWorld.getLastBody());
		
		this.items.add(newTarget);
	}
	
	public void addFreeBox(int levelPos_x, int levelPos_y, E_MBObjectColors color, E_MBFreeBoxKind kind, int xSize, int ySize, 
			boolean rotating, E_MBVerticalCameraPosition vCamPos, E_MBHorizontalCameraPosition hCamPos,
			int freeBoxChainLength)
	{		
		MBFreeBox newFreeBox = new MBFreeBox(color, levelPos_x, levelPos_y, xSize, ySize, kind, rotating, this.blockSize, freeBoxChainLength);
		if (kind == E_MBFreeBoxKind.Box)
		{
			this.phWorld.addFreeBox((levelPos_x * this.blockSize + (xSize * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
					(levelPos_y * this.blockSize + (ySize * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
					(xSize * this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, (ySize * this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, 
					this.difficultyLevel.getFreeObjectsElasticity(), rotating, new MBContactType(newFreeBox, newFreeBox.type, color, vCamPos, hCamPos));
			
			newFreeBox.setPhysicBody(this.phWorld.getLastBody());
		}
		
		if (kind == E_MBFreeBoxKind.Ball)
		{
			
			this.phWorld.addBall((levelPos_x * this.blockSize + (xSize * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
					(levelPos_y * this.blockSize + (xSize * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
					(xSize * this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE / 2.0f, (rotating) ? 1.0f : 0.02f, this.difficultyLevel.getFreeObjectsElasticity(), 
					rotating, new MBContactType(newFreeBox, newFreeBox.type, color, vCamPos, hCamPos));
			
			newFreeBox.setPhysicBody(this.phWorld.getLastBody());
		}
		
		if (kind == E_MBFreeBoxKind.Tip)
		{
			newFreeBox.setLevelPixelPos_x((levelPos_x * this.blockSize + (xSize * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE);
			newFreeBox.setLevelPixelPos_y((levelPos_y * this.blockSize + (ySize * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE);
		}
		
		this.items.add(newFreeBox);
	}
	
	public void addPrize(int levelPos_x, int levelPos_y, E_MBPrizeType type)
	{		
		MBPrize newPrize = new MBPrize(type, this.blockSize);

		this.phWorld.addSensorBall((levelPos_x * this.blockSize + this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								   (levelPos_y * this.blockSize + this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, 
									this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, 
									new MBContactType(newPrize, newPrize.type));			
		
		newPrize.setPhysicBody(this.phWorld.getLastBody());
		
		this.items.add(newPrize);
	}
	
	public void addPlatform(int levelPos_x, int levelPos_y, E_MBObjectColors color, E_MBOrientation orientation, E_MBPlatformEdgeType edgeType, int length, 
			E_MBVerticalCameraPosition vCamPos, E_MBHorizontalCameraPosition hCamPos)
	{		
		MBPlatform newPlatform = new MBPlatform(color, orientation, edgeType, length, this.blockSize, true);				
		newPlatform.setLevelPos_x(levelPos_x);
		newPlatform.setLevelPos_y(levelPos_y);
		this.items.add(newPlatform);
		
		if (orientation == E_MBOrientation.Horizontal)
		{
			if (color == E_MBObjectColors.Floor)
			{
				this.phWorld.addGroundBox((levelPos_x * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(levelPos_y * this.blockSize + this.blockSize / 1.2f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(length * this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(this.blockSize / 2.3f) / SEPhysicsWorldNative.PHYSICS_SCALE, this.difficultyLevel.getHorisontalElasticity(), 
						new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));
			}
			else
			{
				this.phWorld.addGroundBox((levelPos_x * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(levelPos_y * this.blockSize + this.blockSize / 4.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(length * this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(this.blockSize / 2.3f) / SEPhysicsWorldNative.PHYSICS_SCALE, this.difficultyLevel.getHorisontalElasticity(), 
						new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));
			}
		}
		else
		{
			if (color == E_MBObjectColors.LeftWall || color == E_MBObjectColors.RightWall)
			{
				this.phWorld.addGroundBox((levelPos_x * this.blockSize + this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(levelPos_y * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						this.blockSize * 1.8f / SEPhysicsWorldNative.PHYSICS_SCALE, length * this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, 
						this.difficultyLevel.getVerticalElasticity(), new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));	
			}
			else 
			{
				this.phWorld.addGroundBox((levelPos_x * this.blockSize + this.blockSize / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						(levelPos_y * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
						this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, length * this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, 
						this.difficultyLevel.getVerticalElasticity(), new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));
			}
		}
	}
	
	public void addMovingPlatform(int levelPos_x, int levelPos_y, E_MBObjectColors color, E_MBOrientation orientation, 
			int length, int distance, int startPos, float speed, E_MBOrientation movingOrientation,
			E_MBVerticalCameraPosition vCamPos, E_MBHorizontalCameraPosition hCamPos)
	{
		MBMovingPlatform newPlatform = new MBMovingPlatform(color, orientation, 
				length, distance, startPos, speed, movingOrientation, this.blockSize);
		newPlatform.InitLevelPos(levelPos_x, levelPos_y);				
		
		
		if (orientation == E_MBOrientation.Horizontal)
		{
			if (movingOrientation == E_MBOrientation.Horizontal)
						this.phWorld.addMovingGroundBox(((levelPos_x + startPos) * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								(levelPos_y * this.blockSize + this.blockSize / 2.0f - this.blockSize / 4.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								(length * this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, (this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE) / 2.0f, speed, false, this.difficultyLevel.getHorisontalElasticity(), new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));
			else
						this.phWorld.addMovingGroundBox((levelPos_x * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								((levelPos_y + startPos) * this.blockSize + this.blockSize / 2.0f - this.blockSize / 4.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								(length * this.blockSize) / SEPhysicsWorldNative.PHYSICS_SCALE, (this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE) / 2.0f, speed, true, this.difficultyLevel.getHorisontalElasticity(), new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));
		}
		else
		{
			if (movingOrientation == E_MBOrientation.Horizontal)
						this.phWorld.addMovingGroundBox(((levelPos_x + startPos) * this.blockSize + this.blockSize / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								(levelPos_y * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, length * this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, speed, false, this.difficultyLevel.getVerticalElasticity(), new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));
			else
						this.phWorld.addMovingGroundBox((levelPos_x * this.blockSize + this.blockSize / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								((levelPos_y + startPos) * this.blockSize + (length * this.blockSize) / 2.0f) / SEPhysicsWorldNative.PHYSICS_SCALE, 
								this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, length * this.blockSize / SEPhysicsWorldNative.PHYSICS_SCALE, speed, true, this.difficultyLevel.getVerticalElasticity(), new MBContactType(newPlatform, newPlatform.type, color, vCamPos, hCamPos));
		}
		newPlatform.setPhysicBody(this.phWorld.getLastBody());
		
		this.items.add(newPlatform);
	}
	
	public void release()
	{			
		for (int i = 0 ; i < this.items.size(); i++)
			this.items.get(i).release();
		
		this.items.clear();
		this.items = null;
		
		this.gameInfo.release();
		
		this.phWorld.release();
		
		
		if (MBManagers.getSoundManager() != null)
			MBManagers.getSoundManager().stopBackgroundMusic();
	}
	
	public void restore()
	{			
		for (int i = 0 ; i < this.items.size(); i++)
			this.items.get(i).restore();
		
		this.backGround.restore();
	}
	
	public boolean isFinished()
	{
		return (this.finish && (!this.dead) && (!this.deadStart));
	}
	
	private boolean cameraOnPosition = false;
	private PointF currentCameraOffset = E_MBCameraPosition.MiddleCenter.getOffset();
	private E_MBCameraPosition oldCameraPosition = E_MBCameraPosition.MiddleCenter;
	private void updateRenderOffsets(float targetX, float targetY, float scaleX, float scaleY, 
			float screenW, float screenH, E_MBCameraPosition camPos, boolean force)
	{
		PointF cameraOffset = camPos.getOffset();
		
		//float xDensity = ((float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels) / 480.0f;
		//float yDensity = ((float)MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels) / 320.0f;
		
		float xDensity = MBManagers.getApplicationContext().getResources().getDisplayMetrics().density;
		float yDensity = MBManagers.getApplicationContext().getResources().getDisplayMetrics().density;
		
		float centerX = 0.0f; 
		float centerY = 0.0f;
		
		if (!force)
		{
			if (this.oldCameraPosition != camPos && 
			  !(this.currentCameraOffset.x == cameraOffset.x && this.currentCameraOffset.y == cameraOffset.y))
			{
				this.oldCameraPosition = camPos;
				this.cameraOnPosition = false;
				
				this.cameraOffset_dx = 0.0f;
				this.cameraOffset_dy = 0.0f;
				
				PointF start = new PointF(this.currentCameraOffset.x, this.currentCameraOffset.y);
				PointF finish = new PointF(cameraOffset.x, cameraOffset.y);
				float speed = 0.0f;
				float velocity = 5.0f;
				this.cameraPositionParams = new SESimpleShiftingParams(start, finish, speed, velocity, xDensity, yDensity);
				
				this.cameraPositionTick = SystemClock.uptimeMillis();
			}
			
			if (!this.cameraOnPosition)
			{
				this.currentCameraOffset = this.calcNewCameraPos();

				centerX = targetX + this.currentCameraOffset.x / scaleX; 
				centerY = targetY + this.currentCameraOffset.y / scaleY;
			}
			else
			{
				this.currentCameraOffset = cameraOffset;
				
				centerX = targetX + cameraOffset.x / scaleX; 
				centerY = targetY + cameraOffset.y / scaleY;
			}
		}
		else
		{
			//this.currentCameraOffset = cameraOffset;
			
			centerX = targetX + cameraOffset.x / scaleX; 
			centerY = targetY + cameraOffset.y / scaleY;
		}
		
		this.renderOffset_x = (screenW / 2.0f - centerX * scaleX);
		this.renderOffset_y = (screenH / 2.0f - (screenH - centerY) * scaleY);
		
		int maxBallX = (int)((screenW / 2.0f) - (this.blockSize * (float)this.width) * scaleX) + (int)(screenW / 2.0f);
		int maxBallY = (int)((screenH / 2.0f) - (screenH - this.blockSize * (float)this.height) * scaleY) - (int)(screenH / 2.0f);
		
		//if (this.showingFinish <= 0)
		//{
			if (this.renderOffset_x > 0)
				this.renderOffset_x = 0;
			
			if (this.renderOffset_x < maxBallX)
				this.renderOffset_x = maxBallX;		
			
			if (this.renderOffset_y < 0)
				this.renderOffset_y = 0;
			
			if (this.renderOffset_y > maxBallY)
				this.renderOffset_y = maxBallY;
		//}

		/*float backW = (float)this.width * this.blockSize;
		float backH = (float)this.height * this.blockSize;
		
		float ballPosFactorX = 1.0f - centerX / (backW * 2.0f);
		float ballPosFactorY = 1.0f - centerY / (backH * 2.0f);
		
		this.backGround.setPos(backW * ballPosFactorX, backH * ballPosFactorY);
		this.backGround.setSize(backW * 2.0f, backH * 2.0f);*/
			
		this.backGround.setPos(screenW / 2.0f, screenH / 2.0f);
		this.backGround.setSize(screenW, screenH);
	}
	
	/*
	 * ��������� ��� �� ������� ������ � ������, � ���� ���, �� ���� ��������
	 */
	private MBPrize lastPrize = null;
	private void processPrizes()
	{
		MBColoredItem lastStar = this.contactListener.getLastStarCollected();
		MBColoredItem lastMegaJump = this.contactListener.getLastMegaJumpCollected();
		
		if (lastStar != null)
		{
			if (lastStar.getClass() == MBPrize.class)
			{
				this.lastPrize = ((MBPrize)lastStar);
				this.lastPrize.showCollectAnimation();
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_BALL_PRIZE);
			}
		}
		else
		{
			if (this.lastPrize != null && !this.lastPrize.isCollectAnimationPlaying())
			{
				this.phWorld.deleteBody(this.lastPrize.getPhysicBody());
				this.lastPrize.release();
				this.items.remove(this.lastPrize);
				
				this.lastPrize = null;
				
				this.gameInfo.collectStar();
			}
		}
		
		if (lastMegaJump != null)
		{
			this.phWorld.deleteBody(lastMegaJump.getPhysicBody());
			lastMegaJump.release();
			this.items.remove(lastMegaJump);
			
			this.gameInfo.collectMegaJump();
		}
	}
		
	private SESimpleShiftingParams showingFinishParams;
	private SESimpleShiftingParams cameraPositionParams;
	private long showingFinishlastTick = 0;
	private long cameraPositionTick = 0;
	private float showingFinishOld_dx = 0.0f;
	private float showingFinishOld_dy = 0.0f;
	
	private float cameraOffset_dx = 0.0f;
	private float cameraOffset_dy = 0.0f;
	private PointF calcNewCameraPos()
	{
		PointF result = new PointF(0.0f, 0.0f);
		
		if (this.cameraPositionParams == null)
			return result;
		
		float time = (float)(SystemClock.uptimeMillis() - this.cameraPositionTick) / SEShiftingSprite.TIME_RATIO;
		
		float dx = this.cameraPositionParams.Speed.x * time + // vx0 * t
		   this.cameraPositionParams.Velocity.x * time * time / 2.0f; // ax * t^2 / 2;

		float dy = this.cameraPositionParams.Speed.y * time + // vy0 * t
				   this.cameraPositionParams.Velocity.y * time * time / 2.0f; // ay * t^2 / 2
		
		if (((dx >= Math.abs(this.cameraPositionParams.Length.x)) && (Math.abs(this.cameraPositionParams.Length.x) > 0.0f)) ||
			((dy >= Math.abs(this.cameraPositionParams.Length.y)) && (Math.abs(this.cameraPositionParams.Length.y) > 0.0f)) ||
				(Math.abs(this.cameraOffset_dx) > Math.abs(dx)) || (Math.abs(this.cameraOffset_dy) > Math.abs(dy)))
		{
			if (this.cameraPositionParams.Direction.x < 0.0f)
				dx *= -1;
			
			if (this.cameraPositionParams.Direction.y < 0.0f)
				dy *= -1;
			
			result.x = this.cameraPositionParams.Start.x + dx;
			result.y = this.cameraPositionParams.Start.y + dy;

			this.cameraOnPosition = true;
			this.cameraOffset_dx = 0.0f;
			this.cameraOffset_dy = 0.0f;
			return result;
		}
		
		this.cameraOffset_dx = dx;
		this.cameraOffset_dy = dy;
		
		if (this.cameraPositionParams.Direction.x < 0.0f)
			dx *= -1;
		
		if (this.cameraPositionParams.Direction.y < 0.0f)
			dy *= -1;
		
		result.x = this.cameraPositionParams.Start.x + dx;
		result.y = this.cameraPositionParams.Start.y + dy;
		
		return result;
	}
	
	private PointF calcNewShowingFinishPos()
	{
		PointF result = new PointF(0.0f, 0.0f);
		
		float time = (float)(SystemClock.uptimeMillis() - this.showingFinishlastTick) / SEShiftingSprite.TIME_RATIO;
		
		float dx = this.showingFinishParams.Speed.x * time + // vx0 * t
		   		   this.showingFinishParams.Velocity.x * time * time / 2.0f; // ax * t^2 / 2;

		float dy = this.showingFinishParams.Speed.y * time + // vy0 * t
				   this.showingFinishParams.Velocity.y * time * time / 2.0f; // ay * t^2 / 2
		
		if (((dx >= Math.abs(this.showingFinishParams.Length.x)) && (Math.abs(this.showingFinishParams.Length.x) > 0.0f)) ||
			((dy >= Math.abs(this.showingFinishParams.Length.y)) && (Math.abs(this.showingFinishParams.Length.y) > 0.0f)) ||
				(Math.abs(this.showingFinishOld_dx) > Math.abs(dx)) || (Math.abs(this.showingFinishOld_dy) > Math.abs(dy)))
		{
			if (this.showingFinishParams.Direction.x < 0.0f)
				dx *= -1;
			
			if (this.showingFinishParams.Direction.y < 0.0f)
				dy *= -1;
			
			result.x = this.showingFinishParams.Start.x + dx;
			result.y = this.showingFinishParams.Start.y + dy;
			
			this.showingFinish--;
			this.showingFinishOld_dx = 0.0f;
			this.showingFinishOld_dy = 0.0f;
			return result;
		}
		
		this.showingFinishOld_dx = dx;
		this.showingFinishOld_dy = dy;
		
		if (this.showingFinishParams.Direction.x < 0.0f)
			dx *= -1;
		
		if (this.showingFinishParams.Direction.y < 0.0f)
			dy *= -1;
		
		result.x = this.showingFinishParams.Start.x + dx;
		result.y = this.showingFinishParams.Start.y + dy;
		
		return result;
	}
	
	public void skipShowingFinish() 
	{
		this.showingFinish = 0;
		this.showingFinishComplete = true;
		MBManagers.getSoundManager().enableGameplayMusic();
	}
	
	public boolean isShowingFinish()
	{
		return ((this.showingFinish > 0) && (this.showingFinish < 4));
	}
	
	private void updateCameraPosition()
	{
		MBColoredItem notBallItem = this.contactListener.getLastNotBallObject();
		MBMovingPlatform currentMovingPlatform = (notBallItem != null && notBallItem.getClass() == MBMovingPlatform.class) ?
				(MBMovingPlatform)notBallItem : null;
		if (currentMovingPlatform != null)
		{
			if (this.contactListener.getCurrentHCameraPosition() == E_MBHorizontalCameraPosition.Free)
			{
				Vector2 dir = currentMovingPlatform.getLinearVelocity();
				
				if (currentMovingPlatform.getOrientation() == E_MBOrientation.Horizontal)
				{
					if (currentMovingPlatform.getMovingOrientation() == E_MBOrientation.Horizontal)
					{
						if (dir.x != 0.0f)
						{
							if (dir.x > 0.0f)
								this.cameraPosition = this.cameraPosition.getSwitchedToLeft();
							else
								this.cameraPosition = this.cameraPosition.getSwitchedToRight();
						}
					}
				}
			}
			
			if (this.contactListener.getCurrentVCameraPosition() == E_MBVerticalCameraPosition.Free)
			{
				Vector2 dir = currentMovingPlatform.getLinearVelocity();
				
				if (currentMovingPlatform.getOrientation() == E_MBOrientation.Horizontal)
				{
					if (currentMovingPlatform.getMovingOrientation() == E_MBOrientation.Vertical)
					{
						if (dir.y != 0.0f)
						{
							if (dir.y > 0.0f)
								this.cameraPosition = this.cameraPosition.getSwitchedToTop();
							else
								this.cameraPosition = this.cameraPosition.getSwitchedToBottom();
						}
					}
				}
			}
		} else if (this.contactListener.getCurrentHCameraPosition() == E_MBHorizontalCameraPosition.Free)
		{
			if (this.ball_x - this.old_ball_x > 0.0f && this.lastMoveDirection > 0.0f)
				this.cameraPosition = this.cameraPosition.getSwitchedToLeft();
			
			if (this.old_ball_x - this.ball_x > 0.0f && this.lastMoveDirection < 0.0f)
				this.cameraPosition = this.cameraPosition.getSwitchedToRight();
		}
		
		if (this.contactListener.getCurrentHCameraPosition() == E_MBHorizontalCameraPosition.Left)
			this.cameraPosition = this.cameraPosition.getSwitchedToLeft();
		
		if (this.contactListener.getCurrentHCameraPosition() == E_MBHorizontalCameraPosition.Right)
			this.cameraPosition = this.cameraPosition.getSwitchedToRight();
		
		if (this.contactListener.getCurrentHCameraPosition() == E_MBHorizontalCameraPosition.Center)
			this.cameraPosition = this.cameraPosition.getSwitchedToMiddle();
		
		if (this.contactListener.getCurrentVCameraPosition() == E_MBVerticalCameraPosition.Bottom)
			this.cameraPosition = this.cameraPosition.getSwitchedToBottom();
		
		if (this.contactListener.getCurrentVCameraPosition() == E_MBVerticalCameraPosition.Top)
			this.cameraPosition = this.cameraPosition.getSwitchedToTop();
		
		if (this.contactListener.getCurrentVCameraPosition() == E_MBVerticalCameraPosition.Center)
			this.cameraPosition = this.cameraPosition.getSwitchedToCenter();
	}
	
	public void sortItemsByRenderOrder()
	{
		Collections.sort(this.items, new MBLevelItem.RenderOrderComparator());
	}
	
	public void Render(float screenW, float screenH, boolean paused) 
	{	
		this.updateCameraPosition();
		
		float renderScaleX;
		float renderScaleY;
		
		if ((this.showingFinish > 0) && (this.showingFinish < 4))
		{
			renderScaleX = 1.45f;
			renderScaleY = 1.45f;
		}
		else
		{
			renderScaleX = this.scaleX;
			renderScaleY = this.scaleY;
		}
		
		PointF centerPos;
		
		if (!paused)
		{
			this.gameInfo.setPlayTime(SystemClock.uptimeMillis() - this.startTicks);
			
			this.contactListener.newStep();
			this.phWorld.update();
			
			this.processPrizes();
			
			if (this.contactListener.isBallContacted())
			{
				MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_BALL_CONTACT);
				this.vibrate(50);
			}

			this.updateRenderOffsets(this.ball_x, this.ball_y, renderScaleX, renderScaleY, screenW, screenH, this.cameraPosition, false);
		}
		else
		{
			int oldShowingFinish = this.showingFinish;
			centerPos = (this.showingFinish > 0) && (this.showingFinish < 4) ?
					this.calcNewShowingFinishPos() :
					new PointF(this.ball_x, this.ball_y);

			this.updateRenderOffsets(centerPos.x, centerPos.y, renderScaleX, renderScaleY, screenW, screenH, E_MBCameraPosition.MiddleCenter, true);

			if ((this.showingFinish != oldShowingFinish) && (this.showingFinish == 0))
			{
				this.showingFinishComplete = true;
				this.updateRenderOffsets(this.ball_x, this.ball_y, renderScaleX, renderScaleY, screenW, screenH, this.cameraPosition, true);
			}
		}
						
		this.backGround.Render();
		
		for (int i = 0; i < this.items.size(); i++)
		{
			if (this.items.get(i).getClass() == MBBall.class)
			{
				this.old_ball_x = this.ball_x;
				this.old_ball_y = this.ball_y;
				
				MBBall mbBall = (MBBall)this.items.get(i);
				this.ball_x = this.phWorld.getBodyXPosition(this.physicsBallNumber);
				this.ball_y = this.phWorld.getBodyYPosition(this.physicsBallNumber);				
				mbBall.setLevelPixelPos_x(this.ball_x);
				mbBall.setLevelPixelPos_y(this.ball_y);
				this.ball_x *= SEPhysicsWorldNative.PHYSICS_SCALE;
				this.ball_y *= SEPhysicsWorldNative.PHYSICS_SCALE;
				mbBall.setAngle(this.phWorld.getBodyAngle(this.physicsBallNumber));
				mbBall.setColorFromUserData(this.phWorld.getShape(this.physicsBallNumber).getBody().getUserData());
				
				if (this.showingFinish == 4)
				{	
					MBManagers.getSoundManager().enableGameplayMusic();
					
					this.showingFinishOld_dx = 0.0f;
					this.showingFinishOld_dy = 0.0f;
					
					PointF start = new PointF(this.ball_x, this.ball_y);
					PointF finish = new PointF(this.target_x, this.target_y);
					float speed = 0.0f;
					float velocity = 5.0f;
					float xDensity = MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480;
					float yDensity = MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320;
					this.showingFinishParams = new SESimpleShiftingParams(start, finish, speed, velocity, xDensity, yDensity);
					
					this.showingFinishlastTick = SystemClock.uptimeMillis();
					
					this.showingFinish--;
				}
				
				if (this.showingFinish == 2)
				{	
					this.showingFinish--;
					
					this.showingFinishOld_dx = 0.0f;
					this.showingFinishOld_dy = 0.0f;
					
					PointF start = new PointF(this.showingFinishParams.Finish.x, this.showingFinishParams.Finish.y);
					PointF finish = new PointF(this.showingFinishParams.Start.x, this.showingFinishParams.Start.y);
					float speed = 0.0f;
					float velocity = 5.0f;					
					float xDensity = MBManagers.getApplicationContext().getResources().getDisplayMetrics().widthPixels / 480;
					float yDensity = MBManagers.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 320;
					this.showingFinishParams = new SESimpleShiftingParams(start, finish, speed, velocity, xDensity, yDensity);
					
					this.showingFinishlastTick = SystemClock.uptimeMillis();
				}
				
				if (this.contactListener.isColorChanged())
				{
					mbBall.showColorChangeAnimation();					
					MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_COLOR_CHANGE);
					this.vibrate(200);
				}
				
				if ((this.deadStart) && (!mbBall.isDeadAnimationPlaying()))
				{
					this.deadStart = false;
					this.dead = true;
					return;
				}
				
				if ((this.contactListener.isDead()) && (!this.deadStart))
				{
					this.vibrate(700);
					this.deadStart = true;
					mbBall.showDeadAnimation();
					MBManagers.getSoundManager().stopBackgroundMusic();
					MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_BALL_DEAD);
				}
				
				if ((this.finishStart) && (!mbBall.isFinishAnimationPlaying()))
				{
					this.finishStart = false;
					this.finish = true;
					return;
				}
				
				if ((this.contactListener.isFinished()) && (!this.finishStart))
				{
					this.finishStart = true;
					mbBall.showFinishAnimation();
					MBManagers.getSoundManager().stopBackgroundMusic();
					MBManagers.getSoundManager().playSound(MBSoundManager.SOUND_BALL_FINISH);
				}
			}		
						
			if (this.items.get(i).getClass() == MBMovingPlatform.class)
			{
				MBMovingPlatform mPlatform = ((MBMovingPlatform)this.items.get(i));
				mPlatform.setLevelPixelPos_x(mPlatform.getPhysicBody().getPosition().x);
				mPlatform.setLevelPixelPos_y(mPlatform.getPhysicBody().getPosition().y + (this.blockSize / 4.0f) / SEPhysicsWorldNative.PHYSICS_SCALE);
			}
			
			if (this.items.get(i).getClass() == MBFreeBox.class)
			{
				MBFreeBox mPlatform = ((MBFreeBox)this.items.get(i));
				if (mPlatform.getType() != E_MBObjectTypes.Tip)
				{
					mPlatform.setLevelPixelPos_x(mPlatform.getPhysicBody().getPosition().x);
					mPlatform.setLevelPixelPos_y(mPlatform.getPhysicBody().getPosition().y);
					mPlatform.setAngle((float)Math.PI - mPlatform.getPhysicBody().getAngle());
				}
			}
			
			if (this.items.get(i).getClass() == MBPrize.class)
			{
				MBPrize mPrize = ((MBPrize)this.items.get(i));
				mPrize.setLevelPixelPos_x(mPrize.getPhysicBody().getPosition().x);
				mPrize.setLevelPixelPos_y(mPrize.getPhysicBody().getPosition().y);
				mPrize.setAngle((float)Math.PI - mPrize.getPhysicBody().getAngle());
			}
			
			this.gameInfo.Render();
			
			this.items.get(i).Render(this.blockSize, (int)this.renderOffset_x, (int)this.renderOffset_y, 
					renderScaleX, renderScaleY, screenH);
		}
	}
}
