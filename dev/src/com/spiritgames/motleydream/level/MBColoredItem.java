package com.spiritgames.motleydream.level;

import android.content.Context;

import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

abstract public class MBColoredItem extends MBLevelItem 
{
	protected E_MBObjectColors color;
	public E_MBObjectColors getColor() 
	{
		return this.color;
	}
	public void setColor(E_MBObjectColors color) 
	{
		this.color = color;
	}
	
	public void LoadTextures(int floorID, int usualID, int redID, int greenID, int blueID, int yellowID)
	{
		this.addTexture(floorID);
		this.addTexture(usualID);
		this.addTexture(redID);
		this.addTexture(greenID);
		this.addTexture(blueID);
		this.addTexture(yellowID);
	}
	
	public void LoadTextures(int redID, int greenID, int blueID, int yellowID)
	{
		this.addTexture(redID);
		this.addTexture(greenID);
		this.addTexture(blueID);
		this.addTexture(yellowID);
	}

	public MBColoredItem(E_MBObjectColors color, float blockSize, boolean useTextureRenderExtension) 
	{
		super(blockSize, useTextureRenderExtension);
		
		this.color = color;
	}
}
