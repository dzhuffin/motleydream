package com.spiritgames.motleydream.level;

import com.spiritgames.motleydream.R;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBRenderOrder;
import com.spiritgames.spiritengine.graphics.SEAnimatedSprite;

public class MBBall extends MBMovingItem 
{	
	private float scaleX = 1.0f;

	private E_MBObjectColors oldColor;

	private SEAnimatedSprite colorChangeAnimation_y2r;
	private SEAnimatedSprite colorChangeAnimation_y2g;
	private SEAnimatedSprite colorChangeAnimation_y2b;
	
	private SEAnimatedSprite colorChangeAnimation_r2y;
	private SEAnimatedSprite colorChangeAnimation_r2g;
	private SEAnimatedSprite colorChangeAnimation_r2b;
	
	private SEAnimatedSprite colorChangeAnimation_g2r;
	private SEAnimatedSprite colorChangeAnimation_g2y;
	private SEAnimatedSprite colorChangeAnimation_g2b;
	
	private SEAnimatedSprite colorChangeAnimation_b2r;
	private SEAnimatedSprite colorChangeAnimation_b2g;
	private SEAnimatedSprite colorChangeAnimation_b2y;
	
	private SEAnimatedSprite colorChangeAnimationCurrent;

	private SEAnimatedSprite deadAnimation;
	
	private SEAnimatedSprite finishAnimation;
	
	@Override
	public void move(float dx, float dy)
	{
		this.angle -= dx / (this.width / this.scaleX);
	
		super.move(dx, dy);
	}
	
	public MBBall(E_MBObjectColors color, float blockSize) 
	{
		super(color, blockSize, false);
		
		this.renderOrder = E_MBRenderOrder.Ball.getIndex();
		
		this.oldColor = color;
		
		this.type = E_MBObjectTypes.Ball;
		
		this.LoadTextures(R.drawable.ball_red, 
						R.drawable.ball_green,
						R.drawable.ball_blue,
						R.drawable.ball_yellow);
		this.InitColorChangeAnimation();
		this.InitDeadAnimation();
		this.InitFinishAnimation();
	}
	
	private void InitColorChangeAnimation()
	{
		this.colorChangeAnimation_y2r = new SEAnimatedSprite(false);
		this.colorChangeAnimation_y2r.loadAnimation(R.drawable.color_change_y2r_01, 8, 50);			
		this.colorChangeAnimation_y2r.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_y2r.setAnimationLoop(false);
		
		this.colorChangeAnimation_y2g = new SEAnimatedSprite(false);
		this.colorChangeAnimation_y2g.loadAnimation(R.drawable.color_change_y2g_01, 8, 50);			
		this.colorChangeAnimation_y2g.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_y2g.setAnimationLoop(false);
		
		this.colorChangeAnimation_y2b = new SEAnimatedSprite(false);
		this.colorChangeAnimation_y2b.loadAnimation(R.drawable.color_change_y2b_01, 8, 50);			
		this.colorChangeAnimation_y2b.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_y2b.setAnimationLoop(false);
		
		this.colorChangeAnimation_r2y = new SEAnimatedSprite(false);
		this.colorChangeAnimation_r2y.loadAnimation(R.drawable.color_change_r2y_01, 8, 50);			
		this.colorChangeAnimation_r2y.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_r2y.setAnimationLoop(false);
		
		this.colorChangeAnimation_r2g = new SEAnimatedSprite(false);
		this.colorChangeAnimation_r2g.loadAnimation(R.drawable.color_change_r2g_01, 8, 50);			
		this.colorChangeAnimation_r2g.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_r2g.setAnimationLoop(false);
		
		this.colorChangeAnimation_r2b = new SEAnimatedSprite(false);
		this.colorChangeAnimation_r2b.loadAnimation(R.drawable.color_change_r2b_01, 8, 50);			
		this.colorChangeAnimation_r2b.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_r2b.setAnimationLoop(false);
		
		this.colorChangeAnimation_g2r = new SEAnimatedSprite(false);
		this.colorChangeAnimation_g2r.loadAnimation(R.drawable.color_change_g2r_01, 8, 50);			
		this.colorChangeAnimation_g2r.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_g2r.setAnimationLoop(false);
		
		this.colorChangeAnimation_g2y = new SEAnimatedSprite(false);
		this.colorChangeAnimation_g2y.loadAnimation(R.drawable.color_change_g2y_01, 8, 50);			
		this.colorChangeAnimation_g2y.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_g2y.setAnimationLoop(false);
		
		this.colorChangeAnimation_g2b = new SEAnimatedSprite(false);
		this.colorChangeAnimation_g2b.loadAnimation(R.drawable.color_change_g2b_01, 8, 50);			
		this.colorChangeAnimation_g2b.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_g2b.setAnimationLoop(false);
		
		this.colorChangeAnimation_b2r = new SEAnimatedSprite(false);
		this.colorChangeAnimation_b2r.loadAnimation(R.drawable.color_change_b2r_01, 8, 50);			
		this.colorChangeAnimation_b2r.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_b2r.setAnimationLoop(false);
		
		this.colorChangeAnimation_b2g = new SEAnimatedSprite(false);
		this.colorChangeAnimation_b2g.loadAnimation(R.drawable.color_change_b2g_01, 8, 50);			
		this.colorChangeAnimation_b2g.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_b2g.setAnimationLoop(false);
		
		this.colorChangeAnimation_b2y = new SEAnimatedSprite(false);
		this.colorChangeAnimation_b2y.loadAnimation(R.drawable.color_change_b2y_01, 8, 50);			
		this.colorChangeAnimation_b2y.setSize(64.0f, 64.0f);
		this.colorChangeAnimation_b2y.setAnimationLoop(false);
	}
	
	private void InitDeadAnimation()
	{
		this.deadAnimation = new SEAnimatedSprite(false);
		this.deadAnimation.loadAnimation(R.drawable.ball_dead_001, 34, 75);	
		
		this.deadAnimation.setSize(64.0f, 64.0f);
		this.deadAnimation.setAnimationLoop(false);	
	}
	
	private void InitFinishAnimation()
	{
		this.finishAnimation = new SEAnimatedSprite(false);
		this.finishAnimation.loadAnimation(R.drawable.ball_finish_001, 13, 40);	
		
		this.finishAnimation.setSize(64.0f, 64.0f);
		this.finishAnimation.setAnimationLoop(false);	
	}
	
	public void showColorChangeAnimation()
	{
		this.colorChangeAnimationCurrent = null;
		
		if ((this.oldColor == E_MBObjectColors.Yellow) && (this.color == E_MBObjectColors.Red))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_y2r;		
		if ((this.oldColor == E_MBObjectColors.Yellow) && (this.color == E_MBObjectColors.Green))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_y2g;		
		if ((this.oldColor == E_MBObjectColors.Yellow) && (this.color == E_MBObjectColors.Blue))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_y2b;		
		if ((this.oldColor == E_MBObjectColors.Red) && (this.color == E_MBObjectColors.Yellow))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_r2y;
		if ((this.oldColor == E_MBObjectColors.Red) && (this.color == E_MBObjectColors.Green))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_r2g;
		if ((this.oldColor == E_MBObjectColors.Red) && (this.color == E_MBObjectColors.Blue))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_r2b;
		if ((this.oldColor == E_MBObjectColors.Green) && (this.color == E_MBObjectColors.Red))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_g2r;
		if ((this.oldColor == E_MBObjectColors.Green) && (this.color == E_MBObjectColors.Yellow))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_g2y;
		if ((this.oldColor == E_MBObjectColors.Green) && (this.color == E_MBObjectColors.Blue))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_g2b;
		if ((this.oldColor == E_MBObjectColors.Blue) && (this.color == E_MBObjectColors.Red))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_b2r;
		if ((this.oldColor == E_MBObjectColors.Blue) && (this.color == E_MBObjectColors.Green))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_b2g;
		if ((this.oldColor == E_MBObjectColors.Blue) && (this.color == E_MBObjectColors.Yellow))
			this.colorChangeAnimationCurrent = this.colorChangeAnimation_b2y;
		
		if (this.colorChangeAnimationCurrent != null)
			this.colorChangeAnimationCurrent.startAnimation();
		
	}
	
	public void showDeadAnimation()
	{
		if (this.deadAnimation != null)
			this.deadAnimation.startAnimation();
	}
	
	public void showFinishAnimation()
	{
		if (this.finishAnimation != null)
			this.finishAnimation.startAnimation();
	}
	
	public boolean isColorChangeAnimationPlaying()
	{
		return (this.colorChangeAnimation_y2r.isAnimationPlaying() || 
				this.colorChangeAnimation_y2g.isAnimationPlaying() ||
				this.colorChangeAnimation_y2b.isAnimationPlaying() ||
				this.colorChangeAnimation_r2y.isAnimationPlaying() ||
				this.colorChangeAnimation_r2g.isAnimationPlaying() ||
				this.colorChangeAnimation_r2b.isAnimationPlaying() ||
				this.colorChangeAnimation_g2r.isAnimationPlaying() ||
				this.colorChangeAnimation_g2y.isAnimationPlaying() ||
				this.colorChangeAnimation_g2b.isAnimationPlaying() ||
				this.colorChangeAnimation_b2r.isAnimationPlaying() ||
				this.colorChangeAnimation_b2g.isAnimationPlaying() ||
				this.colorChangeAnimation_b2y.isAnimationPlaying());
	}
	
	public boolean isDeadAnimationPlaying()
	{
		return (this.deadAnimation.isAnimationPlaying());
	}
	
	public boolean isFinishAnimationPlaying()
	{
		return (this.finishAnimation.isAnimationPlaying());
	}
	
	public void setColorFromUserData(Object ud)
	{
		if ((ud != null) && (ud.getClass() == MBContactType.class))
			this.setColor(((MBContactType)ud).getColor());
	}
	
	@Override
	public void release() 
    {		
        super.release();
        
        if (this.colorChangeAnimation_y2r != null)
			this.colorChangeAnimation_y2r.release();
        if (this.colorChangeAnimation_y2g != null)
			this.colorChangeAnimation_y2g.release();
        if (this.colorChangeAnimation_y2b != null)
			this.colorChangeAnimation_y2b.release();
        if (this.colorChangeAnimation_r2y != null)
			this.colorChangeAnimation_r2y.release();
        if (this.colorChangeAnimation_r2g != null)
			this.colorChangeAnimation_r2g.release();
        if (this.colorChangeAnimation_r2b != null)
			this.colorChangeAnimation_r2b.release();
        if (this.colorChangeAnimation_g2r != null)
			this.colorChangeAnimation_g2r.release();
        if (this.colorChangeAnimation_g2y != null)
			this.colorChangeAnimation_g2y.release();
        if (this.colorChangeAnimation_g2b != null)
			this.colorChangeAnimation_g2b.release();
        if (this.colorChangeAnimation_b2r != null)
			this.colorChangeAnimation_b2r.release();
        if (this.colorChangeAnimation_b2g != null)
			this.colorChangeAnimation_b2g.release();
        if (this.colorChangeAnimation_b2y != null)
			this.colorChangeAnimation_b2y.release();
        
        if (this.deadAnimation != null)
			this.deadAnimation.release();
        
        if (this.finishAnimation != null)
			this.finishAnimation.release();
    }
	
	@Override
	public void restore() 
    {		
        super.restore();
        
        if (this.colorChangeAnimation_y2r != null)
			this.colorChangeAnimation_y2r.restore();
        if (this.colorChangeAnimation_y2g != null)
			this.colorChangeAnimation_y2g.restore();
        if (this.colorChangeAnimation_y2b != null)
			this.colorChangeAnimation_y2b.restore();
        if (this.colorChangeAnimation_r2y != null)
			this.colorChangeAnimation_r2y.restore();
        if (this.colorChangeAnimation_r2g != null)
			this.colorChangeAnimation_r2g.restore();
        if (this.colorChangeAnimation_r2b != null)
			this.colorChangeAnimation_r2b.restore();
        if (this.colorChangeAnimation_g2r != null)
			this.colorChangeAnimation_g2r.restore();
        if (this.colorChangeAnimation_g2y != null)
			this.colorChangeAnimation_g2y.restore();
        if (this.colorChangeAnimation_g2b != null)
			this.colorChangeAnimation_g2b.restore();
        if (this.colorChangeAnimation_b2r != null)
			this.colorChangeAnimation_b2r.restore();
        if (this.colorChangeAnimation_b2g != null)
			this.colorChangeAnimation_b2g.restore();
        if (this.colorChangeAnimation_b2y != null)
			this.colorChangeAnimation_b2y.restore();
        
        if (this.finishAnimation != null)
			this.finishAnimation.restore();
        
        if (this.deadAnimation != null)
			this.deadAnimation.restore();
    }

	@Override
	public void Render(float blockSize, int renderOffset_x, int renderOffset_y,
			float scaleX, float scaleY, float scrH) 
	{
		this.blockSize = blockSize;
		this.scaleX = scaleX;
		int texID = this.color.getIndex() - 2;
		if (texID >= 0)
			this.setCurrentTexture(texID);

		float x = (float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x;
		float y = (float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y;		
		
		if (this.finishAnimation.isAnimationPlaying())
		{
			this.phBody.setActive(false);
			this.finishAnimation.setPos(x, y);
			this.finishAnimation.setSize((float)blockSize * 1.3f * scaleX, (float)blockSize * 1.3f * scaleY);
			this.finishAnimation.setAngle(0);
			this.finishAnimation.Render();
		}
		else
		{
			if (this.deadAnimation.isAnimationPlaying())
			{
				this.phBody.setActive(false);
				this.deadAnimation.setPos(x, y);
				this.deadAnimation.setSize((float)blockSize * 10 * scaleX, (float)blockSize * 10 * scaleY);
				this.deadAnimation.setAngle(0);
				this.deadAnimation.Render();
			}
			else
			{
				if (this.colorChangeAnimationCurrent != null)
				{
					if (this.colorChangeAnimationCurrent.isAnimationPlaying())
					{
						this.colorChangeAnimationCurrent.setPos(x, y);		
						this.colorChangeAnimationCurrent.setSize((float)blockSize * 1.5f * scaleX, (float)blockSize * 1.5f * scaleY);
						this.colorChangeAnimationCurrent.setAngle(this.angle);
						this.colorChangeAnimationCurrent.Render();
					}
					else
					{
						this.setPos(x, y);			
						this.setSize((float)blockSize * scaleX, (float)blockSize * scaleY);			
						super.Render();
					}
				}
				else
				{
					this.setPos(x, y);			
					this.setSize((float)blockSize * scaleX, (float)blockSize * scaleY);			
					super.Render();
				}
			}
		}
	}

}
