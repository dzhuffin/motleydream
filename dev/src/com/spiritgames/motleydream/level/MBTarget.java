package com.spiritgames.motleydream.level;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;

import com.spiritgames.motleydream.R;
import com.spiritgames.motleydream.enums.E_MBObjectColors;
import com.spiritgames.motleydream.enums.E_MBObjectTypes;
import com.spiritgames.motleydream.enums.E_MBRenderOrder;
import com.spiritgames.spiritengine.graphics.SEAnimatedSprite;
import com.spiritgames.spiritengine.graphics.SEVertexBuffer;

public class MBTarget extends MBMovingItem 
{
	private SEAnimatedSprite portalAnimation;
	
	public MBTarget(float blockSize) 
	{
		super(E_MBObjectColors.Red, blockSize, true);
		
		this.renderOrder = E_MBRenderOrder.Target.getIndex();
		
		this.type = E_MBObjectTypes.Target;
	}

	@Override
	public void release() 
    {		
        super.release();
        
        if (this.portalAnimation != null)
			this.portalAnimation.release();
    }
	
	@Override
	public void restore() 
    {		
        super.release();
        
        if (this.portalAnimation != null)
			this.portalAnimation.restore();
    }
	
	public void InitPortalAnimation()
	{
		this.portalAnimation = new SEAnimatedSprite(true);
		this.portalAnimation.loadAnimation(R.drawable.target001, 4, 150);	
		
		this.portalAnimation.setAnimationLoop(true);		
		this.portalAnimation.startAnimation();
	}

	@Override
	public void Render(float blockSize, int renderOffset_x, int renderOffset_y,
			float scaleX, float scaleY, float scrH) 
	{
		this.portalAnimation.setPos((float)(this.levelPixelPos_x) * scaleX + (float)renderOffset_x, 
				(float)(scrH - this.levelPixelPos_y) * scaleY + (float)renderOffset_y);		
		this.portalAnimation.setSize((float)blockSize * 4.0f * scaleX, (float)blockSize * 1.6f * 4.0f * scaleY);
		this.portalAnimation.Render();
	}
}
