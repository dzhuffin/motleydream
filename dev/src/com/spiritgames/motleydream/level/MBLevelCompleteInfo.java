package com.spiritgames.motleydream.level;

import com.spiritgames.motleydream.enums.E_MBDifficultyLevel;
import com.spiritgames.motleydream.enums.E_MBGameEpisodes;

public class MBLevelCompleteInfo
{
	public int level = 0;
	public E_MBGameEpisodes episode = E_MBGameEpisodes.DAY;
	public int timeM = 0;
	public int timeS = 0;
	public int timeMs = 0;
	public int starsCount = 0;
	public int maxStarsCount = 0;
	public int complited = 0;
	public int deaths = 0;
	public E_MBDifficultyLevel difficultyLevel = E_MBDifficultyLevel.Normal;
	
	public String toTimeString_MSMs()
	{
		return Integer.toString(this.timeM) + ":" + 
			   Integer.toString(this.timeS) + ":" +
			   Integer.toString(this.timeMs);
	}
	
	public String toTimeString_SMs()
	{
		/*return Integer.toString(this.timeM * 60 + this.timeS) + ":" +
			   Integer.toString(this.timeMs);*/
		return Integer.toString(this.timeM * 60 + this.timeS) + " sec";
	}
	
	public String toStarsString()
	{
		return Integer.toString(this.starsCount) + "/" +
			   Integer.toString(this.maxStarsCount);
	}

	public static MBLevelCompleteInfo combineToBestCompleteInfo(MBLevelCompleteInfo info1, MBLevelCompleteInfo info2) 
	{
		MBLevelCompleteInfo result = new MBLevelCompleteInfo();
		
		result.episode = info1.episode;
		result.level = info1.level;
		result.difficultyLevel = info1.difficultyLevel;
		result.maxStarsCount = info1.maxStarsCount;
		
		if ((info1.timeM * 60000 + info1.timeS * 1000 + info1.timeMs > 
			 info2.timeM * 60000 + info2.timeS * 1000 + info2.timeMs) &&
		     !(info2.timeM == 0 && info2.timeS == 0 && info2.timeMs == 0))
		{
			result.timeM = info2.timeM;
			result.timeS = info2.timeS;
			result.timeMs = info2.timeMs;
		}
		else
		{
			result.timeM = info1.timeM;
			result.timeS = info1.timeS;
			result.timeMs = info1.timeMs;
		}
		
		if (info1.starsCount > info2.starsCount)
			result.starsCount = info1.starsCount;
		else
			result.starsCount = info2.starsCount;
		
		if (info1.deaths > info2.deaths)
			result.deaths = info1.deaths;
		else
			result.deaths = info2.deaths;
		
		if (info1.complited > info2.complited)
			result.complited = info1.complited;
		else
			result.complited = info2.complited;
		
		return result;
	}
}