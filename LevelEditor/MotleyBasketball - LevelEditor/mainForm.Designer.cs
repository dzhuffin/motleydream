﻿namespace MotleyBasketball___LevelEditor
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.saveStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exportStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.platformToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mPlatformToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addColorChangerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.propertiesGB = new System.Windows.Forms.GroupBox();
            this.chainLengthGB = new System.Windows.Forms.GroupBox();
            this.chainLengthTB = new System.Windows.Forms.TrackBar();
            this.hCameraPosGB = new System.Windows.Forms.GroupBox();
            this.hFreeRB = new System.Windows.Forms.RadioButton();
            this.hCenterRB = new System.Windows.Forms.RadioButton();
            this.hRightRB = new System.Windows.Forms.RadioButton();
            this.hLeftRB = new System.Windows.Forms.RadioButton();
            this.vCameraPosGB = new System.Windows.Forms.GroupBox();
            this.vFreeRB = new System.Windows.Forms.RadioButton();
            this.vCenterRB = new System.Windows.Forms.RadioButton();
            this.vBottomRB = new System.Windows.Forms.RadioButton();
            this.vTopRB = new System.Windows.Forms.RadioButton();
            this.freeBoxRotatingGB = new System.Windows.Forms.GroupBox();
            this.falseRotRB = new System.Windows.Forms.RadioButton();
            this.trueRotRB = new System.Windows.Forms.RadioButton();
            this.edgeTypeGB = new System.Windows.Forms.GroupBox();
            this.rightOnlyRB = new System.Windows.Forms.RadioButton();
            this.leftOnlyRB = new System.Windows.Forms.RadioButton();
            this.allRB = new System.Windows.Forms.RadioButton();
            this.noneRB = new System.Windows.Forms.RadioButton();
            this.freeBoxKindGB = new System.Windows.Forms.GroupBox();
            this.boxRB = new System.Windows.Forms.RadioButton();
            this.ballRB = new System.Windows.Forms.RadioButton();
            this.freeBoxSizeGB = new System.Windows.Forms.GroupBox();
            this.freeBoxHeightTB = new System.Windows.Forms.TrackBar();
            this.freeBoxWidthTB = new System.Windows.Forms.TrackBar();
            this.startPosGB = new System.Windows.Forms.GroupBox();
            this.startTB = new System.Windows.Forms.TrackBar();
            this.movingOrientationGB = new System.Windows.Forms.GroupBox();
            this.mHorizontalRB = new System.Windows.Forms.RadioButton();
            this.mVerticalRB = new System.Windows.Forms.RadioButton();
            this.distanceGB = new System.Windows.Forms.GroupBox();
            this.distanceTB = new System.Windows.Forms.TrackBar();
            this.levelSizeGB = new System.Windows.Forms.GroupBox();
            this.levelHeightTB = new System.Windows.Forms.TrackBar();
            this.levelWidthTB = new System.Windows.Forms.TrackBar();
            this.typeGB = new System.Windows.Forms.GroupBox();
            this.typeCB = new System.Windows.Forms.ComboBox();
            this.lengthGB = new System.Windows.Forms.GroupBox();
            this.lengthTB = new System.Windows.Forms.TrackBar();
            this.orientationGB = new System.Windows.Forms.GroupBox();
            this.horizontalRB = new System.Windows.Forms.RadioButton();
            this.verticalRB = new System.Windows.Forms.RadioButton();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.deleteBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.platformBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.movingPlatformBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.colorChangerBtn = new System.Windows.Forms.ToolStripButton();
            this.freeBoxBtn = new System.Windows.Forms.ToolStripButton();
            this.prizeBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.newBtn = new System.Windows.Forms.ToolStripButton();
            this.saveBtn = new System.Windows.Forms.ToolStripButton();
            this.resaveBtn = new System.Windows.Forms.ToolStripButton();
            this.loadBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exportBtn = new System.Windows.Forms.ToolStripButton();
            this.itemsImageList = new System.Windows.Forms.ImageList(this.components);
            this.saveLevelDialog = new System.Windows.Forms.SaveFileDialog();
            this.openLevelDialog = new System.Windows.Forms.OpenFileDialog();
            this.renderTimer = new System.Windows.Forms.Timer(this.components);
            this.tipRB = new System.Windows.Forms.RadioButton();
            this.mainMenu.SuspendLayout();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.propertiesGB.SuspendLayout();
            this.chainLengthGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chainLengthTB)).BeginInit();
            this.hCameraPosGB.SuspendLayout();
            this.vCameraPosGB.SuspendLayout();
            this.freeBoxRotatingGB.SuspendLayout();
            this.edgeTypeGB.SuspendLayout();
            this.freeBoxKindGB.SuspendLayout();
            this.freeBoxSizeGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.freeBoxHeightTB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.freeBoxWidthTB)).BeginInit();
            this.startPosGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startTB)).BeginInit();
            this.movingOrientationGB.SuspendLayout();
            this.distanceGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.distanceTB)).BeginInit();
            this.levelSizeGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.levelHeightTB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.levelWidthTB)).BeginInit();
            this.typeGB.SuspendLayout();
            this.lengthGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lengthTB)).BeginInit();
            this.orientationGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(969, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStripMenuItem,
            this.toolStripSeparator5,
            this.saveStripMenuItem,
            this.loadStripMenuItem,
            this.toolStripSeparator4,
            this.exportStripMenuItem1,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newStripMenuItem
            // 
            this.newStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources._new;
            this.newStripMenuItem.Name = "newStripMenuItem";
            this.newStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.newStripMenuItem.Text = "New";
            this.newStripMenuItem.Click += new System.EventHandler(this.newBtn_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(104, 6);
            // 
            // saveStripMenuItem
            // 
            this.saveStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.save;
            this.saveStripMenuItem.Name = "saveStripMenuItem";
            this.saveStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.saveStripMenuItem.Text = "Save";
            this.saveStripMenuItem.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // loadStripMenuItem
            // 
            this.loadStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.load;
            this.loadStripMenuItem.Name = "loadStripMenuItem";
            this.loadStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.loadStripMenuItem.Text = "Load";
            this.loadStripMenuItem.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(104, 6);
            // 
            // exportStripMenuItem1
            // 
            this.exportStripMenuItem1.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.export;
            this.exportStripMenuItem1.Name = "exportStripMenuItem1";
            this.exportStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.exportStripMenuItem1.Text = "Export";
            this.exportStripMenuItem1.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(104, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.platformToolStripMenuItem,
            this.mPlatformToolStripMenuItem,
            this.addColorChangerToolStripMenuItem,
            this.toolStripSeparator6,
            this.deleteSelectedToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // platformToolStripMenuItem
            // 
            this.platformToolStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.platform;
            this.platformToolStripMenuItem.Name = "platformToolStripMenuItem";
            this.platformToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.platformToolStripMenuItem.Text = "Add Platform";
            this.platformToolStripMenuItem.Click += new System.EventHandler(this.baseWallBtn_Click);
            // 
            // mPlatformToolStripMenuItem
            // 
            this.mPlatformToolStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.platform;
            this.mPlatformToolStripMenuItem.Name = "mPlatformToolStripMenuItem";
            this.mPlatformToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.mPlatformToolStripMenuItem.Text = "Add Moving Platform";
            this.mPlatformToolStripMenuItem.Click += new System.EventHandler(this.movingPlatformBtn_Click);
            // 
            // addColorChangerToolStripMenuItem
            // 
            this.addColorChangerToolStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.colorChanger;
            this.addColorChangerToolStripMenuItem.Name = "addColorChangerToolStripMenuItem";
            this.addColorChangerToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.addColorChangerToolStripMenuItem.Text = "Add Color Changer";
            this.addColorChangerToolStripMenuItem.Click += new System.EventHandler(this.colorChangerBtn_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(186, 6);
            // 
            // deleteSelectedToolStripMenuItem
            // 
            this.deleteSelectedToolStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.delete;
            this.deleteSelectedToolStripMenuItem.Name = "deleteSelectedToolStripMenuItem";
            this.deleteSelectedToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.deleteSelectedToolStripMenuItem.Text = "Delete selected";
            this.deleteSelectedToolStripMenuItem.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.about;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // toolStripContainer
            // 
            this.toolStripContainer.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.Controls.Add(this.splitContainer);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(969, 928);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer.LeftToolStripPanelVisible = false;
            this.toolStripContainer.Location = new System.Drawing.Point(0, 24);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.RightToolStripPanelVisible = false;
            this.toolStripContainer.Size = new System.Drawing.Size(969, 1003);
            this.toolStripContainer.TabIndex = 1;
            this.toolStripContainer.Text = "toolStripContainer1";
            // 
            // toolStripContainer.TopToolStripPanel
            // 
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip3);
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip2);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer.Panel1.Controls.Add(this.propertiesGB);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer.Panel2.Controls.Add(this.pictureBox);
            this.splitContainer.Size = new System.Drawing.Size(969, 928);
            this.splitContainer.SplitterDistance = 321;
            this.splitContainer.TabIndex = 0;
            // 
            // propertiesGB
            // 
            this.propertiesGB.Controls.Add(this.chainLengthGB);
            this.propertiesGB.Controls.Add(this.hCameraPosGB);
            this.propertiesGB.Controls.Add(this.vCameraPosGB);
            this.propertiesGB.Controls.Add(this.freeBoxRotatingGB);
            this.propertiesGB.Controls.Add(this.edgeTypeGB);
            this.propertiesGB.Controls.Add(this.freeBoxKindGB);
            this.propertiesGB.Controls.Add(this.freeBoxSizeGB);
            this.propertiesGB.Controls.Add(this.startPosGB);
            this.propertiesGB.Controls.Add(this.movingOrientationGB);
            this.propertiesGB.Controls.Add(this.distanceGB);
            this.propertiesGB.Controls.Add(this.levelSizeGB);
            this.propertiesGB.Controls.Add(this.typeGB);
            this.propertiesGB.Controls.Add(this.lengthGB);
            this.propertiesGB.Controls.Add(this.orientationGB);
            this.propertiesGB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertiesGB.Location = new System.Drawing.Point(0, 0);
            this.propertiesGB.Margin = new System.Windows.Forms.Padding(10);
            this.propertiesGB.Name = "propertiesGB";
            this.propertiesGB.Size = new System.Drawing.Size(321, 928);
            this.propertiesGB.TabIndex = 0;
            this.propertiesGB.TabStop = false;
            this.propertiesGB.Text = "Properties";
            // 
            // chainLengthGB
            // 
            this.chainLengthGB.Controls.Add(this.chainLengthTB);
            this.chainLengthGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.chainLengthGB.Location = new System.Drawing.Point(3, 908);
            this.chainLengthGB.Name = "chainLengthGB";
            this.chainLengthGB.Size = new System.Drawing.Size(315, 65);
            this.chainLengthGB.TabIndex = 13;
            this.chainLengthGB.TabStop = false;
            this.chainLengthGB.Text = "Chain Length";
            this.chainLengthGB.Visible = false;
            // 
            // chainLengthTB
            // 
            this.chainLengthTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.chainLengthTB.BackColor = System.Drawing.Color.White;
            this.chainLengthTB.Location = new System.Drawing.Point(9, 18);
            this.chainLengthTB.Maximum = 50;
            this.chainLengthTB.Minimum = 1;
            this.chainLengthTB.Name = "chainLengthTB";
            this.chainLengthTB.Size = new System.Drawing.Size(300, 45);
            this.chainLengthTB.TabIndex = 0;
            this.chainLengthTB.Value = 25;
            this.chainLengthTB.Scroll += new System.EventHandler(this.chainLengthTB_Scroll);
            // 
            // hCameraPosGB
            // 
            this.hCameraPosGB.Controls.Add(this.hFreeRB);
            this.hCameraPosGB.Controls.Add(this.hCenterRB);
            this.hCameraPosGB.Controls.Add(this.hRightRB);
            this.hCameraPosGB.Controls.Add(this.hLeftRB);
            this.hCameraPosGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.hCameraPosGB.Location = new System.Drawing.Point(3, 836);
            this.hCameraPosGB.Name = "hCameraPosGB";
            this.hCameraPosGB.Size = new System.Drawing.Size(315, 72);
            this.hCameraPosGB.TabIndex = 12;
            this.hCameraPosGB.TabStop = false;
            this.hCameraPosGB.Text = "Horizontal camera position";
            this.hCameraPosGB.Visible = false;
            // 
            // hFreeRB
            // 
            this.hFreeRB.AutoSize = true;
            this.hFreeRB.Location = new System.Drawing.Point(108, 42);
            this.hFreeRB.Name = "hFreeRB";
            this.hFreeRB.Size = new System.Drawing.Size(46, 17);
            this.hFreeRB.TabIndex = 3;
            this.hFreeRB.TabStop = true;
            this.hFreeRB.Text = "Free";
            this.hFreeRB.UseVisualStyleBackColor = true;
            this.hFreeRB.CheckedChanged += new System.EventHandler(this.hFreeRB_CheckedChanged);
            // 
            // hCenterRB
            // 
            this.hCenterRB.AutoSize = true;
            this.hCenterRB.Location = new System.Drawing.Point(108, 19);
            this.hCenterRB.Name = "hCenterRB";
            this.hCenterRB.Size = new System.Drawing.Size(56, 17);
            this.hCenterRB.TabIndex = 2;
            this.hCenterRB.TabStop = true;
            this.hCenterRB.Text = "Center";
            this.hCenterRB.UseVisualStyleBackColor = true;
            this.hCenterRB.CheckedChanged += new System.EventHandler(this.hCenterRB_CheckedChanged);
            // 
            // hRightRB
            // 
            this.hRightRB.AutoSize = true;
            this.hRightRB.Location = new System.Drawing.Point(9, 42);
            this.hRightRB.Name = "hRightRB";
            this.hRightRB.Size = new System.Drawing.Size(50, 17);
            this.hRightRB.TabIndex = 1;
            this.hRightRB.TabStop = true;
            this.hRightRB.Text = "Right";
            this.hRightRB.UseVisualStyleBackColor = true;
            this.hRightRB.CheckedChanged += new System.EventHandler(this.hRightRB_CheckedChanged);
            // 
            // hLeftRB
            // 
            this.hLeftRB.AutoSize = true;
            this.hLeftRB.Location = new System.Drawing.Point(9, 19);
            this.hLeftRB.Name = "hLeftRB";
            this.hLeftRB.Size = new System.Drawing.Size(43, 17);
            this.hLeftRB.TabIndex = 0;
            this.hLeftRB.TabStop = true;
            this.hLeftRB.Text = "Left";
            this.hLeftRB.UseVisualStyleBackColor = true;
            this.hLeftRB.CheckedChanged += new System.EventHandler(this.hLeftRB_CheckedChanged);
            // 
            // vCameraPosGB
            // 
            this.vCameraPosGB.Controls.Add(this.vFreeRB);
            this.vCameraPosGB.Controls.Add(this.vCenterRB);
            this.vCameraPosGB.Controls.Add(this.vBottomRB);
            this.vCameraPosGB.Controls.Add(this.vTopRB);
            this.vCameraPosGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.vCameraPosGB.Location = new System.Drawing.Point(3, 764);
            this.vCameraPosGB.Name = "vCameraPosGB";
            this.vCameraPosGB.Size = new System.Drawing.Size(315, 72);
            this.vCameraPosGB.TabIndex = 11;
            this.vCameraPosGB.TabStop = false;
            this.vCameraPosGB.Text = "Vertical camera position";
            this.vCameraPosGB.Visible = false;
            // 
            // vFreeRB
            // 
            this.vFreeRB.AutoSize = true;
            this.vFreeRB.Location = new System.Drawing.Point(108, 42);
            this.vFreeRB.Name = "vFreeRB";
            this.vFreeRB.Size = new System.Drawing.Size(46, 17);
            this.vFreeRB.TabIndex = 3;
            this.vFreeRB.TabStop = true;
            this.vFreeRB.Text = "Free";
            this.vFreeRB.UseVisualStyleBackColor = true;
            this.vFreeRB.CheckedChanged += new System.EventHandler(this.vFreeRB_CheckedChanged);
            // 
            // vCenterRB
            // 
            this.vCenterRB.AutoSize = true;
            this.vCenterRB.Location = new System.Drawing.Point(108, 19);
            this.vCenterRB.Name = "vCenterRB";
            this.vCenterRB.Size = new System.Drawing.Size(56, 17);
            this.vCenterRB.TabIndex = 2;
            this.vCenterRB.TabStop = true;
            this.vCenterRB.Text = "Center";
            this.vCenterRB.UseVisualStyleBackColor = true;
            this.vCenterRB.CheckedChanged += new System.EventHandler(this.vCenterRB_CheckedChanged);
            // 
            // vBottomRB
            // 
            this.vBottomRB.AutoSize = true;
            this.vBottomRB.Location = new System.Drawing.Point(9, 42);
            this.vBottomRB.Name = "vBottomRB";
            this.vBottomRB.Size = new System.Drawing.Size(58, 17);
            this.vBottomRB.TabIndex = 1;
            this.vBottomRB.TabStop = true;
            this.vBottomRB.Text = "Bottom";
            this.vBottomRB.UseVisualStyleBackColor = true;
            this.vBottomRB.CheckedChanged += new System.EventHandler(this.vBottomRB_CheckedChanged);
            // 
            // vTopRB
            // 
            this.vTopRB.AutoSize = true;
            this.vTopRB.Location = new System.Drawing.Point(9, 19);
            this.vTopRB.Name = "vTopRB";
            this.vTopRB.Size = new System.Drawing.Size(44, 17);
            this.vTopRB.TabIndex = 0;
            this.vTopRB.TabStop = true;
            this.vTopRB.Text = "Top";
            this.vTopRB.UseVisualStyleBackColor = true;
            this.vTopRB.CheckedChanged += new System.EventHandler(this.vTopRB_CheckedChanged);
            // 
            // freeBoxRotatingGB
            // 
            this.freeBoxRotatingGB.Controls.Add(this.falseRotRB);
            this.freeBoxRotatingGB.Controls.Add(this.trueRotRB);
            this.freeBoxRotatingGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.freeBoxRotatingGB.Location = new System.Drawing.Point(3, 721);
            this.freeBoxRotatingGB.Name = "freeBoxRotatingGB";
            this.freeBoxRotatingGB.Size = new System.Drawing.Size(315, 43);
            this.freeBoxRotatingGB.TabIndex = 10;
            this.freeBoxRotatingGB.TabStop = false;
            this.freeBoxRotatingGB.Text = "Free box rotating";
            this.freeBoxRotatingGB.Visible = false;
            // 
            // falseRotRB
            // 
            this.falseRotRB.AutoSize = true;
            this.falseRotRB.Location = new System.Drawing.Point(62, 19);
            this.falseRotRB.Name = "falseRotRB";
            this.falseRotRB.Size = new System.Drawing.Size(50, 17);
            this.falseRotRB.TabIndex = 1;
            this.falseRotRB.TabStop = true;
            this.falseRotRB.Text = "False";
            this.falseRotRB.UseVisualStyleBackColor = true;
            // 
            // trueRotRB
            // 
            this.trueRotRB.AutoSize = true;
            this.trueRotRB.Location = new System.Drawing.Point(9, 19);
            this.trueRotRB.Name = "trueRotRB";
            this.trueRotRB.Size = new System.Drawing.Size(47, 17);
            this.trueRotRB.TabIndex = 0;
            this.trueRotRB.TabStop = true;
            this.trueRotRB.Text = "True";
            this.trueRotRB.UseVisualStyleBackColor = true;
            this.trueRotRB.CheckedChanged += new System.EventHandler(this.trueRotRB_CheckedChanged);
            // 
            // edgeTypeGB
            // 
            this.edgeTypeGB.Controls.Add(this.rightOnlyRB);
            this.edgeTypeGB.Controls.Add(this.leftOnlyRB);
            this.edgeTypeGB.Controls.Add(this.allRB);
            this.edgeTypeGB.Controls.Add(this.noneRB);
            this.edgeTypeGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.edgeTypeGB.Location = new System.Drawing.Point(3, 649);
            this.edgeTypeGB.Name = "edgeTypeGB";
            this.edgeTypeGB.Size = new System.Drawing.Size(315, 72);
            this.edgeTypeGB.TabIndex = 9;
            this.edgeTypeGB.TabStop = false;
            this.edgeTypeGB.Text = "Edge Type";
            this.edgeTypeGB.Visible = false;
            // 
            // rightOnlyRB
            // 
            this.rightOnlyRB.AutoSize = true;
            this.rightOnlyRB.Location = new System.Drawing.Point(108, 42);
            this.rightOnlyRB.Name = "rightOnlyRB";
            this.rightOnlyRB.Size = new System.Drawing.Size(72, 17);
            this.rightOnlyRB.TabIndex = 3;
            this.rightOnlyRB.TabStop = true;
            this.rightOnlyRB.Text = "Right only";
            this.rightOnlyRB.UseVisualStyleBackColor = true;
            this.rightOnlyRB.CheckedChanged += new System.EventHandler(this.rightOnlyRB_CheckedChanged);
            // 
            // leftOnlyRB
            // 
            this.leftOnlyRB.AutoSize = true;
            this.leftOnlyRB.Location = new System.Drawing.Point(108, 19);
            this.leftOnlyRB.Name = "leftOnlyRB";
            this.leftOnlyRB.Size = new System.Drawing.Size(65, 17);
            this.leftOnlyRB.TabIndex = 2;
            this.leftOnlyRB.TabStop = true;
            this.leftOnlyRB.Text = "Left only";
            this.leftOnlyRB.UseVisualStyleBackColor = true;
            this.leftOnlyRB.CheckedChanged += new System.EventHandler(this.leftOnlyRB_CheckedChanged);
            // 
            // allRB
            // 
            this.allRB.AutoSize = true;
            this.allRB.Location = new System.Drawing.Point(9, 42);
            this.allRB.Name = "allRB";
            this.allRB.Size = new System.Drawing.Size(36, 17);
            this.allRB.TabIndex = 1;
            this.allRB.TabStop = true;
            this.allRB.Text = "All";
            this.allRB.UseVisualStyleBackColor = true;
            this.allRB.CheckedChanged += new System.EventHandler(this.allRB_CheckedChanged);
            // 
            // noneRB
            // 
            this.noneRB.AutoSize = true;
            this.noneRB.Location = new System.Drawing.Point(9, 19);
            this.noneRB.Name = "noneRB";
            this.noneRB.Size = new System.Drawing.Size(51, 17);
            this.noneRB.TabIndex = 0;
            this.noneRB.TabStop = true;
            this.noneRB.Text = "None";
            this.noneRB.UseVisualStyleBackColor = true;
            this.noneRB.CheckedChanged += new System.EventHandler(this.noneRB_CheckedChanged);
            // 
            // freeBoxKindGB
            // 
            this.freeBoxKindGB.Controls.Add(this.tipRB);
            this.freeBoxKindGB.Controls.Add(this.boxRB);
            this.freeBoxKindGB.Controls.Add(this.ballRB);
            this.freeBoxKindGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.freeBoxKindGB.Location = new System.Drawing.Point(3, 604);
            this.freeBoxKindGB.Name = "freeBoxKindGB";
            this.freeBoxKindGB.Size = new System.Drawing.Size(315, 45);
            this.freeBoxKindGB.TabIndex = 8;
            this.freeBoxKindGB.TabStop = false;
            this.freeBoxKindGB.Text = "Free box kind";
            this.freeBoxKindGB.Visible = false;
            // 
            // boxRB
            // 
            this.boxRB.AutoSize = true;
            this.boxRB.Location = new System.Drawing.Point(57, 19);
            this.boxRB.Name = "boxRB";
            this.boxRB.Size = new System.Drawing.Size(43, 17);
            this.boxRB.TabIndex = 1;
            this.boxRB.TabStop = true;
            this.boxRB.Text = "Box";
            this.boxRB.UseVisualStyleBackColor = true;
            this.boxRB.CheckedChanged += new System.EventHandler(this.boxRB_CheckedChanged);
            // 
            // ballRB
            // 
            this.ballRB.AutoSize = true;
            this.ballRB.Location = new System.Drawing.Point(9, 19);
            this.ballRB.Name = "ballRB";
            this.ballRB.Size = new System.Drawing.Size(42, 17);
            this.ballRB.TabIndex = 0;
            this.ballRB.TabStop = true;
            this.ballRB.Text = "Ball";
            this.ballRB.UseVisualStyleBackColor = true;
            this.ballRB.CheckedChanged += new System.EventHandler(this.ballRB_CheckedChanged);
            // 
            // freeBoxSizeGB
            // 
            this.freeBoxSizeGB.Controls.Add(this.freeBoxHeightTB);
            this.freeBoxSizeGB.Controls.Add(this.freeBoxWidthTB);
            this.freeBoxSizeGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.freeBoxSizeGB.Location = new System.Drawing.Point(3, 487);
            this.freeBoxSizeGB.Name = "freeBoxSizeGB";
            this.freeBoxSizeGB.Size = new System.Drawing.Size(315, 117);
            this.freeBoxSizeGB.TabIndex = 7;
            this.freeBoxSizeGB.TabStop = false;
            this.freeBoxSizeGB.Text = "Free Box Size";
            this.freeBoxSizeGB.Visible = false;
            // 
            // freeBoxHeightTB
            // 
            this.freeBoxHeightTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.freeBoxHeightTB.BackColor = System.Drawing.Color.White;
            this.freeBoxHeightTB.Location = new System.Drawing.Point(9, 63);
            this.freeBoxHeightTB.Maximum = 50;
            this.freeBoxHeightTB.Minimum = 1;
            this.freeBoxHeightTB.Name = "freeBoxHeightTB";
            this.freeBoxHeightTB.Size = new System.Drawing.Size(300, 45);
            this.freeBoxHeightTB.TabIndex = 1;
            this.freeBoxHeightTB.Value = 25;
            this.freeBoxHeightTB.Scroll += new System.EventHandler(this.freeBoxHeightTB_Scroll);
            // 
            // freeBoxWidthTB
            // 
            this.freeBoxWidthTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.freeBoxWidthTB.BackColor = System.Drawing.Color.White;
            this.freeBoxWidthTB.Location = new System.Drawing.Point(9, 12);
            this.freeBoxWidthTB.Maximum = 50;
            this.freeBoxWidthTB.Minimum = 1;
            this.freeBoxWidthTB.Name = "freeBoxWidthTB";
            this.freeBoxWidthTB.Size = new System.Drawing.Size(300, 45);
            this.freeBoxWidthTB.TabIndex = 0;
            this.freeBoxWidthTB.Value = 25;
            this.freeBoxWidthTB.Scroll += new System.EventHandler(this.freeBoxWidthTB_Scroll);
            // 
            // startPosGB
            // 
            this.startPosGB.Controls.Add(this.startTB);
            this.startPosGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.startPosGB.Location = new System.Drawing.Point(3, 415);
            this.startPosGB.Name = "startPosGB";
            this.startPosGB.Size = new System.Drawing.Size(315, 72);
            this.startPosGB.TabIndex = 6;
            this.startPosGB.TabStop = false;
            this.startPosGB.Text = "Start position";
            this.startPosGB.Visible = false;
            // 
            // startTB
            // 
            this.startTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.startTB.BackColor = System.Drawing.Color.White;
            this.startTB.Location = new System.Drawing.Point(9, 19);
            this.startTB.Maximum = 50;
            this.startTB.Name = "startTB";
            this.startTB.Size = new System.Drawing.Size(300, 45);
            this.startTB.TabIndex = 0;
            this.startTB.Value = 1;
            this.startTB.Scroll += new System.EventHandler(this.startTB_Scroll);
            // 
            // movingOrientationGB
            // 
            this.movingOrientationGB.Controls.Add(this.mHorizontalRB);
            this.movingOrientationGB.Controls.Add(this.mVerticalRB);
            this.movingOrientationGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.movingOrientationGB.Location = new System.Drawing.Point(3, 372);
            this.movingOrientationGB.Name = "movingOrientationGB";
            this.movingOrientationGB.Size = new System.Drawing.Size(315, 43);
            this.movingOrientationGB.TabIndex = 5;
            this.movingOrientationGB.TabStop = false;
            this.movingOrientationGB.Text = "Moving orientation";
            this.movingOrientationGB.Visible = false;
            // 
            // mHorizontalRB
            // 
            this.mHorizontalRB.AutoSize = true;
            this.mHorizontalRB.Location = new System.Drawing.Point(75, 19);
            this.mHorizontalRB.Name = "mHorizontalRB";
            this.mHorizontalRB.Size = new System.Drawing.Size(72, 17);
            this.mHorizontalRB.TabIndex = 1;
            this.mHorizontalRB.TabStop = true;
            this.mHorizontalRB.Text = "Horizontal";
            this.mHorizontalRB.UseVisualStyleBackColor = true;
            // 
            // mVerticalRB
            // 
            this.mVerticalRB.AutoSize = true;
            this.mVerticalRB.Location = new System.Drawing.Point(9, 19);
            this.mVerticalRB.Name = "mVerticalRB";
            this.mVerticalRB.Size = new System.Drawing.Size(60, 17);
            this.mVerticalRB.TabIndex = 0;
            this.mVerticalRB.TabStop = true;
            this.mVerticalRB.Text = "Vertical";
            this.mVerticalRB.UseVisualStyleBackColor = true;
            this.mVerticalRB.CheckedChanged += new System.EventHandler(this.mVerticalRB_CheckedChanged);
            // 
            // distanceGB
            // 
            this.distanceGB.Controls.Add(this.distanceTB);
            this.distanceGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.distanceGB.Location = new System.Drawing.Point(3, 306);
            this.distanceGB.Name = "distanceGB";
            this.distanceGB.Size = new System.Drawing.Size(315, 66);
            this.distanceGB.TabIndex = 4;
            this.distanceGB.TabStop = false;
            this.distanceGB.Text = "Distance";
            this.distanceGB.Visible = false;
            // 
            // distanceTB
            // 
            this.distanceTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.distanceTB.BackColor = System.Drawing.Color.White;
            this.distanceTB.Location = new System.Drawing.Point(9, 16);
            this.distanceTB.Maximum = 50;
            this.distanceTB.Minimum = 1;
            this.distanceTB.Name = "distanceTB";
            this.distanceTB.Size = new System.Drawing.Size(300, 45);
            this.distanceTB.TabIndex = 0;
            this.distanceTB.Value = 1;
            this.distanceTB.Scroll += new System.EventHandler(this.distanceTB_Scroll);
            // 
            // levelSizeGB
            // 
            this.levelSizeGB.Controls.Add(this.levelHeightTB);
            this.levelSizeGB.Controls.Add(this.levelWidthTB);
            this.levelSizeGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.levelSizeGB.Location = new System.Drawing.Point(3, 184);
            this.levelSizeGB.Name = "levelSizeGB";
            this.levelSizeGB.Size = new System.Drawing.Size(315, 122);
            this.levelSizeGB.TabIndex = 3;
            this.levelSizeGB.TabStop = false;
            this.levelSizeGB.Text = "Level Size";
            // 
            // levelHeightTB
            // 
            this.levelHeightTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.levelHeightTB.BackColor = System.Drawing.Color.White;
            this.levelHeightTB.Location = new System.Drawing.Point(9, 65);
            this.levelHeightTB.Maximum = 100;
            this.levelHeightTB.Minimum = 10;
            this.levelHeightTB.Name = "levelHeightTB";
            this.levelHeightTB.Size = new System.Drawing.Size(300, 45);
            this.levelHeightTB.TabIndex = 1;
            this.levelHeightTB.Value = 25;
            this.levelHeightTB.Scroll += new System.EventHandler(this.levelHeightTB_Scroll);
            // 
            // levelWidthTB
            // 
            this.levelWidthTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.levelWidthTB.BackColor = System.Drawing.Color.White;
            this.levelWidthTB.Location = new System.Drawing.Point(9, 14);
            this.levelWidthTB.Maximum = 100;
            this.levelWidthTB.Minimum = 10;
            this.levelWidthTB.Name = "levelWidthTB";
            this.levelWidthTB.Size = new System.Drawing.Size(300, 45);
            this.levelWidthTB.TabIndex = 0;
            this.levelWidthTB.Value = 25;
            this.levelWidthTB.Scroll += new System.EventHandler(this.levelWidthTB_Scroll);
            // 
            // typeGB
            // 
            this.typeGB.Controls.Add(this.typeCB);
            this.typeGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.typeGB.Location = new System.Drawing.Point(3, 133);
            this.typeGB.Name = "typeGB";
            this.typeGB.Size = new System.Drawing.Size(315, 51);
            this.typeGB.TabIndex = 2;
            this.typeGB.TabStop = false;
            this.typeGB.Text = "Type";
            this.typeGB.Visible = false;
            // 
            // typeCB
            // 
            this.typeCB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.typeCB.FormattingEnabled = true;
            this.typeCB.Items.AddRange(new object[] {
            "Usual",
            "Floor",
            "Red",
            "Green",
            "Blue",
            "Yellow",
            "LeftWall",
            "RightWall",
            "Roof"});
            this.typeCB.Location = new System.Drawing.Point(9, 19);
            this.typeCB.Name = "typeCB";
            this.typeCB.Size = new System.Drawing.Size(300, 21);
            this.typeCB.TabIndex = 0;
            this.typeCB.SelectedIndexChanged += new System.EventHandler(this.typeCB_SelectedIndexChanged);
            // 
            // lengthGB
            // 
            this.lengthGB.Controls.Add(this.lengthTB);
            this.lengthGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lengthGB.Location = new System.Drawing.Point(3, 61);
            this.lengthGB.Name = "lengthGB";
            this.lengthGB.Size = new System.Drawing.Size(315, 72);
            this.lengthGB.TabIndex = 1;
            this.lengthGB.TabStop = false;
            this.lengthGB.Text = "Length";
            this.lengthGB.Visible = false;
            // 
            // lengthTB
            // 
            this.lengthTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lengthTB.BackColor = System.Drawing.Color.White;
            this.lengthTB.Location = new System.Drawing.Point(9, 19);
            this.lengthTB.Maximum = 50;
            this.lengthTB.Minimum = 1;
            this.lengthTB.Name = "lengthTB";
            this.lengthTB.Size = new System.Drawing.Size(300, 45);
            this.lengthTB.TabIndex = 0;
            this.lengthTB.Value = 1;
            this.lengthTB.Scroll += new System.EventHandler(this.lengthTB_Scroll);
            // 
            // orientationGB
            // 
            this.orientationGB.Controls.Add(this.horizontalRB);
            this.orientationGB.Controls.Add(this.verticalRB);
            this.orientationGB.Dock = System.Windows.Forms.DockStyle.Top;
            this.orientationGB.Location = new System.Drawing.Point(3, 16);
            this.orientationGB.Name = "orientationGB";
            this.orientationGB.Size = new System.Drawing.Size(315, 45);
            this.orientationGB.TabIndex = 0;
            this.orientationGB.TabStop = false;
            this.orientationGB.Text = "Orientation";
            this.orientationGB.Visible = false;
            // 
            // horizontalRB
            // 
            this.horizontalRB.AutoSize = true;
            this.horizontalRB.Location = new System.Drawing.Point(75, 19);
            this.horizontalRB.Name = "horizontalRB";
            this.horizontalRB.Size = new System.Drawing.Size(72, 17);
            this.horizontalRB.TabIndex = 1;
            this.horizontalRB.TabStop = true;
            this.horizontalRB.Text = "Horizontal";
            this.horizontalRB.UseVisualStyleBackColor = true;
            // 
            // verticalRB
            // 
            this.verticalRB.AutoSize = true;
            this.verticalRB.Location = new System.Drawing.Point(9, 19);
            this.verticalRB.Name = "verticalRB";
            this.verticalRB.Size = new System.Drawing.Size(60, 17);
            this.verticalRB.TabIndex = 0;
            this.verticalRB.TabStop = true;
            this.verticalRB.Text = "Vertical";
            this.verticalRB.UseVisualStyleBackColor = true;
            this.verticalRB.CheckedChanged += new System.EventHandler(this.verticalRB_CheckedChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(644, 928);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseEnter += new System.EventHandler(this.pictureBox_MouseEnter);
            this.pictureBox.MouseLeave += new System.EventHandler(this.pictureBox_MouseLeave);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteBtn});
            this.toolStrip2.Location = new System.Drawing.Point(78, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(72, 25);
            this.toolStrip2.TabIndex = 1;
            // 
            // deleteBtn
            // 
            this.deleteBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.delete;
            this.deleteBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(60, 22);
            this.deleteBtn.Text = "Delete";
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.platformBtn,
            this.toolStripSeparator1,
            this.movingPlatformBtn,
            this.toolStripSeparator7,
            this.colorChangerBtn,
            this.freeBoxBtn,
            this.prizeBtn});
            this.toolStrip1.Location = new System.Drawing.Point(3, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(441, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // platformBtn
            // 
            this.platformBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.platform;
            this.platformBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.platformBtn.Name = "platformBtn";
            this.platformBtn.Size = new System.Drawing.Size(73, 22);
            this.platformBtn.Text = "Platform";
            this.platformBtn.Click += new System.EventHandler(this.baseWallBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // movingPlatformBtn
            // 
            this.movingPlatformBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.platform;
            this.movingPlatformBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.movingPlatformBtn.Name = "movingPlatformBtn";
            this.movingPlatformBtn.Size = new System.Drawing.Size(117, 22);
            this.movingPlatformBtn.Text = "Moving platform";
            this.movingPlatformBtn.Click += new System.EventHandler(this.movingPlatformBtn_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // colorChangerBtn
            // 
            this.colorChangerBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.colorChanger;
            this.colorChangerBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.colorChangerBtn.Name = "colorChangerBtn";
            this.colorChangerBtn.Size = new System.Drawing.Size(104, 22);
            this.colorChangerBtn.Text = "Color Changer";
            this.colorChangerBtn.Click += new System.EventHandler(this.colorChangerBtn_Click);
            // 
            // freeBoxBtn
            // 
            this.freeBoxBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.colorChanger;
            this.freeBoxBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.freeBoxBtn.Name = "freeBoxBtn";
            this.freeBoxBtn.Size = new System.Drawing.Size(71, 22);
            this.freeBoxBtn.Text = "Free Box";
            this.freeBoxBtn.Click += new System.EventHandler(this.freeBoxBtn_Click);
            // 
            // prizeBtn
            // 
            this.prizeBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.colorChanger;
            this.prizeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.prizeBtn.Name = "prizeBtn";
            this.prizeBtn.Size = new System.Drawing.Size(52, 22);
            this.prizeBtn.Text = "Prize";
            this.prizeBtn.Click += new System.EventHandler(this.prizeBtn_Click);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBtn,
            this.saveBtn,
            this.resaveBtn,
            this.loadBtn,
            this.toolStripSeparator2,
            this.exportBtn});
            this.toolStrip3.Location = new System.Drawing.Point(3, 50);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(312, 25);
            this.toolStrip3.TabIndex = 2;
            // 
            // newBtn
            // 
            this.newBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources._new;
            this.newBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newBtn.Name = "newBtn";
            this.newBtn.Size = new System.Drawing.Size(51, 22);
            this.newBtn.Text = "New";
            this.newBtn.Click += new System.EventHandler(this.newBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.save;
            this.saveBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(51, 22);
            this.saveBtn.Text = "Save";
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // resaveBtn
            // 
            this.resaveBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.save;
            this.resaveBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.resaveBtn.Name = "resaveBtn";
            this.resaveBtn.Size = new System.Drawing.Size(79, 22);
            this.resaveBtn.Text = "ReSave all";
            this.resaveBtn.Click += new System.EventHandler(this.resaveBtn_Click);
            // 
            // loadBtn
            // 
            this.loadBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.load;
            this.loadBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(53, 22);
            this.loadBtn.Text = "Load";
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // exportBtn
            // 
            this.exportBtn.Image = global::MotleyBasketball___LevelEditor.Properties.Resources.export;
            this.exportBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(60, 22);
            this.exportBtn.Text = "Export";
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // itemsImageList
            // 
            this.itemsImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.itemsImageList.ImageSize = new System.Drawing.Size(16, 16);
            this.itemsImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // saveLevelDialog
            // 
            this.saveLevelDialog.Filter = "MotleyBasketball Level Project (*.mbp)|*.mbp";
            this.saveLevelDialog.RestoreDirectory = true;
            // 
            // openLevelDialog
            // 
            this.openLevelDialog.Filter = "MotleyBasketball Level Project (*.mbp)|*.mbp";
            this.openLevelDialog.RestoreDirectory = true;
            // 
            // renderTimer
            // 
            this.renderTimer.Enabled = true;
            this.renderTimer.Tick += new System.EventHandler(this.renderTimer_Tick);
            // 
            // tipRB
            // 
            this.tipRB.AutoSize = true;
            this.tipRB.Location = new System.Drawing.Point(104, 19);
            this.tipRB.Name = "tipRB";
            this.tipRB.Size = new System.Drawing.Size(40, 17);
            this.tipRB.TabIndex = 2;
            this.tipRB.TabStop = true;
            this.tipRB.Text = "Tip";
            this.tipRB.UseVisualStyleBackColor = true;
            this.tipRB.CheckedChanged += new System.EventHandler(this.tipRB_CheckedChanged);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(969, 1027);
            this.Controls.Add(this.toolStripContainer);
            this.Controls.Add(this.mainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "mainForm";
            this.Text = "MotleyBasketball - Level Editor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseWheel);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.propertiesGB.ResumeLayout(false);
            this.chainLengthGB.ResumeLayout(false);
            this.chainLengthGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chainLengthTB)).EndInit();
            this.hCameraPosGB.ResumeLayout(false);
            this.hCameraPosGB.PerformLayout();
            this.vCameraPosGB.ResumeLayout(false);
            this.vCameraPosGB.PerformLayout();
            this.freeBoxRotatingGB.ResumeLayout(false);
            this.freeBoxRotatingGB.PerformLayout();
            this.edgeTypeGB.ResumeLayout(false);
            this.edgeTypeGB.PerformLayout();
            this.freeBoxKindGB.ResumeLayout(false);
            this.freeBoxKindGB.PerformLayout();
            this.freeBoxSizeGB.ResumeLayout(false);
            this.freeBoxSizeGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.freeBoxHeightTB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.freeBoxWidthTB)).EndInit();
            this.startPosGB.ResumeLayout(false);
            this.startPosGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startTB)).EndInit();
            this.movingOrientationGB.ResumeLayout(false);
            this.movingOrientationGB.PerformLayout();
            this.distanceGB.ResumeLayout(false);
            this.distanceGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.distanceTB)).EndInit();
            this.levelSizeGB.ResumeLayout(false);
            this.levelSizeGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.levelHeightTB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.levelWidthTB)).EndInit();
            this.typeGB.ResumeLayout(false);
            this.lengthGB.ResumeLayout(false);
            this.lengthGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lengthTB)).EndInit();
            this.orientationGB.ResumeLayout(false);
            this.orientationGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripContainer toolStripContainer;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.ImageList itemsImageList;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.GroupBox propertiesGB;
        private System.Windows.Forms.GroupBox orientationGB;
        private System.Windows.Forms.RadioButton horizontalRB;
        private System.Windows.Forms.RadioButton verticalRB;
        private System.Windows.Forms.GroupBox lengthGB;
        private System.Windows.Forms.TrackBar lengthTB;
        private System.Windows.Forms.GroupBox typeGB;
        private System.Windows.Forms.ComboBox typeCB;
        private System.Windows.Forms.GroupBox levelSizeGB;
        private System.Windows.Forms.TrackBar levelHeightTB;
        private System.Windows.Forms.TrackBar levelWidthTB;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton platformBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton colorChangerBtn;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton deleteBtn;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton newBtn;
        private System.Windows.Forms.ToolStripButton saveBtn;
        private System.Windows.Forms.ToolStripButton loadBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton exportBtn;
        private System.Windows.Forms.SaveFileDialog saveLevelDialog;
        private System.Windows.Forms.ToolStripMenuItem newStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem saveStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem exportStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem platformToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addColorChangerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openLevelDialog;
        private System.Windows.Forms.ToolStripButton movingPlatformBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.Timer renderTimer;
        private System.Windows.Forms.GroupBox distanceGB;
        private System.Windows.Forms.TrackBar distanceTB;
        private System.Windows.Forms.GroupBox movingOrientationGB;
        private System.Windows.Forms.RadioButton mHorizontalRB;
        private System.Windows.Forms.RadioButton mVerticalRB;
        private System.Windows.Forms.GroupBox startPosGB;
        private System.Windows.Forms.TrackBar startTB;
        private System.Windows.Forms.ToolStripMenuItem mPlatformToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton freeBoxBtn;
        private System.Windows.Forms.GroupBox freeBoxSizeGB;
        private System.Windows.Forms.TrackBar freeBoxHeightTB;
        private System.Windows.Forms.TrackBar freeBoxWidthTB;
        private System.Windows.Forms.GroupBox freeBoxKindGB;
        private System.Windows.Forms.RadioButton boxRB;
        private System.Windows.Forms.RadioButton ballRB;
        private System.Windows.Forms.ToolStripButton prizeBtn;
        private System.Windows.Forms.GroupBox edgeTypeGB;
        private System.Windows.Forms.RadioButton rightOnlyRB;
        private System.Windows.Forms.RadioButton leftOnlyRB;
        private System.Windows.Forms.RadioButton allRB;
        private System.Windows.Forms.RadioButton noneRB;
        private System.Windows.Forms.GroupBox freeBoxRotatingGB;
        private System.Windows.Forms.RadioButton falseRotRB;
        private System.Windows.Forms.RadioButton trueRotRB;
        private System.Windows.Forms.GroupBox vCameraPosGB;
        private System.Windows.Forms.RadioButton vCenterRB;
        private System.Windows.Forms.RadioButton vBottomRB;
        private System.Windows.Forms.RadioButton vTopRB;
        private System.Windows.Forms.RadioButton vFreeRB;
        private System.Windows.Forms.ToolStripButton resaveBtn;
        private System.Windows.Forms.GroupBox hCameraPosGB;
        private System.Windows.Forms.RadioButton hFreeRB;
        private System.Windows.Forms.RadioButton hCenterRB;
        private System.Windows.Forms.RadioButton hRightRB;
        private System.Windows.Forms.RadioButton hLeftRB;
        private System.Windows.Forms.GroupBox chainLengthGB;
        private System.Windows.Forms.TrackBar chainLengthTB;
        private System.Windows.Forms.RadioButton tipRB;


    }
}

