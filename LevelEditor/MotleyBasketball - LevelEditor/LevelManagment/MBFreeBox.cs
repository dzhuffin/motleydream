﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBFreeBox : MBItem
    {
        public enum FreeBoxKind
        {
            Ball = 1,
            Box = 2,
            Tip = 3
        }

        private int width = 5;
        private int height = 5;

        private int chainLength = 5;

        public int ChainLength
        {
            get { return this.chainLength; }
            set { if (value > 0) this.chainLength = value; }
        }

        private FreeBoxKind kind = FreeBoxKind.Box;

        private MBColorChanger.ColorChangerType type = MBColorChanger.ColorChangerType.Red;

        private MBPlatform.VerticalCameraPosition vCameraPosition = MBPlatform.VerticalCameraPosition.Center;
        private MBPlatform.HorizontalCameraPosition hCameraPosition = MBPlatform.HorizontalCameraPosition.Free;

        public MBPlatform.VerticalCameraPosition VCameraPosition
        {
            get { return this.vCameraPosition; }
            set { this.vCameraPosition = value; }
        }

        public MBPlatform.HorizontalCameraPosition HCameraPosition
        {
            get { return this.hCameraPosition; }
            set { this.hCameraPosition = value; }
        }

        public FreeBoxKind Kind
        {
            get { return this.kind; }
            set { this.kind = value; }
        }

        public bool IsRotating { get; set; }

        public int Width
        {
            get { return this.width; }
            set { if (value > 0) this.width = value; }
        }

        public int Height
        {
            get { return this.height; }
            set { if (value > 0) this.height = value; }
        }

        public MBColorChanger.ColorChangerType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        public MBFreeBox()
        {
            IsRotating = false;
        }

        public MBFreeBox(MBColorChanger.ColorChangerType type)
        {
            IsRotating = false;
            this.type = type;
        }

        public override void Draw(Graphics gr, bool drawSelected, int halfBlockSize)
        {
            int realWidth = this.width * halfBlockSize * 2;
            int realHeight = this.height * halfBlockSize * 2;
            Color color;

            if (this.kind == FreeBoxKind.Box || this.kind == FreeBoxKind.Ball)
            {
                switch (this.type)
                {
                    case MBColorChanger.ColorChangerType.Red:
                        color = Color.Red;
                        break;
                    case MBColorChanger.ColorChangerType.Green:
                        color = Color.Green;
                        break;
                    case MBColorChanger.ColorChangerType.Blue:
                        color = Color.Blue;
                        break;
                    case MBColorChanger.ColorChangerType.Yellow:
                        color = Color.Yellow;
                        break;
                    default:
                        color = Color.Black;
                        break;
                }
            }
            else
            {
                color = Color.LightBlue;
            }

            var rect = (new Rectangle(this.centerPixelPos.X - realWidth / 2,
                                            this.centerPixelPos.Y - realHeight / 2, realWidth, realHeight));

            this.selectBrush = new HatchBrush(this.selectBrush.HatchStyle, Color.White, color);
            this.normalBrush.Color = color;

            if (drawSelected)
            {
                if (this.kind == FreeBoxKind.Box || this.kind == FreeBoxKind.Tip)
                    gr.FillRectangle(this.selectBrush, rect);
                else
                    gr.FillEllipse(this.selectBrush, rect);
            }
            else
            {
                if (this.kind == FreeBoxKind.Box || this.kind == FreeBoxKind.Tip)
                    gr.FillRectangle(this.normalBrush, rect);
                else
                    gr.FillEllipse(this.normalBrush, rect);
            }

            if (IsRotating && this.kind != FreeBoxKind.Tip)
            {
                var chainRect = (new Rectangle(this.centerPixelPos.X - halfBlockSize / 2,
                                 this.centerPixelPos.Y - (halfBlockSize * 2 * this.chainLength), 
                                 halfBlockSize, halfBlockSize * 2 * this.chainLength));

                this.normalBrush.Color = Color.Gray;
                gr.FillRectangle(this.normalBrush, chainRect);
            }

            if (this.kind == FreeBoxKind.Tip)
                gr.DrawString(this.chainLength.ToString(), new Font("Arial", 26), Brushes.Black, this.centerPixelPos.X, this.centerPixelPos.Y);
        }
    }
}
