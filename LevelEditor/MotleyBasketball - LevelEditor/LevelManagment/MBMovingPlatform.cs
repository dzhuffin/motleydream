﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBMovingPlatform : MBPlatform
    {
        #region Fields
        private int distance = 5;
        private int startPos = 0;
        private int direction = 1;
        private long tickCount = 0;
        private Point currentLevelPos = new Point(0, 0);
        private PlatformOrientation movingOrientation = PlatformOrientation.Horizontal;
        #endregion

        #region Properties
        public int Distance
        {
            get { return this.distance; }
            set { if (value >= 0) this.distance = value; }
        }

        public int StartPos
        {
            get { return this.startPos; }
            set { if (value >= 0) this.startPos = value; }
        }

        public Point CurrentLevelPos
        {
            get { return this.currentLevelPos; }
            set { if ((value.X >= 0) && (value.Y >= 0)) this.currentLevelPos = value; }
        }

        public PlatformOrientation MovingOrientation
        {
            get { return this.movingOrientation; }
            set { this.movingOrientation = value; }
        }

        public new PlatformType Type
        {
            get { return this.type; }
            set
            {
                if (value == PlatformType.LeftWall || value == PlatformType.RightWall ||
                    value == PlatformType.Roof)
                    return;

                this.type = value;
            }
        }  
        #endregion

        public MBMovingPlatform()
            : base()
        {
        }

        public MBMovingPlatform(PlatformType type, PlatformOrientation orientation)
            : base(type, orientation)
        {
            this.currentLevelPos = this.LevelPos;
        }

        public override void Draw(Graphics gr, bool drawSelected, int halfBlockSize)
        {
            if (drawSelected)
                this.currentLevelPos = this.LevelPos;
            else
            {
                if ((System.Environment.TickCount - this.tickCount) > 200L)
                {
                    if (this.movingOrientation == PlatformOrientation.Horizontal)
                    {
                        this.currentLevelPos.Offset(this.direction, 0);

                        if (this.currentLevelPos.X >= (this.LevelPos.X + this.distance))
                            this.direction = -1;

                        if (this.currentLevelPos.X <= this.LevelPos.X)
                            this.direction = 1;

                        this.currentLevelPos.Y = this.LevelPos.Y;
                    }

                    if (this.movingOrientation == PlatformOrientation.Vertical)
                    {
                        this.currentLevelPos.Offset(0, this.direction);

                        if (this.currentLevelPos.Y >= (this.LevelPos.Y + this.distance))
                            this.direction = -1;

                        if (this.currentLevelPos.Y <= this.LevelPos.Y)
                            this.direction = 1;

                        this.currentLevelPos.X = this.LevelPos.X;
                    }

                    this.tickCount = System.Environment.TickCount;
                }
            }

            Point tmp = this.LevelPos;
            this.LevelPos = this.currentLevelPos;

            base.Draw(gr, drawSelected, halfBlockSize);

            int realLength = this.Length * halfBlockSize * 2;

            Font font = new Font("Arial", 10.0f);            
            if (this.Orientation == PlatformOrientation.Horizontal)
            {
                gr.DrawString("M", font, Brushes.Black, this.centerPixelPos.X - realLength / 2, this.centerPixelPos.Y - halfBlockSize);
                if (this.Length > 1)
                    gr.DrawString("M", font, Brushes.Black, this.centerPixelPos.X + realLength / 2 - halfBlockSize * 2, this.centerPixelPos.Y - halfBlockSize);
                if (this.Length > 2)
                    gr.DrawString("M", font, Brushes.Black, this.centerPixelPos.X - halfBlockSize, this.centerPixelPos.Y - halfBlockSize);
            }
            else
            {
                gr.DrawString("M", font, Brushes.Black, this.centerPixelPos.X - halfBlockSize, this.centerPixelPos.Y - realLength / 2);
                if (this.Length > 1)
                    gr.DrawString("M", font, Brushes.Black, this.centerPixelPos.X - halfBlockSize, this.centerPixelPos.Y + realLength / 2 - halfBlockSize * 2);
                if (this.Length > 2)
                    gr.DrawString("M", font, Brushes.Black, this.centerPixelPos.X - halfBlockSize, this.centerPixelPos.Y - halfBlockSize);
            }
            

            this.LevelPos = tmp;
        }
    }
}
