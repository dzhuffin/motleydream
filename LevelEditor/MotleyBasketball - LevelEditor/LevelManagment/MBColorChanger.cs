﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBColorChanger : MBItem
    {
        public enum ColorChangerType
        {
            Red = 2,
            Green = 3,
            Blue = 4, 
            Yellow = 5
        }

        private ColorChangerType type = ColorChangerType.Red;

        public ColorChangerType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        public MBColorChanger()
        {
            this.type = type;
        }

        public MBColorChanger(ColorChangerType type)
        {
            this.type = type;
        }

        public override void Draw(Graphics gr, bool drawSelected, int halfBlockSize)
        {
            Color color;

            switch (this.type)
            {
                case ColorChangerType.Red:
                    color = Color.Red;
                    break;
                case ColorChangerType.Green:
                    color = Color.Green;
                    break;
                case ColorChangerType.Blue:
                    color = Color.Blue;
                    break;
                case ColorChangerType.Yellow:
                    color = Color.Yellow;
                    break;
                default:
                    color = Color.Black;
                    break;
            }

            Rectangle rect = new Rectangle(this.centerPixelPos.X - halfBlockSize * 2,
                                           this.centerPixelPos.Y - halfBlockSize * 2, halfBlockSize * 4, halfBlockSize * 4);

            this.selectBrush = new HatchBrush(this.selectBrush.HatchStyle, Color.White, color);
            this.normalBrush.Color = color;

            if (drawSelected)
                gr.FillEllipse(this.selectBrush, rect);
            else
                gr.FillEllipse(this.normalBrush, rect);
        }
    }
}
