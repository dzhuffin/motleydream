﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBPrize : MBItem
    {
        public enum PrizeType
        {
            Score = 1,
            Teleport = 2
        }

        private PrizeType type = PrizeType.Score;

        public PrizeType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        public MBPrize()
        {
        }

        public override void Draw(Graphics gr, bool drawSelected, int halfBlockSize)
        {
            int realWidth = 2 * halfBlockSize * 2;
            int realHeight = 2 * halfBlockSize * 2;
            Color color = Color.CornflowerBlue;
            
            Rectangle rect = (new Rectangle(this.centerPixelPos.X - realWidth / 2,
                                            this.centerPixelPos.Y - realHeight / 2, realWidth, realHeight));

            this.selectBrush = new HatchBrush(this.selectBrush.HatchStyle, Color.White, color);
            this.normalBrush.Color = color;

            Point[] points = new Point[6];
            points[0].X = rect.Left + rect.Width / 3; points[0].Y = rect.Top;
            points[1].X = rect.Left + rect.Width * 2 / 3; points[1].Y = rect.Top;
            points[2].X = rect.Left + rect.Width; points[2].Y = rect.Top + rect.Height / 2;
            points[3].X = rect.Left + rect.Width * 2 / 3; points[3].Y = rect.Top + rect.Height;
            points[4].X = rect.Left + rect.Width / 3; points[4].Y = rect.Top + rect.Height;
            points[5].X = rect.Left; points[5].Y = rect.Top + rect.Height / 2;

            if (drawSelected)
            {                
                gr.FillPolygon(this.selectBrush, points);
            }
            else
            {
                gr.FillPolygon(this.normalBrush, points);
            }
        }
    }
}
