﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBBasket : MBItem
    {
        public override void Draw(Graphics gr, bool drawSelected, int halfBlockSize)
        {
            int basketSize = halfBlockSize * 8;
            Rectangle rect = new Rectangle(this.centerPixelPos.X - basketSize / 2, 
                                           this.centerPixelPos.Y - basketSize / 2, basketSize, basketSize);

            this.selectBrush = new HatchBrush(this.selectBrush.HatchStyle, Color.White, Color.Coral);
            this.normalBrush.Color = Color.Coral;            

            if (drawSelected)
                gr.FillPie(this.selectBrush, rect, -30.0f, 240.0f);
            else
                gr.FillPie(this.normalBrush, rect, -30.0f, 240.0f);
        }
    }
}