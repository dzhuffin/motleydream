﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBPlatform : MBItem
    {
        public enum PlatformOrientation
        {
            Vertical = 0,
            Horizontal = 1
        }

        public enum PlatformType
        {
            Floor = 0,
            Usual = 1,
            Red = 2,
            Green = 3,
            Blue = 4,
            Yellow = 5,
            LeftWall = 6,
            RightWall = 7,
            Roof = 8
        }

        public enum EdgeType
        {
            None = 0,
            LeftOnly = 1,
            RightOnly = 2,
            All = 3
        }

        public enum VerticalCameraPosition
        {
            Top = 0,
            Bottom = 1,
            Center = 2,
            Free = 3
        }

        public enum HorizontalCameraPosition
        {
            Left = 0,
            Right = 1,
            Center = 2,
            Free = 3
        }

        #region Fields
        private PlatformOrientation orientation = PlatformOrientation.Horizontal;
        protected PlatformType type = PlatformType.Usual;
        private EdgeType edgeType = EdgeType.All;
        private VerticalCameraPosition vCameraPosition = VerticalCameraPosition.Free;
        private HorizontalCameraPosition hCameraPosition = HorizontalCameraPosition.Free;

        private int length = 10; 
        #endregion

        #region Properties

        public VerticalCameraPosition VCameraPosition
        {
            get { return this.vCameraPosition; }
            set { this.vCameraPosition = value; }
        }

        public HorizontalCameraPosition HCameraPosition
        {
            get { return this.hCameraPosition; }
            set { this.hCameraPosition = value; }
        }  

        public PlatformType Type
        {
            get { return this.type; }
            set
            {
                if (value == PlatformType.LeftWall || value == PlatformType.RightWall)
                {
                    this.orientation = PlatformOrientation.Vertical;
                    this.vCameraPosition = VerticalCameraPosition.Free;
                    this.hCameraPosition = HorizontalCameraPosition.Free;
                } 
                else if (value == PlatformType.Roof)
                {
                    this.orientation = PlatformOrientation.Horizontal;
                    this.vCameraPosition = VerticalCameraPosition.Free;
                    this.hCameraPosition = HorizontalCameraPosition.Free;
                }

                this.type = value;
            }
        }  

        public PlatformOrientation Orientation
        {
            get { return this.orientation; }
            set { this.orientation = value; }
        }

        public EdgeType Edge
        {
            get { return this.edgeType; }
            set { this.edgeType = value; }
        }
        
        public int Length
        {
            get { return this.length; }
            set { if (value > 0) this.length = value; }
        } 
        #endregion

        public MBPlatform()
        {
        }

        public MBPlatform(PlatformType type, PlatformOrientation orientation)
        {
            this.type = type;
            this.orientation = orientation;
        }

        public override void Draw(Graphics gr, bool drawSelected, int halfBlockSize)
        {
            int realLength = this.length * halfBlockSize * 2;
            Color color;
            int halfPlatformSize = halfBlockSize;

            switch (this.type)
            {
                case PlatformType.Floor:
                    color = Color.BurlyWood;
                    break;
                case PlatformType.Usual:
                    color = Color.Gray;
                    break;
                case PlatformType.Red:
                    color = Color.Red;
                    break;
                case PlatformType.Green:
                    color = Color.Green;
                    break;
                case PlatformType.Blue:
                    color = Color.Blue;
                    break;
                case PlatformType.Yellow:
                    color = Color.Yellow;
                    break;
                case PlatformType.LeftWall:
                case PlatformType.RightWall:
                    halfPlatformSize = halfBlockSize * 2;
                    color = Color.Purple;
                    break;
                case PlatformType.Roof:
                    color = Color.Pink;
                    break;
                default:
                    color = Color.Black;
                    break;
            }

            Rectangle rect = (this.orientation == PlatformOrientation.Horizontal) ?
                                new Rectangle(this.centerPixelPos.X - realLength / 2,
                                    this.centerPixelPos.Y - halfPlatformSize, realLength, halfPlatformSize * 2) :
                                new Rectangle(this.centerPixelPos.X - halfBlockSize,
                                    this.centerPixelPos.Y - realLength / 2, halfPlatformSize * 2, realLength);

            this.selectBrush = new HatchBrush(this.selectBrush.HatchStyle, Color.White, color);
            this.normalBrush.Color = color;

            if (drawSelected)
                gr.FillRectangle(this.selectBrush, rect);
            else
                gr.FillRectangle(this.normalBrush, rect);
        }
    }
}
