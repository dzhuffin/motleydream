﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    abstract class MBItem : IComparable
    {
        protected Point centerPixelPos = new Point(0, 0);
        protected Point levelPos = new Point(0, 0);

        protected HatchBrush selectBrush = new HatchBrush(HatchStyle.SmallCheckerBoard, Color.Black);
        protected SolidBrush normalBrush = new SolidBrush(Color.Black);

        public int CenterX
        {
            get { return this.centerPixelPos.X; }
            set { this.centerPixelPos.X = value; }
        }
        public int CenterY
        {
            get { return this.centerPixelPos.Y; }
            set { this.centerPixelPos.Y = value; }
        }

        public Point LevelPos
        {
            get { return this.levelPos; }
            set { if ((value.X >= 0) && (value.Y >= 0)) this.levelPos = value; }
        }

        abstract public void Draw(Graphics gr, bool drawSelected, int halfBlockSize);

        #region IComparable Members

        public int CompareTo(object obj)
        {
            MBItem item = (MBItem)obj;
            return this.levelPos.Y.CompareTo(item.levelPos.Y);
        }

        #endregion
    }
}