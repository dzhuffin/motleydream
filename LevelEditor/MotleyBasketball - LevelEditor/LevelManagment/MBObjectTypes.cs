﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    public enum MB_OBJECT_TYPES
    {
        Ball = 0,
        Basket = 1,
        ColorChanger = 2,
        Platform = 3,
        MovingPlatform = 4
    }
}
