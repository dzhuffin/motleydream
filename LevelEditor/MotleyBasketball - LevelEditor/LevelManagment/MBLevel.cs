﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using System.IO;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBLevel
    {
        #region Fields
        private int drawOffsetX = 0;
        private int drawOffsetY = 0;

        private int width = 25;
        private int height = 25;

        private int blockSize = 20;

        private MBItem activeItem = null;

        private Pen borderPen = new Pen(Color.Black);

        private List<MBPlatform> platforms = new List<MBPlatform>();
        private List<MBMovingPlatform> movingPlatforms = new List<MBMovingPlatform>();
        private List<MBColorChanger> colorChangers = new List<MBColorChanger>();
        private List<MBFreeBox> freeBoxes = new List<MBFreeBox>();
        private List<MBPrize> prizes = new List<MBPrize>();
        private MBBasket basket = new MBBasket();
        private MBBall ball = new MBBall();
        #endregion

        #region Properties
        public MBBall Ball
        {
            get { return this.ball; }
        }

        public MBBasket Basket
        {
            get { return this.basket; }
        }

        public int DrawOffsetX
        {
            get { return this.drawOffsetX; }
            set { this.drawOffsetX = value; }
        }

        public int DrawOffsetY
        {
            get { return this.drawOffsetY; }
            set { this.drawOffsetY = value; }
        }

        public int Width
        {
            get { return this.width; }
            set { if (value >= 10) this.width = value; }
        }

        public int Height
        {
            get { return this.height; }
            set { if (value >= 10) this.height = value; }
        }

        public int BlockSize
        {
            get { return this.blockSize; }
        }

        public MBItem ActiveItem
        {
            get { return this.activeItem; }
        }
        #endregion

        public MBLevel()
        {
            this.ball.LevelPos = new Point(2, 2);
            this.basket.LevelPos = new Point(7, 7);
            this.borderPen.Width = 3;
        }

        public int AddPlatform(MBPlatform.PlatformType type, MBPlatform.PlatformOrientation orientation)
        {
            this.platforms.Add(new MBPlatform(type, orientation));
            return this.platforms.Count - 1;
        }

        public int AddMovingPlatform(MBPlatform.PlatformType type, MBPlatform.PlatformOrientation orientation)
        {
            this.movingPlatforms.Add(new MBMovingPlatform(type, orientation));
            return this.movingPlatforms.Count - 1;
        }

        public int AddColorChanger(MBColorChanger.ColorChangerType type)
        {
            this.colorChangers.Add(new MBColorChanger(type));
            return this.colorChangers.Count - 1;
        }

        public int AddFreeBox(MBColorChanger.ColorChangerType type)
        {
            this.freeBoxes.Add(new MBFreeBox(type));
            return this.freeBoxes.Count - 1;
        }

        public int AddPrize()
        {
            this.prizes.Add(new MBPrize());
            return this.prizes.Count - 1;
        }

        public void SelectItem(int pixelX, int pixelY)
        {
            int levelX = (pixelX - this.drawOffsetX) / this.blockSize;
            int levelY = (pixelY - this.drawOffsetY) / this.blockSize;

            this.activeItem = null;
            foreach (var platform in this.platforms)
            {
                if (platform.Orientation == MBPlatform.PlatformOrientation.Horizontal)
                    this.activeItem = ((levelX >= (platform.LevelPos.X)) &&
                                       (levelX <= (platform.LevelPos.X) + platform.Length - 1) &&
                                       (platform.LevelPos.Y == levelY)) ? 
                                        platform : null;
                else
                    this.activeItem = ((levelY >= (platform.LevelPos.Y)) &&
                                       (levelY <= (platform.LevelPos.Y) + platform.Length - 1) &&
                                       (platform.LevelPos.X == levelX)) ? 
                                        platform : null;

                if (this.activeItem != null)
                    return;
            }

            foreach (var movingPlatform in this.movingPlatforms)
            {
                if (movingPlatform.Orientation == MBPlatform.PlatformOrientation.Horizontal)
                    this.activeItem = ((levelX >= (movingPlatform.CurrentLevelPos.X)) &&
                                       (levelX <= (movingPlatform.CurrentLevelPos.X) + movingPlatform.Length - 1) &&
                                       (movingPlatform.CurrentLevelPos.Y == levelY)) ?
                                        movingPlatform : null;
                else
                    this.activeItem = ((levelY >= (movingPlatform.CurrentLevelPos.Y)) &&
                                       (levelY <= (movingPlatform.CurrentLevelPos.Y) + movingPlatform.Length - 1) &&
                                       (movingPlatform.CurrentLevelPos.X == levelX)) ?
                                        movingPlatform : null;

                if (this.activeItem != null)
                    return;
            }

            foreach (var freeBox in this.freeBoxes)
            {
                this.activeItem = ((levelX >= (freeBox.LevelPos.X)) && (levelX <= (freeBox.LevelPos.X) + freeBox.Width - 1) &&
                                   (levelY >= (freeBox.LevelPos.Y)) && (levelY <= (freeBox.LevelPos.Y) + freeBox.Height - 1)) ?
                                        freeBox : null;

                if (this.activeItem != null)
                    return;
            }

            foreach (var colorChanger in this.colorChangers)
            {
                if (((levelX == colorChanger.LevelPos.X) || (levelX == colorChanger.LevelPos.X + 1)) && 
                    ((levelY == colorChanger.LevelPos.Y) || (levelY == colorChanger.LevelPos.Y + 1)))
                    this.activeItem = colorChanger;

                if (this.activeItem != null)
                    return;
            }

            foreach (var prize in this.prizes)
            {
                if (((levelX == prize.LevelPos.X) || (levelX == prize.LevelPos.X + 1)) &&
                    ((levelY == prize.LevelPos.Y) || (levelY == prize.LevelPos.Y + 1)))
                    this.activeItem = prize;

                if (this.activeItem != null)
                    return;
            }

            if (((levelX >= this.basket.LevelPos.X - 1) && (levelX <= this.basket.LevelPos.X + 3)) &&
                ((levelY >= this.basket.LevelPos.Y - 1) && (levelY <= this.basket.LevelPos.Y + 3)))
                this.activeItem = this.basket;
            else
            {
                if ((levelX == this.ball.LevelPos.X) && (levelY == this.ball.LevelPos.Y))
                    this.activeItem = this.ball;
                else
                    this.activeItem = null;
            }
        }

        public void MoveSelected(int pixelX, int pixelY)
        {
            if (this.activeItem == null)
                return;

            int levelX = (pixelX - this.drawOffsetX) / this.blockSize;
            int levelY = (pixelY - this.drawOffsetY) / this.blockSize;

            if ((levelX >= 0) && (levelX < this.width) && (levelY >= 0) && (levelY < this.height))
                this.activeItem.LevelPos = new Point(levelX, levelY);
        }

        public void Delete()
        {
            if (this.activeItem == null)
                return;

            if (this.activeItem.GetType() == typeof(MBPlatform))
                this.platforms.Remove((MBPlatform)this.activeItem);

            if (this.activeItem.GetType() == typeof(MBMovingPlatform))
                this.movingPlatforms.Remove((MBMovingPlatform)this.activeItem);

            if (this.activeItem.GetType() == typeof(MBColorChanger))
                this.colorChangers.Remove((MBColorChanger)this.activeItem);

            if (this.activeItem.GetType() == typeof(MBFreeBox))
                this.freeBoxes.Remove((MBFreeBox)this.activeItem);

            if (this.activeItem.GetType() == typeof(MBPrize))
                this.prizes.Remove((MBPrize)this.activeItem);
        }

        public void New()
        {
            this.activeItem = null;

            this.movingPlatforms.Clear();
            this.platforms.Clear();
            this.colorChangers.Clear();
            this.freeBoxes.Clear();
            this.prizes.Clear();

            this.ball.LevelPos = new Point(2, 2);
            this.basket.LevelPos = new Point(7, 7);
        }

        public void ResaveAll()
        {
            var basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var backUpPath = Path.Combine(basePath, "backup." + DateTime.Now.ToString("yy.MM.dd.HH.mm.ss.fff"));
            var levels= Directory.GetFiles(basePath, "*.mbp");

            if (Directory.Exists(backUpPath))
                Directory.Delete(backUpPath, true);

            Directory.CreateDirectory(backUpPath);

            foreach (var level in levels)
            {
                File.Copy(level, Path.Combine(backUpPath, Path.GetFileName(level)));
                this.Load(level);
                this.Save(level);
            }
        }

        public void Save(string fileName)
        {
            FileStream stream = new FileStream(fileName, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(this.width);
            writer.Write(this.height);

            writer.Write(this.basket.LevelPos.X);
            writer.Write(this.basket.LevelPos.Y);

            writer.Write(this.ball.LevelPos.X);
            writer.Write(this.ball.LevelPos.Y);

            writer.Write(this.platforms.Count);

            foreach (var platform in this.platforms)
            {
                writer.Write(platform.Length);
                writer.Write(platform.LevelPos.X);
                writer.Write(platform.LevelPos.Y);
                writer.Write((int)platform.Orientation);
                writer.Write((int)platform.Type);
                writer.Write((int)platform.Edge);
                writer.Write((int)platform.VCameraPosition);
                writer.Write((int)platform.HCameraPosition);
            }

            writer.Write(this.movingPlatforms.Count);

            foreach (var movingPlatform in this.movingPlatforms)
            {
                writer.Write(movingPlatform.Length);
                writer.Write(movingPlatform.LevelPos.X);
                writer.Write(movingPlatform.LevelPos.Y);
                writer.Write((int)movingPlatform.Orientation);
                writer.Write((int)movingPlatform.Type);
                writer.Write(movingPlatform.Distance);
                writer.Write((int)movingPlatform.MovingOrientation);
                writer.Write(movingPlatform.StartPos);
                writer.Write((int)movingPlatform.VCameraPosition);
                writer.Write((int)movingPlatform.HCameraPosition);
            }

            writer.Write(this.colorChangers.Count);

            foreach (var colorChanger in this.colorChangers)
            {
                writer.Write(colorChanger.LevelPos.X);
                writer.Write(colorChanger.LevelPos.Y);
                writer.Write((int)colorChanger.Type);
            }

            writer.Write(this.freeBoxes.Count);

            foreach (var freeBox in this.freeBoxes)
            {
                writer.Write(freeBox.LevelPos.X);
                writer.Write(freeBox.LevelPos.Y);
                writer.Write(freeBox.Width);
                writer.Write(freeBox.Height);
                writer.Write((int)freeBox.Kind);
                writer.Write((int)freeBox.Type);
                writer.Write(freeBox.IsRotating ? 1 : 0);
                writer.Write((int)freeBox.VCameraPosition);
                writer.Write((int)freeBox.HCameraPosition);
                writer.Write(freeBox.ChainLength);
            }            
            //
            int starsCount = 0;
            foreach (var prize in this.prizes)
            {
                if (prize.Type == MBPrize.PrizeType.Score)
                    starsCount++;
            }
            writer.Write(this.prizes.Count);
            writer.Write(starsCount);

            foreach (var prize in this.prizes)
            {
                writer.Write(prize.LevelPos.X);
                writer.Write(prize.LevelPos.Y);
                writer.Write((int)prize.Type);
            } 

            writer.Close();
        }

        #region Write Objects
        private void WritePlatform(BinaryWriter writer, MBPlatform platform)
        {
            writer.Write((int)MB_OBJECT_TYPES.Platform);
            writer.Write(platform.Length);
            writer.Write(platform.LevelPos.X);
            writer.Write(platform.LevelPos.Y);
            writer.Write((int)platform.Orientation);
            writer.Write((int)platform.Type);
        }

        private void WriteMovingPlatform(BinaryWriter writer, MBMovingPlatform movingPlatform)
        {
            writer.Write((int)MB_OBJECT_TYPES.MovingPlatform);
            writer.Write(movingPlatform.Length);
            writer.Write(movingPlatform.LevelPos.X);
            writer.Write(movingPlatform.LevelPos.Y);
            writer.Write((int)movingPlatform.Orientation);
            writer.Write((int)movingPlatform.Type);
            writer.Write(movingPlatform.Distance);
            writer.Write((int)movingPlatform.MovingOrientation);
            writer.Write(movingPlatform.StartPos);
        }

        private void WriteColorChanger(BinaryWriter writer, MBColorChanger colorChanger)
        {
            writer.Write((int)MB_OBJECT_TYPES.ColorChanger);
            writer.Write(colorChanger.LevelPos.X);
            writer.Write(colorChanger.LevelPos.Y);
            writer.Write((int)colorChanger.Type);
        }

        private void WriteBasket(BinaryWriter writer)
        {
            writer.Write((int)MB_OBJECT_TYPES.Basket);
            writer.Write(this.basket.LevelPos.X);
            writer.Write(this.basket.LevelPos.Y);
        }

        private void WriteBall(BinaryWriter writer)
        {
            writer.Write((int)MB_OBJECT_TYPES.Ball);
            writer.Write(this.ball.LevelPos.X);
            writer.Write(this.ball.LevelPos.Y);
        }

        private void WriteItem(BinaryWriter writer, object item)
        {
            if (item.GetType() == typeof(MBPlatform))
                this.WritePlatform(writer, (MBPlatform)item);

            if (item.GetType() == typeof(MBMovingPlatform))
                this.WriteMovingPlatform(writer, (MBMovingPlatform)item);

            if (item.GetType() == typeof(MBColorChanger))
                this.WriteColorChanger(writer, (MBColorChanger)item);

            if (item.GetType() == typeof(MBBasket))
                this.WriteBasket(writer);

            if (item.GetType() == typeof(MBBall))
                this.WriteBall(writer);
        }
        #endregion

        public void Export(string fileName)
        {
            FileStream stream = new FileStream(fileName, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(this.width);
            writer.Write(this.height);

            List<MBItem> items = new List<MBItem>();

            foreach (var platform in this.platforms)
                items.Add(platform);

            foreach (var movingPlatform in this.movingPlatforms)
                items.Add(movingPlatform);

            foreach (var colorChanger in this.colorChangers)
                items.Add(colorChanger);

            items.Add(this.ball);
            items.Add(this.basket);

            items.Sort();

            foreach (var item in items)
                this.WriteItem(writer, item);

            writer.Close();
        }

        public void Load(string fileName)
        {
            FileStream stream = new FileStream(fileName, FileMode.Open);
            BinaryReader reader = new BinaryReader(stream);

            this.width = reader.ReadInt32();
            this.height = reader.ReadInt32();

            int levelX = 0, levelY = 0;

            levelX = reader.ReadInt32();
            levelY = reader.ReadInt32();
            this.basket.LevelPos = new Point(levelX, levelY);

            levelX = reader.ReadInt32();
            levelY = reader.ReadInt32();
            this.ball.LevelPos = new Point(levelX, levelY);

            int plCount = reader.ReadInt32();
            this.platforms.Clear();
            MBPlatform newPlatform;            

            for (int i = 0; i < plCount; i++)
            {
                newPlatform = new MBPlatform();
                newPlatform.Length = reader.ReadInt32();
                levelX = reader.ReadInt32();
                levelY = reader.ReadInt32();
                newPlatform.LevelPos = new Point(levelX, levelY);
                newPlatform.Orientation = (MBPlatform.PlatformOrientation)reader.ReadInt32();
                newPlatform.Type = (MBPlatform.PlatformType)reader.ReadInt32();
                newPlatform.Edge = (MBPlatform.EdgeType)reader.ReadInt32();
                newPlatform.VCameraPosition = (MBPlatform.VerticalCameraPosition)reader.ReadInt32();
                newPlatform.HCameraPosition = (MBPlatform.HorizontalCameraPosition)reader.ReadInt32();

                this.platforms.Add(newPlatform);
            }

            int mplCount = reader.ReadInt32();
            this.movingPlatforms.Clear();
            MBMovingPlatform newMPlatform;
            levelX = levelY = 0;

            for (int i = 0; i < mplCount; i++)
            {
                newMPlatform = new MBMovingPlatform();
                newMPlatform.Length = reader.ReadInt32();
                levelX = reader.ReadInt32();
                levelY = reader.ReadInt32();
                newMPlatform.LevelPos = new Point(levelX, levelY);                
                newMPlatform.Orientation = (MBPlatform.PlatformOrientation)reader.ReadInt32();
                newMPlatform.Type = (MBPlatform.PlatformType)reader.ReadInt32();
                newMPlatform.Distance = reader.ReadInt32();
                newMPlatform.MovingOrientation = (MBPlatform.PlatformOrientation)reader.ReadInt32();
                newMPlatform.StartPos = reader.ReadInt32();
                if (newMPlatform.MovingOrientation == MBPlatform.PlatformOrientation.Horizontal)
                    newMPlatform.CurrentLevelPos = new Point(newMPlatform.LevelPos.X + newMPlatform.StartPos, newMPlatform.LevelPos.Y);
                else
                    newMPlatform.CurrentLevelPos = new Point(newMPlatform.LevelPos.X, newMPlatform.LevelPos.Y + newMPlatform.StartPos);
                newMPlatform.VCameraPosition = (MBPlatform.VerticalCameraPosition)reader.ReadInt32();
                newMPlatform.HCameraPosition = (MBPlatform.HorizontalCameraPosition)reader.ReadInt32();

                this.movingPlatforms.Add(newMPlatform);
            }

            int ccCount = reader.ReadInt32();
            this.colorChangers.Clear();
            MBColorChanger newColorChanger;

            for (int i = 0; i < ccCount; i++)
            {
                newColorChanger = new MBColorChanger();
                levelX = reader.ReadInt32();
                levelY = reader.ReadInt32();
                newColorChanger.LevelPos = new Point(levelX, levelY);
                newColorChanger.Type = (MBColorChanger.ColorChangerType)reader.ReadInt32();

                this.colorChangers.Add(newColorChanger);
            }

            int fbCount = reader.ReadInt32();
            this.freeBoxes.Clear();
            MBFreeBox newFreeBox;

            for (int i = 0; i < fbCount; i++)
            {
                newFreeBox = new MBFreeBox();
                levelX = reader.ReadInt32();
                levelY = reader.ReadInt32();
                newFreeBox.LevelPos = new Point(levelX, levelY);
                newFreeBox.Width = reader.ReadInt32();
                newFreeBox.Height = reader.ReadInt32();
                newFreeBox.Kind = (MBFreeBox.FreeBoxKind)reader.ReadInt32();
                newFreeBox.Type = (MBColorChanger.ColorChangerType)reader.ReadInt32();
                newFreeBox.IsRotating = reader.ReadInt32() == 1;
                newFreeBox.VCameraPosition = (MBPlatform.VerticalCameraPosition)reader.ReadInt32();
                newFreeBox.HCameraPosition = (MBPlatform.HorizontalCameraPosition)reader.ReadInt32();
                newFreeBox.ChainLength = reader.ReadInt32();

                this.freeBoxes.Add(newFreeBox);
            }

            int pCount = reader.ReadInt32();
            reader.ReadInt32(); // read stars count
            this.prizes.Clear();
            MBPrize newPrize;

            for (int i = 0; i < pCount; i++)
            {
                newPrize = new MBPrize();
                levelX = reader.ReadInt32();
                levelY = reader.ReadInt32();
                newPrize.LevelPos = new Point(levelX, levelY);
                newPrize.Type = (MBPrize.PrizeType)reader.ReadInt32();

                this.prizes.Add(newPrize);
            }

            reader.Close();
        }

        public void Draw(Graphics gr)
        {            
            foreach (var platform in this.platforms)
            {
                platform.CenterX = (platform.Orientation == MBPlatform.PlatformOrientation.Horizontal) ?
                                    (platform.LevelPos.X) * this.blockSize + ((platform.Length * this.blockSize) / 2) :
                                    (platform.LevelPos.X) * this.blockSize + this.blockSize / 2;

                platform.CenterY =  (platform.Orientation == MBPlatform.PlatformOrientation.Horizontal) ?
                                    (platform.LevelPos.Y) * this.blockSize + this.blockSize / 2 :
                                    (platform.LevelPos.Y) * this.blockSize + ((platform.Length * this.blockSize) / 2);

                platform.CenterX += this.drawOffsetX;
                platform.CenterY += this.drawOffsetY;

                platform.Draw(gr, (platform == this.activeItem), this.blockSize / 2);
            }

            foreach (var movingPlatform in this.movingPlatforms)
            {
                movingPlatform.CenterX = (movingPlatform.Orientation == MBPlatform.PlatformOrientation.Horizontal) ?
                                    (movingPlatform.CurrentLevelPos.X) * this.blockSize + ((movingPlatform.Length * this.blockSize) / 2) :
                                    (movingPlatform.CurrentLevelPos.X) * this.blockSize + this.blockSize / 2;

                movingPlatform.CenterY = (movingPlatform.Orientation == MBPlatform.PlatformOrientation.Horizontal) ?
                                    (movingPlatform.CurrentLevelPos.Y) * this.blockSize + this.blockSize / 2 :
                                    (movingPlatform.CurrentLevelPos.Y) * this.blockSize + ((movingPlatform.Length * this.blockSize) / 2);

                movingPlatform.CenterX += this.drawOffsetX;
                movingPlatform.CenterY += this.drawOffsetY;

                movingPlatform.Draw(gr, (movingPlatform == this.activeItem), this.blockSize / 2);
            }

            foreach (var colorChanger in this.colorChangers)
            {
                colorChanger.CenterX = (colorChanger.LevelPos.X) * this.blockSize + this.blockSize;
                colorChanger.CenterY = (colorChanger.LevelPos.Y) * this.blockSize + this.blockSize;
                colorChanger.CenterX += this.drawOffsetX;
                colorChanger.CenterY += this.drawOffsetY;

                colorChanger.Draw(gr, (colorChanger == this.activeItem), this.blockSize / 2);
            }

            foreach (var freeBox in this.freeBoxes)
            {
                freeBox.CenterX = (freeBox.LevelPos.X) * this.blockSize + ((freeBox.Width * this.blockSize) / 2);
                freeBox.CenterY = (freeBox.LevelPos.Y) * this.blockSize + ((freeBox.Height * this.blockSize) / 2);
                freeBox.CenterX += this.drawOffsetX;
                freeBox.CenterY += this.drawOffsetY;

                freeBox.Draw(gr, (freeBox == this.activeItem), this.blockSize / 2);
            }

            foreach (var prize in this.prizes)
            {
                prize.CenterX = (prize.LevelPos.X) * this.blockSize + this.blockSize;
                prize.CenterY = (prize.LevelPos.Y) * this.blockSize + this.blockSize;
                prize.CenterX += this.drawOffsetX;
                prize.CenterY += this.drawOffsetY;

                prize.Draw(gr, (prize == this.activeItem), this.blockSize / 2);
            }

            this.basket.CenterX = (this.basket.LevelPos.X) * this.blockSize + this.blockSize;
            this.basket.CenterY = (this.basket.LevelPos.Y) * this.blockSize + this.blockSize;
            this.basket.CenterX += this.drawOffsetX;
            this.basket.CenterY += this.drawOffsetY;
            this.basket.Draw(gr, (this.basket == this.activeItem), this.blockSize / 2);

            this.ball.CenterX = (this.ball.LevelPos.X) * this.blockSize + this.blockSize / 2;
            this.ball.CenterY = (this.ball.LevelPos.Y) * this.blockSize + this.blockSize / 2;
            this.ball.CenterX += this.drawOffsetX;
            this.ball.CenterY += this.drawOffsetY;
            this.ball.Draw(gr, (this.ball == this.activeItem), this.blockSize / 2);

            gr.DrawRectangle(this.borderPen, new Rectangle(this.drawOffsetX, this.drawOffsetY, 
                                                           this.width * this.blockSize, this.height * this.blockSize));
        }
    }
}
