﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace MotleyBasketball___LevelEditor.LevelManagment
{
    class MBBall : MBItem
    {
        public override void Draw(Graphics gr, bool drawSelected, int halfBlockSize)
        {
            Rectangle rect = new Rectangle(this.centerPixelPos.X - halfBlockSize,
                                           this.centerPixelPos.Y - halfBlockSize, halfBlockSize * 2, halfBlockSize * 2);

            this.selectBrush = new HatchBrush(this.selectBrush.HatchStyle, Color.White, Color.Red);
            this.normalBrush.Color = Color.Red;

            if (drawSelected)
            {
                gr.FillPie(this.selectBrush, rect, 0.0f, 90.0f);
                gr.FillPie(Brushes.Black, rect, 90.0f, 90.0f);
                gr.FillPie(this.selectBrush, rect, 180.0f, 90.0f);
                gr.FillPie(Brushes.Black, rect, 270.0f, 90.0f);
            }
            else
            {
                gr.FillPie(this.normalBrush, rect, 0.0f, 90.0f);
                gr.FillPie(Brushes.Black, rect, 90.0f, 90.0f);
                gr.FillPie(this.normalBrush, rect, 180.0f, 90.0f);
                gr.FillPie(Brushes.Black, rect, 270.0f, 90.0f);
            }
        }
    }
}
