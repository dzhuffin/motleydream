﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using MotleyBasketball___LevelEditor.LevelManagment;

namespace MotleyBasketball___LevelEditor
{
    public partial class mainForm : Form
    {
        private Matrix zoomMatrix = new Matrix();

        private bool moving = false;
        private bool scrolling = false;
        private int oldScrollX = 0;
        private int oldScrollY = 0;

        private float zoom = 1.0f;
        private float Zoom
        {
            get { return this.zoom; }
            set
            {
                this.zoom = value;
                zoomMatrix.Reset();
                zoomMatrix.Scale(value, value);
                this.splitContainer.Panel2.Invalidate();
            }
        }

        private LevelManagment.MBLevel level;

        public mainForm()
        {
            InitializeComponent();

            this.level = new LevelManagment.MBLevel();
        }

        private int GetZoomedMouseCoord(int mouseCoord)
        {
            return (int)((float)mouseCoord / this.zoom);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if ((this.moving) && (e.Button == System.Windows.Forms.MouseButtons.Left))
                this.level.MoveSelected(GetZoomedMouseCoord(e.X), 
                                        GetZoomedMouseCoord(e.Y));

            if ((this.scrolling) && (e.Button == System.Windows.Forms.MouseButtons.Right))
            {
                this.level.DrawOffsetX += e.X - this.oldScrollX;
                this.level.DrawOffsetY += e.Y - this.oldScrollY;

                this.oldScrollX = e.X;
                this.oldScrollY = e.Y;
            }

            this.pictureBox.Invalidate();
        }

        private void pictureBox_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox.Focus();
        }

        private void pictureBox_MouseLeave(object sender, EventArgs e)
        {
            this.moving = false;
            this.Focus();
        }  

        private void pictureBox_MouseWheel(object sender, MouseEventArgs e)
        {
            this.Zoom = this.zoom + (float)e.Delta / 1000.0f;
            this.pictureBox.Invalidate();
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Transform = this.zoomMatrix;
            this.level.Draw(e.Graphics);
        }

        private void baseWallBtn_Click(object sender, EventArgs e)
        {
            this.level.AddPlatform(LevelManagment.MBPlatform.PlatformType.Usual,
                LevelManagment.MBPlatform.PlatformOrientation.Horizontal);
            this.pictureBox.Invalidate();
        }

        private void movingPlatformBtn_Click(object sender, EventArgs e)
        {
            this.level.AddMovingPlatform(LevelManagment.MBPlatform.PlatformType.Usual,
                LevelManagment.MBPlatform.PlatformOrientation.Horizontal);

            this.pictureBox.Invalidate();
        } 

        private void colorChangerBtn_Click(object sender, EventArgs e)
        {
            this.level.AddColorChanger(LevelManagment.MBColorChanger.ColorChangerType.Red);
            this.pictureBox.Invalidate();
        }

        private void freeBoxBtn_Click(object sender, EventArgs e)
        {
            this.level.AddFreeBox(LevelManagment.MBColorChanger.ColorChangerType.Red);
            this.pictureBox.Invalidate();
        }

        private void prizeBtn_Click(object sender, EventArgs e)
        {
            this.level.AddPrize();
            this.pictureBox.Invalidate();
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            this.level.Delete();
            this.pictureBox.Invalidate();
        } 

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            LevelManagment.MBPlatform selectedPlatform;
            LevelManagment.MBColorChanger selectedColorChanger;
            LevelManagment.MBFreeBox selectedFreeBox;

            this.moving = (e.Button == System.Windows.Forms.MouseButtons.Left);

            this.scrolling = false;
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                this.scrolling = true;
                this.oldScrollX = e.X;
                this.oldScrollY = e.Y;
            }
            
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.level.SelectItem(GetZoomedMouseCoord(e.X),
                                      GetZoomedMouseCoord(e.Y));

                if (this.level.ActiveItem != null)
                {
                    if ((this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform)) || 
                        (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform)))
                    {
                        this.chainLengthGB.Visible = this.freeBoxRotatingGB.Visible = this.freeBoxKindGB.Visible = this.freeBoxSizeGB.Visible = false;
                        this.hCameraPosGB.Visible = this.vCameraPosGB.Visible = this.edgeTypeGB.Visible = this.orientationGB.Visible = this.lengthGB.Visible = this.typeGB.Visible = true;

                        if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                        {
                            this.movingOrientationGB.Visible = true;
                            if (((LevelManagment.MBMovingPlatform)this.level.ActiveItem).MovingOrientation == LevelManagment.MBPlatform.PlatformOrientation.Horizontal)
                            {
                                this.mVerticalRB.Checked = false;
                                this.mHorizontalRB.Checked = true;
                            }
                            else
                            {
                                this.mVerticalRB.Checked = true;
                                this.mHorizontalRB.Checked = false;
                            }

                            this.startPosGB.Visible = this.distanceGB.Visible = true;
                            this.startTB.Value = ((LevelManagment.MBMovingPlatform)this.level.ActiveItem).StartPos;
                            this.distanceTB.Value = ((LevelManagment.MBMovingPlatform)this.level.ActiveItem).Distance;
                        }

                        selectedPlatform = ((LevelManagment.MBPlatform)this.level.ActiveItem);

                        if (selectedPlatform.VCameraPosition == MBPlatform.VerticalCameraPosition.Top)
                        {
                            this.vTopRB.Checked = true;
                            this.vBottomRB.Checked = false;
                            this.vCenterRB.Checked = false;
                            this.vFreeRB.Checked = false;
                        } 
                        else if (selectedPlatform.VCameraPosition == MBPlatform.VerticalCameraPosition.Bottom)
                        {
                            this.vTopRB.Checked = false;
                            this.vBottomRB.Checked = true;
                            this.vCenterRB.Checked = false;
                            this.vFreeRB.Checked = false;
                        }
                        else if (selectedPlatform.VCameraPosition == MBPlatform.VerticalCameraPosition.Center)
                        {
                            this.vTopRB.Checked = false;
                            this.vBottomRB.Checked = false;
                            this.vCenterRB.Checked = true;
                            this.vFreeRB.Checked = false;
                        }
                        else if (selectedPlatform.VCameraPosition == MBPlatform.VerticalCameraPosition.Free)
                        {
                            this.vTopRB.Checked = false;
                            this.vBottomRB.Checked = false;
                            this.vCenterRB.Checked = false;
                            this.vFreeRB.Checked = true;
                        }

                        if (selectedPlatform.HCameraPosition == MBPlatform.HorizontalCameraPosition.Left)
                        {
                            this.hLeftRB.Checked = true;
                            this.hRightRB.Checked = false;
                            this.hCenterRB.Checked = false;
                            this.hFreeRB.Checked = false;
                        }
                        else if (selectedPlatform.HCameraPosition == MBPlatform.HorizontalCameraPosition.Right)
                        {
                            this.hLeftRB.Checked = false;
                            this.hRightRB.Checked = true;
                            this.hCenterRB.Checked = false;
                            this.hFreeRB.Checked = false;
                        }
                        else if (selectedPlatform.HCameraPosition == MBPlatform.HorizontalCameraPosition.Center)
                        {
                            this.hLeftRB.Checked = false;
                            this.hRightRB.Checked = false;
                            this.hCenterRB.Checked = true;
                            this.hFreeRB.Checked = false;
                        }
                        else if (selectedPlatform.HCameraPosition == MBPlatform.HorizontalCameraPosition.Free)
                        {
                            this.hLeftRB.Checked = false;
                            this.hRightRB.Checked = false;
                            this.hCenterRB.Checked = false;
                            this.hFreeRB.Checked = true;
                        }


                        if (selectedPlatform.Orientation == LevelManagment.MBPlatform.PlatformOrientation.Horizontal)
                        {
                            this.horizontalRB.Checked = true;
                            this.verticalRB.Checked = false;
                        }
                        else
                        {
                            this.verticalRB.Checked = true;
                            this.horizontalRB.Checked = false;
                        }

                        if (selectedPlatform.Edge == LevelManagment.MBPlatform.EdgeType.All)
                        {
                            this.allRB.Checked = true;
                            this.leftOnlyRB.Checked = false;
                            this.rightOnlyRB.Checked = false;
                            this.noneRB.Checked = false;
                        } else if (selectedPlatform.Edge == LevelManagment.MBPlatform.EdgeType.LeftOnly)
                        {
                            this.allRB.Checked = false;
                            this.leftOnlyRB.Checked = true;
                            this.rightOnlyRB.Checked = false;
                            this.noneRB.Checked = false;
                        } else  if (selectedPlatform.Edge == LevelManagment.MBPlatform.EdgeType.RightOnly)
                        {
                            this.allRB.Checked = false;
                            this.leftOnlyRB.Checked = false;
                            this.rightOnlyRB.Checked = true;
                            this.noneRB.Checked = false;
                        } else if (selectedPlatform.Edge == LevelManagment.MBPlatform.EdgeType.None)
                        {
                            this.allRB.Checked = false;
                            this.leftOnlyRB.Checked = false;
                            this.rightOnlyRB.Checked = false;
                            this.noneRB.Checked = true;
                        }

                        this.lengthTB.Value = selectedPlatform.Length;

                        switch (selectedPlatform.Type)
                        {
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.Floor:
                                this.typeCB.Text = "Floor";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.Usual:
                                this.typeCB.Text = "Usual";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.Red:
                                this.typeCB.Text = "Red";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.Green:
                                this.typeCB.Text = "Green";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.Blue:
                                this.typeCB.Text = "Blue";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.Yellow:
                                this.typeCB.Text = "Yellow";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.LeftWall:
                                this.typeCB.Text = "LeftWall";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.RightWall:
                                this.typeCB.Text = "RightWall";
                                break;
                            case MotleyBasketball___LevelEditor.LevelManagment.MBPlatform.PlatformType.Roof:
                                this.typeCB.Text = "Roof";
                                break;
                            default:
                                this.typeCB.Text = "";
                                break;
                        }
                    }
                    else
                    {
                        if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBColorChanger))
                        {
                            this.chainLengthGB.Visible = this.hCameraPosGB.Visible = this.vCameraPosGB.Visible = this.freeBoxRotatingGB.Visible = this.edgeTypeGB.Visible = this.freeBoxKindGB.Visible = this.freeBoxSizeGB.Visible = this.orientationGB.Visible = this.lengthGB.Visible = false;
                            this.typeGB.Visible = true;

                            selectedColorChanger = ((LevelManagment.MBColorChanger)this.level.ActiveItem);

                            switch (selectedColorChanger.Type)
                            {
                                case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Red:
                                    this.typeCB.Text = "Red";
                                    break;
                                case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Green:
                                    this.typeCB.Text = "Green";
                                    break;
                                case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Blue:
                                    this.typeCB.Text = "Blue";
                                    break;
                                case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Yellow:
                                    this.typeCB.Text = "Yellow";
                                    break;
                                default:
                                    this.typeCB.Text = "";
                                    break;
                            }
                        }
                        else
                        {
                            if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                            {
                                this.edgeTypeGB.Visible = this.orientationGB.Visible = this.lengthGB.Visible = false;
                                this.chainLengthGB.Visible = this.hCameraPosGB.Visible = this.vCameraPosGB.Visible = this.freeBoxRotatingGB.Visible = this.freeBoxKindGB.Visible = this.typeGB.Visible = this.freeBoxSizeGB.Visible = true;

                                selectedFreeBox = ((LevelManagment.MBFreeBox)this.level.ActiveItem);

                                this.freeBoxWidthTB.Value = selectedFreeBox.Width;
                                this.freeBoxHeightTB.Value = selectedFreeBox.Height;
                                this.chainLengthTB.Value = selectedFreeBox.ChainLength;

                                if (selectedFreeBox.VCameraPosition == MBPlatform.VerticalCameraPosition.Top)
                                {
                                    this.vTopRB.Checked = true;
                                    this.vBottomRB.Checked = false;
                                    this.vCenterRB.Checked = false;
                                    this.vFreeRB.Checked = false;
                                }
                                else if (selectedFreeBox.VCameraPosition == MBPlatform.VerticalCameraPosition.Bottom)
                                {
                                    this.vTopRB.Checked = false;
                                    this.vBottomRB.Checked = true;
                                    this.vCenterRB.Checked = false;
                                    this.vFreeRB.Checked = false;
                                }
                                else if (selectedFreeBox.VCameraPosition == MBPlatform.VerticalCameraPosition.Center)
                                {
                                    this.vTopRB.Checked = false;
                                    this.vBottomRB.Checked = false;
                                    this.vCenterRB.Checked = true;
                                    this.vFreeRB.Checked = false;
                                }
                                else if (selectedFreeBox.VCameraPosition == MBPlatform.VerticalCameraPosition.Free)
                                {
                                    this.vTopRB.Checked = false;
                                    this.vBottomRB.Checked = false;
                                    this.vCenterRB.Checked = false;
                                    this.vFreeRB.Checked = true;
                                }

                                if (selectedFreeBox.HCameraPosition == MBPlatform.HorizontalCameraPosition.Left)
                                {
                                    this.hLeftRB.Checked = true;
                                    this.hRightRB.Checked = false;
                                    this.hCenterRB.Checked = false;
                                    this.hFreeRB.Checked = false;
                                }
                                else if (selectedFreeBox.HCameraPosition == MBPlatform.HorizontalCameraPosition.Right)
                                {
                                    this.hLeftRB.Checked = false;
                                    this.hRightRB.Checked = true;
                                    this.hCenterRB.Checked = false;
                                    this.hFreeRB.Checked = false;
                                }
                                else if (selectedFreeBox.HCameraPosition == MBPlatform.HorizontalCameraPosition.Center)
                                {
                                    this.hLeftRB.Checked = false;
                                    this.hRightRB.Checked = false;
                                    this.hCenterRB.Checked = true;
                                    this.hFreeRB.Checked = false;
                                }
                                else if (selectedFreeBox.HCameraPosition == MBPlatform.HorizontalCameraPosition.Free)
                                {
                                    this.hLeftRB.Checked = false;
                                    this.hRightRB.Checked = false;
                                    this.hCenterRB.Checked = false;
                                    this.hFreeRB.Checked = true;
                                }

                                if (selectedFreeBox.Kind == LevelManagment.MBFreeBox.FreeBoxKind.Box)
                                {
                                    this.ballRB.Checked = false;
                                    this.boxRB.Checked = true;
                                    this.tipRB.Checked = false;
                                }
                                else
                                {
                                    if (selectedFreeBox.Kind == LevelManagment.MBFreeBox.FreeBoxKind.Ball)
                                    {
                                        this.ballRB.Checked = true;
                                        this.boxRB.Checked = false;
                                        this.tipRB.Checked = false;
                                    }
                                    else
                                    {
                                        this.ballRB.Checked = false;
                                        this.boxRB.Checked = false;
                                        this.tipRB.Checked = true;
                                    }
                                }

                                if (selectedFreeBox.IsRotating)
                                {
                                    this.trueRotRB.Checked = true;
                                    this.falseRotRB.Checked = false;
                                }
                                else
                                {
                                    this.trueRotRB.Checked = false;
                                    this.falseRotRB.Checked = true;
                                }

                                switch (selectedFreeBox.Type)
                                {
                                    case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Red:
                                        this.typeCB.Text = "Red";
                                        break;
                                    case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Green:
                                        this.typeCB.Text = "Green";
                                        break;
                                    case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Blue:
                                        this.typeCB.Text = "Blue";
                                        break;
                                    case MotleyBasketball___LevelEditor.LevelManagment.MBColorChanger.ColorChangerType.Yellow:
                                        this.typeCB.Text = "Yellow";
                                        break;
                                    default:
                                        this.typeCB.Text = "";
                                        break;
                                }
                            }
                            else
                                this.chainLengthGB.Visible = this.hCameraPosGB.Visible = this.vCameraPosGB.Visible = this.freeBoxRotatingGB.Visible = this.edgeTypeGB.Visible = this.freeBoxSizeGB.Visible = this.freeBoxKindGB.Visible = this.startPosGB.Visible = this.movingOrientationGB.Visible = this.distanceGB.Visible = this.orientationGB.Visible = this.lengthGB.Visible = this.typeGB.Visible = false;
                        }                           
                    }
                }
                else
                    this.hCameraPosGB.Visible = this.vCameraPosGB.Visible = this.freeBoxRotatingGB.Visible = this.edgeTypeGB.Visible = this.freeBoxKindGB.Visible = this.startPosGB.Visible = this.movingOrientationGB.Visible = this.distanceGB.Visible = this.orientationGB.Visible = this.lengthGB.Visible = this.typeGB.Visible = false;
            }

            this.pictureBox.Invalidate();
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            this.scrolling = this.moving = false;
        }

        private void noneRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform))
                    if (this.noneRB.Checked)
                        ((LevelManagment.MBPlatform) this.level.ActiveItem).Edge = MBPlatform.EdgeType.None;

            this.pictureBox.Invalidate();
        }

        private void allRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform))
                    if (this.allRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).Edge = MBPlatform.EdgeType.All;

            this.pictureBox.Invalidate();
        }

        private void leftOnlyRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform))
                    if (this.leftOnlyRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).Edge = MBPlatform.EdgeType.LeftOnly;

            this.pictureBox.Invalidate();
        }

        private void rightOnlyRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform))
                    if (this.rightOnlyRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).Edge = MBPlatform.EdgeType.RightOnly;

            this.pictureBox.Invalidate();
        }

        private void verticalRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if ((this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform)) || 
                    (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform)))
                    ((LevelManagment.MBPlatform)this.level.ActiveItem).Orientation = (this.verticalRB.Checked) ?
                        LevelManagment.MBPlatform.PlatformOrientation.Vertical :
                        LevelManagment.MBPlatform.PlatformOrientation.Horizontal;

            this.pictureBox.Invalidate();
        }

        private void mVerticalRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                    ((LevelManagment.MBMovingPlatform)this.level.ActiveItem).MovingOrientation = (this.mVerticalRB.Checked) ?
                        LevelManagment.MBPlatform.PlatformOrientation.Vertical :
                        LevelManagment.MBPlatform.PlatformOrientation.Horizontal;

            this.pictureBox.Invalidate();
        }  

        private void lengthTB_Scroll(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if ((this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform)) || 
                    (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform)))
                    ((LevelManagment.MBPlatform)this.level.ActiveItem).Length = this.lengthTB.Value;

            this.pictureBox.Invalidate();
        }

        private void freeBoxWidthTB_Scroll(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                    ((LevelManagment.MBFreeBox)this.level.ActiveItem).Width = this.freeBoxWidthTB.Value;

            this.pictureBox.Invalidate();
        }

        private void chainLengthTB_Scroll(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                    ((LevelManagment.MBFreeBox)this.level.ActiveItem).ChainLength = this.chainLengthTB.Value;

            this.pictureBox.Invalidate();
        } 

        private void freeBoxHeightTB_Scroll(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                    ((LevelManagment.MBFreeBox)this.level.ActiveItem).Height = this.freeBoxHeightTB.Value;

            this.pictureBox.Invalidate();
        } 

        private void distanceTB_Scroll(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                    ((LevelManagment.MBMovingPlatform)this.level.ActiveItem).Distance = this.distanceTB.Value;

            this.pictureBox.Invalidate();
        }

        private void startTB_Scroll(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                    ((LevelManagment.MBMovingPlatform)this.level.ActiveItem).StartPos = this.startTB.Value;

            this.pictureBox.Invalidate();
        }  

        private void levelHeightTB_Scroll(object sender, EventArgs e)
        {
            this.level.Height = this.levelHeightTB.Value;
            this.pictureBox.Invalidate();
        }

        private void levelWidthTB_Scroll(object sender, EventArgs e)
        {
            this.level.Width = this.levelWidthTB.Value;
            this.pictureBox.Invalidate();
        }

        private void typeCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            LevelManagment.MBPlatform selectedPlatform;
            LevelManagment.MBColorChanger selectedColorChanger;
            LevelManagment.MBFreeBox selectedFreeBox;

            if (this.level.ActiveItem != null)
            {
                if ((this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform)) ||
                    (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform)))
                {
                    selectedPlatform = ((LevelManagment.MBPlatform)this.level.ActiveItem);

                    switch (this.typeCB.Text)
                    {
                        case "Floor":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Floor;
                            break;
                        case "Usual":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Usual;
                            break;
                        case "Red":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Red;
                            break;
                        case "Green":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Green;
                            break;
                        case "Blue":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Blue;
                            break;
                        case "Yellow":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Yellow;
                            break;
                        case "LeftWall":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.LeftWall;
                            break;
                        case "RightWall":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.RightWall;
                            break;
                        case "Roof":
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Roof;
                            break;
                        default:
                            selectedPlatform.Type = LevelManagment.MBPlatform.PlatformType.Usual;
                            break;
                    }
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBColorChanger))
                {
                    selectedColorChanger = ((LevelManagment.MBColorChanger)this.level.ActiveItem);

                    switch (this.typeCB.Text)
                    {
                        case "Red":
                            selectedColorChanger.Type = LevelManagment.MBColorChanger.ColorChangerType.Red;
                            break;
                        case "Green":
                            selectedColorChanger.Type = LevelManagment.MBColorChanger.ColorChangerType.Green;
                            break;
                        case "Blue":
                            selectedColorChanger.Type = LevelManagment.MBColorChanger.ColorChangerType.Blue;
                            break;
                        case "Yellow":
                            selectedColorChanger.Type = LevelManagment.MBColorChanger.ColorChangerType.Yellow;
                            break;
                        default:
                            selectedColorChanger.Type = LevelManagment.MBColorChanger.ColorChangerType.Red;
                            break;
                    }
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                {
                    selectedFreeBox = ((LevelManagment.MBFreeBox)this.level.ActiveItem);

                    switch (this.typeCB.Text)
                    {
                        case "Red":
                            selectedFreeBox.Type = LevelManagment.MBColorChanger.ColorChangerType.Red;
                            break;
                        case "Green":
                            selectedFreeBox.Type = LevelManagment.MBColorChanger.ColorChangerType.Green;
                            break;
                        case "Blue":
                            selectedFreeBox.Type = LevelManagment.MBColorChanger.ColorChangerType.Blue;
                            break;
                        case "Yellow":
                            selectedFreeBox.Type = LevelManagment.MBColorChanger.ColorChangerType.Yellow;
                            break;
                        default:
                            selectedFreeBox.Type = LevelManagment.MBColorChanger.ColorChangerType.Red;
                            break;
                    }
                }
            }

            this.pictureBox.Invalidate();
        }

        private void newBtn_Click(object sender, EventArgs e)
        {
            this.level.New();

            this.pictureBox.Invalidate();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (this.saveLevelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.level.Save(this.saveLevelDialog.FileName);

            this.pictureBox.Invalidate();
        }

        private void loadBtn_Click(object sender, EventArgs e)
        {
            if (this.openLevelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.level.Load(this.openLevelDialog.FileName);

            this.pictureBox.Invalidate();
        }

        private void exportBtn_Click(object sender, EventArgs e)
        {
            //if (this.saveLevelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //    this.level.Export(this.saveLevelDialog.FileName);
            this.level.Export("");

            this.pictureBox.Invalidate();
        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            this.pictureBox.Invalidate();
        }

        private void ballRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                    ((LevelManagment.MBFreeBox)this.level.ActiveItem).Kind = (this.ballRB.Checked) ?
                        LevelManagment.MBFreeBox.FreeBoxKind.Ball :
                        ((this.boxRB.Checked) ? LevelManagment.MBFreeBox.FreeBoxKind.Box : LevelManagment.MBFreeBox.FreeBoxKind.Tip);

            this.pictureBox.Invalidate();
        }

        
        private void boxRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                    ((LevelManagment.MBFreeBox)this.level.ActiveItem).Kind = (this.ballRB.Checked) ?
                        LevelManagment.MBFreeBox.FreeBoxKind.Ball :
                        ((this.boxRB.Checked) ? LevelManagment.MBFreeBox.FreeBoxKind.Box : LevelManagment.MBFreeBox.FreeBoxKind.Tip);

            this.pictureBox.Invalidate();
        }

        private void tipRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                    ((LevelManagment.MBFreeBox)this.level.ActiveItem).Kind = (this.ballRB.Checked) ?
                        LevelManagment.MBFreeBox.FreeBoxKind.Ball :
                        ((this.boxRB.Checked) ? LevelManagment.MBFreeBox.FreeBoxKind.Box : LevelManagment.MBFreeBox.FreeBoxKind.Tip);

            this.pictureBox.Invalidate();
        } 

        private void trueRotRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                    ((LevelManagment.MBFreeBox)this.level.ActiveItem).IsRotating = this.trueRotRB.Checked;

            this.pictureBox.Invalidate();
        }

        private void vTopRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof (LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof (LevelManagment.MBMovingPlatform))
                {
                    if (this.vTopRB.Checked)
                        ((LevelManagment.MBPlatform) this.level.ActiveItem).VCameraPosition = 
                            MBPlatform.VerticalCameraPosition.Top;
                }

                if (this.level.ActiveItem.GetType() == typeof (LevelManagment.MBFreeBox))
                {
                    if (this.vTopRB.Checked)
                        ((LevelManagment.MBFreeBox) this.level.ActiveItem).VCameraPosition =
                            MBPlatform.VerticalCameraPosition.Top;
                }

                this.pictureBox.Invalidate();
            }
        }

        private void vBottomRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                {
                    if (this.vBottomRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).VCameraPosition =
                            MBPlatform.VerticalCameraPosition.Bottom;
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                {
                    if (this.vBottomRB.Checked)
                        ((LevelManagment.MBFreeBox)this.level.ActiveItem).VCameraPosition =
                            MBPlatform.VerticalCameraPosition.Bottom;
                }

                this.pictureBox.Invalidate();
            }
        }

        private void vCenterRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                {
                    if (this.vCenterRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).VCameraPosition =
                            MBPlatform.VerticalCameraPosition.Center;
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                {
                    if (this.vCenterRB.Checked)
                        ((LevelManagment.MBFreeBox)this.level.ActiveItem).VCameraPosition =
                            MBPlatform.VerticalCameraPosition.Center;
                }

                this.pictureBox.Invalidate();
            }
        }

        private void vFreeRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                {
                    if (this.vFreeRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).VCameraPosition =
                            MBPlatform.VerticalCameraPosition.Free;
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                {
                    if (this.vFreeRB.Checked)
                        ((LevelManagment.MBFreeBox)this.level.ActiveItem).VCameraPosition =
                            MBPlatform.VerticalCameraPosition.Free;
                }

                this.pictureBox.Invalidate();
            }
        }

        private void resaveBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "Resave", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                this.level.ResaveAll();

            this.pictureBox.Invalidate();
        }

        private void hLeftRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                {
                    if (this.hLeftRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Left;
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                {
                    if (this.hLeftRB.Checked)
                        ((LevelManagment.MBFreeBox)this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Left;
                }

                this.pictureBox.Invalidate();
            }
        }

        private void hRightRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                {
                    if (this.hRightRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Right;
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                {
                    if (this.hRightRB.Checked)
                        ((LevelManagment.MBFreeBox)this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Right;
                }

                this.pictureBox.Invalidate();
            }
        }

        private void hCenterRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof(LevelManagment.MBMovingPlatform))
                {
                    if (this.hCenterRB.Checked)
                        ((LevelManagment.MBPlatform)this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Center;
                }

                if (this.level.ActiveItem.GetType() == typeof(LevelManagment.MBFreeBox))
                {
                    if (this.hCenterRB.Checked)
                        ((LevelManagment.MBFreeBox)this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Center;
                }

                this.pictureBox.Invalidate();
            }
        }

        private void hFreeRB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.level.ActiveItem != null)
            {
                if (this.level.ActiveItem.GetType() == typeof (LevelManagment.MBPlatform) ||
                    this.level.ActiveItem.GetType() == typeof (LevelManagment.MBMovingPlatform))
                {
                    if (this.hFreeRB.Checked)
                        ((LevelManagment.MBPlatform) this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Free;
                }

                if (this.level.ActiveItem.GetType() == typeof (LevelManagment.MBFreeBox))
                {
                    if (this.hFreeRB.Checked)
                        ((LevelManagment.MBFreeBox) this.level.ActiveItem).HCameraPosition =
                            MBPlatform.HorizontalCameraPosition.Free;
                }

                this.pictureBox.Invalidate();
            }

        }

    }
}